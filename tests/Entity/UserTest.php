<?php

namespace App\Tests\Entity;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UserTest extends KernelTestCase
{
    private const EMAIL_CONSTRAINT_MESSAGE = "Veuillez indiquer une adresse email valide";
    
    private function setUp(): void
    {
        $kernel = self::bootKernel();
        
        $this->validator = $kernel->getContainer()->get('validator');
    }

    public function fdp(): void
    {
        $kernel = self::bootKernel();
        
        $this->validator = $kernel->getContainer()->get('validator');
    }
}
