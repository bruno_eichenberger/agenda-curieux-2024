<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200220194906 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE place ADD map_url VARCHAR(255) DEFAULT NULL, ADD street_url VARCHAR(255) DEFAULT NULL, ADD email VARCHAR(255) DEFAULT NULL, ADD phone VARCHAR(255) DEFAULT NULL, ADD ip VARCHAR(255) DEFAULT NULL, ADD transport LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', ADD data_link VARCHAR(255) DEFAULT NULL, ADD opinion VARCHAR(255) DEFAULT NULL, ADD summary VARCHAR(255) DEFAULT NULL, ADD wifi VARCHAR(255) DEFAULT NULL, ADD last_service TIME DEFAULT NULL, ADD parking VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE place DROP map_url, DROP street_url, DROP email, DROP phone, DROP ip, DROP transport, DROP data_link, DROP opinion, DROP summary, DROP wifi, DROP last_service, DROP parking');
    }
}
