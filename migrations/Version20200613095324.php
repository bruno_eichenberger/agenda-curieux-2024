<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200613095324 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE comment ADD place_id INT DEFAULT NULL, ADD festival_id INT DEFAULT NULL, ADD artist_id INT DEFAULT NULL, ADD organizer_id INT DEFAULT NULL, ADD moderator_id INT DEFAULT NULL, ADD is_alerted TINYINT(1) DEFAULT NULL, ADD is_checked TINYINT(1) DEFAULT NULL, ADD is_spammed TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526CDA6A219 FOREIGN KEY (place_id) REFERENCES place (id)');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C8AEBAF57 FOREIGN KEY (festival_id) REFERENCES festival (id)');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526CB7970CF8 FOREIGN KEY (artist_id) REFERENCES artist (id)');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C876C4DDA FOREIGN KEY (organizer_id) REFERENCES organizer (id)');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526CD0AFA354 FOREIGN KEY (moderator_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_9474526CDA6A219 ON comment (place_id)');
        $this->addSql('CREATE INDEX IDX_9474526C8AEBAF57 ON comment (festival_id)');
        $this->addSql('CREATE INDEX IDX_9474526CB7970CF8 ON comment (artist_id)');
        $this->addSql('CREATE INDEX IDX_9474526C876C4DDA ON comment (organizer_id)');
        $this->addSql('CREATE INDEX IDX_9474526CD0AFA354 ON comment (moderator_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526CDA6A219');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526C8AEBAF57');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526CB7970CF8');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526C876C4DDA');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526CD0AFA354');
        $this->addSql('DROP INDEX IDX_9474526CDA6A219 ON comment');
        $this->addSql('DROP INDEX IDX_9474526C8AEBAF57 ON comment');
        $this->addSql('DROP INDEX IDX_9474526CB7970CF8 ON comment');
        $this->addSql('DROP INDEX IDX_9474526C876C4DDA ON comment');
        $this->addSql('DROP INDEX IDX_9474526CD0AFA354 ON comment');
        $this->addSql('ALTER TABLE comment DROP place_id, DROP festival_id, DROP artist_id, DROP organizer_id, DROP moderator_id, DROP is_alerted, DROP is_checked, DROP is_spammed');
    }
}
