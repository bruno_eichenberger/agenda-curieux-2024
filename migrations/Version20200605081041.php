<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200605081041 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE artist_user (artist_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_312D50D6B7970CF8 (artist_id), INDEX IDX_312D50D6A76ED395 (user_id), PRIMARY KEY(artist_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE organizer_user (organizer_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_9744570B876C4DDA (organizer_id), INDEX IDX_9744570BA76ED395 (user_id), PRIMARY KEY(organizer_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_festival (user_id INT NOT NULL, festival_id INT NOT NULL, INDEX IDX_E5F11389A76ED395 (user_id), INDEX IDX_E5F113898AEBAF57 (festival_id), PRIMARY KEY(user_id, festival_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE artist_user ADD CONSTRAINT FK_312D50D6B7970CF8 FOREIGN KEY (artist_id) REFERENCES artist (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE artist_user ADD CONSTRAINT FK_312D50D6A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE organizer_user ADD CONSTRAINT FK_9744570B876C4DDA FOREIGN KEY (organizer_id) REFERENCES organizer (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE organizer_user ADD CONSTRAINT FK_9744570BA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_festival ADD CONSTRAINT FK_E5F11389A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_festival ADD CONSTRAINT FK_E5F113898AEBAF57 FOREIGN KEY (festival_id) REFERENCES festival (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE artist ADD author_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE artist ADD CONSTRAINT FK_1599687F675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_1599687F675F31B ON artist (author_id)');
        $this->addSql('ALTER TABLE festival ADD author_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE festival ADD CONSTRAINT FK_57CF789F675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_57CF789F675F31B ON festival (author_id)');
        $this->addSql('ALTER TABLE organizer ADD author_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE organizer ADD CONSTRAINT FK_99D47173F675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_99D47173F675F31B ON organizer (author_id)');
        $this->addSql('ALTER TABLE place ADD author_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE place ADD CONSTRAINT FK_741D53CDF675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_741D53CDF675F31B ON place (author_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE artist_user');
        $this->addSql('DROP TABLE organizer_user');
        $this->addSql('DROP TABLE user_festival');
        $this->addSql('ALTER TABLE artist DROP FOREIGN KEY FK_1599687F675F31B');
        $this->addSql('DROP INDEX IDX_1599687F675F31B ON artist');
        $this->addSql('ALTER TABLE artist DROP author_id');
        $this->addSql('ALTER TABLE festival DROP FOREIGN KEY FK_57CF789F675F31B');
        $this->addSql('DROP INDEX IDX_57CF789F675F31B ON festival');
        $this->addSql('ALTER TABLE festival DROP author_id');
        $this->addSql('ALTER TABLE organizer DROP FOREIGN KEY FK_99D47173F675F31B');
        $this->addSql('DROP INDEX IDX_99D47173F675F31B ON organizer');
        $this->addSql('ALTER TABLE organizer DROP author_id');
        $this->addSql('ALTER TABLE place DROP FOREIGN KEY FK_741D53CDF675F31B');
        $this->addSql('DROP INDEX IDX_741D53CDF675F31B ON place');
        $this->addSql('ALTER TABLE place DROP author_id');
    }
}
