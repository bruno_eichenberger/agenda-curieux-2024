<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191021130008 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE placekind_place');
        $this->addSql('ALTER TABLE place ADD placekind_primary_id INT DEFAULT NULL, ADD placekind_secondary_id INT DEFAULT NULL, ADD placekind_tertiary_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE place ADD CONSTRAINT FK_741D53CD68BE3751 FOREIGN KEY (placekind_primary_id) REFERENCES placekind (id)');
        $this->addSql('ALTER TABLE place ADD CONSTRAINT FK_741D53CD1E7EF0F6 FOREIGN KEY (placekind_secondary_id) REFERENCES placekind (id)');
        $this->addSql('ALTER TABLE place ADD CONSTRAINT FK_741D53CD6D7F92E8 FOREIGN KEY (placekind_tertiary_id) REFERENCES placekind (id)');
        $this->addSql('CREATE INDEX IDX_741D53CD68BE3751 ON place (placekind_primary_id)');
        $this->addSql('CREATE INDEX IDX_741D53CD1E7EF0F6 ON place (placekind_secondary_id)');
        $this->addSql('CREATE INDEX IDX_741D53CD6D7F92E8 ON place (placekind_tertiary_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE placekind_place (placekind_id INT NOT NULL, place_id INT NOT NULL, INDEX IDX_4316FC5A9EB16C79 (placekind_id), INDEX IDX_4316FC5ADA6A219 (place_id), PRIMARY KEY(placekind_id, place_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE placekind_place ADD CONSTRAINT FK_4316FC5A9EB16C79 FOREIGN KEY (placekind_id) REFERENCES placekind (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE placekind_place ADD CONSTRAINT FK_4316FC5ADA6A219 FOREIGN KEY (place_id) REFERENCES place (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE place DROP FOREIGN KEY FK_741D53CD68BE3751');
        $this->addSql('ALTER TABLE place DROP FOREIGN KEY FK_741D53CD1E7EF0F6');
        $this->addSql('ALTER TABLE place DROP FOREIGN KEY FK_741D53CD6D7F92E8');
        $this->addSql('DROP INDEX IDX_741D53CD68BE3751 ON place');
        $this->addSql('DROP INDEX IDX_741D53CD1E7EF0F6 ON place');
        $this->addSql('DROP INDEX IDX_741D53CD6D7F92E8 ON place');
        $this->addSql('ALTER TABLE place DROP placekind_primary_id, DROP placekind_secondary_id, DROP placekind_tertiary_id');
    }
}
