<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210926013840 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE event ADD verified_by_id INT DEFAULT NULL, ADD verified_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE event ADD CONSTRAINT FK_3BAE0AA769F4B775 FOREIGN KEY (verified_by_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_3BAE0AA769F4B775 ON event (verified_by_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE event DROP FOREIGN KEY FK_3BAE0AA769F4B775');
        $this->addSql('DROP INDEX IDX_3BAE0AA769F4B775 ON event');
        $this->addSql('ALTER TABLE event DROP verified_by_id, DROP verified_at');
    }
}
