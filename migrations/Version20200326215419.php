<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200326215419 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE place_like (id INT AUTO_INCREMENT NOT NULL, place_id INT NOT NULL, user_id INT NOT NULL, liked_at DATETIME NOT NULL, INDEX IDX_66D6305FDA6A219 (place_id), INDEX IDX_66D6305FA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE room (id INT AUTO_INCREMENT NOT NULL, place_id INT NOT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, INDEX IDX_729F519BDA6A219 (place_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE place_like ADD CONSTRAINT FK_66D6305FDA6A219 FOREIGN KEY (place_id) REFERENCES place (id)');
        $this->addSql('ALTER TABLE place_like ADD CONSTRAINT FK_66D6305FA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE room ADD CONSTRAINT FK_729F519BDA6A219 FOREIGN KEY (place_id) REFERENCES place (id)');
        $this->addSql('ALTER TABLE event ADD rooms_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE event ADD CONSTRAINT FK_3BAE0AA78E2368AB FOREIGN KEY (rooms_id) REFERENCES room (id)');
        $this->addSql('CREATE INDEX IDX_3BAE0AA78E2368AB ON event (rooms_id)');
        $this->addSql('ALTER TABLE user ADD last_connection_at DATETIME DEFAULT NULL, ADD level TINYINT(1) DEFAULT NULL, ADD facebook_id INT DEFAULT NULL, CHANGE introduction introduction VARCHAR(255) DEFAULT NULL, CHANGE presentation presentation LONGTEXT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE event DROP FOREIGN KEY FK_3BAE0AA78E2368AB');
        $this->addSql('DROP TABLE place_like');
        $this->addSql('DROP TABLE room');
        $this->addSql('DROP INDEX IDX_3BAE0AA78E2368AB ON event');
        $this->addSql('ALTER TABLE event DROP rooms_id');
        $this->addSql('ALTER TABLE user DROP last_connection_at, DROP level, DROP facebook_id, CHANGE introduction introduction VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE presentation presentation LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
