<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210112234005 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE subdomain_reduction (subdomain_id INT NOT NULL, reduction_id INT NOT NULL, INDEX IDX_30F711E58530A5DC (subdomain_id), INDEX IDX_30F711E5C03CB092 (reduction_id), PRIMARY KEY(subdomain_id, reduction_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE subdomain_reduction ADD CONSTRAINT FK_30F711E58530A5DC FOREIGN KEY (subdomain_id) REFERENCES subdomain (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE subdomain_reduction ADD CONSTRAINT FK_30F711E5C03CB092 FOREIGN KEY (reduction_id) REFERENCES reduction (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE subdomain ADD network LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE subdomain_reduction');
        $this->addSql('ALTER TABLE subdomain DROP network');
    }
}
