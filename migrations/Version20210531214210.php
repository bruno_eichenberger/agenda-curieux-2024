<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210531214210 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE reduction_place (reduction_id INT NOT NULL, place_id INT NOT NULL, INDEX IDX_5825B6CBC03CB092 (reduction_id), INDEX IDX_5825B6CBDA6A219 (place_id), PRIMARY KEY(reduction_id, place_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reduction_organizer (reduction_id INT NOT NULL, organizer_id INT NOT NULL, INDEX IDX_36D1ECFCC03CB092 (reduction_id), INDEX IDX_36D1ECFC876C4DDA (organizer_id), PRIMARY KEY(reduction_id, organizer_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reduction_festival (reduction_id INT NOT NULL, festival_id INT NOT NULL, INDEX IDX_AFA3420C03CB092 (reduction_id), INDEX IDX_AFA34208AEBAF57 (festival_id), PRIMARY KEY(reduction_id, festival_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE reduction_place ADD CONSTRAINT FK_5825B6CBC03CB092 FOREIGN KEY (reduction_id) REFERENCES reduction (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE reduction_place ADD CONSTRAINT FK_5825B6CBDA6A219 FOREIGN KEY (place_id) REFERENCES place (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE reduction_organizer ADD CONSTRAINT FK_36D1ECFCC03CB092 FOREIGN KEY (reduction_id) REFERENCES reduction (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE reduction_organizer ADD CONSTRAINT FK_36D1ECFC876C4DDA FOREIGN KEY (organizer_id) REFERENCES organizer (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE reduction_festival ADD CONSTRAINT FK_AFA3420C03CB092 FOREIGN KEY (reduction_id) REFERENCES reduction (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE reduction_festival ADD CONSTRAINT FK_AFA34208AEBAF57 FOREIGN KEY (festival_id) REFERENCES festival (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE reduction_place');
        $this->addSql('DROP TABLE reduction_organizer');
        $this->addSql('DROP TABLE reduction_festival');
    }
}
