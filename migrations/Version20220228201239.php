<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220228201239 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE prices_reduction (prices_id INT NOT NULL, reduction_id INT NOT NULL, INDEX IDX_C475E761D9C9DE39 (prices_id), INDEX IDX_C475E761C03CB092 (reduction_id), PRIMARY KEY(prices_id, reduction_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE prices_reduction ADD CONSTRAINT FK_C475E761D9C9DE39 FOREIGN KEY (prices_id) REFERENCES prices (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE prices_reduction ADD CONSTRAINT FK_C475E761C03CB092 FOREIGN KEY (reduction_id) REFERENCES reduction (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE prices_reduction');
    }
}
