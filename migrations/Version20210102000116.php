<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210102000116 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE subdomain (id INT AUTO_INCREMENT NOT NULL, manager_id INT DEFAULT NULL, main_borough_id INT DEFAULT NULL, name VARCHAR(100) NOT NULL, slug VARCHAR(100) NOT NULL, slogan VARCHAR(255) DEFAULT NULL, is_created TINYINT(1) DEFAULT NULL, is_activated TINYINT(1) DEFAULT NULL, created_at DATETIME NOT NULL, color VARCHAR(20) DEFAULT NULL, data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', legal LONGTEXT DEFAULT NULL, resident LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', wallpaper VARCHAR(255) DEFAULT NULL, INDEX IDX_C1D5962E783E3463 (manager_id), UNIQUE INDEX UNIQ_C1D5962E95ED82D (main_borough_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE subdomain_user (subdomain_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_6FCF989B8530A5DC (subdomain_id), INDEX IDX_6FCF989BA76ED395 (user_id), PRIMARY KEY(subdomain_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE subdomain_subdomain (subdomain_source INT NOT NULL, subdomain_target INT NOT NULL, INDEX IDX_40C5D3A37DE05625 (subdomain_source), INDEX IDX_40C5D3A3640506AA (subdomain_target), PRIMARY KEY(subdomain_source, subdomain_target)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE subdomain_borough (subdomain_id INT NOT NULL, borough_id INT NOT NULL, INDEX IDX_21754B9E8530A5DC (subdomain_id), INDEX IDX_21754B9E31A6AC4E (borough_id), PRIMARY KEY(subdomain_id, borough_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE subdomain ADD CONSTRAINT FK_C1D5962E783E3463 FOREIGN KEY (manager_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE subdomain ADD CONSTRAINT FK_C1D5962E95ED82D FOREIGN KEY (main_borough_id) REFERENCES borough (id)');
        $this->addSql('ALTER TABLE subdomain_user ADD CONSTRAINT FK_6FCF989B8530A5DC FOREIGN KEY (subdomain_id) REFERENCES subdomain (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE subdomain_user ADD CONSTRAINT FK_6FCF989BA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE subdomain_subdomain ADD CONSTRAINT FK_40C5D3A37DE05625 FOREIGN KEY (subdomain_source) REFERENCES subdomain (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE subdomain_subdomain ADD CONSTRAINT FK_40C5D3A3640506AA FOREIGN KEY (subdomain_target) REFERENCES subdomain (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE subdomain_borough ADD CONSTRAINT FK_21754B9E8530A5DC FOREIGN KEY (subdomain_id) REFERENCES subdomain (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE subdomain_borough ADD CONSTRAINT FK_21754B9E31A6AC4E FOREIGN KEY (borough_id) REFERENCES borough (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE borough DROP slogan, DROP color');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE subdomain_user DROP FOREIGN KEY FK_6FCF989B8530A5DC');
        $this->addSql('ALTER TABLE subdomain_subdomain DROP FOREIGN KEY FK_40C5D3A37DE05625');
        $this->addSql('ALTER TABLE subdomain_subdomain DROP FOREIGN KEY FK_40C5D3A3640506AA');
        $this->addSql('ALTER TABLE subdomain_borough DROP FOREIGN KEY FK_21754B9E8530A5DC');
        $this->addSql('DROP TABLE subdomain');
        $this->addSql('DROP TABLE subdomain_user');
        $this->addSql('DROP TABLE subdomain_subdomain');
        $this->addSql('DROP TABLE subdomain_borough');
        $this->addSql('ALTER TABLE borough ADD slogan VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, ADD color VARCHAR(20) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
