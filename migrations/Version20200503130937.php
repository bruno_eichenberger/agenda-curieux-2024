<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200503130937 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ticket_resource (id INT AUTO_INCREMENT NOT NULL, place_id INT DEFAULT NULL, city_id INT DEFAULT NULL, ticketing_id INT NOT NULL, ticket_id INT DEFAULT NULL, rubric_id INT DEFAULT NULL, id_resource VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, date_start DATE NOT NULL, date_end DATE NOT NULL, time_start TIME DEFAULT NULL, time_end TIME DEFAULT NULL, description LONGTEXT DEFAULT NULL, description_short VARCHAR(255) DEFAULT NULL, price NUMERIC(5, 2) DEFAULT NULL, currency VARCHAR(255) DEFAULT NULL, cover_image VARCHAR(255) DEFAULT NULL, link_affiliate VARCHAR(255) DEFAULT NULL, link_resource VARCHAR(255) DEFAULT NULL, place_resource LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', keywords LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\', multi_date_time LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_39F80897DA6A219 (place_id), INDEX IDX_39F808978BAC62AF (city_id), INDEX IDX_39F8089725C26A92 (ticketing_id), UNIQUE INDEX UNIQ_39F80897700047D2 (ticket_id), INDEX IDX_39F80897A29EC0FC (rubric_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ticket_resource ADD CONSTRAINT FK_39F80897DA6A219 FOREIGN KEY (place_id) REFERENCES place (id)');
        $this->addSql('ALTER TABLE ticket_resource ADD CONSTRAINT FK_39F808978BAC62AF FOREIGN KEY (city_id) REFERENCES city (id)');
        $this->addSql('ALTER TABLE ticket_resource ADD CONSTRAINT FK_39F8089725C26A92 FOREIGN KEY (ticketing_id) REFERENCES ticketing (id)');
        $this->addSql('ALTER TABLE ticket_resource ADD CONSTRAINT FK_39F80897700047D2 FOREIGN KEY (ticket_id) REFERENCES ticket (id)');
        $this->addSql('ALTER TABLE ticket_resource ADD CONSTRAINT FK_39F80897A29EC0FC FOREIGN KEY (rubric_id) REFERENCES rubric (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE ticket_resource');
    }
}
