<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    
    <style>

    .card-image-wrapper {
    display: flex;
    width: 100%;
    height: 250px;
    /* Just to show the size of the image wrapper */
    background-color: darkgrey;
    }

    .card-img-top {
    display: block;
    width: auto;
    max-height: 100%;
    margin: auto;
    }
    </style>

    <link rel='stylesheet' href='https://cdn.jsdelivr.net/gh/StudioKonKon/bootstrap-konkon@1.3.0-beta/dist/css/studio-konkon.min.css'>
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900'>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css'>

</head>
<body>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Strasbourg Curieux</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="/">Accueil</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/agenda">Agenda</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Les tests</a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="/test/1.php" target="_blank">Test 1</a>
                            <li><a class="dropdown-item" href="/test/2.php" target="_blank">Test 2</a>
                            <li><a class="dropdown-item" href="/test/3.php" target="_blank">Test 3</a>
                            <li><a class="dropdown-item" href="/test/4.php" target="_blank">Test 4</a>
                            <li><hr class="dropdown-divider"></li>
                            <li><a class="dropdown-item" href="/annuaire">Annuaire</a></li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                    </li>
                </ul>
                <form class="d-flex">
                    <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success" type="submit">Search</button>
                </form>
            </div>
        </div>
    </nav>

    <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="https://picsum.photos/1000/300" class="d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                    <h5>First slide label</h5>
                    <p>Some representative placeholder content for the first slide.</p>
                </div>
            </div>
            <div class="carousel-item">
                <img src="https://picsum.photos/1000/301" class="d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                    <h5>Second slide label</h5>
                    <p>Some representative placeholder content for the second slide.</p>
                </div>
            </div>
            <div class="carousel-item">
                <img src="https://picsum.photos/1000/302" class="d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                    <h5>Third slide label</h5>
                    <p>Some representative placeholder content for the third slide.</p>
                </div>
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>

    <div class="container">

        <ul class="nav justify-content-center my-3">
            <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="#">Active</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Link</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Link</a>
            </li>
            <li class="nav-item">
                <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
            </li>
        </ul>

        <div class="card text-center mt-4">
            <div class="card-header">
                <ul class="nav nav-tabs card-header-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="true" href="#">Active</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Link</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                    </li>
                </ul>
            </div>
            <div class="card-body">
                <h5 class="card-title">Special title treatment</h5>
                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                <a href="#" class="btn btn-primary">Go somewhere</a>
            </div>
        </div>

        <h1 class="mt-5">Les concerts de ce soir</h1>
        <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
            <div class="col">
                <div class="card">
                    <div class="card-image-wrapper">
                        <img src="https://via.placeholder.com/450x150/FFFFFF" class="card-img-top img-fluid" alt="...">
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                </div>
            </div>
                
            <div class="col">
                <div class="card">
                    <div class="card-image-wrapper">
                        <img src="https://via.placeholder.com/1920x1080/FFFFFF" class="card-img-top img-fluid" alt="...">
                    </div>
                    
                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                </div>
            </div>
                
            <div class="col">
                <div class="card">
                    <div class="card-image-wrapper">
                        <img src="https://via.placeholder.com/350x800/FFFFFF" class="card-img-top img-fluid" alt="...">
                    </div>
                    
                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row row-cols-1 row-cols-md-3 g-4 mt-2">
            <div class="col">
                <div class="card h-100">
                    <img src="https://picsum.photos/600/250" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                    </div>
                    <div class="card-footer">
                        <small class="text-muted">Last updated 3 mins ago</small>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card h-100">
                    <img src="https://picsum.photos/600/250" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
                    </div>
                    <div class="card-footer">
                        <small class="text-muted">Last updated 3 mins ago</small>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card h-100">
                    <img src="https://picsum.photos/600/250" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
                    </div>
                    <div class="card-footer">
                        <small class="text-muted">Last updated 3 mins ago</small>
                    </div>
                </div>
            </div>
        </div>

        <h1 class="mt-5 mb-3">Les accordéons</h1>
        <div class="accordion" id="accordionPanelsStayOpenExample">
            <div class="accordion-item">
                <h2 class="accordion-header" id="panelsStayOpen-headingOne">
                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true" aria-controls="panelsStayOpen-collapseOne">
                        Accordion Item #1
                    </button>
                </h2>
                <div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
                    <div class="accordion-body">
                        <nav class="nav flex-column">
                            <a class="nav-link active" aria-current="page" href="#">Active</a>
                            <a class="nav-link" href="#">Link</a>
                            <a class="nav-link" href="#">Link</a>
                            <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="panelsStayOpen-headingTwo">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseTwo" aria-expanded="false" aria-controls="panelsStayOpen-collapseTwo">
                        Accordion Item #2
                    </button>
                </h2>
                <div id="panelsStayOpen-collapseTwo" class="accordion-collapse collapse" aria-labelledby="panelsStayOpen-headingTwo">
                    <div class="accordion-body">
                        <strong>This is the second item's accordion body.</strong> It is hidden by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="panelsStayOpen-headingThree">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseThree" aria-expanded="false" aria-controls="panelsStayOpen-collapseThree">
                        Accordion Item #3
                    </button>
                </h2>
                <div id="panelsStayOpen-collapseThree" class="accordion-collapse collapse" aria-labelledby="panelsStayOpen-headingThree">
                    <div class="accordion-body">
                        <strong>This is the third item's accordion body.</strong> It is hidden by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
                    </div>
                </div>
            </div>
        </div>

        <h1 class="mt-5 mb-3">Les alertes</h1>
        <div class="alert alert-primary" role="alert">A simple primary alert—check it out!</div>
        <div class="alert alert-secondary" role="alert">A simple secondary alert—check it out!</div>
        <div class="alert alert-success" role="alert">A simple success alert—check it out!</div>
        <div class="alert alert-danger" role="alert">A simple danger alert—check it out!</div>
        <div class="alert alert-warning" role="alert">A simple warning alert—check it out!</div>
        <div class="alert alert-info" role="alert">A simple info alert—check it out!</div>
        <div class="alert alert-light" role="alert">A simple light alert—check it out!</div>
        <div class="alert alert-dark" role="alert">A simple dark alert—check it out!</div>
        <div class="alert alert-primary d-flex align-items-center" role="alert">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-exclamation-triangle-fill flex-shrink-0 me-2" viewBox="0 0 16 16" role="img" aria-label="Warning:">
                <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
            </svg>
            <div>
                An example alert with an icon
            </div>
        </div>


        <!--
        • https://getbootstrap.com/docs/5.1/utilities/position/
        • https://getbootstrap.com/docs/5.1/helpers/stacks/#horizontal
        • https://getbootstrap.com/docs/5.1/components/progress/
        • https://getbootstrap.com/docs/5.1/components/navs-tabs/
        //-->

        <div class="py-4">
            <div class="position-relative mb-4"
                style="height:2rem;">
                <div class="w-100 position-absolute top-50 start-50 translate-middle">
                    <div class="progress"
                        style="height:.25rem;">
                    <div class="progress-bar bg-primary"
                            id="progress-tab"
                            role="progressbar"
                            style="width:25%;"
                            aria-valuenow="50"
                            aria-valuemin="0"
                            aria-valuemax="100"></div>
                    </div>
                    <ul class="nav nav-tabs border-0 w-100 hstack justify-content-between position-absolute top-50 start-50 translate-middle"
                        id="skk-tabs">
                    <li class="nav-item" role="presentation">
                        <button type="button"
                                class="btn btn-sm btn-primary rounded-pill"
                                style="width:2rem;height:2rem;"
                                data-bs-toggle="tab"
                                data-bs-target="#tab1"
                                data-skk-value="1">1</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button type="button"
                                class="btn btn-sm btn-primary rounded-pill active"
                                style="width:2rem;height:2rem;"
                                data-bs-toggle="tab"
                                data-bs-target="#tab2"
                                data-skk-value="2">2</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button type="button"
                                class="btn btn-sm btn-secondary rounded-pill"
                                style="width:2rem;height:2rem;"
                                data-bs-toggle="tab"
                                data-bs-target="#tab3"
                                data-skk-value="3">3</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button type="button"
                                class="btn btn-sm btn-secondary rounded-pill"
                                style="width:2rem;height:2rem;"
                                data-bs-toggle="tab"
                                data-bs-target="#tab4"
                                data-skk-value="4">4</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button type="button"
                                class="btn btn-sm btn-secondary rounded-pill"
                                style="width:2rem;height:2rem;"
                                data-bs-toggle="tab"
                                data-bs-target="#tab5"
                                data-skk-value="5">5</button>
                    </li>
                    </ul>
                </div>
            </div>
            <div class="tab-content p-4">
                <div class="tab-pane animate__animated animate__flipInX" id="tab1">
                    <h4 class="">Tab 1</h4>
                    <p class="">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse feugiat sed leo sed finibus. Donec ac risus metus. Aenean porttitor, metus in elementum consequat, sapien sapien pulvinar arcu, vitae viverra dui nibh sed urna. Morbi vestibulum posuere elit ut mollis. Mauris eleifend rhoncus dui, et accumsan nibh finibus non. Ut ut urna facilisis magna faucibus commodo sodales at leo. Donec elit tellus, suscipit commodo egestas ut, venenatis quis sem. Nunc vitae fermentum nisi. Aliquam eget nibh vitae mauris convallis auctor vitae in tellus.</p>
                </div>
                <div class="tab-pane animate__animated animate__flipInX active" id="tab2">
                    <h4 class="">Tab 2</h4>
                    <p class="">Aenean vel nibh venenatis, iaculis metus quis, dapibus ante. Ut vel dapibus mauris. Etiam varius, odio non condimentum fermentum, dolor velit convallis nunc, in ultrices lectus lectus in ipsum. Morbi posuere tincidunt nisl, vitae volutpat eros interdum sit amet. Morbi quis sollicitudin quam. Praesent quis augue erat. Nunc purus sem, hendrerit sit amet posuere id, egestas eget nulla. Mauris vulputate commodo velit a pharetra. Quisque feugiat enim vitae dictum molestie. Aenean sit amet ante non quam elementum tristique. Integer scelerisque mi ut imperdiet tincidunt. Curabitur aliquam feugiat tristique.</p>
                </div>
                <div class="tab-pane animate__animated animate__flipInX" id="tab3">
                    <h4 class="">Tab 3</h4>
                    <p class="">Morbi nunc nunc, pretium et lacus in, blandit sagittis enim. Nunc vitae ipsum non magna volutpat ornare id nec leo. Aenean et accumsan lacus, sed hendrerit massa. Duis dapibus euismod eros, id fringilla turpis pharetra at. Proin quam augue, mattis sed mi nec, efficitur auctor elit. Donec sed lobortis enim. Maecenas varius ut mi quis tempor.</p>
                </div>
                <div class="tab-pane animate__animated animate__flipInX" id="tab4">
                    <h4 class="">Tab 4</h4>
                    <p class="">Donec auctor velit vel risus vulputate, quis posuere enim cursus. Aenean vestibulum, libero vulputate tempor egestas, lectus lacus porta neque, at molestie orci sem id est. Mauris eu eros placerat, condimentum diam vel, dignissim mi. Curabitur euismod lorem ac bibendum congue. In laoreet molestie nisi in maximus. Nulla facilisi. Vivamus maximus vel nisi a elementum. Nam ultrices purus nec varius viverra. Mauris vitae nibh sollicitudin, sagittis tellus quis, venenatis augue. Nam sed lacinia lectus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam blandit malesuada laoreet. Nam porta sem rhoncus iaculis suscipit.</p>
                </div>
                <div class="tab-pane animate__animated animate__flipInX" id="tab5">
                    <h4 class="">Tab 5</h4>
                    <p class="">Etiam varius leo quis mauris accumsan viverra. Phasellus sed ante vel enim auctor cursus ut quis leo. Curabitur in condimentum lorem. Integer in maximus neque, eget mattis metus. Praesent aliquam mauris at nisl venenatis elementum. Praesent volutpat dignissim sapien quis porta. Maecenas auctor sapien quis arcu ullamcorper, vel vestibulum arcu condimentum. Integer vel tincidunt dui. Morbi aliquet nulla at augue sollicitudin iaculis. Maecenas tempor placerat vestibulum. Cras scelerisque est et eros gravida, id volutpat enim vestibulum. Duis porta purus egestas fermentum vehicula.</p>
                </div>
            </div>
        </div>

        <div class="modal fade" id="exampleModalToggle" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalToggleLabel">Modal 1</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        Show a second modal and hide this one with the button below.
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" data-bs-target="#exampleModalToggle2" data-bs-toggle="modal" data-bs-dismiss="modal">Open second modal</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="exampleModalToggle2" aria-hidden="true" aria-labelledby="exampleModalToggleLabel2" tabindex="-1">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalToggleLabel2">Modal 2</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    Hide this modal and show the first with the button below.
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" data-bs-target="#exampleModalToggle" data-bs-toggle="modal" data-bs-dismiss="modal">Back to first</button>
                </div>
                </div>
            </div>
        </div>
        <a class="btn btn-primary" data-bs-toggle="modal" href="#exampleModalToggle" role="button">Open first modal</a>

    </div>

    <!-- Bootstrap Bundle -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    
    <script>
    console.clear();
    "use strict";

    // Example only, not for production use

    (function() {
    var tabContainer = document.querySelector("#skk-tabs"),
        tabEl = tabContainer.querySelectorAll("[data-bs-toggle='tab']"),
        progressTab = document.querySelector("#progress-tab");
    
    function tabEventShow(event) {
        var currentItem = this.parentNode,
            list = Array.from(currentItem.parentNode.children),
            position = list.indexOf(currentItem),
            length = list.length - 1,
            progressWidth = 0;
        
        progressWidth = (typeof(position) == "number") ? position : 0;
        progressWidth = (progressWidth > 0 && length > 0) ? (progressWidth / length * 100) : 0;
        
        progressTab.style.width = progressWidth + "%";

        tabEl.forEach(function(tab){
            if(list.indexOf(tab.parentNode) <= position) {
                tab.classList.add("btn-primary");
                tab.classList.remove("btn-secondary");
            } else {
                tab.classList.add("btn-secondary");
                tab.classList.remove("btn-primary");
            }
        });
    }
    
    tabEl.forEach(function(tab){
        tab.addEventListener("show.bs.tab", tabEventShow);
    });
    
    })();


    </script>
</body>
</html>