$( "#event_light_dateStart" ).datepicker({
  dateFormat: "dd/mm/yy",
  showAnim: "slideDown",
  minDate: "-1y",
  maxDate: "+2y",
  firstDay: 1,
  numberOfMonths: 2
})

$( "#event_light_dateEnd" ).datepicker({
  dateFormat: "dd/mm/yy",
  showAnim: "slideDown",
  minDate: "-1y",
  maxDate: "+2y",
  firstDay: 1,
  numberOfMonths: 2
})     
      
$( function() {
  var dateFormat = "dd/mm/yy",
    from = $( "#event_dateStart" )
      .datepicker({
        dateFormat: "dd/mm/yy",
        showAnim: "slideDown",
        minDate: "-1y",
        maxDate: "+2y",
        firstDay: 1,
        numberOfMonths: 2
      })
      .on( "change", function() {
        to.datepicker( "option", "minDate", getDate( this ) );
        multi.datepicker( "option", "minDate", getDateAfterStart( this ) );
      }),
    to = $( "#event_dateEnd" )
      .datepicker({
        dateFormat: "dd/mm/yy",
        showAnim: "slideDown",
        minDate: "-1y",
        maxDate: "+2y",
        firstDay: 1,
        numberOfMonths: 2
    })
    .on( "change", function() {
      from.datepicker( "option", "maxDate", getDate( this ) );
      multi.datepicker( "option", "maxDate", getDateBeforeEnd( this ) );
    }),
    multi = $('#event_dateMultiple').multiDatesPicker({
      dateFormat: "dd/mm/yy",
      minDate: "-1y",
      maxDate: "+2y",
      firstDay: 1,
      numberOfMonths: 2,
      altField: '#event_dateMultiple'
    });

  function getDate( element ) {
    var date;
    try {
      date = $.datepicker.parseDate( dateFormat, element.value );
    } catch( error ) {
      date = null;
    }

    return date;
  }

  function getDateAfterStart( element ) {
    var date;
    try {
      var date2 = $.datepicker.parseDate( dateFormat, element.value );
      date2.setDate(date2.getDate()+1);
      date = date2;
    } catch( error ) {
      date = null;
    }
    return date;
  }

  function getDateBeforeEnd( element ) {
    var date;
    try {
      var date2 = $.datepicker.parseDate( dateFormat, element.value );
      date2.setDate(date2.getDate()-1);
      date = date2;
    } catch( error ) {
      date = null;
    }
    return date;
  }

});

// Traduit en francais
jQuery(function($){
  $.datepicker.regional['fr'] = {
      monthNamesShort: ['Jan.','Fév.','Mars','Avril','Mai','Juin','Juil.','Août','Sept.','Oct.','Nov.','Déc.'],
      monthNames: ['Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
      dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
      };
  $.datepicker.setDefaults($.datepicker.regional['fr']);
});