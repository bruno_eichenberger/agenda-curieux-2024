// Permet d'afficher le nom du fichier téléchargé
$('.custom-file-input').on('change', function(event) {
    var inputFile = event.currentTarget;
    $(inputFile).parent()
        .find('.custom-file-label')
        .html(inputFile.files[0].name);
});

// Permet d'afficher la date de fin

$("#event_dateStart").on("change", function(){
    var dateStart = $("#event_dateStart").val();
    if (dateStart.length < 10) {
        $('#boutonDateFin').hide();
    } else {
        $('#boutonDateFin').show();
    }
});

$('#boutonDateFin').on("click",function(){
    $('#blocDateFin').show();
    $('#boutonDateFin').hide();
    $('#event_dateEnd').focus();
});

// Permet d'afficher l'horaire' de fin

$("#event_timeStart").on("change", function(){
    var dateStart = $("#event_timeStart").val();
    if (dateStart.length < 5) {
        $('#bouton_blockTimeEnd').hide();
    } else {
        $('#bouton_blockTimeEnd').show();
    }
});

$('#boutonTimeEnd').on("click",function(){
    $('#blockTimeEnd').show();
    $('#boutonTimeEnd').hide();
    $('#event_timeEnd').focus();
});

$("#event_choiceOfDays").select2({
    closeOnSelect : false,
    placeholder : "Tous les ...",
    //allowHtml: true,
    //allowClear: true,
    //tags: true
});

/**
 * fix bug width input search
 */
$(".select2-selection__rendered").each(function (index, value) {
    var li = $(value).find('.select2-selection__choice');
    if($(li).length <= 0){
        var inputSearch = $(value).find('.select2-search__field');
        $(inputSearch).css('width', '100%');
    }
});

// Pour cacher le calendrier multidate si pas nécessaire
//$(document).ready(function(){
//});
$("#event_precisionDate").on('change',function(){
    if($(this).val() == "2"){
        $("#block_choiceOfDays").show();
    }
    else {
        $("#block_choiceOfDays").hide();
    }
    
    if($(this).val() == "3"){
        $("#event_dateMultiple").show();
    }
    else {
        $("#event_dateMultiple").hide();
    }
});


// Pour sélectionner les horaires avec clockpicker
var input = $('#event_timeStart');
input.clockpicker({
    autoclose: true
});
var input = $('#event_timeEnd');
input.clockpicker({
    autoclose: true
});

// Gestion des suggestions de category
$(".blocSubDetail button").on("click", function(){
    var id = $(this).attr("data-id");
    var text = $(this).attr("data-text");

    //console.log($("#event_category").val());

    if (
        $("#event_category").val().length < 4 &&
        $("#event_category").val().includes(id) === false
        //$("#event_category").find("option[value='" + id + "']").length == 0
    ) {
        var newOption = new Option(text, id, false, true);
        $('#event_category').append(newOption).trigger('change');
    }
    //var selections = ( JSON.stringify($(this).select2('data')) );
    //console.log(selections.length);
});

// Cacher form category multi select 2 si rubric est vide
$('#event_rubric').on('change',function() {
    var rubric = $(this).val();
        //console.log(rubric);
        if (rubric == ""){
            $(".block-cat .blocSubDetail").hide();
        } else {
            $(".block-cat .blocSubDetail").show();
        }
});






// Interaction si lieu dans l'annuaire
var $place = $('#event_place');

$place.on("select2:selecting", function(e) {
    
    // ... retrieve the corresponding form.
    var $form = $(this).closest('form');

    // Simulate form data, but only include the selected place value.
    var data = {};
    data["id"] = e.params.args.data.id;
    var $formRoom = $('#event_rooms');

    // Si l'id est un nombre, le lieu est en base de donnée
    if (typeof(data["id"]) === 'number') {


        // Submit data via AJAX to the form's action path.
        $.ajax({
            url : "/search_room",
            data : data,
            complete: function(html) {

                /*
                $('#event_rooms').empty().trigger("change");

                $('#event_rooms').select2({
                    data: JSON.parse(html.responseText)
                })
                */

                var arrayOfRooms = JSON.parse(html.responseText);

                $formRoom.empty();

                if (arrayOfRooms.length > 0){
                        
                    $formRoom.show();

                    $formRoom.append($('<option>', {
                        value: null,
                        text: 'Préciser la salle'
                    }));

                    $.each(arrayOfRooms, function (i, item) {
                        $formRoom.append($('<option>', { 
                            value: item.id,
                            text : item.text 
                        }));
                    });
                
                } else {
                    $formRoom.empty();
                    $formRoom.hide();
                }


            }
        });


        /*
        //console.log(e.params.args.data);
        var idplace = e.params.args.data.text;
        if (idplace.includes("(Hors annuaire)") === true){
            $(".block-city").show();
        }
        else {
            $(".block-city").hide();
        }
        */
       
        $(".block-city").hide();


    } else {
        $formRoom.empty();
        $formRoom.hide();
        $(".block-city").show();
    }


   
});

// Afficher form Adresse si la ville est renseigné
$("#event_city").on("select2:selecting", function(e) {
    $("#form-address").show();
});

$("#event_precisionPrice").on('change',function(){
    
    if ($(this).val() == "1" || $(this).val() == "2"){
        $("#show_price").hide();
        $("#show_prices").hide();
        $("#show_tickets").show();
    } 
    else if ($(this).val() == "3"){
        $("#show_price").show();
        $("#show_prices").show();
        $("#show_tickets").show();
    } 
    else {
        $("#show_price").hide();
        $("#show_prices").hide();
        $("#show_tickets").hide();
    }

});

/*
$('#testButton').on('click', function() {

    var types = ['green','sweet'];
    $("#event_category").val(types).trigger('change');
    
});
*/

// Pour la mise en page de la description

/*
$.trumbowyg.svgPath = '/js/icons.svg';

$('#event_description').trumbowyg({
    lang: 'fr',
    btns: [
        ['viewHTML'],
        ['undo', 'redo'], // Only supported in Blink browsers
        ['formatting'],
        ['bold', 'italic', 'underline'], // 'strikethrough'
        //['strong', 'em', 'del'],
        //['superscript', 'subscript'],
        ['link'],
        //['insertImage'],
        ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
        ['unorderedList', 'orderedList'],
        ['horizontalRule'],
        ['removeformat'],
        ['fullscreen']
    ]
});
*/
// Pour la recherche de vidéos Youtube
function keyWordsearch(){
    $(".loader").show();
    gapi.client.setApiKey("AIzaSyClSmgG5JEs-F-wNNCWkGsqNqmk6-4q5n4");
    gapi.client.load('youtube', 'v3', function() {
        makeRequest();
    });
}

$('#event_more').on('click',function(){
    if( $('#event_more').is(':checked') ){
        $("#input_price").hide();
    } else {
        $("#input_price").show();
    }
});


// Pour générer les champs infinis

$('#add-image').on('click',function(){
    // Je récupère le numéro des champs que je vais créer
    const index = +$('#widgets-counter-image').val();

    // Je récupère le prototype des entrées
    const tmpl = $('#event_images').data('prototype').replace(/__name__/g, index);

    // J'injecte ce code dans la div
    $('#event_images').append(tmpl);

    $('#widgets-counter-image').val(index + 1);

    // Je gère le bouton de suppression
    handleDeleteButtons();

    //console.log(tmpl);
});

function handleDeleteButtons() {
    $('button[data-action="delete"]').on('click', function(){
        const target = this.dataset.target;
        //console.log(target);
        $(target).remove();
    });
}

function updateCounter() {
    const count = +$('#event_images div.form-group').length;

    $('#widgets-counter-image').val(count);
}

// J'appele les fonctions au chargement
updateCounter();
handleDeleteButtons();




// Copier-coller pour les vidéos

$('#add-video').on('click',function(){
    // Je récupère le numéro des champs que je vais créer
    const index = +$('#widgets-counter-video').val();

    // Je récupère le prototype des entrées
    const tmpl = $('#event_videos').data('prototype').replace(/__name__/g, index);

    // J'injecte ce code dans la div
    $('#event_videos').append(tmpl);

    $('#widgets-counter-video').val(index + 1);

    // Je gère le bouton de suppression
    handleDeleteButtonsVideo();

    //console.log(tmpl);
});

function handleDeleteButtonsVideo() {
    $('button[data-action="delete"]').on('click',function(){
        const target = this.dataset.target;
        //console.log(target);
        $(target).remove();
    });
}

function updateCounterVideo() {
    const count = +$('#event_videos div.form-group').length;

    $('#widgets-counter-video').val(count);
}

// J'appele les fonctions au chargement
updateCounterVideo();
handleDeleteButtonsVideo();









// Copier-coller pour les prix

$('#add-price').on('click',function(){
    // Je récupère le numéro des champs que je vais créer
    const index = +$('#widgets-counter-price').val();

    // Je récupère le prototype des entrées
    const tmpl = $('#event_prices').data('prototype').replace(/__name__/g, index);

    // J'injecte ce code dans la div
    $('#event_prices').append(tmpl);

    $('#widgets-counter-price').val(index + 1);

    // Je gère le bouton de suppression
    handleDeleteButtonsPrice();

    //console.log(tmpl);
});

function handleDeleteButtonsPrice() {
    $('button[data-action="delete"]').on('click',function(){
        const target = this.dataset.target;
        //console.log(target);
        $(target).remove();
    });
}

function updateCounterPrice() {
    const count = +$('#event_prices div.form-group').length;

    $('#widgets-counter-price').val(count);
}

// J'appele les fonctions au chargement
updateCounterPrice();
handleDeleteButtonsPrice();




// Copier-coller pour les billeteries

$('#add-ticket').on('click',function(){
    // Je récupère le numéro des champs que je vais créer
    const index = +$('#widgets-counter-ticket').val();

    // Je récupère le prototype des entrées
    const tmpl = $('#event_tickets').data('prototype').replace(/__name__/g, index);

    // J'injecte ce code dans la div
    $('#event_tickets').append(tmpl);

    $('#widgets-counter-ticket').val(index + 1);

    // Je gère le bouton de suppression
    handleDeleteButtonsTicket();

    //console.log(tmpl);
});

function handleDeleteButtonsTicket() {
    $('button[data-action="delete"]').on('click',function(){
        const target = this.dataset.target;
        //console.log(target);
        $(target).remove();
    });
}

function updateCounterTicket() {
    const count = +$('#event_tickets div.form-group').length;

    $('#widgets-counter-ticket').val(count);
}

// J'appele les fonctions au chargement
updateCounterTicket();
handleDeleteButtonsTicket();


$('#add-link').on('click',function(){
    // Je récupère le numéro des champs que je vais créer
    const index = +$('#widgets-counter-link').val();

    // Je récupère le prototype des entrées
    const tmpl = $('#event_links').data('prototype').replace(/__name__/g, index);

    // J'injecte ce code dans la div
    $('#event_links').append(tmpl);

    $('#widgets-counter-link').val(index + 1);

    // Je gère le bouton de suppression
    handleDeleteButtonsLink();

    //console.log(tmpl);
});

function handleDeleteButtonsLink() {
    $('button[data-action="delete"]').on('click',function(){
        const target = this.dataset.target;
        //console.log(target);
        $(target).remove();
    });
}

function updateCounterLink() {
    const count = +$('#event_links div.form-group').length;

    $('#widgets-counter-link').val(count);
}

// J'appele les fonctions au chargement
updateCounterLink();
handleDeleteButtonsLink();

