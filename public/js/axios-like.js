function onClickBtnLike(data){
    data.preventDefault();

    const url = this.href;
    const spanCount = this.querySelector('span.js-likes');
    const spanText = this.querySelector('span.js-label');
    const icone = this.querySelector('i');

    axios.post(url).then(function(response){
        spanCount.textContent = response.data.likes;
        if(icone.classList.contains('fa-star')){
            icone.classList.replace('fa-star','fa-heart');
            spanText.textContent = 'Retirer des favoris';
        } else {
            icone.classList.replace('fa-heart','fa-star');
            spanText.textContent = 'Ajouter aux favoris';
        }

    }).catch(function (error) {
        if(error.response.status === 403) {
            window.alert("Vous devez vous connecter pour ajouter à vos favoris")
        } else {
            window.alert("Une erreur s'est produite, veuillez réessayer plus tard")
        }
    });
}

document.querySelectorAll('a.js-like').forEach(function (link) {
    link.addEventListener('click', onClickBtnLike);
})