//Cross Domain
/*
$.ajaxPrefilter(function(options) {
    if (options.crossDomain && jQuery.support.cors) {
        var http = (window.location.protocol === 'http:' ? 'http:' : 'https:');
        options.url = http + '//cors-anywhere.herokuapp.com/' + options.url;
        //options.url = "http://cors.corsproxy.io/url=" + options.url;
    }
});


*/
function localPicChange() {
    
    var localPicFiles = document.getElementById('event_coverImageFile').files;
    
    // Get the file
    if (FileReader && localPicFiles.length == 1) {
        var fr = new FileReader();
        fr.onload = function () {
            SetOutput(fr.result);
        };
        fr.readAsDataURL(localPicFiles[0]);
    }
}


function localPicChangePlace() {
    
    var localPicFiles = document.getElementById('place_coverImageFile').files;
    
    // Get the file
    if (FileReader && localPicFiles.length == 1) {
        var fr = new FileReader();
        fr.onload = function () {
            SetOutput(fr.result);
        };
        fr.readAsDataURL(localPicFiles[0]);
    }
}

/*
function urlPicChange() {
    var urlPic = document.getElementById('urlPic').value;
    $.get(urlPic, function(data){
            var canvas = document.createElement('CANVAS'),
    ctx = canvas.getContext('2d'),
        dataURL;
        canvas.height = this.height;
        canvas.width = this.width;
        ctx.drawImage(this, 0, 0);
        dataURL = canvas.toDataURL('image/png');
        SetOutput(dataURL);
        canvas = null;
    })
    img.src = urlPic;
}
*/
function SetOutput(outP) {
    const newLocal = 'outputPic';
    document.getElementById(newLocal).src = outP;
    document.getElementById('outputTxt').value = outP;
}

