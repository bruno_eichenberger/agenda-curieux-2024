/*** RESIZE THE MULTI IFRAME ***/

function resizeEvent(event) {
    var res = event.data.split("-");
    if (res[0] == 'weezmulti' || res[0].indexOf('weezuniq') == 0) {
        if (document.getElementById(res[0])) {
            document.getElementById(res[0]).height = res[1] + 'px';
        }
    } else if (event.data.indexOf('SCROLLFORM') === 0) {
        var internalOffsetHeigth = res[1];
        var iframeContentHeigth = parseFloat(res[2]);
        var iframe = document.getElementById('weezmulti');
        var iframeHeigth = parseFloat(iframe.scrollHeight);
        var offset = 0;
        if (iframeHeigth >= iframeContentHeigth) {
            offset = internalOffsetHeigth;
        }
        window.scrollTo(0, parseFloat(elmYPosition(iframe.id)) + parseFloat(offset));
    }
    if (document.getElementsByClassName('weezmulticlass').length > 1 ){
        for(var i = 0; i <= document.getElementsByClassName('weezmulticlass').length; i++) {
            if (document.getElementsByClassName('weezmulticlass')[i]) {
                document.getElementsByClassName('weezmulticlass')[i].height = res[1] + 'px';
            }
        }
    }

}

if (!window.addEventListener) {
    window.attachEvent("onmessage", resizeEvent);
} else {
    window.addEventListener("message", resizeEvent, false);
}


/*** SMOOTH SCROLL TO THE TOP OF THE FRAME ONLOAD ***/

function currentYPosition() {
    if (self.pageYOffset) return self.pageYOffset;
    if (document.documentElement && document.documentElement.scrollTop) return document.documentElement.scrollTop;
    if (document.body.scrollTop) return document.body.scrollTop;
    return 0;
}

function elmYPosition(eID) {
    var elm = document.getElementById(eID);
    var y = elm.offsetTop;
    var node = elm;
    while (node.offsetParent && node.offsetParent != document.body) {
        node = node.offsetParent;
        y += node.offsetTop;
    }
    return y;
}

function smoothScroll(eID) {
    var startY = currentYPosition();
    var stopY = elmYPosition(eID);
    var distance = stopY > startY ? stopY - startY : startY - stopY;
    if (distance < 100) {
        scrollTo(0, stopY); return;
    }
    var speed = Math.round(distance / 100);
    if (speed >= 20) speed = 20;
    var step = Math.round(distance / 25);
    var leapY = stopY > startY ? startY + step : startY - step;
    var timer = 0;
    if (stopY > startY) {
        for ( var i=startY; i<stopY; i+=step ) {
            setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
            leapY += step; if (leapY > stopY) leapY = stopY; timer++;
        } return;
    }
    for ( var i=startY; i>stopY; i-=step ) {
        setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
        leapY -= step; if (leapY < stopY) leapY = stopY; timer++;
    }
}

/*** ONLOAD BINDIG ***/

document.getElementById("weezmulti").onload = function(){
  document.getElementById("weezmulti").onload = function(){
    smoothScroll('weezmulti');
  };
};
