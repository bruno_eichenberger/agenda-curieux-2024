function focusDateFin() {
    var editor = $('#event_dateEnd');
    var value = editor.val();
    editor.val("");
    editor.focus();
    editor.val(value);
}

function focusTimeEnd() {
    var editor = $('#event_timeEnd');
    var value = editor.val();
    editor.val("");
    editor.focus();
    editor.val(value);
}

function afficher_cacherDate(id)
{
    if(document.getElementById(id).style.display=="none")
    {
        document.getElementById(id).style.display="block";
        document.getElementById('bouton_'+id).innerHTML="<h5 class='form-title'>jusqu'au</h3>";
    }
    return true;
}

function afficher_cacherTime(id)
{
    if(document.getElementById(id).style.display=="none")
    {
        document.getElementById(id).style.display="block";
        document.getElementById('bouton_'+id).innerHTML="<h5 class='form-title'>jusqu'à</h3>";
    }
    return true;
}

$(document).ready(function() {

    $('#event_dateStart').bind('change', function() {
        var elements = $('button#boutonDateFin').hide(); 
        var value = $(this).val();

        if (value!='') {elements.filter('#boutonDateFin').show();}
    }).trigger('change');

    $('#event_timeStart').bind('change', function() {
        var elements = $('button#boutonTimeEnd').hide(); 
        var value = $(this).val();

        if (value!='') {elements.filter('#boutonTimeEnd').show();}
    }).trigger('change');

});

//afficher_cacherDate('blocDateFin');