$('#add-title').click(function(){
    // Je récupère le numéro des champs que je vais créer
    const index = +$('#widgets-counter-title').val();

    // Je récupère le prototype des entrées
    const tmpl = $('#place_multiTitles').data('prototype').replace(/__name__/g, index);

    // J'injecte ce code dans la div
    $('#place_multiTitles').append(tmpl);

    $('#widgets-counter-title').val(index + 1);

    // Je gère le bouton de suppression
    handleDeleteButtonsTitle();

    //console.log(tmpl);
});

function handleDeleteButtonsTitle() {
    $('button[data-action="delete"]').click(function(){
        const target = this.dataset.target;
        //console.log(target);
        $(target).remove();
    });
}

function updateCounterTitle() {
    const count = +$('#place_multiTitles div.form-group').length;

    $('#widgets-counter-title').val(count);
}

// J'appele les fonctions au chargement
updateCounterTitle();
handleDeleteButtonsTitle();


$('#add-link').click(function(){
    // Je récupère le numéro des champs que je vais créer
    const index = +$('#widgets-counter-link').val();

    // Je récupère le prototype des entrées
    const tmpl = $('#place_links').data('prototype').replace(/__name__/g, index);

    // J'injecte ce code dans la div
    $('#place_links').append(tmpl);

    $('#widgets-counter-link').val(index + 1);

    // Je gère le bouton de suppression
    handleDeleteButtonsLink();

    //console.log(tmpl);
});

function handleDeleteButtonsLink() {
    $('button[data-action="delete"]').click(function(){
        const target = this.dataset.target;
        //console.log(target);
        $(target).remove();
    });
}

function updateCounterLink() {
    const count = +$('#place_links div.form-group').length;

    $('#widgets-counter-link').val(count);
}

// J'appele les fonctions au chargement
updateCounterLink();
handleDeleteButtonsLink();


$('#add-room').click(function(){
    // Je récupère le numéro des champs que je vais créer
    const index = +$('#widgets-counter-room').val();

    // Je récupère le prototype des entrées
    const tmpl = $('#place_rooms').data('prototype').replace(/__name__/g, index);

    // J'injecte ce code dans la div
    $('#place_rooms').append(tmpl);

    $('#widgets-counter-room').val(index + 1);

    // Je gère le bouton de suppression
    handleDeleteButtonsRoom();

    //console.log(tmpl);
});

function handleDeleteButtonsRoom() {
    $('button[data-action="delete"]').click(function(){
        const target = this.dataset.target;
        //console.log(target);
        $(target).remove();
    });
}

function updateCounterRoom() {
    const count = +$('#place_rooms div.form-group').length;

    $('#widgets-counter-room').val(count);
}

// J'appele les fonctions au chargement
updateCounterRoom();
handleDeleteButtonsRoom();


/* Permettrait d'afficher le formulaire 'quartier' dès qu'une ville est choisi : https://www.youtube.com/watch?v=F0Z-D3MSjA0
$(document).on('change','#place_city, #place_district',function() {
    let $field = $(this)
    let $form = $field.closest('form')
    let data = {}
    data[$field.attr('name')] = $field.val()
    $.post($form.attr('action'), $data).then(function (data){
        let $input = $(data).find('#place_district')
        $('#place_district').replaceWith($input)
    })
})
*/