<?php

namespace App\ApiKernel;

use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpKernel\KernelEvents;
use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use App\Entity\Event;

class EventAuthorSubscriber implements EventSubscriberInterface 
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
    * @return array
    */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['setAuthorForEvent', EventPriorities::PRE_VALIDATE]
        ];
    }

    public function setAuthorForEvent(GetResponseForControllerResultEvent $response)
    {
        $result = $response->getControllerResult();
        $method = $response->getRequest()->getMethod();

        if ($result instanceof Event and $method === "POST"){
            // Utilisateur actuellement connecté
            $author = $this->security->getUser();
            //dd($user);
            // Associer l'utilisateur à l'évènement qu'il est en train de créer
            $result->setAuthor($author);
        }
    }
 
}