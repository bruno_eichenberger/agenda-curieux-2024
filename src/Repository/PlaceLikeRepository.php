<?php

namespace App\Repository;

use App\Entity\PlaceLike;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PlaceLike|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlaceLike|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlaceLike[]    findAll()
 * @method PlaceLike[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlaceLikeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlaceLike::class);
    }

    // /**
    //  * @return PlaceLike[] Returns an array of PlaceLike objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PlaceLike
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
