<?php

namespace App\Repository;

use App\Entity\Artist;
use Doctrine\ORM\Query;
use App\Entity\ArtistSearch;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Artist|null find($id, $lockMode = null, $lockVersion = null)
 * @method Artist|null findOneBy(array $criteria, array $orderBy = null)
 * @method Artist[]    findAll()
 * @method Artist[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArtistRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Artist::class);
    }

    /**
     * @return Query
     */
    public function findForVerification($borough, $request=NULL): Query
    {
        $query = $this->createQueryBuilder('a');
        $query->orderBy('a.id','DESC');

        if ($borough){
            $query  ->leftJoin('a.events', 'e')
                    ->leftJoin('e.place', 'p')
                    ->leftJoin('p.city', 'cp')
                    ->leftJoin('e.city', 'ce')
                    ->leftJoin('cp.borough', 'bp')
                    ->leftJoin('ce.borough', 'be')
                    ->andWhere('bp.slug = :borough')
                    ->orWhere('be.slug = :borough')
                    ->setParameter('borough', $borough);
        }

        return $query->getQuery();
    }

    /**
     * @return Query
     */
    public function findAllSearchByFilter(ArtistSearch $search, $borough): Query
    {
        $query = $this->findAllSearch($borough);
        
        if ($search->getNom())
        {
            $query = $query ->andWhere('a.name LIKE :nom')
                            ->setParameter('nom', '%'.$search->getNom().'%' )
            ;
        }
        /*
        if ($search->getGenre()){

            $query = $query ->leftJoin('a.artistkind', 'k')
                            ->andWhere('k.slug = :artistkind')
                            ->setParameter('artistkind', $slug);
            ;
        }
        */
        if ($search->getVille())
        {
            $query = $query ->andWhere('cp.slug = :city OR ce.slug = :city')
                            ->setParameter('city', $search->getVille()->getSlug());
            ;
        }
        
        return $query->getQuery();
    }

    /**
     * @return Query
     */
    public function findAllArtists($borough): Query
    {
        $query = $this ->findAllSearch($borough);
        return $query->getQuery();
    }
    
    /**
     * @return Query
     */
    public function findTopArtist($borough): Query
    {
        $query = $this ->findAllSearch($borough);
        $query ->setMaxResults(5);
        return $query->getQuery();
    }

    /**
     * @return Query
     */
    public function findByArtistkind($slug, $borough): Query
    {
        $query = $this->findAllSearch($borough);

        $query  ->leftJoin('a.artistkind', 'k')
                ->andWhere('k.slug = :artistkind')
                ->setParameter('artistkind', $slug);

        return $query->getQuery();
    }

    /**
    * @return QueryBuilder
    */ 
    public function findAllSearch($borough=null): QueryBuilder
    {
        $query = $this->createQueryBuilder('a');

        $query  ->leftJoin('a.events', 'e')
                ->groupBy('a')
                ->orderby('COUNT(e)', 'DESC');
 
        if ($borough){
 
             $query  ->leftJoin('e.place', 'p')
                     ->leftJoin('p.city', 'cp')
                     ->leftJoin('e.city', 'ce')
                     ->leftJoin('cp.borough', 'bp')
                     ->leftJoin('ce.borough', 'be')
                     ->andWhere('bp.slug = :borough OR be.slug = :borough')
                     ->setParameter('borough', $borough);
        }

        return $query;
    }
 
    public function findByName($text): array {
        $qb =  $this->createQueryBuilder('a')
                    ->where('a.name LIKE :name')
                    ->setParameter('name', '%'.$text.'%')
                    ->getQuery();
        return $qb->execute();
    }

    // /**
    //  * @return Artist[] Returns an array of Artist objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Artist
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
