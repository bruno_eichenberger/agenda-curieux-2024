<?php

namespace App\Repository;

use Doctrine\ORM\Query;
use App\Entity\Festival;
use App\Entity\FestivalSearch;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Festival|null find($id, $lockMode = null, $lockVersion = null)
 * @method Festival|null findOneBy(array $criteria, array $orderBy = null)
 * @method Festival[]    findAll()
 * @method Festival[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FestivalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Festival::class);
    }

    /**
     * @return Query
     */
    public function findForVerification($borough, $request=NULL): Query
    {
        $query = $this->createQueryBuilder('f');
        $query->orderBy('f.id','DESC');

        if ($borough){
            $query  ->leftJoin('f.events', 'e')
                    ->leftJoin('e.place', 'p')
                    ->leftJoin('p.city', 'cp')
                    ->leftJoin('e.city', 'ce')
                    ->leftJoin('cp.borough', 'bp')
                    ->leftJoin('ce.borough', 'be')
                    ->andWhere('bp.slug = :borough')
                    ->orWhere('be.slug = :borough')
                    ->setParameter('borough', $borough);
        }

        return $query->getQuery();
    }


    public function findByTitle($text): array {
        $qb =  $this->createQueryBuilder('f')
                    ->where('f.title LIKE :title')
                    ->setParameter('title', '%'.$text.'%')
                    ->getQuery();
        return $qb->execute();
    }

    /**
     * @return Query
     */
    public function findAllSearchByFilter(FestivalSearch $search, $borough): Query
    {
        $query = $this->findAllSearch($borough);

        if ($search->getNom())
        {
            $query = $query ->andWhere('f.title LIKE :nom')
                            ->setParameter('nom', '%'.$search->getNom().'%' )
            ;
        }
        
        if ($search->getRubrique()){

            $query = $query ->leftJoin('e.rubric', 'r')
                            ->andWhere('r.slug = :rubric')
                            ->setParameter('rubric', $search->getRubrique()->getSlug());
            ;
        }
        
        if ($search->getVille())
        {
            $query = $query ->andWhere('cp.slug = :city OR ce.slug = :city')
                            ->setParameter('city', $search->getVille()->getSlug());
            ;
        }

        return $query->getQuery();
    }

    /*
    public function findAllVisibleQuery($borough): Query
    {
        return $this->findAllSearch($borough)->getQuery();
    }
    */

    /**
     * @return Query
     */
    public function findByCity($slug, $borough): Query
    {
        $query = $this->findAllSearch($borough);

        $query  ->andWhere('cp.slug = :city OR ce.slug = :city')
                ->setParameter('city', $slug);
        
        //Prévoir une redirection si slug de la ville ne correspond pas à l'arrondissement
        /*
        $query = $this->findAllSearch();

        $query  ->leftJoin('e.place', 'p')
                ->leftJoin('p.city', 'cp')
                ->leftJoin('e.city', 'ce')
                ->andWhere('cp.slug = :city OR ce.slug = :city')
                ->setParameter('city', $slug);
        */

        return $query->getQuery();
    }

    /**
     * @return Query
     */
    public function findByRubric($slug, $borough): Query
    {
        $query = $this->findAllSearch($borough);

        $query  ->leftJoin('e.rubric', 'r')
                ->andWhere('r.slug = :rubric')
                ->setParameter('rubric', $slug);

        return $query->getQuery();
    }
    
    /**
     * @return QueryBuilder
     */ 
    public function findAllSearch($borough=null): QueryBuilder
    {
        $query = $this->createQueryBuilder('f');

        $query  ->leftJoin('f.events', 'e')
                ->leftJoin('e.place', 'p')
                ->leftJoin('p.city', 'cp')
                ->leftJoin('e.city', 'ce')
                ->addSelect('COUNT(f.id) AS countOfEvents')
                ->addSelect('MIN(e.dateStart) as minDateStart')
                ->addSelect('MAX(e.dateEnd) as maxDateEnd')
                ->addSelect('e.coverImage as eventImage')
                ->groupBy('f.id')
                ->addOrderBy('countOfEvents', 'DESC')
                ->addOrderBy('minDateStart', 'ASC')
                ->addOrderBy('maxDateEnd', 'DESC');

        if ($borough){

            $query  
                    ->leftJoin('cp.borough', 'bp')
                    ->leftJoin('ce.borough', 'be')
                    ->andWhere('bp.slug = :borough OR be.slug = :borough')
                    ->setParameter('borough', $borough);
        }

        $query  ->andWhere('e.dateEnd >= :today')
                ->setParameter('today', date('Y-m-d'));

        return $query;
    }

    // /**
    //  * @return Festival[] Returns an array of Festival objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Festival
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
