<?php

namespace App\Repository;

use App\Entity\Rubric;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Rubric|null find($id, $lockMode = null, $lockVersion = null)
 * @method Rubric|null findOneBy(array $criteria, array $orderBy = null)
 * @method Rubric[]    findAll()
 * @method Rubric[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RubricRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Rubric::class);
    }

    /**
     * @return Query
     */
    public function findAllByCategory(): Query
    {
        $query = $this->createQueryBuilder('r');

        $query  ->leftJoin('r.category', 'c')
                ->addSelect('r')
                ->addSelect('c')
                ->orderBy('c.name')
                ->groupBy('c.id')
                ->addGroupBy('r.id');
            
        return $query->getQuery();
    }

    /**
     * @return Query
     */
    public function findByCategory($slug, $borough): Query
    {
        $query = $this->findAllSearch($borough);

        $query  ->leftJoin('e.category', 'c')
                ->andWhere('c.slug = :slug')
                ->setParameter('slug', $slug);
            
        return $query->getQuery();
    }

    /**
     * @return QueryBuilder
     */
    public function findAllSearch($borough=NULL): QueryBuilder
    {
        $query = $this->createQueryBuilder('r');

        $query->join('r.events', 'e');

        if ($borough)
        {
            $query  ->leftJoin('e.place', 'place')
                    ->leftJoin('place.city', 'cityByPlace')
                    ->leftJoin('cityByPlace.borough', 'boroughByPlace')
                    ->leftJoin('e.city', 'city')
                    ->leftJoin('city.borough', 'boroughByCity')
                    ->andWhere('boroughByPlace.slug = :borough')
                    ->orWhere('boroughByCity.slug = :borough')
                    ->setParameter('borough', $borough);
        }

        $query  ->select('r.name','r.slug','count(e.id) AS countOfEvents')
                ->groupBy('r.id')
                ->andWhere('e.dateEnd >= :today')
                ->setParameter('today', date('Y-m-d'))
                ->orderBy('countOfEvents', 'DESC')
                ;

        return $query;

    }
    // /**
    //  * @return Rubric[] Returns an array of Rubric objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Rubric
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
