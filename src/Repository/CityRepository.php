<?php

namespace App\Repository;

use App\Entity\City;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method City|null find($id, $lockMode = null, $lockVersion = null)
 * @method City|null findOneBy(array $criteria, array $orderBy = null)
 * @method City[]    findAll()
 * @method City[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, City::class);
    }

    public function findByName($text): array {
        $qb =  $this->createQueryBuilder('c')
                    ->where('c.name LIKE :name or c.slug LIKE :name')
                    ->setParameter('name', $text.'%')
                    ->setMaxResults(10)
                    ->getQuery();
        return $qb->execute();
    }

    public function findByBorough($borough): array {
        $qb =  $this->createQueryBuilder('c')
                    ->leftJoin('c.borough','b')
                    ->where('b.slug = :slug')
                    ->setParameter('slug', $borough)
                    ->getQuery();

        return $qb->execute();
    }

    public function searchCity($slug,$postcode=null)
    {
        $query = $this->createQueryBuilder('c');

        $query  ->andWhere('c.slug LIKE :slug')
                ->setParameter('slug', '%'.$slug.'%')
                ->setMaxResults(1)
        ;

        if ($postcode){
            $query  ->leftJoin('c.postcodes','p')
                    ->andWhere('p.code = :postal')
                    ->setParameter('postal', $postcode)
            ;
        }

        return $query->getQuery()->getResult();
    }

    // /**
    //  * @return City[] Returns an array of City objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?City
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
