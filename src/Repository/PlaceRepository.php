<?php

namespace App\Repository;

use App\Entity\Place;
use App\Entity\PlaceSearch;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Place|null find($id, $lockMode = null, $lockVersion = null)
 * @method Place|null findOneBy(array $criteria, array $orderBy = null)
 * @method Place[]    findAll()
 * @method Place[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlaceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Place::class);
    }

    public function findIfAlreadyExist($titleSlug, $titleLower, $addressLower, $city)
    {
        $query = $this->createQueryBuilder('p');

        $query  ->leftJoin('p.city','c')
                ->andWhere('c = :city')
                ->andWhere('(p.slug like :titleslug or LOWER(p.title) like :titlelower or LOWER(p.multiTitles) like :titlelower) and (LOWER(p.address) like :addresslower)')
                ->setParameter('city', $city)
                ->setParameter('titlelower', '%'.$titleLower.'%')
                ->setParameter('titleslug', '%'.$titleSlug.'%')
                ->setParameter('addresslower', '%'.$addressLower.'%')
                //->setMaxResults(1)
        ;

        return $query->getQuery()->getResult();
    }


    public function searchPlace($slugplace,$slugcity)
    {
        $query = $this->createQueryBuilder('p');

        $query  ->leftJoin('p.city','c')
                ->andWhere('c.slug LIKE :slugcity')
                ->andWhere('p.slug LIKE :slugplace')
                ->setParameter('slugcity', '%'.$slugcity.'%')
                ->setParameter('slugplace', '%'.$slugplace.'%')
                ->setMaxResults(1)
        ;

        return $query->getQuery()->getResult();
    }

    /*
    public function findBySlug($slug)
    {
        $query = $this->createQueryBuilder('p');

        $query  //->leftJoin('p.rooms','r')
                //->addSelect('r.id')
                //->addSelect('r.name')
                ->andWhere('p.slug LIKE :slug')
                ->setParameter('slug', $slug)
                ->setMaxResults(1)
        ;

        return $query->getQuery()->getResult();
    }
    */

    public function findByName($text): array
    {
        $qb =  $this->createQueryBuilder('p')
                    ->leftJoin('p.city', 'c')
                    ->select('p.id')
                    ->addSelect('p.title')
                    ->addSelect('p.slug')
                    ->addSelect('c.name')
                    ->where('p.title LIKE :title or p.slug LIKE :title')
                    ->setParameter('title', '%'.$text.'%')
                    ->getQuery();
        return $qb->execute();
    }

    /**
     * @return Query
     */
    public function findForVerification($subdomain, $request=NULL, $search=NULL): Query
    {
        $query = $this->createQueryBuilder('p');
        $query->orderBy('p.createdAt','DESC');

        if ($subdomain){

            $query  ->leftJoin('p.city', 'c')
                    ->leftJoin('c.borough', 'b')
                    ->leftJoin('b.subdomains', 's')
                    ->andWhere('s.slug = :slug')
                    ->setParameter('slug', $subdomain)
            ;

            /*
            $query  ->leftJoin('p.city', 'c')
                    ->leftJoin('c.borough', 'b')
                    ->andWhere('b.slug = :borough')
                    ->setParameter('borough', $subdomain);
            */

        }


        if ($request){

            $filtre = $request->get('filtre');

            if ($filtre == "anonyme"){
                $query->andWhere('p.author is null');
            }
            elseif ($filtre == "connecte"){
                $query->andWhere('p.author is not null');
            }
            elseif ($filtre == "certifie"){

            } elseif ($filtre == "administrateur"){

            }

        }

        if ($search and $search->getNom()){
            $query = $query
                ->andWhere('p.title LIKE :nom')
                ->setParameter('nom', '%'.$search->getNom().'%' );
        }

        if ($search and $search->getType()){
            $query  ->addSelect('CASE WHEN pri.slug = :placekind THEN 1 ELSE 0 END AS HIDDEN sortPrimary')
                    ->addSelect('CASE WHEN sec.slug = :placekind THEN 1 ELSE 0 END AS HIDDEN sortSecondary')
                    ->addOrderBy('sortPrimary,sortSecondary', 'DESC')
                    ->leftJoin('p.placekindPrimary', 'pri')
                    ->leftJoin('p.placekindSecondary', 'sec')
                    ->leftJoin('p.placekindTertiary', 'ter')
                    ->andWhere('pri.slug = :placekind OR sec.slug = :placekind OR ter.slug = :placekind')
                    ->setParameter('placekind', $search->getType()->getSlug());
        }

        if ($search and $search->getVille()){
            $query = $query
                ->andWhere('c.slug = :city')
                ->setParameter('city', $search->getVille()->getSlug())
            ;
        }

        return $query->getQuery();
    }


    /**
     * @return Query
     */
    public function findAllSearchQuery($subdomain, $all=NULL, $map=NULL): Query
    {
        $query = $this->findAllSearch($subdomain, $all, false, false, $map);
        return $query->getQuery();
    }

    /**
     * @return Query
     */
    public function findAllSearchByFilter(PlaceSearch $search, $subdomain, $all=NULL, $counter=null, $withoutEvent=null): Query
    {
        $query = $this->findAllSearch($subdomain, $all, $counter, $withoutEvent, null, null, $search);

        return $query->getQuery();
    }

    /**
     * @return Query
     */
    public function findByLike($user, $counter = null, $withoutEvent = null): Query
    {
        $query = $this->findAllSearch(null, null, $counter, $withoutEvent);

        $query  ->leftJoin('p.likes', 'l')
                ->andWhere('l.user = :user')
                ->setParameter('user', $user);

        if($counter){
            $query->select('count(p.id)');
        }

        return $query->getQuery();
    }

    /**
     * @return Query
     */
    public function findByPlacekind($slug, $subdomain, $counter=null, $withoutEvent=null, $map=NULL): Query
    {
        $query = $this->findAllSearch($subdomain, null, $counter, $withoutEvent, $map);

        if($counter){
            $query->select('p.id');
        }
        else {
            $query  ->addSelect('CASE WHEN pri.slug = :placekind THEN 1 ELSE 0 END AS HIDDEN sortPrimary')
                    ->addSelect('CASE WHEN sec.slug = :placekind THEN 1 ELSE 0 END AS HIDDEN sortSecondary')
                    ->addOrderBy('sortPrimary', 'DESC')
                    ->addOrderBy('sortSecondary', 'DESC');
        }

        $query  ->leftJoin('p.placekindPrimary', 'pri')
                ->leftJoin('p.placekindSecondary', 'sec')
                ->leftJoin('p.placekindTertiary', 'ter')
                ->andWhere('pri.slug = :placekind OR sec.slug = :placekind OR ter.slug = :placekind')
                ->setParameter('placekind', $slug);

        return $query->getQuery();
    }


    /**
     * @return Query
     */
    public function findByPlacetag($slug, $subdomain, $counter=null, $withoutEvent=null, $map=NULL): Query
    {
        $query = $this->findAllSearch($subdomain, null, $counter, $withoutEvent, $map);

        if($counter){
            $query->select('p.id');
        }

        $query  ->leftJoin('p.placetags', 'tag')
                ->andWhere('tag.slug = :slug')
                ->setParameter('slug', $slug);

        return $query->getQuery();
    }


    /**
     * @return Query
     */
    public function findByCity($slug, $subdomain, $counter = null, $withoutEvent = null, $map=NULL): Query
    {
        $query = $this->findAllSearch(null, null, $counter, $withoutEvent, $map);

        $query  ->andWhere('c.slug = :city')
                ->setParameter('city', $slug);

        if($counter){
            $query->select('p.id');
        }

        return $query->getQuery();
    }

    /**
     * @return QueryBuilder
     */
    public function findAllSearch($subdomain=NULL, $all=NULL, $counter=NULL, $withoutEvent=NULL, $map=NULL, $validation=NULL, $search=NULL): QueryBuilder
    {
        $query = $this->createQueryBuilder('p');

        $query  ->leftJoin('p.events', 'e')
                ->leftJoin('p.city', 'c')
                ->groupBy('p.id');

        if ($validation === null){
            $query  ->andWhere('e.isValidated = true')
                    ->andWhere('e.isPublished = true')
                    ->andWhere('e.isCanceled is null');
        }

        if ($search and $search->getNom()){
            $query = $query
                ->andWhere('p.title LIKE :nom')
                ->setParameter('nom', '%'.$search->getNom().'%' );
        }

        if ($search and $search->getType()){
            if($counter){
                $query->select('p.id');
            }
            else {
                $query  ->addSelect('CASE WHEN pri.slug = :placekind THEN 1 ELSE 0 END AS HIDDEN sortPrimary')
                        ->addSelect('CASE WHEN sec.slug = :placekind THEN 1 ELSE 0 END AS HIDDEN sortSecondary')
                        ->addOrderBy('sortPrimary,sortSecondary', 'DESC');
            }

            $query  ->leftJoin('p.placekindPrimary', 'pri')
                    ->leftJoin('p.placekindSecondary', 'sec')
                    ->leftJoin('p.placekindTertiary', 'ter')
                    ->andWhere('pri.slug = :placekind OR sec.slug = :placekind OR ter.slug = :placekind')
                    ->setParameter('placekind', $search->getType()->getSlug());
        }

        /*
        if ($search and $search->getType()){

            $query  ->addSelect('CASE WHEN pri.slug = :placekind THEN 1 ELSE 0 END AS HIDDEN sortPrimary')
                    ->addSelect('CASE WHEN sec.slug = :placekind THEN 1 ELSE 0 END AS HIDDEN sortSecondary')
                    ->leftJoin('p.placekindPrimary', 'pri')
                    ->leftJoin('p.placekindSecondary', 'sec')
                    ->leftJoin('p.placekindTertiary', 'ter')
                    ->andWhere('pri.slug = :placekind OR sec.slug = :placekind OR ter.slug = :placekind')
                    ->setParameter('placekind', $search->getType()->getSlug() )
                    ->addOrderBy('sortPrimary,sortSecondary', 'DESC')
            ;
        }
        */

        if ($search and $search->getVille()){
            $query = $query
                ->andWhere('c.slug = :city')
                ->setParameter('city', $search->getVille()->getSlug())
            ;
        }

        if ($all)
        {
            if ($all == "show-all" AND $counter !== true){
                //$query->addOrderby('countOfEvents', 'DESC');
            }  // POUR LA RECHERCHE PERSONNALISEE
            else {$query->addOrderby('p.id', 'DESC');} // POUR L'ADMIN
        }
        else {

            if ($subdomain)
            {
                $query  ->leftJoin('c.borough', 'b')
                        ->leftJoin('b.subdomains', 's')
                        ->andWhere('s.slug = :slug')
                        ->setParameter('slug', $subdomain)
                ;
                /*
                $query  ->leftJoin('c.borough', 'b')
                        ->andWhere('b.slug = :subdomain')
                        ->setParameter('subdomain', $subdomain);
                */
            }


            if ($withoutEvent)
            {
                $query  ->andWhere('e.dateEnd < :today')
                        ->setParameter('today', date('Y-m-d'));
            } else {
                $query  ->andWhere('e.dateEnd >= :today')
                        ->setParameter('today', date('Y-m-d'));
            }

        }

        if ($counter){
            $query  ->select('p.id');
        } else {
            $query  ->addSelect('COUNT(e.id) AS countOfEvents')
                    ->addSelect('MIN(e.dateStart) as minDate')
                    ->addSelect('MAX(e.dateEnd) as maxDate')
                    ->addSelect('(CASE WHEN MIN(e.dateStart) = :todaytest or MAX(e.dateEnd) = :todaytest THEN 1 ELSE 0 END) AS HIDDEN ORDER_DATE')
                    ->addOrderBy('ORDER_DATE','DESC')
                    ->addOrderby('countOfEvents', 'DESC')
                    ->setParameter('todaytest', date('Y-m-d'));

            $query  ->leftJoin('p.likes', 'likes')
                    ->addSelect('likes');

            $query  ->leftJoin('p.placetags', 'placetags')
                    ->addSelect('placetags');

            $query  ->leftJoin('p.city', 'city')
                    ->addSelect('city');
        }

        if ($map){
            $query  ->andWhere('p.latitude IS NOT NULL')
                    ->andWhere('p.longitude IS NOT NULL');
        }

        return $query;

    }
}
