<?php

namespace App\Repository;

use Doctrine\ORM\Query;
use App\Entity\Category;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
    }

    public function findByName($text): array {
        $qb =  $this->createQueryBuilder('c')
                    ->where('c.name LIKE :name')
                    ->setParameter('name', '%'.$text.'%')
                    ->getQuery();
        return $qb->execute();
    }
    
    /**
     * @return Query
     */
    public function findByRubric($slug, $borough): Query
    {
        $query = $this->findAllSearch($borough);

        $query  ->leftJoin('e.rubric', 'r')
                ->andWhere('r.slug = :slug')
                ->setParameter('slug', $slug);
            
        return $query->getQuery();
    }
    
    /**
     * @return Query
     */
    public function findByReduction($slug, $borough): Query
    {
        $query = $this->findAllSearch($borough);

        $query  ->leftJoin('e.prices', 'pr')
                ->leftJoin('pr.reduction', 'r')
                ->andWhere('r.slug = :slug')
                ->setParameter('slug', $slug);
            
        return $query->getQuery();
    }

    /**
     * @return Query
     */
    public function findByDate($date, $borough): Query
    {
        $query = $this->findAllSearch($borough, $date);
        return $query->getQuery();
    }

    /**
     * @return QueryBuilder
     */
    public function findAllSearch($borough=NULL, $date=null): QueryBuilder
    {
        $query = $this->createQueryBuilder('cat');
        
        $query->join('cat.events', 'e');
        
        if ($borough)
        {
            $query  ->leftJoin('e.place', 'place')
                    ->leftJoin('place.city', 'cityByPlace')
                    ->leftJoin('cityByPlace.borough', 'boroughByPlace')
                    ->leftJoin('e.city', 'city')
                    ->leftJoin('city.borough', 'boroughByCity')
                    ->andWhere('boroughByPlace.slug = :borough')
                    ->orWhere('boroughByCity.slug = :borough')
                    ->setParameter('borough', $borough);
        }

        if ($date){
            $query  ->andWhere('e.dateStart <= :date')
                    ->andWhere('e.dateEnd >= :date')
                    ->setParameter('date', $date);
        } else {
            $query  ->andWhere('e.dateEnd >= :today')
                    ->setParameter('today', date('Y-m-d'));
        }

        $query  ->select('cat.name','cat.slug','count(e.id) AS countOfEvents')
                ->groupBy('cat.id')
                ->orderBy('countOfEvents', 'DESC')
                ->setMaxResults(20)
                ;

        return $query;

    }

    // /**
    //  * @return Category[] Returns an array of Category objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Category
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
