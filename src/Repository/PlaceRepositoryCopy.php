<?php

namespace App\Repository;

use App\Entity\Place;
use App\Entity\PlaceSearch;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Place|null find($id, $lockMode = null, $lockVersion = null)
 * @method Place|null findOneBy(array $criteria, array $orderBy = null)
 * @method Place[]    findAll()
 * @method Place[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlaceRepositoryCopy extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Place::class);
    }

    public function findByName($text): array {
        $qb =  $this->createQueryBuilder('p')
                    ->leftJoin('p.city', 'c')
                    ->addSelect('p.id')
                    ->addSelect('p.title')
                    ->addSelect('c.name')
                    ->where('p.title LIKE :title')
                    ->setParameter('title', '%'.$text.'%')
                    ->getQuery();
        return $qb->execute();
    }
    
    /**
     * @return Query
     */
    public function findAllSearchQuery($borough, $all = NULL): Query
    {
        return $this->findAllSearch($borough, $all)
                    ->getQuery();
    }

    /**
     * @return Query
     */
    public function findAllSearchByFilter(PlaceSearch $search, $borough, $all = NULL): Query
    {
        $query = $this->findAllSearch($borough, $all);

        if ($search->getNom()){
            $query = $query
                ->andWhere('p.title LIKE :nom')
                ->setParameter('nom', '%'.$search->getNom().'%' );
        }
        
        if ($search->getType()){

            $query = $query
                ->leftJoin('p.placekindPrimary', 'pri')
                ->leftJoin('p.placekindSecondary', 'sec')
                ->leftJoin('p.placekindTertiary', 'ter')
                ->addSelect('CASE WHEN pri.slug = :placekind THEN 1 ELSE 0 END AS HIDDEN sortPrimary')
                ->addSelect('CASE WHEN sec.slug = :placekind THEN 1 ELSE 0 END AS HIDDEN sortSecondary')
                ->andWhere('pri.slug = :placekind OR sec.slug = :placekind OR ter.slug = :placekind')
                ->setParameter('placekind', $search->getType()->getSlug() )
                ->addOrderBy('sortPrimary', 'DESC')
                ->addOrderBy('sortSecondary', 'DESC')
            ;
        }
        
        if ($search->getVille()){
            $query = $query
                ->andWhere('c.slug = :city')
                ->setParameter('city', $search->getVille()->getSlug())
            ;
        }

        return $query->getQuery();
    }

    /**
     * @return Query
     */
    public function findByLike($user): Query
    {
        $query = $this->findAllSearch();

        $query  ->leftJoin('p.likes', 'l')
                ->andWhere('l.user = :user')
                ->setParameter('user', $user);

        return $query->getQuery();
    }

    /**
     * @return Query
     */
    public function findByPlacekind($slug, $borough, $counter = null, $withoutEvent = null): Query
    {
        $query = $this->findAllSearch($borough, null, $withoutEvent, $counter);

        if($counter){
            $query->select('count(p.id)');
        }
        else {
            $query  ->addSelect('CASE WHEN pri.slug = :placekind THEN 1 ELSE 0 END AS HIDDEN sortPrimary')
                    ->addSelect('CASE WHEN sec.slug = :placekind THEN 1 ELSE 0 END AS HIDDEN sortSecondary')
                    ->addOrderBy('sortPrimary', 'DESC')
                    ->addOrderBy('sortSecondary', 'DESC');
        }
        
        $query  ->leftJoin('p.placekindPrimary', 'pri')
                ->leftJoin('p.placekindSecondary', 'sec')
                ->leftJoin('p.placekindTertiary', 'ter')
                ->andWhere('pri.slug = :placekind OR sec.slug = :placekind OR ter.slug = :placekind')
                ->setParameter('placekind', $slug);

        return $query->getQuery();
    }

    /**
     * @return Query
     */
    public function findByCity($slug, $borough): Query
    {
        $query = $this->findAllSearch($borough);

        $query  ->andWhere('c.slug = :city')
                ->setParameter('city', $slug);

        return $query->getQuery();
    }

    /**
     * @return QueryBuilder
     */ 
    public function findAllSearch($borough=NULL, $all = NULL, $withoutEvent = NULL, $counter = NULL): QueryBuilder
    {
        $query = $this->createQueryBuilder('p');

        $query  ->leftJoin('p.events', 'e')
                ->leftJoin('p.city', 'c');

        if ($counter !== true){
            $query  ->select('COUNT(e.id) AS countOfEvents')
                    ->groupBy('p.id');
        }

        if ($all)
        {
            if ($all == "show-all")
            {
                // POUR LA RECHERCHE PERSONNALISEE
                $query->addOrderby('countOfEvents', 'DESC');
            } else { 
                // POUR L'ADMIN
                $query->addOrderby('p.id', 'DESC');
            }
        }
        else {

            if ($borough)
            {
                $query  ->leftJoin('c.borough', 'b')
                        ->andWhere('b.slug = :borough')
                        ->setParameter('borough', $borough);
            }

            if ($withoutEvent)
            {
                $query  ->andWhere('e.dateEnd < :today')
                        ->setParameter('today', date('Y-m-d'));
            } else {
                $query  ->andWhere('e.dateEnd >= :today')
                        ->setParameter('today', date('Y-m-d'));
            }

            if ($counter !== true){
                $query->addOrderby('countOfEvents', 'DESC');
            }
        }

        return $query;


    }
    


    // /**
    //  * @return Place[] Returns an array of Place objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Place
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
