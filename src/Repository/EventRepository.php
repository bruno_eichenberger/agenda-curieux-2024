<?php

namespace App\Repository;

use DateTime;
use App\Entity\Event;
use Doctrine\ORM\Query;
use App\Entity\EventSearch;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Event|null find($id, $lockMode = null, $lockVersion = null)
 * @method Event|null findOneBy(array $criteria, array $orderBy = null)
 * @method Event[]    findAll()
 * @method Event[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Event::class);
    }

    /**
    * @return Event[] Returns an array of Event objects
    */

    /**
     * @return Query
     */
    public function findForCanceled($start, $end): array
    {
        $query = $this->createQueryBuilder('e');

        $query  ->andWhere('(e.dateStart >= :start and e.dateEnd <= :end) or (e.dateEnd >= :start and e.dateEnd <= :end) ') 
                ->andWhere('e.isValidated = true')
                ->andWhere('e.isPublished = true')
                ->setParameter('start', $start)
                ->setParameter('end', $end);

        return $query->getQuery()->execute();
    }

    /**
     * @return Query
     */
    public function findByTitle($title, $borough=null): array
    {
        $query = $this->findAllSearch($borough);

        $query  //->select('e.title')
                //->addSelect('e.slug')
                ->andWhere('e.title LIKE :searchTitle')
                ->setParameter('searchTitle', '%'.$title.'%' )
                ->setMaxResults(10);

        return $query->getQuery()->execute();
    }

    /**
     * @return Query
     */
    public function findForVerification($borough, $page): Query
    {
        $query = $this->findAllSearch($borough, true, null, null, null, true);

        if ($page == "tout"){
            $query  ->orderBy('e.createdAt', 'DESC')
                    ->setMaxResults(100);
        } else {
            $query->orderBy('e.dateStart', 'ASC');
            if ($page == "brouillon"){
                $query->andWhere('e.isPublished != true');
            }
            elseif ($page == "spam"){
                $query->andWhere('e.isSpam = true');
            }
            elseif ($page == "doublon"){
                $query->andWhere('e.isDuplicated = true');
            }
            elseif ($page == "erreur"){
                $query->andWhere('e.isMistake = true');
            }
            elseif ($page == "annule"){
                $query->andWhere('e.isCanceled = true');
            }
            elseif ($page == "complet"){
                $query->andWhere('e.isFull = true');
            }
            else {$query->andWhere('e.isValidated IS NULL');}
        }
        
        return $query->getQuery();
    }


    /**
     * @return Query
     */
    public function findAlreadyPublished($dateStart, $dateEnd, $title, $place, $placeTemporary): array
    {
        $query = $this->createQueryBuilder('e');
        
        $query  ->andWhere('e.dateStart = :dateStart')
                ->andWhere('e.dateEnd = :dateEnd or e.dateEnd = :dateStart')
                ->andWhere('e.title LIKE :title')
                ->setParameter('dateStart', $dateStart)
                ->setParameter('dateEnd', $dateEnd)
                ->setParameter('title', '%'.$title.'%')
                ->setMaxResults('1');

        if ($place){
            $query  ->leftJoin('e.place', 'p')
                    ->andWhere('p.title = :place or e.placeTemporary LIKE :place')
                    ->setParameter('place', $place->getTitle());
        } else {
            $query  ->leftJoin('e.place', 'p')
                    ->andWhere('e.placeTemporary LIKE :placeTemporary or p.title LIKE :placeTemporary')
                    ->setParameter('placeTemporary', '%'.$placeTemporary.'%');
        }

        return $query->getQuery()->execute();
    }

    /**
     * @return Query
     */
    public function findEventsForMenu($date, $borough): array
    {
        $query = $this->findAllSearch($borough);

        $query  ->andWhere('e.dateStart = :date')
                ->setParameter('date', $date)
                ->setMaxResults('2')
                ->orderBy('e.timeEnd', 'ASC')
                ->orderBy('e.timeStart', 'ASC')
                ->orderBy('e.dateStart', 'ASC')
                ->orderBy('e.dateEnd', 'ASC');

        return $query->getQuery()->execute();
    }

    /**
     * @return Query
     */
    public function findAllSearchQuery(EventSearch $search, $borough, $all=NULL, $period=NULL, $counter=NULL): Query
    {
        $query = $this->findAllSearch($borough, $all, $period, $counter, null, null, true);

        if($counter)
        {
            $query->select('count(e.id)');
        }

        if ($search->getPrix() !== NULL)
        {
            $query  ->andWhere('e.price <= :maxprice')
                    ->setParameter('maxprice', $search->getPrix())
            ;
        }

        if ($search->getTitre()){
            $query  ->andWhere('e.title LIKE :searchTitle')
                    ->setParameter('searchTitle', '%'.$search->getTitre().'%' )
            ;
        }

        if ($search->getDate())
        {
            $query  ->andWhere('e.dateStart <= :searchDate')
                    ->andWhere('e.dateEnd >= :searchDate')
                    ->setParameter('searchDate', $search->getDate())
            ;
        }
        
        if ($search->getRubrique())
        {
            $query  ->andWhere('rubric.slug LIKE :rubricSlug')
                    ->setParameter('rubricSlug', '%'.$search->getRubrique()->getSlug())
                    //->join('e.rubric', 'r')
            ;
        }

        if ($search->getLieu())
        {
            //dd($search->getLieu());
            $query  ->andWhere('place.slug = :placeSlug or place.title LIKE :placeTitle or e.placeTemporary LIKE :placeTitle')
                    ->setParameter('placeTitle', '%'.$search->getLieu()->getTitle().'%')
                    ->setParameter('placeSlug', $search->getLieu()->getSlug())
                    //->leftJoin('e.place', 'p')
            ;
        }

        if ($search->getVille())
        {
            $query  ->andWhere('place.city = :cityObject OR city.slug = :citySlug')
                    ->setParameter('cityObject', $search->getVille())
                    ->setParameter('citySlug', $search->getVille()->getSlug())
                    //->leftJoin('e.place', 'p')
                    //->leftJoin('e.city', 'c')
            ;
        }

        return $query->getQuery();
    }




    
    /**
     * @return Query
     */
    public function findByLike($user, $period = null, $counter = null): Query
    {
        $query = $this->findAllSearch(null, null, $period, $counter, null, null, true);

        $query  ->leftJoin('e.eventLikes', 'l')
                ->andWhere('l.user = :user')
                ->setParameter('user', $user);

        return $query->getQuery();
    }

    /**
     * @return Query
     */
    public function findByRubric($slug, $borough, $period = null, $counter = null, $map = null): Query
    {
        $query = $this->findAllSearch($borough, null, $period, $counter, $map, null, true);

        $query  ->leftJoin('e.rubric', 'r')
                ->andWhere('r.slug = :slug')
                ->setParameter('slug', $slug);
        
        return $query->getQuery();
    }

    /**
     * @return Query
     */
    public function findByCategory($slug, $borough, $period = null, $counter = null, $map = null): Query
    {
        $query = $this->findAllSearch($borough, null, $period, $counter, $map, null, true);

        $query  ->leftJoin('e.category', 'c')
                ->andWhere('c.slug = :slug')
                ->setParameter('slug', $slug);

        return $query->getQuery();
    }
    
    /**
     * @return Query
     */
    public function findByReduction($slug, $borough, $period = null, $counter = null, $map = null): Query
    {
        $query = $this->findAllSearch($borough, null, $period, $counter, $map, null, true);

        $query  ->leftJoin('e.prices', 'pr')
                ->leftJoin('pr.reductions', 'r')
                ->andWhere('r.slug = :slug')
                ->setParameter('slug', $slug);

        return $query->getQuery();
    }

    /**
     * @return Query
     */
    public function findByPlace($entityPlace, $period = null, $counter = null): Query
    {
        $query = $this->findAllSearch(null, null, $period, $counter);

        $query  //->leftJoin('e.place', 'p')
                ->andWhere('place.slug = :slug or e.placeTemporary = :title')
                ->setParameter('slug', $entityPlace->getSlug())
                ->setParameter('title', '%'.$entityPlace->getTitle().'%');

        return $query->getQuery();
    }

    /**
     * @return Query
     */
    public function findByFestival($festival): Query
    {
        $query = $this->findAllSearch();

        $query  ->leftJoin('e.festival', 'f')
                ->andWhere('f.slug = :slug')
                ->setParameter('slug', $festival);

        return $query->getQuery();
    }

    /**
     * @return Query
     */
    public function findByArtist($artist): Query
    {
        $query = $this->findAllSearch();

        $query  ->leftJoin('e.artists', 'a')
                ->andWhere('a.slug = :slug')
                ->setParameter('slug', $artist);

        return $query->getQuery();
    }

    /**
     * @return Query
     */
    public function findByOrganizer($organizer): Query
    {
        $query = $this->findAllSearch();

        $query  ->leftJoin('e.organizer', 'o')
                ->andWhere('o.slug = :slug')
                ->setParameter('slug', $organizer);

        return $query->getQuery();
    }

    /*
    /**
     * @return Query
     *
    public function findAllOfThisWeek($borough): array
    {
        $query = $this->findAllSearch($borough);

        $today = date('Y-m-d');
        $dayOfNextWeek = date('Y-m-d', strtotime('7 days'));

        $query  ->andWhere('e.dateStart <= :dayOfNextWeek')
                ->andWhere('e.dateEnd >= :today')
                ->setParameter('today', $today)
                ->setParameter('dayOfNextWeek', $dayOfNextWeek);

        return $query->getQuery()->execute();
    }
    */

    /**
     * @return Query
     */
    public function findAllOfThisWeek($borough, $counter = null, $map = null): array
    {
        $query = $this->findAllSearch($borough, null, null, $counter, $map, null, "index");

        $today = date('Y-m-d');
        $dayOfNextWeek = date('Y-m-d', strtotime('7 days'));

        $query  ->andWhere('e.dateStart <= :dayOfNextWeek')
                ->andWhere('e.dateEnd >= :today')
                ->setParameter('today', $today)
                ->setParameter('dayOfNextWeek', $dayOfNextWeek);

        return $query->getQuery()->execute();
    }


    /**
     * @return Query
     */
    public function findAllOfThisMonth($borough, $counter = null, $map = null): array
    {
        $query = $this->findAllSearch($borough, null, null, $counter, $map);

        $today = new DateTime();
        $now = new DateTime();
        $nextMonth = $now->modify('+1 month');

        $query  ->andWhere('e.dateStart <= :dayOfNextWeek')
                ->andWhere('e.dateEnd >= :today')
                ->setParameter('today', $today->format("Y-m-d"))
                ->setParameter('dayOfNextWeek', $nextMonth->format("Y-m-d"));

        return $query->getQuery()->execute();
    }


    /**
     * @return Query
     */
    public function findByDate($date, $borough, $counter = null, $map = null): array
    {

        $query = $this->findAllSearch($borough, null, null, $counter, $map, null, true);

        $query  ->andWhere('e.dateStart <= :date')
                ->andWhere('e.dateEnd >= :date')
                ->setParameter('date', $date)
                //->andWhere('e.dateStart = :date or e.dateEnd = :date')
        ;

        return $query->getQuery()->execute();
    }

    /**
     * @return Query
     */
    public function findByDateForMap($date, $borough): array
    {

        $query = $this->findAllSearch($borough);

        $query  ->andWhere('e.dateStart <= :date')
                ->andWhere('e.dateEnd >= :date')
                ->setParameter('date', $date)
                ->select('e')
                ->addSelect('COUNT(e.id) AS countOfEvents')
                //->leftJoin('e.place', 'p')
                ->groupBy('place.id')
                ->andWhere('place.id IS NOT NULL')
                ->andWhere('place.latitude IS NOT NULL')
                ->andWhere('place.longitude IS NOT NULL');

        return $query->getQuery()->execute();
    }

    /**
     * @return Query
     */
    public function findByDateSecondary($date, $borough): array
    {

        $query = $this->findAllSearch($borough);

        $query  ->andWhere('e.dateStart < :date')
                ->andWhere('e.dateEnd > :date')
                ->setParameter('date', $date);

        return $query->getQuery()->execute();
    }

    public function findByDate2($date, $borough): array {
        $qb =  $this->createQueryBuilder('e')
                    ->where('e.dateStart = :date')
                    ->setParameter('date', $date)
                    ->getQuery();
        return $qb->execute();
    }

    /**
     * @return QueryBuilder
     */
    public function findAllSearch($borough=NULL, $all=NULL, $period=NULL, $counter=NULL, $map=NULL, $validation=NULL, $order=NULL): QueryBuilder
    {
        $query = $this->createQueryBuilder('e');

        $query  ->leftJoin('e.place', 'place')
                ->leftJoin('place.city', 'cityByPlace')
                ->leftJoin('e.city', 'city')
                ->leftJoin('e.rubric', 'rubric');

        if($counter){
            $query->select('count(e.id)');
        }
        else {
            $query  ->addSelect('place')
                    ->addSelect('cityByPlace')
                    ->addSelect('city')
                    ->addSelect('rubric');

            $query  ->leftJoin('e.category', 'category')
                    ->addSelect('category');

            $query  ->leftJoin('e.festival', 'festival')
                    ->addSelect('festival');

            $query  ->leftJoin('e.tickets', 'tickets')
                    ->leftJoin('tickets.ticketing', 'ticketing')
                    ->addSelect('tickets')
                    ->addSelect('ticketing');

            /*
            $query  ->leftJoin('tickets.resource', 'resource')
                    ->addSelect('resource');
            */
            
            $query  ->leftJoin('e.prices', 'prices')
                    ->leftJoin('prices.reduction', 'reduction')
                    ->addSelect('prices')
                    ->addSelect('reduction');

            $query  ->leftJoin('e.eventLikes', 'likes')
                    ->addSelect('likes');
        }

        if ($borough)
        {
            $query  ->leftJoin('cityByPlace.borough', 'b1')
                    ->leftJoin('city.borough', 'b2')
                    ->leftJoin('b1.subdomains', 's1')
                    ->leftJoin('b2.subdomains', 's2')
                    ->andWhere('s1.slug = :borough')
                    ->orWhere('s2.slug = :borough')
                    ->setParameter('borough', $borough)
            ;

            /*
            $query  ->leftJoin('cityByPlace.borough', 'b1')
                    ->leftJoin('city.borough', 'b2')
                    ->leftJoin('b1.subdomains', 's1')
                    ->leftJoin('b2.subdomains', 's2')
                    ->leftJoin('b1.mainSubdomain', 's3')
                    ->leftJoin('b2.mainSubdomain', 's4')
                    ->andWhere('s1.slug = :borough')
                    ->orWhere('s2.slug = :borough')
                    ->orWhere('s3.slug = :borough')
                    ->orWhere('s4.slug = :borough')
                    ->setParameter('borough', $borough)
            ;

            $query  ->leftJoin('cityByPlace.borough', 'boroughByPlace')
                    ->leftJoin('city.borough', 'boroughByCity')
                    ->andWhere('boroughByPlace.slug = :borough')
                    ->orWhere('boroughByCity.slug = :borough')
                    ->setParameter('borough', $borough);
            */
        }
        
        if($map){
            $query  ->select('e')
                    ->addSelect('COUNT(e.id) AS countOfEvents')
                    //->addSelect('MIN(p.latitude) AS latMin')
                    //->addSelect('MAX(p.latitude) AS latMax')
                    ->groupBy('place.id')
                    ->andWhere('place.id IS NOT NULL')
                    ->andWhere('place.latitude IS NOT NULL')
                    ->andWhere('place.longitude IS NOT NULL');
        }

        if ($all){
            $query->orderBy('e.id', 'DESC');
        } 
        else {
            $query->setParameter('today', date('Y-m-d'));

            if ($period == "before") {
                $query  ->andWhere('e.dateEnd < :today')
                        ->orderBy('e.dateEnd', 'DESC')
                        ->addOrderBy('e.dateStart', 'ASC')
                        ->addOrderBy('e.timeStart', 'DESC')
                        ->addOrderBy('e.timeEnd', 'DESC')
                ;
            } 
            elseif ($period == "today"){
                $query  ->andWhere('e.dateStart <= :today') 
                        ->andWhere('e.dateEnd >= :today')
                ;
            }
            elseif ($period == "after"){
                $query  ->andWhere('e.dateStart > :today')
                        ->andWhere('e.dateEnd > :today')
                ;
            }
            else {
                if ($order == "index"){
                    //test à faire
                 }
                 elseif ($order == "byday"){
                    //test à faire
                 }
                 elseif (isset($order)){
                    $query  ->addSelect("(CASE
                                WHEN e.dateStart = :today THEN 3
                                WHEN e.dateEnd = :today THEN 2
                                WHEN e.dateStart = :today THEN 1
                                ELSE 0
                            END) AS HIDDEN ORDER_DATE")
                            ->addOrderBy("ORDER_DATE", "DESC")
                    ;
                }

                $query  ->andWhere('e.dateEnd >= :today')
                        ->addOrderBy('e.dateStart', 'ASC')
                        ->addOrderBy('e.dateEnd', 'ASC')
                        ->addOrderBy('e.timeStart', 'ASC')
                        ->addOrderBy('e.timeEnd', 'ASC')
                ;

            }
        }

        if ($validation === null){
            $query  ->andWhere('e.isPublished = true')
                    ->andWhere('e.isValidated = true')
            ;
        }

        /*
        $query->leftJoin('e.rubric', 'rubric');
        $query->leftJoin('e.category', 'category');
        */

        return $query;

    }


    /*
    public function findAllVisibleQuery(EventSearch $search)
    {

        $query =  $this->findAll();

        if ($search->getMaxPrice()){
            $query = $this->createQueryBuilder('e')
            ->andWhere('e.price <= :maxprice')
            ->setParameter('maxprice', $search->getMaxPrice())
            ->getQuery()
            ->getResult()
            ;
        }
        

/*
        if ($search->getSearchDate()){
            $query = $this->createQueryBuilder('e')
            ->andWhere('e.date_start = "2019-05-07" ')
            ->getQuery()
            ->getResult()
            ;
        }

        if ($search->getSearchRubric()){
            $query = $this->createQueryBuilder('e')
            ->andWhere('e.rubric.slug = :searchRubric')
            ->setParameter('searchRubric', $search->getSearchRubric())
            ->getQuery()
            ->getResult()
            ;
        }
       



        return $query;
 */

        /*
        
        $query =  $this->findAll();

        if ($search->getMaxPrice()){
            $query = $query
                ->where('e.price <= :maxprice')
                ->setParameter('maxprice', $search->getMaxPrice());
        }
        return $query->getQuery();
       

    }
 */


    /*


    public function findAllEventsByDate2($date): array {
        $sql = 'SELECT * FROM event, rubric WHERE date_start = :date';
        $params = array(
            'date' => '$date'
        );
        return $this->getEntityManager()->getConnection()->executeQuery($sql, $params)->fetchAll();
    }

    public function findAllEventsByRubric($rubric): array {
        $sql = 'SELECT * FROM event e, rubric r WHERE e.rubric_id = r.id AND r.slug = :rubric';
        $params = array(
            'rubric' => '$rubric',
        );
        return $this->getEntityManager()->getConnection()->executeQuery($sql, $params)->fetchAll();
    }
    */



  /* 
    public function findByDate($value): array
    {
        return $this->createQueryBuilder('p')
                    ->andWhere('p.price = :val')
                    ->setParameter('val', $value)
                    ->orderBy('p.id', 'ASC')
                    ->setMaxResults(10)
                    ->getQuery()
                    ->getResult()
                    ;
    }

  
    public function findOneBySomeField($value): ?Event
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
