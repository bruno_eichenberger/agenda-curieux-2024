<?php

namespace App\Repository;

use App\Entity\EditorEvent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EditorEvent|null find($id, $lockMode = null, $lockVersion = null)
 * @method EditorEvent|null findOneBy(array $criteria, array $orderBy = null)
 * @method EditorEvent[]    findAll()
 * @method EditorEvent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EditorEventRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EditorEvent::class);
    }

    // /**
    //  * @return EditorEvent[] Returns an array of EditorEvent objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EditorEvent
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
