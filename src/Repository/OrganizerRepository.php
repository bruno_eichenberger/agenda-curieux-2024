<?php

namespace App\Repository;

use Doctrine\ORM\Query;
use App\Entity\Organizer;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Organizer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Organizer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Organizer[]    findAll()
 * @method Organizer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrganizerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Organizer::class);
    }

    /**
     * @return Query
     */
    public function findForVerification($borough, $request=NULL): Query
    {
        $query = $this->createQueryBuilder('o');
        $query->orderBy('o.id','DESC');

        if ($borough){
            $query  ->leftJoin('o.events', 'e')
                    ->leftJoin('e.place', 'p')
                    ->leftJoin('p.city', 'cp')
                    ->leftJoin('e.city', 'ce')
                    ->leftJoin('cp.borough', 'bp')
                    ->leftJoin('ce.borough', 'be')
                    ->andWhere('bp.slug = :borough')
                    ->orWhere('be.slug = :borough')
                    ->setParameter('borough', $borough);
        }

        return $query->getQuery();
    }

    /**
     * @return Query
     */
    public function findAllOrganizers($borough): Query
    {
        $query = $this ->findAllSearch($borough);
        return $query->getQuery();
    }
    
    /**
     * @return Query
     */
    public function findTopOrganizer($borough): Query
    {
        $query = $this ->findAllSearch($borough);
        $query ->setMaxResults(5);
        return $query->getQuery();
    }

    /**
     * @return Query
     */
    public function findByOrganizerkind($slug, $borough): Query
    {
        $query = $this->findAllSearch($borough);

        $query  ->leftJoin('o.organizerkind', 'k')
                ->andWhere('k.slug = :organizerkind')
                ->setParameter('organizerkind', $slug);

        return $query->getQuery();
    }

    /**
    * @return QueryBuilder
    */ 
    public function findAllSearch($borough=null): QueryBuilder
    {    
        $query = $this->createQueryBuilder('o');

        $query  ->leftJoin('o.events', 'e')
               ->groupBy('o');
               //->orderby('COUNT(e)', 'DESC')

        if ($borough){

            $query  ->leftJoin('e.place', 'p')
                    ->leftJoin('p.city', 'cp')
                    ->leftJoin('e.city', 'ce')
                    ->leftJoin('cp.borough', 'bp')
                    ->leftJoin('ce.borough', 'be')
                    ->andWhere('bp.slug = :borough OR be.slug = :borough')
                    ->setParameter('borough', $borough);
        }

        return $query;
    }


    public function findByName($text): array {
        $qb =  $this->createQueryBuilder('o')
                    ->where('o.name LIKE :name')
                    ->setParameter('name', '%'.$text.'%')
                    ->getQuery();
        return $qb->execute();
    }

    // /**
    //  * @return Organizer[] Returns an array of Organizer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Organizer
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
