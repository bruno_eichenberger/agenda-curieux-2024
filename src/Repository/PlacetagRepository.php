<?php

namespace App\Repository;

use App\Entity\Placetag;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Placetag|null find($id, $lockMode = null, $lockVersion = null)
 * @method Placetag|null findOneBy(array $criteria, array $orderBy = null)
 * @method Placetag[]    findAll()
 * @method Placetag[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlacetagRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Placetag::class);
    }

    public function findByName($text): array {
        $qb =  $this->createQueryBuilder('t')
                    ->where('t.name LIKE :name')
                    ->setParameter('name', '%'.$text.'%')
                    ->getQuery();
        return $qb->execute();
    }

    // /**
    //  * @return Placetag[] Returns an array of Placetag objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Placetag
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
