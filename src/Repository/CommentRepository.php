<?php

namespace App\Repository;

use App\Entity\Comment;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Comment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Comment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Comment[]    findAll()
 * @method Comment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Comment::class);
    }

    /**
     * @return Query
     */
    public function findForVerification($borough, $request): Query
    {
        $query = $this->createQueryBuilder('c');

        if ($request->get('_route') == "comments_verification"){
            $query->andWhere('c.isAlerted is NULL or c.isAlerted = false');
        } 
        elseif ($request->get('_route') == "alerts_verification"){
            $query->andWhere('c.isAlerted = true');
        }

        //if ($request->get("validation")){}
    
        if ($request->get("user") == "anonymous"){
            $query->andWhere('c.author IS NULL ');
        } 
        elseif ($request->get("user") == "connected"){
            $query->andWhere('c.author IS NOT NULL');
        }
            
        if ($request->get("entity") == "event"){
            $query->join('c.event', 'e');
        } elseif ($request->get("entity") == "place"){
            $query->join('c.place', 'p');
        }

        if ($request->get("order") == "old"){
            $query->orderBy('c.createdAt','ASC');
        } else {
            $query->orderBy('c.createdAt','DESC');
        }

        if ($request->get("content") != ""){
            $query  ->andWhere('c.content like :text')
                    ->setParameter('text', '%'.$request->get("content").'%' );
        }
        
        if ($borough){
            
            $query  ->setParameter('borough', $borough)

                    ->leftJoin('c.event', 'e')

                    ->leftJoin('e.place', 'pe')
                    ->leftJoin('pe.city', 'cpe')

                    ->leftJoin('e.city', 'ce')
                    ->leftJoin('ce.borough', 'bce')
                    ->leftJoin('cpe.borough', 'bcpe')
                    
                    ->leftJoin('c.place', 'p')
                    ->leftJoin('p.city', 'cp')
                    ->leftJoin('cp.borough', 'bcp')

                    ->andWhere('c.event is not null and (bce.slug = :borough or bcpe.slug = :borough)')
                    ->orWhere('c.place is not null and bcp.slug = :borough')
            ;
                    
        }
        
        return $query->getQuery();
    }

    /**
     * @return Query
     */
    public function findByFilter($borough, $alert=NULL, $anonymous=NULL, $text=NULL): Query
    {
        $query = $this->createQueryBuilder('c');

        if ($alert){
            if ($anonymous){
                $query->andWhere('c.author is NULL');
            }
            if ($text){
                $query  ->andWhere('c.content like :text')
                        ->setParameter('text', '%'.$text.'%' );
            }
            $query->andWhere('c.isAlerted = true');
        } else {
            $query->andWhere('c.isAlerted is NULL or c.isAlerted = false');
        }

        return $query->getQuery();
    }


    // /**
    //  * @return Comment[] Returns an array of Comment objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Comment
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
