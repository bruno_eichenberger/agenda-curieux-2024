<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\ORM\Query;
use App\Entity\UserSearch;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function findByName($text): array 
    {
        $qb =  $this->createQueryBuilder('u')
                    ->where('u.firstName LIKE :name')
                    ->orWhere('u.lastName LIKE :name')
                    ->setParameter('name', '%'.$text.'%')
                    ->getQuery();
        return $qb->execute();
    }

    /**
     * @return QueryBuilder
     */
    public function findAllSearchByFilter(UserSearch $search): QueryBuilder
    {
        $query = $this->createQueryBuilder('u');

        if ($search and $search->getNom()){
            $query  ->where('u.firstName LIKE :text')
                    ->orWhere('u.lastName LIKE :text')
                    ->setParameter('text', '%'.$search->getNom().'%')
                    ->getQuery();
        }
        if ($search and $search->getEmail()){
            $query  ->where('u.email LIKE :text')
                    ->setParameter('text', '%'.$search->getEmail().'%')
                    ->getQuery();
        }
        return $query;
    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
