<?php

namespace App\Repository;

use App\Entity\Claim;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Claim|null find($id, $lockMode = null, $lockVersion = null)
 * @method Claim|null findOneBy(array $criteria, array $orderBy = null)
 * @method Claim[]    findAll()
 * @method Claim[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClaimRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Claim::class);
    }
    /**
     * @return Query
     */
    public function findForVerification($borough, $request): Query
    {
        $query = $this->createQueryBuilder('c');

        if ($borough){
            
            $query  ->setParameter('borough', $borough)

                    ->leftJoin('c.event', 'e')

                    ->leftJoin('e.place', 'pe')
                    ->leftJoin('pe.city', 'cpe')

                    ->leftJoin('e.city', 'ce')
                    ->leftJoin('ce.borough', 'bce')
                    ->leftJoin('cpe.borough', 'bcpe')
                    
                    ->leftJoin('c.place', 'p')
                    ->leftJoin('p.city', 'cp')
                    ->leftJoin('cp.borough', 'bcp')

                    ->andWhere('c.event is not null and (bce.slug = :borough or bcpe.slug = :borough)')
                    ->orWhere('c.place is not null and bcp.slug = :borough')
            ;
                    
        }

        if ($request->get("filtre")){
            if ($request->get("filtre") == "accepte"){
                $query->andWhere('c.response = TRUE');
            }
            elseif ($request->get("filtre") == "refuse") {
                $query->andWhere('c.response = FALSE');
            }
        } else {
            $query->andWhere('c.response IS NULL');
        }

        $query->orderBy('c.dateRequest','DESC');

        return $query->getQuery();
    }


        
    // /**
    //  * @return Claim[] Returns an array of Claim objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Claim
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
