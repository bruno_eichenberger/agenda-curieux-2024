<?php

namespace App\Repository;

use App\Entity\Organizerkind;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Organizerkind|null find($id, $lockMode = null, $lockVersion = null)
 * @method Organizerkind|null findOneBy(array $criteria, array $orderBy = null)
 * @method Organizerkind[]    findAll()
 * @method Organizerkind[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrganizerkindRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Organizerkind::class);
    }

    // /**
    //  * @return Organizerkind[] Returns an array of Organizerkind objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Organizerkind
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
