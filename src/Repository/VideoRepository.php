<?php

namespace App\Repository;

use App\Entity\Video;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Video|null find($id, $lockMode = null, $lockVersion = null)
 * @method Video|null findOneBy(array $criteria, array $orderBy = null)
 * @method Video[]    findAll()
 * @method Video[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VideoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Video::class);
    }

    /**
     * @return Query
     */
    public function findByRubric($slug, $borough): Query
    {
        $query = $this->findAllSearch($borough);

        $query  ->leftJoin('e.rubric', 'r')
                ->andWhere('r.slug = :slug')
                ->setParameter('slug', $slug);
            
        return $query->getQuery();
    }

    /**
     * @return Query
     */
    public function findByCategory($slug, $borough): Query
    {
        $query = $this->findAllSearch($borough);

        $query  ->leftJoin('e.category', 'c')
                ->andWhere('c.slug = :slug')
                ->setParameter('slug', $slug);

        return $query->getQuery();
    }

    /**
     * @return Query
     */
    public function findByReduction($slug, $borough): Query
    {
        $query = $this->findAllSearch($borough);

        $query  ->leftJoin('e.prices', 'pr')
                ->leftJoin('pr.reduction', 'r')
                ->andWhere('r.slug = :slug')
                ->setParameter('slug', $slug);

        return $query->getQuery();
    }

    /**
     * @return Query
     */
    public function findByDate($date, $borough): Query
    {
        $query = $this->findAllSearch($borough, $date);
        return $query->getQuery();
    }

    /**
     * @return QueryBuilder
     */
    public function findAllSearch($borough=NULL, $date=NULL): QueryBuilder
    {
        $query = $this->createQueryBuilder('v');

        $query->join('v.event', 'e');

        if ($borough)
        {
            $query  ->leftJoin('e.place', 'place')
                    ->leftJoin('place.city', 'cityByPlace')
                    ->leftJoin('cityByPlace.borough', 'boroughByPlace')
                    ->leftJoin('e.city', 'city')
                    ->leftJoin('city.borough', 'boroughByCity')
                    ->andWhere('boroughByPlace.slug = :borough')
                    ->orWhere('boroughByCity.slug = :borough')
                    ->setParameter('borough', $borough);
        }

        if ($date){
            $query  ->andWhere('e.dateStart <= :date')
                    ->andWhere('e.dateEnd >= :date')
                    ->setParameter('date', $date);
        } else {
            $query  ->andWhere('e.dateEnd >= :today')
                    ->setParameter('today', date('Y-m-d'));
        }

        $query  ->leftJoin('v.videostore', 'vs')
                ->addSelect('vs')
                ->addSelect('e')
                ->orderBy('e.dateEnd', 'ASC')
                ->addOrderBy('e.dateStart', 'DESC')
                ->addOrderBy('e.timeStart', 'ASC')
                ->addOrderBy('e.timeEnd', 'ASC')
                ;
                
        



        return $query;

    }

    // /**
    //  * @return Video[] Returns an array of Video objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Video
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */


}
