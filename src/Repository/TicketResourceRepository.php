<?php

namespace App\Repository;

use Doctrine\ORM\Query;
use App\Entity\TicketSearch;
use App\Entity\TicketResource;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method TicketResource|null find($id, $lockMode = null, $lockVersion = null)
 * @method TicketResource|null findOneBy(array $criteria, array $orderBy = null)
 * @method TicketResource[]    findAll()
 * @method TicketResource[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TicketResourceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TicketResource::class);
    }

    /**
     * @return Query
     */
    public function findWithoutLocation($text=null, $count=null, $page=null): Query
    {
        $query = $this->createQueryBuilder('e');
        
        if ($text === null or $text == "city"){
            $query->andWhere('e.city IS NULL');
        }
        if ($text === null or $text == "place"){
            $query->andWhere('e.place IS NULL');
        }
        if ($count){
            $query->select('count(e.id)');
        } else {
            $query->setMaxResults('1000');
        }
        if ($page){
            $query->setFirstResult($page*1000);
        }

        return $query->getQuery();
    }

    /**
     * @return Query
     */
    public function findAllSameEvents($entity): array
    {
        $query = $this->createQueryBuilder('e');
        
        $query  ->andWhere('e.dateStart = :dateStart')
                ->andWhere('e.dateEnd = :dateEnd')
                ->andWhere('e.title LIKE :title')
                ->andWhere('e.city = :city')
                ->setParameter('dateStart', $entity->getDateStart())
                ->setParameter('dateEnd', $entity->getDateEnd())
                ->setParameter('title', '%'.$entity->getTitle().'%')
                ->setParameter('city', $entity->getCity());

        /*
        if ($place){
            $query  ->leftJoin('e.place', 'p')
                    ->andWhere('p.title = :place or e.placeResource LIKE :place')
                    ->setParameter('place', $place->getTitle());
        } else {
            $query  ->leftJoin('e.place', 'p')
                    ->andWhere('e.placeResource LIKE :placeTemporary or p.title LIKE :placeTemporary')
                    ->setParameter('placeTemporary', '%'.$placeTemporary.'%');
        }
        */


        return $query->getQuery()->execute();
    }

    /**
     * @return Query
     */
    public function findAllSearchQuery(TicketSearch $search, $borough): Query
    {
        $query = $this->findAllSearch($borough);

        if ($search->getSearchTitle()){
            $query = $query
                ->andWhere('e.title LIKE :searchTitle')
                ->setParameter('searchTitle', '%'.$search->getSearchTitle().'%' );
        }

        if ($search->getSearchDate()){
            $query = $query
                ->andWhere('e.dateStart <= :searchDate')
                ->andWhere('e.dateEnd >= :searchDate')
                ->setParameter('searchDate', $search->getSearchDate());
        }
        
        if ($search->getSearchPlace()){
            if ($search->getSearchPlace()->getId()){
                $query
                    ->leftJoin('e.place', 'pl')
                    ->andWhere('pl = :placeObject OR pl.slug = :placeSlug')
                    ->setParameter('placeObject', $search->getSearchPlace())
                    ->setParameter('placeSlug', $search->getSearchPlace()->getSlug());
            }
            else {
                $query
                    ->andWhere('e.placeResource LIKE :placeTitle')
                    ->setParameter('placeTitle', '%'.$search->getSearchPlace()->getTitle().'%');
            }
        }

        if ($search->getSearchCity()){
            if ($search->getSearchCity()->getId()){
                $query
                    ->leftJoin('e.place', 'p')
                    ->leftJoin('e.city', 'c')
                    ->andWhere('p.city = :cityObject OR c.slug = :citySlug')
                    ->setParameter('cityObject', $search->getSearchCity())
                    ->setParameter('citySlug', $search->getSearchCity()->getSlug());
            }
            else {
                $query
                    ->andWhere('e.placeResource LIKE :cityName')
                    ->setParameter('cityName', '%"city"%'.$search->getSearchCity()->getName().'%');
            }
        }

        return $query->getQuery();
    }


    
    /**
     * @return QueryBuilder
     */
    public function findAllSearch($borough=NULL): QueryBuilder
    {
        $query = $this->createQueryBuilder('e');

        if ($borough)
        {
            $query  ->leftJoin('e.place', 'place')
                    ->leftJoin('place.city', 'cityByPlace')
                    ->leftJoin('cityByPlace.borough', 'boroughByPlace')
                    ->leftJoin('e.city', 'city')
                    ->leftJoin('city.borough', 'boroughByCity')
                    ->andWhere('boroughByPlace.slug = :borough')
                    ->orWhere('boroughByCity.slug = :borough')
                    ->setParameter('borough', $borough);
        }

        

        $query->setParameter('today', date('Y-m-d'));

        $query  ->addSelect('t')
                ->leftJoin('e.ticketing','t')
                ->andWhere('e.dateEnd >= :today')
                ->orderBy('e.dateStart', 'ASC')
                ->addOrderBy('e.title', 'ASC')
                ;

        return $query;

    }


    // /**
    //  * @return TicketResource[] Returns an array of TicketResource objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TicketResource
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
