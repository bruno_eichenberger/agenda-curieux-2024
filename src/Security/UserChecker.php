<?php
namespace App\Security;
 
use App\Entity\User as AppUser;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccountExpiredException;
 
class UserChecker implements UserCheckerInterface
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    
    public function checkPreAuth(UserInterface $user)
    {
        if (!$user instanceof AppUser) {
            return;
        }

        // user is deleted, show a generic Account Not Found message.
        /*
        if ($user->isDeleted()) {
            throw new AccountDeletedException();
        }
        */

    }
 
    public function checkPostAuth(UserInterface $user)
    {
        if (!$user instanceof AppUser) {
            return;
        }

        // user account is expired, the user may be notified
        if ($user->getIsActivated() === null) {
            throw new AccountExpiredException("Vous devez répondre à notre mail pour vous connecter");
            //throw new \Exception("Ce membre n'est pas actif");
        } else {
            // mémorise la date de la dernière connexion
            $user->setLastConnectionAt(new \Datetime());
            $this->entityManager->persist($user);
            $this->entityManager->flush();
        }
    }
}