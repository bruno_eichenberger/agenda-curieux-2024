<?php

namespace App\Security;

use App\Entity\User;
use App\Entity\Event;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class EventVoter extends Voter
{
    private $security;
    const VIEW = 'view';
    const EDIT = 'edit';
    
    public function __construct(Security $security, RequestStack $requestStack)
    {
        $this->security = $security;
        $this->requestStack = $requestStack;
    }

    protected function supports(string $attribute, $subject): bool
    {   
        // only vote on `Event` objects
        if (!$subject instanceof Event) {
            return false;
        }

        // if the attribute isn't one we support, return false
        if (!in_array($attribute, [self::VIEW, self::EDIT])) {
            return false;
        }

        return true;       
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        // you know $subject is a Event object, thanks to `supports()`
        /** @var Event $event */
        $event = $subject;
    
        if ($user instanceof User)
        {
            switch ($attribute) {
                case self::VIEW:
                    return $this->canView($event, $user);
                case self::EDIT:
                    return $this->canEdit($event, $user);
            }
        } 
        else {
            $tokenSession = $this->requestStack->getSession()->get('token');
            $tokenEvent = $event->getSecretAnonymous();

            if (
                $tokenEvent and $tokenSession and $tokenSession == $tokenEvent
            ){
                return true;
            }
        }

        return false;
    }
    
    private function canView(Event $event, User $user): bool
    {
        // if they can edit, they can view
        if ($this->canEdit($event, $user)) {
            return true;
        }

        // Autoriser tout le monde pour le moment !
        return true;

        // the Event object could have, for example, a method `isPrivate()`
        //return !$event->isPrivate();
    }

    private function canEdit(Event $event, User $user): bool
    {
        $author = $event->getAuthor();
        $editors = $event->getEditors();
        
        if (
            $this->security->isGranted('ROLE_ADMIN') or
            $user === $author or 
            $editors->contains($user) 
            //or $user->is_granted('ROLE_ADMIN')
        ){
            return true;
        }
        
        return false;
    }

}