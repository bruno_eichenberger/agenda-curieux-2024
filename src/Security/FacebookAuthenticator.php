<?php

namespace App\Security;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Component\HttpFoundation\RedirectResponse;
use KnpU\OAuth2ClientBundle\Client\Provider\FacebookClient;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use KnpU\OAuth2ClientBundle\Security\Authenticator\SocialAuthenticator;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class FacebookAuthenticator extends SocialAuthenticator
{
    private $clientRegistry;
    private $em;
    private $router;

    public function __construct(ClientRegistry $clientRegistry, EntityManagerInterface $em, RouterInterface $router, UserPasswordHasherInterface $encoder)
    {
        $this->clientRegistry = $clientRegistry;
        $this->em = $em;
	    $this->router = $router;
	    $this->encoder = $encoder;
    }

    /**
    * @return bool
    */
    public function supports(Request $request)
    {
        // continue ONLY if the current ROUTE matches the check ROUTE
        return $request->attributes->get('_route') === 'connect_facebook_check';
    }

    /**
    * @return mixed
    */
    public function getCredentials(Request $request)
    {
        // this method is only called if supports() returns true
        return $this->fetchAccessToken($this->getFacebookClient());
    }

    /**
    * @return ?UserInterface
    */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        /** @var FacebookUser $facebookUser */
        $fbUser = $this->getFacebookClient()->fetchUserFromToken($credentials);

        // 1) have they logged in with Facebook before? Easy!
        $existingUser = $this->em->getRepository(User::class)->findOneBy(['facebookId' => $fbUser->getId()]);
        if ($existingUser) {
            return $existingUser;
        }

        // 2) do we have a matching user by email?
        $user = $this->em->getRepository(User::class)->findOneBy(['email' => $fbUser->getEmail()]);

        if($user){
            return $user;
        } else {

            $user = new User();

            $user   ->setFacebookId($fbUser->getId())
                    ->setFirstName($fbUser->getFirstName())
                    ->setLastName($fbUser->getLastName())
                    ->setLevel(false);

            //$hash = $this->encoder->encodePassword($user, uniqid());
            //$user->setHash($hash);

            if($fbUser->getEmail() !== null){
                $user->setEmail($fbUser->getEmail());
            }
            
            if($fbUser->getPictureUrl() !== null){
                $user->setAvatar($fbUser->getPictureUrl());
            }

            $this->em->persist($user);
            $this->em->flush();
        }

        return $user;
    }

    /**
     * @return FacebookClient
     */
    private function getFacebookClient()
    {
        // "facebook_main" is the key used in config/packages/knpu_oauth2_client.yaml
        return $this->clientRegistry->getClient('facebook_main');
	}

    /**
     * @return ?Response
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        $targetUrl = $this->router->generate('event_index');
        return new RedirectResponse($targetUrl);
    
        // or, on success, let the request continue to be handled by the controller
        //return null;
    }

    /**
     * @return ?Response
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $message = strtr($exception->getMessageKey(), $exception->getMessageData());
        return new Response($message, Response::HTTP_FORBIDDEN);
    }

    /**
     * Called when authentication is needed, but it's not sent.
     * This redirects to the 'login'.
     * @return Response
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new RedirectResponse(
            '/connect/', // might be the site, where users choose their oauth provider
            Response::HTTP_TEMPORARY_REDIRECT
        );
    }
}