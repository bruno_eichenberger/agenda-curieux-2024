<?php

namespace App\Service;

use App\Entity\Subdomain;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class Website extends AbstractController {

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function getArray() :array
    {
        $website = [
            'name'      => "Curieux.net",
            'slogan'    => "Agenda culturel et participatif",
            'metadata'  => [
                'title'         => "Agenda culturel et participatif",
                'description'   => "Agenda des sorties culturelles, festives et sportives",
                'keywords'      => "agenda, concerts, soirees, theatre, exposition, spectacles, sorties, cinema, culture, guide, tourisme, sports, ateliers, gratuit, entrée libre, billetterie, bons plans, festival"
            ],
            'address'   => [
                'subdomain' => $this->getSlugOfSubdomain(),
                'domain'    => $this->getDomain()
            ],
            'color'     => null,
            'city'      => null,
            'arounds'   => null
        ];
        
        // Personnalisation par sous-domaine
        if ($this->getSlugOfSubdomain()){

            $subdomain = $this->doctrine->getRepository(Subdomain::class)->findOneBy(['slug' => $this->getSlugOfSubdomain()]);
        
            $website['name'] = $subdomain->getName();
            $website['slogan'] = $subdomain->getSlogan();
            $website['color'] = $subdomain->getColor();
            $website['network'] = $subdomain->getNetwork();

            if ($subdomain->getMainBorough()){
                $website['city'] = $subdomain->getMainBorough()->getChiefTown();
            }
            
            foreach($subdomain->getArounds() as $around){
                $website['arounds'][] = [
                    'name'  => $around->getName(),
                    'slug'  => $around->getSlug()
                ];
            }

            if ($subdomain->getData()['description']){
                $website['metadata']['description'] = $subdomain->getData()['description'];
            } else {
                $chieftown = $subdomain->getMainBorough()->getChiefTown();
                $website['metadata']['description'] = "Sortir à ". $chieftown .", agenda des sorties, concerts, spectacles, soirées, expositions, bons plans, cinéma, culture, évènementiel, que faire à ". $chieftown;
            }

            if ($subdomain->getData()['keywords']){
                $website['metadata']['keywords'] = $subdomain->getData()['keywords'];
            } else {
                $ville = "";
                if ($subdomain->getMainBorough() and $subdomain->getMainBorough()->getChieftown()){
                    $ville = $subdomain->getMainBorough()->getChieftown() .",";
                }
                $website['metadata']['keywords'] = "sortir, que faire, ". $ville ." ". $website['metadata']['keywords'];
            }

            if ($subdomain->getResident()['singular']['masculine']){
                $slogan = "Agenda culturel ". $subdomain->getResident()['singular']['masculine'];
                $website['slogan'] = $slogan;
                $website['metadata']['title'] = $slogan;
            }
        } else {
            $subdomains = $this->doctrine->getRepository(Subdomain::class)->findAll();
        
            foreach($subdomains as $subdomain){
                $website['arounds'][] = [
                    'name'  => $subdomain->getName(),
                    'slug'  => $subdomain->getSlug()
                ];
            }
        }
        
        return $website;
    }

    public function getSlugOfSubdomain()
    {
        /* Pour les tests */
        if (
            $this->getSubdomain() == "localhost" || 
            $this->getSubdomain() == "localhost:8000" || 
            $this->getSubdomain() == "127"
        ){
            return "strasbourg";
            //return "colmar";
            //return "metz";
            //return "nancy";
            //return null;
        } 
        elseif (
            $this->getSubdomain() == "www" ||
            $this->getSubdomain() == "billetterie" ||
            $this->getSubdomain() == "curieum"
        ){
            return null;
        } 
        else {
            return $this->getSubdomain();
        }
    }

    public function getSubdomain() {

        return explode(".",$_SERVER['SERVER_NAME'])[0];
    }

    public function getDomain() {

        $domain = explode(".",$_SERVER['SERVER_NAME']);

        if (count($domain) == "3"){
            return $domain[1].".".$domain[2];
        } else {
            return "curieux.net";
        }
        
    }

}


/*
    if ($subdomain->getMainCity()->getBorough()->getResident()){
        $resident['plural']['masculine'] = $subdomain->getMainCity()->getBorough()->getResident();
        if (substr($resident['plural']['masculine'], -4) == "iens"){
            $resident['singular']['masculine'] = str_replace("iens","ien",$resident['plural']['masculine']);
            $resident['singular']['feminine'] = str_replace("iens","ienne",$resident['plural']['masculine']);
            $resident['plural']['feminine'] = str_replace("iens","iennes",$resident['plural']['masculine']);
        }
        elseif (substr($resident['plural']['masculine'], -5) == "ssins"){
            $resident['singular']['masculine'] = str_replace("ssins","ssin",$resident['plural']['masculine']);
            $resident['singular']['feminine'] = str_replace("ssins","ssine",$resident['plural']['masculine']);
            $resident['plural']['feminine'] = str_replace("ssins","ssines",$resident['plural']['masculine']);
        }
        else {
            $resident['singular']['masculine'] = $resident['plural']['masculine'];
            $resident['singular']['feminine'] = $resident['plural']['masculine']."e";
            $resident['plural']['feminine'] = $resident['plural']['masculine']."es";
        }
    } else {
        $resident = null;
    }
*/