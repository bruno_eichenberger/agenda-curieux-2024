<?php

namespace App\Service;

use DateTime;
use App\Entity\User;
use App\Entity\Place;
use App\Service\Website;
use App\Entity\Ticketing;
use App\Entity\Videostore;
use Cocur\Slugify\Slugify;
use App\Service\UploaderHelper;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class DataTreatment extends AbstractController
{
    public function __construct(Website $website, ManagerRegistry $doctrine, UploaderHelper $uploaderHelper, RequestStack $request)
    {
        $this->request = $request;
        $this->uploader = $uploaderHelper;
        $this->website = $website->getArray();
        $this->subdomain = $website->getArray()['address']['subdomain'];
        $this->doctrine = $doctrine;
    }


    public function check($form)
    {
        // Alerte si le tarif d'une réduction n'est pas valide ou supérieur à 1000
        foreach($form['prices'] as $price)
        {
            if (is_numeric($price['price']->getData()) === false)
            {
                 $this->addFlash(
                    'danger',
                    "La réduction '". $price['reduction']->getData()->getTitle() ."' n'a pas un tarif valide !"
                );
                return false;
            }
            elseif ($price['price']->getData() >= 1000 )
            {
                $this->addFlash(
                   'danger',
                   "La réduction '". $price['reduction']->getData()->getTitle() ."' doit être inférieur à 1000€ !"
               );
               return false;
           }
        }

        // Alerte si le tarif d'une billetterie n'est pas valide
        foreach($form['tickets'] as $ticket)
        {
            if (filter_var($ticket["url"]->getData(), FILTER_VALIDATE_URL) === false)
            {
                $this->addFlash(
                   'danger',
                   "L'adresse de la billetterie '". $ticket['url']->getData() ."' n'est pas valide !"
               );
               return false;
            }
            if (is_numeric($ticket['price']->getData()) === false && $ticket['price']->getData())
            {
                 $this->addFlash(
                    'danger',
                    "Le tarif de la billetterie '". $ticket['url']->getData() ."' n'est pas valide !"
                );
                return false;
            }
            elseif ($ticket['price']->getData() >= 1000 && $ticket['price']->getData())
            {
                $this->addFlash(
                   'danger',
                   "Le tarif de la billetterie '". $ticket['url']->getData() ."' doit être inférieur à 1000€ !"
               );
               return false;
           }
        }

        if (
            (
                (
                    $this->getUser() and
                    in_array('ROLE_ADMIN',$this->getUser()->getRoles()) === NULL
                ) or (
                    $this->getUser() === NULL
                )
            ) and (
                (
                    $form['dateEnd']->getData() === null and
                    $form['dateStart']->getData()->format("Y-m-d") < date("Y-m-d")
                ) or (
                    $form['dateEnd']->getData() !== null and
                    $form['dateEnd']->getData()->format("Y-m-d") < date("Y-m-d")
                )
            )
        ){
            $this->addFlash(
                'danger',
                "Vous ne pouvez pas modifier un évènement déjà terminé !"
            );
            return false;
        }

        // Si la ville n'est pas indiqué et que le lieu n'est pas en base de donnée
        if ($form['city']->getData() === NULL && $form['place']->getData()->getId() === null)
        {
            $this->addFlash(
                'danger',
                "Veuillez préciser la ville ! Vous pouvez également choisir ou créer un lieu dans l'annuaire !"
            );

            return false;

        }

        return true;
    }


    public function event($form)
    {
        $route = $this->request->getCurrentRequest()->get('_route');

        $manager = $this->doctrine->getManager();
        $event = $form->getData();
        $places = [];

        if ($form->get('submitPublished')->isClicked()){
            $event->setIsPublished(true);
        }

        // Demande réévaluation de l'évènement ou validation par admin
        if (
            $form->offsetExists('submitWithError') and
            $form->get('submitWithError')->isClicked())
        {
            if (in_array("ROLE_ADMIN", $this->getUser()->getRoles())){
                $event->setIsValidated(true);
            } else {
                $event->setIsValidated(null);
            }

            // Initialisation
            $event  ->setIsSpam(false)
                    ->setIsDuplicated(false)
                    ->setisMistake(false)
                    ->setIsFull(false)
                    ->setIsCanceled(false)
                    ->setIsPostponed(false)
            ;

        }

        // Mise à jour du statut via formulaire (Complet, annulé, spam...)
        if ($form->offsetExists('validation'))
        {
            $validation = $form->get('validation')->getData();

            if ($validation){
                $event  ->setIsSpam(false)
                        ->setIsDuplicated(false)
                        ->setisMistake(false)
                        ->setIsFull(false)
                        ->setIsCanceled(false)
                        ->setIsPostponed(false)
                        //->setIsPublished(false)
                ;

                if ($validation == "spam"){
                    $event  ->setIsSpam(true)
                            ->setIsValidated(false);
                }
                elseif ($validation == "duplicated"){
                    $event  ->setIsDuplicated(true)
                            ->setIsValidated(false);
                }
                elseif ($validation == "mistake"){
                    $event  ->setisMistake(true)
                            ->setIsValidated(false);
                }
                elseif ($validation == "full"){
                    $event->setIsFull(true);
                }
                elseif ($validation == "canceled"){
                    $event->setIsCanceled(true);
                }
                elseif ($validation == "postponed"){
                    $event  ->setIsPostponed(true)
                            ->setIsCanceled(true);
                }
            }
        }

        // Validation d'un admin via formulaire
        if (
            $form->offsetExists('submitWithValidation') and
            $form->get('submitWithValidation')->isClicked() and
            in_array("ROLE_ADMIN", $this->getUser()->getRoles())
        ){
            $event  ->setIsValidated(true)
                    ->setVerifiedBy($this->getUser())
                    ->setVerifiedAt(new DateTime())
            ;
        }

        if (isset($form['coverImageFile'])){
            /** @var UploadedFile $uploadedFile */
            //$uploadedFile = $request->files->get( 'image' );
            $uploadedFile = $form['coverImageFile']->getData();

            $slugify = new Slugify();
            $slugImg = $slugify->slugify($event->getTitle());

            if ($form['coverImageFile']->getData()){
                $uploadedFile = $form['coverImageFile']->getData();
            }
            elseif ($event->getCoverImage() and $route == "event_create"){
                $event->setCoverImageByUrl($event->getCoverImage(),$slugImg);
            }
            //dd($uploadedFile);

            if ($uploadedFile){
                $newFilename = $this->uploader->uploadEventImage($uploadedFile, $slugImg);
                $event->setCoverImage($newFilename);
            }
        }

        if (isset($form['images'])){
            foreach($form['images'] as $images){

                $uploadedImage = $images['imageFile']->getData();

                if ($uploadedImage){
                    $image = $images->getData();
                    $newFilename2 = $this->uploader->uploadEventImage($uploadedImage);
                    $image  ->setUrl($newFilename2)
                            ->setEvent($event);

                    $manager->persist($image);
                }
            }
        }

        if ($event->getPrecisionPrice() === NULL){
            $event->setPrice(null);
        }
        elseif ($event->getPrecisionPrice() == "FREE" OR $event->getPrecisionPrice() == "AWARE"){
            $event->setPrice(0);
        }
        elseif ($event->getPrecisionPrice() == "PAYING"){

            if ($form['more']->getData()){
                $event  ->setPrecisionPrice('MORE')
                        ->setPrice(null);
            }
            elseif ($event->getPrice() == "0"){
                $event->setPrecisionPrice('FREE');
            }
        }

        // initialisation des dates multiples
        /*if ($event->getDateStart() == $event->getDateEnd()){
            $event  ->setPrecisionDate(null)
                    ->setChoiceOfDays(null)
                    ->setDateMultiple(null);
        }
        else*/
        if ($event->getPrecisionDate() == "ALL_DAYS"){
            $event->setChoiceOfDays(null);
            $event->setDateMultiple(null);
        }
        elseif ($event->getPrecisionDate() == "DAYS_OPENING"){
            $event->setDateMultiple(null);
        }
        elseif ($event->getPrecisionDate() == "DATES_OPENING"){
            $event->setChoiceOfDays(null);
        }

        //dd($form['choiceOfDays']->getData());

        // Si place.id est détecté dans le tableau event
        if ($event->getPlace() and $event->getPlace()->getId()){
            $event->setPlaceTemporary(NULL);
            $event->setCity(NULL);
        }
        else {

            $slugify = new Slugify();
            $title = $form['place']->getData()->getTitle();
            $address = $form['address']->getData();
            $city = $form['city']->getData();

            // Si l'adresse est indiqué
            if ($address) {

                // Recherche si le lieu existe déjà
                $placeSlug = $slugify->slugify($title);
                $placeLower = mb_strtolower($title);
                $places = $this->doctrine->getRepository(Place::class)->findIfAlreadyExist($placeSlug, $placeLower, $this->detectForSearch(mb_strtolower($address))['address'], $city);

                //dd($places);
                // S'il y a au moins un résultat, associer le lieu déjà en base de données
                /*
                if ($result != []) {
                    $event  ->setPlace($result[0])
                            ->setPlaceTemporary(NULL)
                            ->setCity(NULL);

                    $this->addFlash(
                        'warning',
                        "Le lieu était déjà annoncé dans l'annuaire : <strong>{$result[0]->getTitle()}</strong> !"
                    );
                }
                */

                // S'il n'y a pas de suggestion, créer un nouveau lieu en base de données
                if ($places == []) {

                    $place = $event->getPlace();
                    $place  ->setName($this->getRewritingPlaceTitle($title))
                            ->setNumber($this->detectForSearch(mb_strtolower($address))['number'])
                            ->setAddress($this->detectForSearch($address)['address'])
                            ->setCity($city)
                    ;

                    if ($this->getUser()){
                        $place->setAuthor($this->getUser());
                        $this->addFlash(
                            'warning',
                            "Vous avez ajouté un nouveau lieu dans l'annuaire : <strong>{$this->getRewritingPlaceTitle($title)}</strong>"
                        );
                    }

                    $manager->persist($place);

                    $event  ->setPlace($place)
                            ->setPlaceTemporary(NULL)
                            ->setCity(NULL);
                } else {
                    // Ajouter le lieu soumis à la fin des suggestions
                    //$places[] = $event->getPlace();

                    if ($places != []) {
                        $event  ->setPlace($places[0])
                                ->setPlaceTemporary(NULL)
                                ->setCity(NULL);

                        $this->addFlash(
                            'warning',
                            "Le lieu était déjà annoncé dans l'annuaire : <strong>{$places[0]->getTitle()}</strong> !"
                        );
                    }
                    /*
                    $event  ->setPlaceTemporary($this->getRewritingPlaceTitle($title))
                            ->setPlace(NULL)
                    ;

                    $this->addFlash(
                        'danger',
                        "<a href='" . $this->generateUrl('event_edit', ['slug'=> $event->getSlug()]) . "#choisir-un-lieu'>Nous avons détecté ". count($places) ." lieux pouvant correspondre à votre adresse !</a>"
                    );
                    */
                }
            } else {

                $event  ->setPlaceTemporary($this->getRewritingPlaceTitle($title))
                        ->setPlace(NULL)
                ;
            }


            //dd($event);
            /*
            if ($event->getPlace()) {
                $event->setPlaceTemporary($this->getRewritingPlaceTitle($event->getPlace()->getTitle()));
                $event->setPlace(NULL);
            }
            */
        }


        $i = 0;
        foreach($event->getCategory() as $category){
            if ($i < 4) {
                $category->addEvent($event);
                $manager->persist($category);
            } else {
                $event->removeCategory($category);
            }
            $i++;
        }

        $j = 0;
        foreach($event->getArtists() as $artist){
            if ($j < 10) {
                if ($this->getUser() and $artist->getId() === NULL){
                    $artist->setAuthor($this->getUser());
                }
                $artist->addEvent($event);
                $manager->persist($artist);
            } else {
                $event->removeArtist($artist);
            }
            $j++;
        }

        $organizer = $event->getOrganizer();
        if ($organizer){
            if ($this->getUser() && $organizer->getId() === NULL){
                $organizer->setAuthor($this->getUser());
            }
            $organizer->addEvent($event);
            $manager->persist($organizer);
        }

        $festival = $event->getFestival();
        if ($festival){
            if ($this->getUser() and $festival->getId() === NULL){
                $festival->setAuthor($this->getUser());
            }
            $festival->addEvent($event);
            $manager->persist($festival);
        }

        foreach($event->getPrices() as $price)
        {
            $price->setEvent($event);
            $manager->persist($price);
        }

        /*
        foreach($data["prices"] as $priceWithReductions)
        {
            foreach($priceWithReductions["reductions"] as $id){
                $prices = new Prices;

                $prices->setPrice($priceWithReductions["price"]);
                $prices->setReduction($this->doctrine->getRepository(Reduction::class)->findOneBy(['id' => $id]));

                //$prices->setEvent($event);
                $event->addPrice($prices);
                $manager->persist($event);
            }

        }
        */



        // Récupère la liste des billetteries
        $ticketings = $this->doctrine->getRepository(Ticketing::class)->findAll();

        foreach($event->getTickets() as $ticket)
        {
            $parsing = parse_url($ticket->getUrl());
            //dd($parsing);
            $arrayOfDomain = array_slice(explode(".",strtolower($parsing['host'])), -2);
            $nameOfDomain = $arrayOfDomain[0];
            $domain = implode(".", $arrayOfDomain);

            if (isset($parsing['path']) and $parsing['path'] != "/"){

                if($ticket->getTicketing() === null){
                    // Vérifier les URL des billets pour attribuer une billetterie si elle n'est pas connu
                    foreach($ticketings as $ticketing){

                        if (strpos($parsing['host'], $ticketing->getDomain()) !== false)
                        {
                            if ($domain == "yurplan.com" and strpos($parsing['path'],"/event/") !== false)
                            {
                                $ticket ->setTicketing($ticketing)
                                        ->setUrlEmbed('https://'.$parsing['host'].''.$parsing['path'].'/tickets/widget');
                            }
                            elseif ($domain == "helloasso.com" and strpos($parsing['path'],"/evenements/") !== false)
                            {
                                $ticket ->setTicketing($ticketing)
                                        ->setUrlEmbed('https://'.$parsing['host'].''.$parsing['path'].'/widget');
                            }
                            elseif ($nameOfDomain == "eventbrite" and strpos($parsing['path'],"/e/") !== false)
                            {
                                $pathEventBrite = explode('-', $parsing['path']);
                                $ticket ->setUrlEmbed('https://'.$parsing['host'].'/tickets-external?eid='.end($pathEventBrite))
                                        ->setTicketing($ticketing);
                            }
                            elseif ($nameOfDomain == "weezevent"){
                                //dd($parsing);
                                if (
                                    $parsing['path'] == '/widget_billeterie.php' or
                                    strpos($parsing['path'],"/ticket/") !== false or
                                    (isset($parsing['query']) and strpos($parsing['query'],"id_evenement=") !== false)
                                ){
                                    if (isset($parsing['query'])){
                                        parse_str($parsing['query'],$query);
                                        if (isset($query['id_evenement'])){
                                            $ticket ->setUrl('https://www.weezevent.com/evenement.php?id_evenement='.$query['id_evenement'])
                                                    ->setUrlEmbed('https://www.weezevent.com/widget_billeterie.php?id_evenement='.$query['id_evenement'])
                                                    //->setUrlEmbed('https://'.$parsing['host'].''.$parsing['path'].'?id_evenement='.$query['id_evenement'])
                                                    ->setTicketing($ticketing);
                                        } else {
                                            $event->removeTicket($ticket);
                                        }
                                    } else {
                                        $event->removeTicket($ticket);
                                    }
                                }
                                elseif ($parsing['path'] == '/widget_multi.php')
                                {
                                    $ticket ->setTicketing($ticketing)
                                            ->setUrlEmbed($ticket->getUrl());
                                } else {
                                    if (isset($parsing['query'])){
                                        parse_str($parsing['query'],$query);
                                        if (isset($query['id_evenement'])){
                                            $ticket ->setUrl('https://www.weezevent.com'.$parsing['path'])
                                                    ->setUrlEmbed('https://www.weezevent.com/widget_billeterie.php?id_evenement='.$query['id_evenement']);
                                        }
                                    }
                                    $ticket->setTicketing($ticketing);
                                }

                            }
                            else {
                                $ticket->setTicketing($ticketing);
                                if (
                                    $ticketing->getEmbed() and
                                    strpos($ticket->getUrl(),$ticketing->getEmbed()) === false
                                ){
                                    $ticket->setUrlEmbed(str_replace($ticketing->getWebsite(),$ticketing->getEmbed(),$ticket->getUrl()));
                                }
                            }

                        }
                    }

                    // Si l'URL du billet ne correspond à aucune billetterie, création d'une nouvelle billetterie
                    if ($ticket->getTicketing() === NULL){

                        $ticketing = new Ticketing();
                        $ticketing  ->setName(ucfirst($nameOfDomain))
                                    ->setSetting('2')
                                    ->setWebsite('https://'.$parsing['host'])
                                    ->setDomain($domain);

                        $ticket->setTicketing($ticketing);
                        $manager->persist($ticketing);

                        // Ajouter la nouvelle billetterie au tableau
                        array_push($ticketings, $ticketing);
                    }
                }

                $ticket ->setPrice($ticket->getPrice())
                        ->setEvent($event);

                $manager->persist($ticket);

            } else {

                $this->addFlash(
                    'warning',
                    "L'adresse <b>'{$ticket->getUrl()}'</b> n'est pas assez précise, du coup nous l'avons supprimé !"
                );
                $event->removeTicket($ticket);
            }
        }

        // Récupère la liste des hébergeurs de vidéo
        $videostores = $this->doctrine->getRepository(Videostore::class)->findAll();

        foreach($event->getVideos() as $video)
        {
            $parsing = parse_url($video->getUrl());
            // Vérifier les URL des vidéos pour attribuer un site si elle n'est pas connu
            foreach($videostores as $videostore){
                // Si l'URL du billet correspond à une billetterie
                if (
                    strpos($videostore->getWebsite(), $parsing['host']) !== false or
                    strpos($videostore->getShortcut(), $parsing['host']) !== false
                ){
                    $video->setVideostore($videostore);

                    // Traduire l'adresse en un lien cliquable
                    if (strpos($video->getUrl(),$videostore->getShortcut()) !== false)
                    {
                        $newUrl = str_replace($videostore->getShortcut(),$videostore->getLinked(),$video->getUrl());
                        $video->setUrl($newUrl);
                    }
                    elseif (strpos($video->getUrl(),$videostore->getEmbed()) !== false)
                    {
                        $newUrl = str_replace($videostore->getEmbed(),$videostore->getLinked(),$video->getUrl());
                        $video->setUrl($newUrl);
                    }
                }
            }

            // Si l'URL de la vidéo ne correspond à aucun site, création d'un nouveau hébergement de vidéo
            if ($video->getVideostore() === NULL){

                $nameOfVideostore = ucfirst(array_slice(explode(".",$parsing['host']), -2)['0']);

                $videostore = new Videostore();
                $videostore ->setName($nameOfVideostore)
                            ->setWebsite($parsing['scheme'].'//'.$parsing['host']);

                $video->setVideostore($videostore);
                $manager->persist($videostore);

                // Ajouter la nouvelle billetterie au tableau
                array_push($videostores, $videostore);
            }

            $video ->setEvent($event);
            $manager->persist($video);
        }

        if ($event->getMailAnonymous()){
            $userAnonymous = $this->doctrine->getRepository(User::class)->findOneBy(['email' => $event->getMailAnonymous()]);
            $event->setAuthor($userAnonymous);
        }

        return ['event' => $event, 'places' => $places];
    }

    function getRewritingPlaceTitle($text)
    {
        if (
            $text == strtolower($text) or
            $text == strtoupper($text)
        ){
            $text = str_replace(".",".$ ",$text);
            $text = str_replace("'","'$ ",$text);
            $text = ucwords(strtolower($text));
            $text = str_replace(".$ ",".",$text);
            $text = str_replace("'$ ","'",$text);
        }

        $text = str_replace("Theatre","Théâtre",$text);
        $text = str_replace("Cinema","Cinéma",$text);
        $text = str_replace("Zenith","Zénith",$text);
        $text = str_replace(" D'"," d'",$text);
        $text = str_replace(" L'"," l'",$text);
        $text = str_replace(" Du "," du ",$text);
        $text = str_replace(" De "," de ",$text);
        $text = str_replace(" Des "," des ",$text);
        $text = str_replace(" La "," la ",$text);
        $text = str_replace(" Le "," le ",$text);
        $text = str_replace(" Les "," les ",$text);
        $text = str_replace(" Au "," au ",$text);
        $text = str_replace(" Aux "," aux ",$text);

        return $text;
    }

    function detectForSearch($text){

        // supprime les espaces et caracteres spéciaux aux extrémités
        $address = trim($text," ?!");

        // Sépare le numéro si il est au début
        $addressTab = explode(" ", $address);

        $number = null;
        if (
            is_numeric($addressTab[0]) or
            is_numeric(str_replace("a","",$addressTab[0])) or
            is_numeric(str_replace("b","",$addressTab[0]))
        ){
            $number = $addressTab[0];
            array_shift($addressTab);
        }
        $address = implode(" ", $addressTab);

        // supprime à nouveau les espaces et les caracteres spéciaux aux extrémités
        $address = trim($address," ?!");

        /*
        // Si l'adresse commence par un mot defini, garder uniquement ce qui est après
        $words = ["rue","avenue","boulevard","place", "chemin", "passage", "impasse", "route"];
        foreach (explode(" ", $address) as $word){
            $addressTab[] = $word;
            if (in_array($wordTmp,$words)){
                $addressTab = [];
            }
        }

        if($addressTab[0] != ""){
            $address = implode(" ", $addressTab);
        }
        */

        return [
            'number' => $number,
            'address' => $address
        ];
    }
}