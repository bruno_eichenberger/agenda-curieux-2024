<?php

namespace App\Service;

use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ArrayOfData extends AbstractController {

    public function createArrayOfData($pagination, $title)
    {   
        $data['title'] = $title['head'];

        foreach($pagination as $event){
            //$event->setDateText($this->dateSentence->getText($event));

            if (
                $event->getDateStart() <= new DateTime('today') and 
                $event->getDateEnd() >= new DateTime('today')
            ){
                $events['today'][] = $event;
            } else {
                $events['otherday'][] = $event;
            }
        }

        if (isset($events['today'])){
            $data['list'][] = [
                'title' => $title['today'],
                'events' => $this->separator($events['today'])
            ];
            //dd($data['list']);
        }

        if (isset($events['otherday'])){
            $data['list'][] = [
                'title' => $title['text'],
                'events' => $this->separator($events['otherday'])
            ];
        }

        if ($title['button']){
            $data['button'] = $title['button'];
        }

        $data['pagination'] = $pagination;

        return $data;
    }

    public function separator($events)
    {
        if ($events){
            $lastTime = "";
            $lastDateStart = "";
            $lastDateEnd = "";
            foreach ($events as $event){

                if ($event->getDateStart()->format('Y-m-d') == date('Y-m-d')){
                    if (isset($lastTime) and $event->getTimeStart() == $lastTime){
                        $event->setSameDateTime(true);
                    }
                } else {
                    if (
                        isset($lastDateStart) and isset($lastDateEnd) and 
                        $event->getDateStart() == $lastDateStart and
                        $event->getDateEnd() == $lastDateEnd
                    ){
                        $event->setSameDateTime(true);
                    }
                }

                $lastDateStart = $event->getDateStart();
                $lastDateEnd = $event->getDateEnd();
                $lastTime = $event->getTimeStart();
            }
            return $events;
        }
        else {
            return null;
        }

    }

    public function separatorByTime($events)
    {
        $lastTime = "";
        foreach ($events as $event){
            if ($event->getTimeStart() == $lastTime){
                $event->setSameDateTime(true);
            }
            $lastTime = $event->getTimeStart();
        }
        return $events;
    }

    public function separatorByDate($events)
    {
        $lastDateStart = "";
        $lastDateEnd = "";
        foreach ($events as $event){
            if (
                isset($lastDateStart) and isset($lastDateEnd) and 
                $event->getDateStart() == $lastDateStart and
                $event->getDateEnd() == $lastDateEnd
            ){
                $event->setSameDateTime(true);
            }
            $lastDateStart = $event->getDateStart();
            $lastDateEnd = $event->getDateEnd();
        }

        return $events;
    }
}