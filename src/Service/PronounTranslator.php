<?php

namespace App\Service;

class PronounTranslator {

    public function translate($data = NULL) {
        
        if ($data == "Le"){$pronoun = "du ";}
        elseif ($data == "La"){$pronoun = "de la ";}
        elseif ($data == "Les"){$pronoun = "des ";}
        elseif ($data == "L'"){$pronoun = "de l'";}
        else {$pronoun = "de ";}

        return $pronoun;
    }
}
