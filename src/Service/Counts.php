<?php

namespace App\Service;

use App\Service\Website;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class Counts extends AbstractController {

    public function __construct(Website $website)
    {
        $this->website = $website->getArray();
        $this->subdomain = $website->getArray()['address']['subdomain'];
    }
    
    public function getCounts($repo, $request, $entity=null, $search=null) : array
    {
        $borough = $this->subdomain;
        $slug = $request->attributes->get('slug');
        $route = $request->attributes->get('_route');

        if ($route == "events_by_rubric")
        {
            $count['today'] = $repo->findByRubric($slug, $borough, 'today', true)->getSingleScalarResult();
            $count['after'] = $repo->findByRubric($slug, $borough, 'after', true)->getSingleScalarResult();
            $count['button'] = $repo->findByRubric($slug, $borough, 'before', true)->getSingleScalarResult();
        }
        elseif ($route == "old_events_by_rubric")
        {
            $count['after'] = $repo->findByRubric($slug, $borough, 'before', true)->getSingleScalarResult();
            $count['button'] = $repo->findByRubric($slug, $borough, null, true)->getSingleScalarResult();
        }
        
        if ($route == "events_by_category")
        {
            $count['today'] = $repo->findByCategory($slug, $borough, 'today', true)->getSingleScalarResult();
            $count['after'] = $repo->findByCategory($slug, $borough, 'after', true)->getSingleScalarResult();
            $count['button'] = $repo->findByCategory($slug, $borough, 'before', true)->getSingleScalarResult();
        }
        elseif ($route == "old_events_by_category")
        {
            $count['after'] = $repo->findByCategory($slug, $borough, 'before', true)->getSingleScalarResult();
            $count['button'] = $repo->findByCategory($slug, $borough, null, true)->getSingleScalarResult();
        }

        if ($route == "events_by_reduction")
        {
            $count['today'] = $repo->findByReduction($slug, $borough, 'today', true)->getSingleScalarResult();
            $count['after'] = $repo->findByReduction($slug, $borough, 'after', true)->getSingleScalarResult();
            $count['button'] = $repo->findByReduction($slug, $borough, 'before', true)->getSingleScalarResult();
        }
        elseif ($route == "old_events_by_reduction")
        {
            $count['after'] = $repo->findByReduction($slug, $borough, 'before', true)->getSingleScalarResult();
            $count['button'] = $repo->findByReduction($slug, $borough, null, true)->getSingleScalarResult();
        }

        if ($route == "events_by_like")
        {
            $count['today'] = $repo->findByLike($entity, 'today', true)->getSingleScalarResult();
            $count['after'] = $repo->findByLike($entity, 'after', true)->getSingleScalarResult();
            $count['button'] = $repo->findByLike($entity, 'before', true)->getSingleScalarResult();
        }
        elseif ($route == "old_events_by_like")
        {
            $count['after'] = $repo->findByLike($entity, 'before', true)->getSingleScalarResult();
            $count['button'] = $repo->findByLike($entity, null, true)->getSingleScalarResult();
        }

        if ($route == "search_by_filter")
        {
            $count['today'] = $repo->findAllSearchQuery($search, $borough, null, 'today', true)->getSingleScalarResult();
            $count['after'] = $repo->findAllSearchQuery($search, $borough, null, 'after', true)->getSingleScalarResult();
            $count['button'] = $repo->findAllSearchQuery($search, $borough, null, 'before', true)->getSingleScalarResult();
        }
        elseif ($route == "old_search_by_filter")
        {
            $count['after'] = $repo->findAllSearchQuery($search, $borough, null, 'before', true)->getSingleScalarResult();
            $count['button'] = $repo->findAllSearchQuery($search, $borough, null, null, true)->getSingleScalarResult();
        }

        if ($route == "place_show")
        {
            $count['today'] = $repo->findByPlace($entity, 'today', true)->getSingleScalarResult();
            $count['after'] = $repo->findByPlace($entity, 'after', true)->getSingleScalarResult();
            $count['button'] = $repo->findByPlace($entity, 'before', true)->getSingleScalarResult();
        }
        elseif ($route == "old_place_show")
        {
            $count['after'] = $repo->findByPlace($entity, 'before', true)->getSingleScalarResult();
            $count['button'] = $repo->findByPlace($entity, null, true)->getSingleScalarResult();
        }


        if ($route == "place_index")
        {
            $count['after'] = $repo->findAllSearchByFilter($slug, $borough, 'after', true)->getSingleScalarResult();
            $count['button'] = $repo->findAllSearchByFilter($slug, $borough, 'before', true)->getSingleScalarResult();
        }


        //dd($count);

        /*
        if ($route == "event_by_day")
        {
            $count['today'] = $repo->findByDay($slug, $borough, 'today', true)->getSingleScalarResult();
            $count['after'] = $repo->findByDay($slug, $borough, 'after', true)->getSingleScalarResult();
            $count['button'] = $repo->findByDay($slug, $borough, 'before', true)->getSingleScalarResult();
        }
        */
        return $count;
        
    }
}