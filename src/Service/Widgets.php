<?php

namespace App\Service;

use App\Entity\Event;
use App\Entity\Place;
use App\Entity\Video;
use App\Entity\Category;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class Widgets extends AbstractController 
{
    public function __construct(Website $website, ManagerRegistry $doctrine)
    {
        $this->website = $website->getArray();
        $this->subdomain = $website->getArray()['address']['subdomain'];
        $this->doctrine = $doctrine;
    }


    public function getWidgets($cache, $request, $date=null) : array
    {
        if (strpos($request->attributes->get('_route'),"old_") !== false){return array();}

        if ($request->attributes->get('slug')){
            $nameOfCache = $request->attributes->get('_route').'_'.$request->attributes->get('slug').'_widget_'.$this->subdomain;
        }
        elseif ($request->attributes->get('_route') == "event_index"){
            $nameOfCache = $request->attributes->get('_route').'_widget_'.$this->subdomain;
        }
        elseif ($date){
            $nameOfCache = $request->attributes->get('_route').'_'.$date.'_widget_'.$this->subdomain;
        } else {
            $nameOfCache = $request->attributes->get('_route').'_widget_'.$this->subdomain;
        }

        return $cache->get($nameOfCache, function (ItemInterface $item) use ($request, $date) {
            
            $item->expiresAfter(60*63);
            //$item->expiresAfter(2);

            if ($request->attributes->get('_route') == "events_by_rubric")
            {
                //$map = $this->getPositionOfMap($this->doctrine->getRepository(Event::class)->findByRubric($request->attributes->get('slug'), $this->subdomain, null, null, true)->getResult());
                $player = $this->doctrine->getRepository(Video::class)->findByRubric($request->attributes->get('slug'), $this->subdomain)->getResult();

                $suggestions = $this->doctrine->getRepository(Category::class)->findByRubric($request->attributes->get('slug'), $this->subdomain)->getResult();
            }

            if ($request->attributes->get('_route') == "events_by_category")
            {
                $map = $this->getPositionOfMap($this->doctrine->getRepository(Event::class)->findByCategory($request->attributes->get('slug'), $this->subdomain, null, null, true)->getResult());
                $player = $this->doctrine->getRepository(Video::class)->findByCategory($request->attributes->get('slug'), $this->subdomain)->getResult();
            }

            if ($request->attributes->get('_route') == "events_by_reduction")
            {
                $map = $this->getPositionOfMap($this->doctrine->getRepository(Event::class)->findByReduction($request->attributes->get('slug'), $this->subdomain, null, null, true)->getResult());
                $player = $this->doctrine->getRepository(Video::class)->findByReduction($request->attributes->get('slug'), $this->subdomain)->getResult();
                $suggestions = $this->doctrine->getRepository(Category::class)->findByReduction($request->attributes->get('slug'), $this->subdomain)->getResult();
            }
            
            if ($request->attributes->get('_route') == "event_index")
            {
                /*
                $borough = $this->doctrine->getRepository(Borough::class)->findOneBy(['slug' => $this->subdomain]);
                if ($borough->getLatitude() and $borough->getLongitude()){
                    $entityBorough = $borough;
                } else {
                    $entityBorough = null;
                }
                $map = $this->getPositionOfMap($this->doctrine->getRepository(Event::class)->findByDateForMap($date, $this->subdomain),null,$entityBorough);
                */
                $map = $this->getPositionOfMap($this->doctrine->getRepository(Event::class)->findByDateForMap($date, $this->subdomain));
                $player = $this->doctrine->getRepository(Video::class)->findByDate($date, $this->subdomain)->getResult();
                $suggestions = $this->doctrine->getRepository(Category::class)->findByDate($date, $this->subdomain)->getResult();
            }
            /**/
            if ($request->attributes->get('_route') == "event_by_day")
            {
                $map = $this->getPositionOfMap($this->doctrine->getRepository(Event::class)->findByDateForMap($date, $this->subdomain));
                $player = $this->doctrine->getRepository(Video::class)->findByDate($date, $this->subdomain)->getResult();
                $suggestions = $this->doctrine->getRepository(Category::class)->findByDate($date, $this->subdomain)->getResult();
            }

            if ($request->attributes->get('_route') == "place_index"){
                $map = $this->getPositionOfMap($this->doctrine->getRepository(Place::class)->findAllSearchQuery($this->subdomain, false, true)->getResult(),true);
            }

            if ($request->attributes->get('_route') == "search_by_placekind"){
                $map = $this->getPositionOfMap($this->doctrine->getRepository(Place::class)->findByPlacekind($request->attributes->get('slug'), $this->subdomain, false, false, true)->getResult(),true);
            }
            
            if ($request->attributes->get('_route') == "search_by_placetag"){
                $map = $this->getPositionOfMap($this->doctrine->getRepository(Place::class)->findByPlacetag($request->attributes->get('slug'), $this->subdomain, false, false, true)->getResult(),true);
            }
            
            if ($request->attributes->get('_route') == "search_by_city"){
                $map = $this->getPositionOfMap($this->doctrine->getRepository(Place::class)->findByCity($request->attributes->get('slug'), $this->subdomain, false, false, true)->getResult(),true);
            }

            //dd($map);
            if (isset($map) === false or empty($map)){$map = null;}
            if (isset($player) === false or empty($player)){$player = null;}

            if (isset($suggestions) && !empty($suggestions)){
                $suggestion['buttons'] = $suggestions;
                $suggestion['route'] = 'events_by_category';
            } else {
                $suggestion = null;
            }

            return ['player' => $player, 'map' => $map, 'suggestion' => $suggestion];
        });
    }

    public function getPositionOfMap($markers, $entityPlace=null, $entityBorough=null)
    {
        /*
        if($events == []){

            if ($this->subdomain){
                $borough = $this->doctrine->getRepository(Borough::class)->findOneBy(['slug' => $this->subdomain]);
                $map['center'] = $borough->getLatitude() .",". $borough->getLongitude();
                $map['zoom'] = "12";
            } else {
                $map['center'] = "47, 1.5";
                $map['zoom'] = "5";
            }
        }
        */

        //if($markers != []){
        if(!empty($markers)){

            $map['markers'] = $markers;
            
            if ($entityBorough){

                $map['center'] = $entityBorough->getLatitude() .",". $entityBorough->getLongitude();
                if ($entityBorough->getZoom()){
                    $map['zoom'] = $entityBorough->getZoom();
                } else {
                    $map['zoom'] = "12";
                }
                $map['info'] = null;

            } else {

                foreach($markers as $marker){
                    if ($entityPlace){
                        $place = $marker[0];
                    } else {
                        $place = $marker[0]->getPlace();
                    }

                    if (isset($latMin) === false or $place->getLatitude() < $latMin){$latMin = $place->getLatitude();}
                    if (isset($latMax) === false or $place->getLatitude() > $latMax){$latMax = $place->getLatitude();}
                    if (isset($lonMin) === false or $place->getLongitude() < $lonMin){$lonMin = $place->getLongitude();}
                    if (isset($lonMax) === false or $place->getLongitude() > $lonMax){$lonMax = $place->getLongitude();}

                    /*
                    if (isset($latMin) === false){$latMin = $place->getLatitude();}
                    if (isset($latMax) === false){$latMax = $place->getLatitude();}
                    if (isset($lonMin) === false){$lonMin = $place->getLongitude();}
                    if (isset($lonMax) === false){$lonMax = $place->getLongitude();}

                    if (isset($latMin) and $place->getLatitude() < $latMin){$latMin = $place->getLatitude();}
                    if (isset($latMax) and $place->getLatitude() > $latMax){$latMax = $place->getLatitude();}
                    if (isset($lonMin) and $place->getLongitude() < $lonMin){$lonMin = $place->getLongitude();}
                    if (isset($lonMax) and $place->getLongitude() > $lonMax){$lonMax = $place->getLongitude();}
                    */
                }

                $latDif = $latMax - $latMin;
                $lonDif = $lonMax - $lonMin;
                //dump($latDif ." - ". $lonDif);

                if ($latDif < 0.02 and $lonDif < 0.02){$map['zoom'] = "14";}
                elseif ($latDif < 0.04 and $lonDif < 0.04){$map['zoom'] = "13";}
                elseif ($latDif < 0.1 and $lonDif < 0.1){$map['zoom'] = "12";}
                elseif ($latDif < 0.18 and $lonDif < 0.18){$map['zoom'] = "11";}
                elseif ($latDif < 0.35 and $lonDif < 0.35){$map['zoom'] = "10";}
                elseif ($latDif < 0.7 and $lonDif < 0.7){$map['zoom'] = "9";}
                elseif ($latDif < 1.5 and $lonDif < 1.5){$map['zoom'] = "8";}
                elseif ($latDif < 2.3 and $lonDif < 2.3){$map['zoom'] = "7";}
                elseif ($latDif < 4 and $lonDif < 4){$map['zoom'] = "6";}
                elseif ($latDif < 5.5 and $lonDif < 5.5){$map['zoom'] = "5";}
                else{$map['zoom'] = "4";}

                $latMoy = ($latMax + $latMin) / 2;
                $lonMoy = ($lonMax + $lonMin) / 2;

                $map['center'] = $latMoy.",".$lonMoy;
                $map['info'] = $latDif.",".$lonDif;
            }
            
        } else {
            return null;
        }

        return $map;

    }
}