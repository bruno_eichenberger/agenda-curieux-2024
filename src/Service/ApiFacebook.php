<?php

namespace App\Service;

use Facebook\Facebook;
use App\Service\Website;
use App\Entity\TicketResource;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Exceptions\FacebookResponseException;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ApiFacebook extends AbstractController {
    
    public function __construct(Website $website, RequestStack $requestStack)
    {
        $this->website = $website->getArray();
        $this->session = $requestStack->getSession();
        $this->facebook = new Facebook([
            'app_id'              => '528367174008732',
            'app_secret'          => '06db823468a39aab62caddacd569cf63',
            'graph_api_version'   => 'v5.7',
        ]);
    }

    public function getObject()
    {
        return $this->facebook;
    }

    public function getLoginUrl()
    {
        $helper = $this->facebook->getRedirectLoginHelper();
        $permissions = ['email']; // optional 'user_likes'
        return $helper->getLoginUrl("https://".$this->website['address']['subdomain'].".curieux.net/".$this->generateUrl('log_fb'), $permissions);
    }

    public function getEventsFB()
    {
        //dd($this->website);
        //dd($this->session->get('facebook_access_token'));
        try {
            // Get your UserNode object, replace {access-token} with your token
            if ($_SERVER['SERVER_NAME'] == "localhost"){
                $token = "EAAHgjBG3v5wBAGnsMUAdsUgzCgukYsigKA5STdNdi8VqmEjZAow0LxBZBdCS8xCgeKqYhHFKRlPVbtkFuZBv9S1HxZALOIL7CIlotPjQ6pa2PCnEIt8U6OwlumoilZBWDFgLY54k6RiTrQfLQfFdA1nIePN6gTG0QtSnEbN28PZCc4Iur7hfK4QoowIDCmS5DMKy7x5X1mXbDrQ4xieuPM4B9JZCmFSbaQZD";
            } else {
                $token = $this->session->get('facebook_access_token');
            }

            $response = $this->facebook->get(
                'me/events?limit=5&fields=name,start_time,end_time,event_times,description,place,cover,ticket_uri,ticketing_terms_uri,timezone,type,is_online,interested_count,created_time,attending_count,updated_time,declined_count',
                $token
            );
        } catch(FacebookResponseException $e) {       
            // Returns Graph API errors when they occur
            echo "Facebook Graph retourne une erreur : " . $e->getMessage();
            exit;
        } catch(FacebookSDKException $e) {
            // Returns SDK errors when validation fails or other local issues
            echo "Facebook SDK retourne une erreur : " . $e->getMessage();
            exit;
        }
        
        //$me = $response->getGraphUser();
        $apifb = $response->getGraphEdge();

        $events = [];

        foreach($apifb as $data){

            /*
            $postcode = $data['place']['location']['zip'];
            $latitude = $data['place']['location']['latitude'];
            $longitude = $data['place']['location']['longitude'];
            $country = $data['place']['location']['country'];
            $city = $data['place']['location']['city'];
            */

            $event = new TicketResource;
            
            $event  ->setIdResource($data['id'])
                    ->setTitle($data['name'])
                    ->setDescription($data['description'])
                    //->setPlaceTemporary($data['place']['name'])
                    ->setDateStart($data['start_time'])
                    ->setTimeStart($data['start_time'])
                    ->setCoverImage($data['cover']['source'])
            ;

            if (isset($data['end_time'])){
                $event  ->setDateEnd($data['end_time'])
                        ->setTimeEnd($data['end_time'])
                ;
            } else {
                $event  ->setDateEnd($data['start_time']);
            }

            //dd($event);
            $events[] = $event;
        }

        return ['events' => $events, 'pagination' => $apifb->getMetaData()['paging']];

    }
}