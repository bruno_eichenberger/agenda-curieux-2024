<?php

namespace App\Service;

use Twig\Environment;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Templating\EngineInterface;
use Symfony\Component\Mailer\Bridge\Google\Transport\GmailSmtpTransport;

/**
 * Class Sender
 */
class Sender
{
    private $engine;
    private $mailer;

    public function __construct(Environment $engine, MailerInterface $mailer)
    {
        $this->engine = $engine;
        $this->mailer = $mailer;
    }

    public function sendMessage($from, $to, $subject, $body, $attachement = null)
    {
        $mail = (new Email())
        ->from($from)
        ->to($to)
        //->cc('cc@example.com')
        //->bcc('bcc@example.com')
        //->replyTo('fabien@example.com')
        //->priority(Email::PRIORITY_HIGH)
        ->subject($subject)
        ->html($body);

        //$transport = new GmailSmtpTransport('strascurieux@gmail.com', 'qyarzswcgrjykqbi');
        //$mailer = new Mailer($transport);
        //return $mailer->send($mail);

        return $this->mailer->send($mail);

    }

    public function createBodyMail($view, array $parameters)
    {
        return $this->engine->render($view, $parameters);
    }
}