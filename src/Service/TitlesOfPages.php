<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TitlesOfPages extends AbstractController {

    
    public function getTitlesOfRubric($route, $count, $rubric)
    {
        $rubricFemale = ['soiree','exposition','cinema','action-citoyenne'];
        $rubricApostrophe = ['exposition','action-citoyenne','autre'];
        
        if (in_array($rubric->getSlug(),$rubricFemale)){
            $female = true;
        } else {
            $female = null;
        }

        if ($female){
            $empty = "Aucune";
            $pronoun = [
                'maj' => "La ",
                'min' => "la "
            ];
            $archive = [
                'single' => "archivée",
                'plural' => "archivées",
            ];
        } else {
            $empty = "Aucun";
            $pronoun = [
                'maj' => "Le ",
                'min' => "le "
            ];
            $archive = [
                'single' => "archivé",
                'plural' => "archivés",
            ];
        }

        if (in_array($rubric->getSlug(),$rubricApostrophe)){
            $pronoun = [
                'maj'   => "L'",
                'min' => "l'"
            ];
        }

        if ($route == "events_by_rubric"){

            // Attribut 'head'
            $title['head'] = "Les ".$rubric->getPlural()." à venir";

            // Attribut 'today'
            if ($count['today'] == 1){
                $title['today'] = $pronoun['maj']."".$rubric->getSingle()." du jour";
            } elseif ($count['today'] > 1){
                $title['today'] = "Aujourd'hui, ".$count['today']." ".$rubric->getPlural();
            } else {
                $title['today'] = null;
            }

            // Attribut 'text'
            if ($count['after'] == 1){
                $title['text'] = $pronoun['maj']."".$rubric->getSingle()." à venir";
            } elseif ($count['after'] > 1) {
                $title['text'] = "Les ".$count['after']." ".$rubric->getPlural()." à venir";
            } else {
                $title['text'] = $empty." ".$rubric->getSingle()." à venir";
            }

            // Attribut 'url'
            $title['button']['url'] = $this->generateUrl('old_events_by_rubric', ['slug' => $rubric->getSlug()]);
            
            // Attribut 'button'
            if ($count['button'] == 1){
                $title['button']['title'] = "Voir ".$pronoun['min']."".$rubric->getSingle()." ".$archive['single'];
            } elseif ($count['button'] > 1){
                $title['button']['title'] = "Voir les ".$count['button']." ".$rubric->getPlural()." ".$archive['plural'];
            } else {
                $title['button'] = null;
            }
        }
        else {

            // Attribut 'head'
            $title['head'] = "Les ".$rubric->getPlural()." ".$archive['plural'];

            // Attribut 'text'
            if ($count['after'] == 1){
                $title['text'] = $pronoun['maj']."".$rubric->getSingle()." ".$archive['single'];
            } elseif ($count['after'] > 1){
                $title['text'] = "Les ".$count['after']." ".$rubric->getPlural()." ".$archive['plural'];
            } else {
                $title['text'] = $empty." ".$rubric->getSingle()." ".$archive['single'];
            }

            // Attribut 'url'
            $title['button']['url'] = $this->generateUrl('events_by_rubric', ['slug' => $rubric->getSlug()]);
            
            // Attribut 'button'
            if ($count['button'] == 1){
                $title['button']['title'] = "Voir ".$pronoun['min']."".$rubric->getSingle()." à venir";
            } elseif ($count['button'] > 1){
                $title['button']['title'] = "Voir les ".$count['button']." ".$rubric->getPlural()." à venir";
            } else {
                $title['button'] = null;
            }

        }

        return $title;
    }

    public function getTitlesSimple($route, $count, $entity)
    {
        //dd(get_class($entity));

        if (get_class($entity) == "App\Entity\EventSearch"){
            $text = "correspondant à votre recherche";
            if($entity->getTitre()){$parameters["titre"] = $entity->getTitre();}
            if($entity->getRubrique()){$parameters["rubrique"] = $entity->getRubrique()->getSlug();}
            if($entity->getLieu()){$parameters["lieu"] = $entity->getLieu()->getSlug();}
            if($entity->getVille()){$parameters["ville"] = $entity->getVille()->getSlug();}
            if($entity->getDate()){$parameters["date"] = $entity->getDate()->format("Y-m-d");}
            if($entity->getPrix()){$parameters["prix"] = $entity->getPrix();}
        }
        elseif (get_class($entity) == "App\Entity\Category"){
            $text = "dans la catégorie '".$entity->getName()."'";
            $parameters["slug"] = $entity->getSlug();
        } 
        elseif (get_class($entity) == "App\Entity\Reduction") {
            $text = "avec la réduction '".$entity->getTitle()."'";
            $parameters["slug"] = $entity->getSlug();
        }
        elseif (get_class($entity) == "App\Entity\User") {
            $text = "dans vos favoris";
            $parameters["slug"] = $entity->getSlug();
        }
        elseif (get_class($entity) == "App\Entity\Place") {
            if ($entity->getPronoun() == "La"){$pronoun = "à la ";}
            elseif ($entity->getPronoun() == "Les"){$pronoun = "aux ";}
            elseif ($entity->getPronoun() == "L'"){$pronoun = "à l'";}
            else {$pronoun = "au ";}
            $text = $pronoun ."". $entity->getName();
            $parameters["slug"] = $entity->getSlug();
        }
        
        if (strpos($route,"old_") === false){
            
            // Attribut 'head'
            $title['head'] = "Les évènements à venir ".$text;

            // Attribut 'today'
            if ($count['today'] == 1){
                $title['today'] = "Aujourd'hui, un évènement ".$text;
            } 
            elseif ($count['today'] > 1){
                $title['today'] = "Les ".$count['today']." évènements d'aujourd'hui ".$text;
            } 
            else {
                $title['today'] = null;
            }

            // Attribut 'text'
            if ($count['after'] == 1){
                $title['text'] = "L'évènement à venir ".$text;
            } 
            elseif ($count['after'] > 1) {
                $title['text'] = "Les ".$count['after']." évènements à venir ".$text;
            } 
            else {
                $title['text'] = "Aucun évènement à venir ".$text;
            }

            // Attribut 'url'
            $title['button']['url'] = $this->generateUrl('old_'.$route, $parameters);
            
            // Attribut 'button'
            if ($count['button'] == 1){
                $title['button']['title'] = "Voir l'évènement archivé ".$text;
            } 
            elseif ($count['button'] > 1){
                $title['button']['title'] = "Voir les ".$count['button']." évènements archivés ".$text;
            } 
            else {
                $title['button'] = null;
            }
        }
        else {

            // Attribut 'head'
            $title['head'] = "Les évènements passés ".$text;


            // Attribut 'text'
            if ($count['after'] == 1){
                $title['text'] = "L'évènement archivé ".$text;
            } 
            elseif ($count['after'] > 1) {
                $title['text'] = "Les ".$count['after']." évènements archivés ".$text;
            } 
            else {
                $title['text'] = "Aucun évènement archivé ".$text;
            }

            // Attribut 'url'
            $title['button']['url'] = $this->generateUrl(str_replace("old_","",$route), $parameters);
            
            // Attribut 'button'
            if ($count['button'] == 1){
                $title['button']['title'] = "Voir l'évènement à venir ".$text;
            } 
            elseif ($count['button'] > 1){
                $title['button']['title'] = "Voir les ".$count['button']." évènements à venir ".$text;
            } 
            else {
                $title['button'] = null;
            }

        }
        //dd($title);
        return $title;
    }


    /*
    public function getTitlesOfSearch($request, $count, $routeButton)
    {
        $titles = [
            "search_by_filter" => [
                "text"      => [
                    "single"    => "Un évènement trouvé",
                    "plural"    => "Les ".$count['after']." évènements trouvés"
                ], "button" => [
                    "single"    => "Voir l'évènement trouvé",
                    "plural"    => "Voir les ".$count['button']." évènements trouvés dans les archives",
                    "parameter" => $request->query->all()
                ]
            ],
            "old_search_by_filter" => [
                "text" => [
                    "single"    => "Une archive trouvée",
                    "plural"    => "Les ".$count['after']." archives trouvées"
                ], "button" => [
                    "single"    => "Voir l'évènement à venir trouvé",
                    "plural"    => "Voir les ".$count['button']." évènements à venir",
                    "parameter" => $request->query->all()
                ]
            ]
        ];

        return $this->condition($request, $count, $routeButton, $titles);
    }

    public function getTitlesOfReduction($request, $count, $routeButton, $reduction)
    {
        $text = "avec '".$reduction->getTitle()."'";

        $titles = [
            "events_by_reduction" => [
                "text"      => [
                    "single"    => "Un évènement à venir",
                    "plural"    => "Les ".$count['after']." évènements à venir ".$text
                ], "button" => [
                    "single"    => "Voir l'évènement archivé ".$text,
                    "plural"    => "Voir les archives des ".$count['button']." évènements passés ".$text,
                    "parameter" => ["slug" => $reduction->getSlug()]
                ]
            ], "old_events_by_reduction" => [
                "text"      => [
                    "single"    => "Un évènement archivé ".$text,
                    "plural"    => "Les ".$count['after']." évènements passés ".$text,
                ], "button" => [
                    "single"    => "Voir l'évènement à venir ".$text,
                    "plural"    => "Voir les ".$count['button']." évènements à venir ".$text,
                    "parameter" => ["slug" => $reduction->getSlug()]
                ]
            ]
        ];

        return $this->condition($request, $count, $routeButton, $titles);
    }

    public function getTitlesOfLike($request, $count, $routeButton)
    {
        $text = "que vous aimez";

        $titles = [
            "events_by_like" => [
                "text"      => [
                    "single"    => "Un évènement à venir",
                    "plural"    => "Les ".$count['after']." évènements à venir ".$text
                ], "button" => [
                    "single"    => "Voir l'évènement archivé ".$text,
                    "plural"    => "Voir les archives des ".$count['button']." évènements passés ".$text,
                    "parameter" => null
                ]
            ], "old_events_by_like" => [
                "text"      => [
                    "single"    => "Un évènement archivé ".$text,
                    "plural"    => "Les ".$count['after']." évènements passés ".$text,
                ], "button" => [
                    "single"    => "Voir l'évènement à venir ".$text,
                    "plural"    => "Voir les ".$count['button']." évènements à venir ".$text,
                    "parameter" => null
                ]
            ]
        ];

        return $this->condition($request, $count, $routeButton, $titles);
    }
    */
    
    public function getTitlesOfEventsByPlace($request, $count, $routeButton, $place)
    {
        if ($place->getPronoun() == "La"){$pronoun = "à la ";}
        elseif ($place->getPronoun() == "Les"){$pronoun = "aux ";}
        elseif ($place->getPronoun() == "L'"){$pronoun = "à l'";}
        else {$pronoun = "au ";}
        $text = $pronoun ."". $place->getName();

        $titles = [
            "place_show" => [
                "text"      => [
                    "single"    => "Un évènement programmé",
                    "plural"    => "Les ". $count['today'] ." évènements programmés ".$text
                ], "button" => [
                    "single"    => "Voir l'évènement archivé ".$text,
                    "plural"    => "Voir les archives des ".$count['button']." évènements passés ".$text,
                    "parameter" => ["slug" => $place->getSlug()]
                ]
            ], "old_place_show" => [
                "text"      => [
                    "single"    => "Un évènement archivé ".$text,
                    "plural"    => "Les ".$count['after']." évènements passés ".$text
                ], "button" => [
                    "single"    => "Revenir à l'évènement programmé ".$text,
                    "plural"    => "Revenir aux ".$count['button']." évènements programmés ".$text,
                    "parameter" => ["slug" => $place->getSlug()]
                ]
            ]
        ];

        return $this->condition($request, $count, $routeButton, $titles);
    }

    public function getTitlesOfPlaceSearch($request, $count, $routeButton)
    {

        $textProgram = "avec des évènements programmés";
        $textArchive = "avec des évènements archivés";

        $titles = [
            "place_index" => [
                "text"      => [
                    "single"    => "Un lieu trouvé ".$textProgram,
                    "plural"    => "Les ".$count['after']." lieux ".$textProgram,
                ], "button" => [
                    "single"    => "Voir le lieu ".$textArchive,
                    "plural"    => "Voir les ".$count['button']." lieux ".$textArchive,
                    "parameter" => $request->query->all()
                ]
            ],
            "old_place_index" => [
                "text" => [
                    "single"    => "Le lieu ".$textArchive,
                    "plural"    => "Les ".$count['after']." lieux ".$textArchive,
                ], "button" => [
                    "single"    => "Voir le lieu trouvé ".$textProgram,
                    "plural"    => "Voir les ".$count['button']." lieux ".$textProgram,
                    "parameter" => $request->query->all()
                ]
            ]
        ];

        return $this->condition($request, $count, $routeButton, $titles);
    }

    public function getTitlesOfPlacekind($request, $count, $routeButton, $entity)
    {       
        $textProgram = "avec des évènements programmés";
        $textArchive = "avec des évènements archivés";

        $titles = [
            "search_by_placekind" => [
                "text"      => [
                    "single"    => "Un lieu ".$textProgram,
                    "plural"    => "Les ".$count['after']." ".$entity->getPlural()." ".$textProgram,
                ], "button" => [
                    "single"    => "Voir le ".$entity->getPlural()." ".$textArchive,
                    "plural"    => "Voir les archives des ".$count['button']." ".$entity->getPlural()." ".$textArchive,
                    "parameter" => ["slug" => $entity->getSlug()]
                ]
            ],
            "old_search_by_placekind" => [
                "text"      => [
                    "single"    => "Un lieu ".$textArchive,
                    "plural"    => "Les ".$count['after']." ".$entity->getPlural()." ".$textArchive,
                ], "button" => [
                    "single"    => "Voir le lieu ".$textProgram,
                    "plural"    => "Voir les ".$count['button']." ".$entity->getPlural()." ".$textProgram,
                    "parameter" => ["slug" => $entity->getSlug()]
                ]
            ]

        ];

        return $this->condition($request, $count, $routeButton, $titles);
    }

    public function getTitlesOfPlacetag($request, $count, $routeButton, $entity)
    {
        $titles = [
            "search_by_placetag" => [
                "text"      => [
                    "single"    => "Un lieu avec des évènements programmés",
                    "plural"    => "Les ".$count['after']." lieux '".$entity->getName()."' avec des évènements programmés"
                ], "button" => [
                    "single"    => "Voir le lieu avec des évènements archivés",
                    "plural"    => "Voir les archives des ".$count['button']." lieux '".$entity->getName()."' avec des évènements passés",
                    "parameter" => ["slug" => $entity->getSlug()]
                ]
            ],
            "old_search_by_placetag" => [
                "text"      => [
                    "single"    => "Un lieu avec des évènements archivés",
                    "plural"    => "Les ".$count['after']." lieux avec des évènements archivés"
                ], "button" => [
                    "single"    => "Voir le lieu avec des évènements programmés",
                    "plural"    => "Voir les ".$count['button']." lieux avec des évènements programmés",
                    "parameter" => ["slug" => $entity->getSlug()]
                ]
            ]

        ];

        return $this->condition($request, $count, $routeButton, $titles);
    }

    public function placesByCity($request, $count, $routeButton, $entity)
    {
        //dd($entity);
        $titles = [
            "search_by_city" => [
                "text"      => [
                    "single"    => "Un lieu avec des évènements programmés",
                    "plural"    => "Les ".$count['after']." lieux à ".$entity->getName()." avec des évènements programmés"
                ], "button" => [
                    "single"    => "Voir le lieu à ".$entity->getName()." avec des évènements archivés",
                    "plural"    => "Voir les archives des ".$count['button']." lieux à ".$entity->getName()." avec des évènements passés",
                    "parameter" => ["slug" => $entity->getSlug()]
                ]
            ],
            "old_search_by_city" => [
                "text"      => [
                    "single"    => "Un lieu avec des évènements archivés",
                    "plural"    => "Les ".$count['after']." lieux à ".$entity->getName()." avec des évènements archivés"
                ], "button" => [
                    "single"    => "Voir le lieu à ".$entity->getName()." avec des évènements programmés",
                    "plural"    => "Voir les ".$count['button']." lieux à ".$entity->getName()." avec des évènements programmés",
                    "parameter" => ["slug" => $entity->getSlug()]
                ]
            ]

        ];

        return $this->condition($request, $count, $routeButton, $titles);
    }

    private function condition($request, $count, $routeButton, $titles){

        if (array_key_exists($request->attributes->get('_route'),$titles)){

            $tab = $titles[$request->attributes->get('_route')];
            
            if (isset($count['today']) and $count['today'] == 1 and $count['after'] == 0){
                $title['primary'] = "Aujourd'hui, ".$tab["text"]["single"];
            } elseif ($count['after'] == 1 ){
                $title['primary'] = $tab["text"]["single"];
            } else {
                $title['primary'] = $tab["text"]["plural"];
            }

            if ($count['button'] > 0){
                if ($count['button'] == 1){
                    $title['button'] = $tab["button"]["single"];
                } else {
                    $title['button'] = $tab["button"]["plural"];
                }
                if ($tab["button"]["parameter"]){
                    $title['route'] = $this->generateUrl($routeButton, $tab["button"]["parameter"]);
                } else {
                    $title['route'] = $this->generateUrl($routeButton);
                }
            }

        };

        return $title;

    }

}
