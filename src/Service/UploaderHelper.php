<?php

namespace App\Service;

//use App\Service\UploaderHelper;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploaderHelper
{   
    const EVENT_IMAGE = 'e';
    const PLACE_IMAGE = 'p';
    const FESTIVAL_IMAGE = 'f';
    const USER_IMAGE = 'u';
    const TICKET_DATA = 't';

    private $uploadsPath;

    public function __construct (string $uploadsPath) 
    { 
        $this->uploadsPath = $uploadsPath; 
    } 


    public function uploadEventImage (UploadedFile $uploadedFile, $slug) : string
    {
        //$destination = $this->getParameter( 'kernel.project_dir' ). '/public/uploads/image' ;
        $destination = $this->uploadsPath. '/'.self::EVENT_IMAGE. '/'.date("Y").'/'.date("m");

        $originalFilename = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);
        //$newFilename = Urlizer::urlize($originalFilename).'-'.uniqid().'.'.$uploadedFile->guessExtension();
        $newFilename = date("Y").'/'.date("m").'/'.uniqid(). '-' .$slug. '.' .$uploadedFile->guessExtension();

        $uploadedFile->move(
            $destination,
            $newFilename
        );

        return $newFilename;
    }
    
    public function uploadPlaceImage (UploadedFile $uploadedFile) : string
    {
        //$destination = $this->getParameter( 'kernel.project_dir' ). '/public/uploads/image' ;
        $destination = $this ->uploadsPath. '/'.self::PLACE_IMAGE. '/'.date("Y").'/'.date("m");


        $originalFilename = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);
        //$newFilename = Urlizer::urlize($originalFilename).'-'.uniqid().'.'.$uploadedFile->guessExtension();
        $newFilename = date("Y").'/'.date("m").'/'. uniqid().'.'.$uploadedFile->guessExtension();

        $uploadedFile->move(
            $destination,
            $newFilename
        );

        return $newFilename;
    }

    public function uploadFestivalImage (UploadedFile $uploadedFile) : string
    {
        //$destination = $this->getParameter( 'kernel.project_dir' ). '/public/uploads/image' ;
        //$destination = $this ->uploadsPath. '/'.self::FESTIVAL_IMAGE;
        $destination = $this ->uploadsPath. '/'.self::FESTIVAL_IMAGE. '/'.date("Y").'/'.date("m");


        $originalFilename = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);
        //$newFilename = Urlizer::urlize($originalFilename).'-'.uniqid().'.'.$uploadedFile->guessExtension();
        $newFilename = date("Y").'/'.date("m").'/'. uniqid().'.'.$uploadedFile->guessExtension();
        //$newFilename = 'festival'.uniqid().'.'.$uploadedFile->guessExtension();

        $uploadedFile->move(
            $destination,
            $newFilename
        );

        return $newFilename;
    }    
    
    public function uploadUserImage (UploadedFile $uploadedFile) : string
    {
        //$destination = $this->getParameter( 'kernel.project_dir' ). '/public/uploads/image' ;
        $destination = $this ->uploadsPath. '/'.self::USER_IMAGE;


        $originalFilename = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);
        //$newFilename = Urlizer::urlize($originalFilename).'-'.uniqid().'.'.$uploadedFile->guessExtension();
        $newFilename = 'avatar'.uniqid().'.'.$uploadedFile->guessExtension();

        $uploadedFile->move(
            $destination,
            $newFilename
        );

        return $newFilename;
    }

    public function uploadTicketData (UploadedFile $uploadedFile) : string
    {
        //$destination = $this->getParameter( 'kernel.project_dir' ). '/public/uploads/image' ;
        $destination = $this->uploadsPath. '/'.self::TICKET_DATA;


        //$originalFilename = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);
        //$newFilename = Urlizer::urlize($originalFilename).'-'.uniqid().'.'.$uploadedFile->guessExtension();
        $newFilename = 'flux.xml';

        $uploadedFile->move(
            $destination,
            $newFilename
        );

        return $newFilename;
    }

    /* Suite du tutoriel : https://symfonycasts.com/screencast/symfony-uploads/public-path
    public function getPublicPath (string $path) : string 
    { 
        return 'uploads/' .$path; 
    }
    */

}