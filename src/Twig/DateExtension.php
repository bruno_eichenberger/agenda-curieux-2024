<?php

namespace App\Twig;

use DateTime;
use Twig\TwigFilter;
use Twig\TwigFunction;
use App\Entity\TicketResource;
use Twig\Extension\AbstractExtension;

class DateExtension extends AbstractExtension
{
    /**
    * @return array
    */
    public function getFilters()
    {
        return [new TwigFilter('dateText', [$this, 'dateText'],['is_safe' => ['html']])];
    }
    
    public function dateText($date)
    {
        return $this->week()[$date->format("w")]." ".$date->format('j')." ".$this->month()[$date->format('n')]." ".$date->format('Y');
    }
    
    public function getFunctions(): array
    {
        return [
            new TwigFunction('datePrimary', [$this, 'primary']),
            new TwigFunction('showDateMultiple', [$this, 'secondary']),
        ];
    }

    public function primary($event)
    {
        $timePrecision = null;
        $textDetail = null;
        $textPrimary = null;
        $textSecondary = null;

        if ($event->getTimeStart() and $event->getTimeEnd()){
            $timePrecision = " de ";
            if ($event->getTimeStart()->format('i') == '00'){
                $timePrecision .= $event->getTimeStart()->format('H\\h');
            } else {
                $timePrecision .= $event->getTimeStart()->format('H\\hi');
            }
            $timePrecision .= " à ";
            if ($event->getTimeEnd()->format('i') == '00'){
                $timePrecision .= $event->getTimeEnd()->format('H\\h');
            } else {
                $timePrecision .= $event->getTimeEnd()->format('H\\hi');
            }
        } 
        elseif ($event->getTimeStart()){
            $timePrecision = " à partir de ";
            if (
                $event->getTimeStart()->format('H') == '00' and
                $event->getTimeStart()->format('i') == '00'
            ){
                $timePrecision .= "minuit";
            } elseif ($event->getTimeStart()->format('i') == '00'){
                $timePrecision .= $event->getTimeStart()->format('H\\h');
            } else {
                $timePrecision .= $event->getTimeStart()->format('H\\hi');
            }
        } 
        elseif ($event->getTimeEnd()){
            $timePrecision = " jusqu'à ";
            if (
                $event->getTimeEnd()->format('H') == '00' and
                $event->getTimeEnd()->format('i') == '00'
            ){
                $timePrecision .= "minuit";
            } elseif ($event->getTimeEnd()->format('i') == '00'){
                $timePrecision .= $event->getTimeEnd()->format('H\\h');
            } else {
                $timePrecision .= $event->getTimeEnd()->format('H\\hi');
            }
        }

        // Sur une journée
        if ($event->getDateStart() == $event->getDateEnd()){
            if ($event->getDateStart() == new DateTime('today')){
                $textPrimary = "Aujourd'hui le ";
            } elseif ($event->getDateStart() == new DateTime('tomorrow')) {
                $textPrimary = "Demain, le ";
            } else {
                $textPrimary = "Le ";
            }
            
            $textPrimary .= $this->week()[$event->getDateStart()->format('w')] ." ";
            $textPrimary .= $event->getDateStart()->format('j') ." ";
            $textPrimary .= $this->month()[$event->getDateStart()->format('n')] ." ";
            $textPrimary .= $event->getDateStart()->format('Y');

        }
        // Sur plusieurs jours
        else {

            // Si ticket
            $ticketResource = new TicketResource;
            if ($event instanceof $ticketResource) {
                $textPrimary = "Du ";
                if ($event->getDateStart()->format('Y') != $event->getDateEnd()->format('Y')){
                    $textPrimary .= $this->makeDateText($event->getDateStart());
                } 
                elseif ($event->getDateStart()->format('n') != $event->getDateEnd()->format('n')){
                    $textPrimary .= $this->makeDateText($event->getDateStart(),"year");
                } 
                else {
                    $textPrimary .= $this->makeDateText($event->getDateStart(),"month");
                }
                $textPrimary .= "au ";
                $textPrimary .= $this->makeDateText($event->getDateEnd());

            } 
            
            // Sinon Event 
            else {

                // Si évènement est passé - Date de fin inférieur à aujourd'hui
                if ($event->getDateEnd() < new DateTime('today')){
                    $textDetail = "Terminé";
                }
                // Si évènement est à venir - Date de début supérieur à aujourd'hui
                elseif ($event->getDateStart() > new DateTime('today')){
                    // Si commence demain
                    if ($event->getDateStart() == new DateTime('tomorrow'))
                    {
                        $textDetail = "Commence demain";
                    }
                }
                // Du coup l'évènement est en cours
                else {
                    // Si commence aujourdhui
                    if ($event->getDateStart() == new DateTime('today'))
                    {
                        $textDetail = "Commence aujourd'hui";
                        if ($event->getDateEnd() == new DateTime('tomorrow')){
                            $textDetail .= " et termine demain";
                        }
                    }
                    // Si termine aujourdhui
                    elseif ($event->getDateEnd() == new DateTime('today'))
                    {
                        $textDetail = "Termine aujourd'hui";
                        /*
                        if ($event->getDateStart() == new DateTime('yesterday')){
                            $textDetail .= "et commencé hier";
                        }
                        */
                    }
                    // Si termine demain
                    elseif ($event->getDateEnd() == new DateTime('tomorrow'))
                    {
                        $textDetail = "Termine demain";
                        /*
                        if ($event->getDateStart() == new DateTime('yesterday')){
                            $textDetail .= "et commencé hier";
                        }
                        */
                    }
                    // Si ouvert tous les jours
                    elseif ($event->getPrecisionDate() == 'ALL_DAYS'){
                        $textDetail = "Ouvert tous les jours";
                    }
                    // Si dates régulières
                    elseif ($event->getPrecisionDate() == 'DAYS_OPENING'){
                        
                        $days = $event->getChoiceOfDays();
                        $tabDays[1] = in_array('Monday', $days);
                        $tabDays[2] = in_array('Tuesday', $days);
                        $tabDays[3] = in_array('Wednesday', $days);
                        $tabDays[4] = in_array('Thursday', $days);
                        $tabDays[5] = in_array('Friday', $days);
                        $tabDays[6] = in_array('Saturday', $days);
                        $tabDays[7] = in_array('Sunday', $days);

                        $i = 0;
                        $period = [];
                        $lastValue = false;
                        foreach($tabDays as $key => $value){
                            if ($lastValue and $lastValue != $value){$i++;}
                            if ($value){$period[$i][] = $key;}
                            $lastValue = $value;
                        }

                        if (count($period) == 1){
                            if (count($period[0]) == 1){
                                $textDetail = "Tous les ". $this->week()[$period[0][0]] ."s";
                            }
                            elseif (count($period[0]) == 2){
                                $textDetail = "Les ". $this->week()[$period[0][0]] ."s et ". $this->week()[$period[0][1]] ."s";
                            }
                            else {
                                $textDetail = "Du ". $this->week()[$period[0][0]] ." au ". $this->week()[end($period[0])];
                            }
                        }
                        elseif (count($period) >= 2){

                            foreach($tabDays as $day => $bol){
                                if ($bol){
                                    $daysByOrder[] = $day;
                                }
                            }

                            $textDetail = "Tous les";
                            $k = 1;
                            $count = count($daysByOrder);

                            foreach($daysByOrder as $day){
                                if ($count == $k) {$textDetail .= " et";}
                                elseif ($k != 1 and $count != $k ){$textDetail .= ",";}
                                
                                if ($day == 1){$textDetail .= " lundis";}
                                elseif ($day == 2){$textDetail .= " mardis";}
                                elseif ($day == 3){$textDetail .= " mercredis";}
                                elseif ($day == 4){$textDetail .= " jeudis";}
                                elseif ($day == 5){$textDetail .= " vendredis";}
                                elseif ($day == 6){$textDetail .= " samedis";}
                                elseif ($day == 7){$textDetail .= " dimanches";}
                                $k++;
                            }
                        }
                    }
                }
                
                /*
                if (
                    $event->getDateStart()->format('Y-m-d') <= date('Y-m-d') and 
                    $event->getDateEnd()->format('Y-m-d') >= date('Y-m-d')
                ){
                    $isActually = true;
                } else {
                    $isActually = false;
                }
                */

                //$isActually ? $textPrimary = "Depuis le ": $textPrimary = "Du ";

                $textPrimary = "Du ";
                
                // Si année différente, affiche date complete
                if ($event->getDateStart()->format('Y') != $event->getDateEnd()->format('Y')){
                    $textPrimary .= $this->makeDateText($event->getDateStart());
                } 
                // Si le mois est différent, caché l'année et affiche le mois
                elseif ($event->getDateStart()->format('n') != $event->getDateEnd()->format('n')){
                    $textPrimary .= $this->makeDateText($event->getDateStart(),"year");
                } 
                // Du coup c'est le même mois de la même année, cache mois et année
                else {
                    $textPrimary .= $this->makeDateText($event->getDateStart(),"month");
                }
                $textPrimary .= " au ".$this->makeDateText($event->getDateEnd());
            }

        }

        $text = null;

        if ($textPrimary){
            $text .= "<div class='d-inline-block mr-2'>".$textPrimary."</div>";
        }
        if ($textSecondary){
            $text .= "<div class='d-inline-block mr-2'>".$textSecondary."</div>";
        }
        if ($timePrecision and empty($textDetail)){
            $text .= "<div class='d-inline-block mr-2'>".$timePrecision."</div>";
        }
        if ($textDetail)
        {
            $text .= "<div class='d-inline-block mr-2'>- ";
            $text .= $textDetail;
            if ($timePrecision){
                $text .= $timePrecision;
            }
            $text .= "</div>";
        }

        return $text;
    }

    public function makeDateText($date,$remove=null)
    {
        $text = $this->week()[$date->format("w")]." ";
        $text .= $date->format('j')." ";

        if ($remove != "month"){
            $text .= $this->month()[$date->format('n')]." ";
        }

        if ($remove != "year" and $remove != "month"){
            $text .= $date->format('Y');
        } 

        return $text;
    }

    public function arrayDateMultiple($event)
    {
        $dateMultiple = explode(", ",$event->getDateMultiple());

        //Changer le format
        foreach ($dateMultiple as $day){
            $tabFullDates[] = DateTime::createFromFormat('d/m/Y', $day)->format('Y-m-d'); 
        }

        // Ajouter date de début et de fin
        $tabFullDates[] = $event->getDateStart()->format('Y-m-d');
        $tabFullDates[] = $event->getDateEnd()->format('Y-m-d');

        // trier
        sort($tabFullDates);
        
        return $tabFullDates;
    }

    

    public function secondary($event)
    {
        /*
        // Si l'evenement est en cours
        if (
            $event->getDateStart()->format('Y-m-d') <= date('Y-m-d') and 
            $event->getDateEnd()->format('Y-m-d') >= date('Y-m-d')
        ){
            
            if ($event->getDateStart() == new DateTime('today')){
                $text = "Commence aujourd'hui";
            } else {
                $text = "Commencé le ";
                $text .= $this->week()[$event->getDateStart()->format("w")]." ";
                $text .= $event->getDateStart()->format('j')." ";
                $text .= $this->month()[$event->getDateStart()->format('n')]." ";
                $text .= $event->getDateStart()->format('Y');
            }

            return $text;
        }
        */

        // Si dates périodiques
        //if ($event->getPrecisionDate() == 'DATES_OPENING' and $event->getDateMultiple()){

            foreach($this->arrayDateMultiple($event) as $d){
                $day = new DateTime($d);

                if ($day->format('Y-m-d') >= date('Y-m-d')){
                    $textDate = $this->week()[$day->format('w')] ." ";
                    $textDate .= $day->format('j') ." ";
                    $textDate .= $this->month()[$day->format('n')] ." ";
                    $textDate .= $day->format('Y');
                    /*
                    if ($textPrimary == []){
                        dd($day->format('Y-m-d'));
                        if ($day->format('Y-m-d') == date('Y-m-d')){
                            $text = "À partir d'aujourd'hui, le ". $textDate;
                        } else {
                            $text = "Prochaine date le ". $textDate;
                        }
                    }
                    */
                    $listOfDate[] = "<li>". ucfirst($textDate) ."</li>";
                } 
            }
            //$text['primary'] .= $timePrecision;
            return "<p class='mt-5'><u>Les ". count($listOfDate) ." dates à venir :</u><ul>". implode($listOfDate) ."</ul></p>";
        
        //}

        //return null;

    }

    public function week(): array
    {
        return ["dimanche","lundi","mardi","mercredi","jeudi","vendredi","samedi","dimanche"];
    }

    public function month(): array
    {
        return ["décembre","janvier","février","mars","avril","mai","juin","juillet","août","septembre","octobre","novembre","décembre"];
    }
}
