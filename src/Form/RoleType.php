<?php

namespace App\Form;

use App\Entity\Role;
use App\Entity\User;
use App\Entity\Place;
use App\Entity\Artist;
use App\Entity\Organizer;
use App\Entity\Subdomain;
use Symfony\Component\Form\AbstractType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class RoleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            /*
            ->add(
                'userRoles',
                Select2EntityType::class, [
                    'label' => "Les rôles utilisateurs",
                    'placeholder' => 'Les droits administrateurs',
                    'remote_route' => 'tetranz_subdomain_search',
                    'multiple' => true,
                    'language' => 'fr',
                    'minimum_input_length' => 1,
                    'class' => Role::class,
                    'primary_key' => 'id',
                    'text_property' => 'title',
                ]
            )
            */
            ->add(
                'isAdministrator',
                CheckboxType::class, [
                    'mapped'    => false,
                    'required'  => false,
                    'label'     => false
                ]
            )
            ->add(
                'subdomains',
                Select2EntityType::class, [
                    'label' => "Les sites associés",
                    'placeholder' => 'Indiquer le site associé',
                    'remote_route' => 'tetranz_subdomain_search',
                    'multiple' => true,
                    'language' => 'fr',
                    'minimum_input_length' => 1,
                    'class' => Subdomain::class,
                    'primary_key' => 'id',
                    'text_property' => 'name'
                ]
            )

            ->add(
                'submit',
                SubmitType::class, [
                    'label' => "Modifier",
                    'attr' => [
                        'class' =>"btn btn-primary btn-block mt-5"
                    ]
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
