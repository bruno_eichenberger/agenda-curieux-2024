<?php

namespace App\Form;

use App\Entity\User;
use App\Form\ApplicationType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class PasswordForgotType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /*
        $builder
            ->add(
                'newPassword', 
                PasswordType::class, 
                $this->getConfiguration("Nouveau mot de passe","Votre nouveau mot de passe")
            )
            ->add(
                'confirmPassword', 
                PasswordType::class, 
                $this->getConfiguration("Confirmation mot de passe","Confirmez votre nouveau mot de passe")
            )
        ;
        */

        $builder
            ->add(
                'hash', 
                RepeatedType::class, [
                    'type' => PasswordType::class,
                    'first_options' => [
                        'label' => 'Nouveau mot de passe',
                        'attr' => [
                            'placeholder' => "Votre nouveau mot de passe"
                        ]
                    ],
                    'second_options' => [
                        'label' => 'Confirmer le mot de passe',
                        'attr' => [
                            'placeholder' => "Confirmation du nouveau mot de passe"
                        ]
                    ],
                    'invalid_message' => 'Les 2 mots de passe ne sont pas identiques.',
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
            'data_class' => User::class
        ]);
    }
}
