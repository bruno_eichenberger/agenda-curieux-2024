<?php

namespace App\Form;

use App\Entity\City;
use App\Entity\Event;
use App\Entity\Place;
use App\Entity\EventRedirect;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class EventRedirectType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder

            ->add(
                'searchTitle', 
                Select2EntityType::class, [
                    'placeholder' => "Chercher par titre",
                    'remote_route' => 'event_autocomplete',
                    'class' => Event::class,
                    'language' => 'fr',
                    'minimum_input_length' => 3,
                    'primary_key' => 'slug',
                    'text_property' => 'title',
                    'cache' => false,
                    'page_limit' => 10,
                    'delay' => 1000,
                    /*
                    'allow_add' => [
                        'enabled' => true,
                        'new_tag_text' => " (Rechercher)",
                        'tag_separators' => '[]'
                    ],
                    */
                    //'cache_timeout' => 600000, // if 'cache' is true
                ]
            )
                
            ->add(
                'searchPlace', 
                Select2EntityType::class, [
                    'placeholder' => 'Chercher par lieu',
                    'remote_route' => 'place_by_slug_search',
                    'class' => Place::class,
                    'language' => 'fr',
                    'minimum_input_length' => 3,
                    'primary_key' => 'slug',
                    'text_property' => 'title',
                    'cache' => false,
                    'page_limit' => 10,
                    'delay' => 1000,
                    //'cache' => true,
                    //'cache_timeout' => 600000, // if 'cache' is true
                    /*
                    'allow_add' => [
                        'enabled' => true,
                        'new_tag_text' => " (Rechercher)",
                        'tag_separators' => '[]'
                    ],
                    */

                ]
            )

            ->add(
                'searchCity', 
                Select2EntityType::class, [
                    'remote_route' => 'city_by_slug_search',
                    'primary_key' => 'slug',
                    'text_property' => 'name',
                    'language' => 'fr',
                    'minimum_input_length' => 3,
                    'placeholder' => 'Chercher par ville',
                    'class' => City::class,
                    'page_limit' => 10,
                    'delay' => 500,
                    //'cache' => true,
                    //'cache_timeout' => 600000, // if 'cache' is true
                ]
            )

            ->add('searchDate', DateType::class, [
                'label' => 'Date',
                'widget' => 'single_text',
                'required' => false
            ])   

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => EventRedirect::class,
            'method' => 'get',
            'csrf_protection' => false
        ]);
    }
    
    /**
    * @return bool
    */
    public function getBlockPrefix()
    {
        return '';
    }
}
