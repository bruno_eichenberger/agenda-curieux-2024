<?php

namespace App\Form;

use App\Entity\Image;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\Image as Constraint;

class ImageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $imageConstraints = [
            new Constraint([
                'maxSize' => '5M'
            ])
        ];
        
        $builder
            /*
            ->add('url', UrlType::class, [
                'attr' => [
                    'placeholder' => "URL de l'image"
                ]
            ])
            */
            ->add(
                'imageFile',
                FileType::class, [
                    'mapped' => false,
                    'required' => false,
                    'attr' => [
                        'placeholder' => "Ajouter un image",
                        'class' => "input-file-hidden" ,
                        'accept' => "image/*"
                    ],
                    'constraints' => $imageConstraints
                    /*
                    'constraints' => [
                        new CoverImage()
                    ]
                    'constraints' => $imageConstraints
                    */
                ]
            )
            ->add('caption', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => "Ajouter un texte"
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Image::class,
        ]);
    }
}
