<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Place;
use App\Entity\Artist;
use App\Entity\Organizer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class AccountAffirmerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'placeAffirmers',
                Select2EntityType::class, [
                    'label' => "Responsable d'un lieu",
                    'placeholder' => 'Préciser un ou plusieurs lieux',
                    'remote_route' => 'tetranz_place_search',
                    'multiple' => true,
                    'language' => 'fr',
                    'minimum_input_length' => 2,
                    'class' => Place::class,
                    'primary_key' => 'id',
                    'text_property' => 'title',
                    /*'attr' => [
                        'data-maximum-selection-length' => 4,
                        ''
                    ],
                    
                    'allow_add' => [
                        'enabled' => true,
                        'new_tag_text' => ' (NOUVEAU)',
                        'new_tag_prefix' => '__',
                        'tag_separators' => '[",", ""]'
                    ]
                    */
                ]
            )
            ->add(
                'organizerAffirmers',
                Select2EntityType::class, [
                    'label' => "Membre d'une organisation",
                    'placeholder' => 'Préciser un ou plusieurs organisateurs',
                    'remote_route' => 'tetranz_organizer_search',
                    'multiple' => true,
                    'language' => 'fr',
                    'minimum_input_length' => 1,
                    'class' => Organizer::class,
                    'primary_key' => 'id',
                    'text_property' => 'name',
                    'allow_add' => [
                        'enabled' => true,
                        'new_tag_text' => ' (NOUVEAU)',
                        'new_tag_prefix' => '__',
                        'tag_separators' => '[",", ""]'
                    ]
                ]
            )
            ->add(
                'artistAffirmers',
                Select2EntityType::class, [
                    'label' => "Associé à une artiste",
                    'placeholder' => "Nom d'un artiste",
                    'remote_route' => 'tetranz_artist_search',
                    'multiple' => true,
                    'language' => 'fr',
                    'minimum_input_length' => 1,
                    'class' => Artist::class,
                    'primary_key' => 'id',
                    'text_property' => 'name',
                    'allow_add' => [
                        'enabled' => true,
                        'new_tag_text' => ' (NOUVEAU)',
                        'new_tag_prefix' => '__',
                        'tag_separators' => '[",", ""]'
                    ]
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
