<?php

namespace App\Form;

use App\Entity\City;
use App\Entity\Rubric;
use App\Entity\FestivalSearch;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class FestivalSearchType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'placeholder' => 'Titre',
                ]
            ])   

            ->add(
                'rubrique', 
                EntityType::class, [
                    'required' => false,
                    'class' => Rubric::class,
                    'label' => false,
                    'choice_label' => 'name',
                    'choice_value' => 'slug',
                    'placeholder' => 'Choisir un type...'
                ]
            ) 

            ->add(
                'ville', 
                Select2EntityType::class, [
                    'remote_route' => 'tetranz_city_search',
                    'language' => 'fr',
                    'minimum_input_length' => 3,
                    'placeholder' => 'Choisir une ville',
                    'class' => City::class,
                    'label' => false,
                    'primary_key' => 'slug',
                    'text_property' => 'name'
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => FestivalSearch::class,
            'method' => 'get',
            'csrf_protection' => false
        ]);
    }

    /**
    * @return bool
    */
    public function getBlockPrefix()
    {
        return '';
    }
}
