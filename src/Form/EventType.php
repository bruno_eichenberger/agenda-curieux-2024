<?php

namespace App\Form;

use App\Entity\City;
use App\Entity\Room;
use App\Entity\Event;
use App\Entity\Place;
use App\Entity\Artist;
use App\Entity\Rubric;
use App\Form\ImageType;
use App\Form\PriceType;
use App\Form\VideoType;
use App\Entity\Category;
use App\Entity\Festival;
use App\Form\TicketType;
use App\Entity\Organizer;
use App\Form\ApplicationType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Security\Core\Security;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class EventType extends ApplicationType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {


        $imageConstraints = [
            new Image([
                'maxSize' => '2M'
            ])
        ];

        /*
        if (!$isEdit || !$event->getCoverImage()) {
            $imageConstraints[] = new NotNull([
                'message' => 'Veuillez télecharger une image valide',
            ]);
        }
        */

        $event = $options['data'];

        if ($event->getIsCanceled()){
            $builder
                ->add('isPostponed', CheckboxType::class, [
                    'label'     => "L'évènement est reporté",
                    'required'  => false
                ])
                ->add('datePostponed', DateType::class,[
                    'label'     => "Préciser la date :",
                    'required'  => false,
                    'mapped'    => false,
                    'widget'    => 'single_text',
                    'html5'     => false,
                    'required'  => false,
                    'format'    => 'dd/MM/yyyy',
                    'attr'      => ['placeholder' => 'JJ/MM/AAAA']
                ])
            ;
        }

        if (
            $event->getIsPublished() and
            $event->getIsValidated() and
            $event->getIsCanceled() !== true and
            $event->getIsFull() !== true
        ){
            $tabChoices = [
                "Signaler que l'évènement est complet ou annulé !" => null,
                "Complet"   => 'full',
                "Annulé"    => 'canceled',
                "Reporté"   => 'postponed'
            ];

            if (
                $this->security->getUser() and
                in_array('ROLE_ADMIN',$this->security->getUser()->getRoles())
            ){
                $tabChoicesOption = [
                    "Doublon (Cacher l'évènement)"  => 'duplicated',
                    "Erreur (Cacher l'évènement)"   => 'mistake',
                    "Spam (Cacher l'évènement)"     => 'spam'
                ];
                $tabChoices = array_merge($tabChoices, $tabChoicesOption);
            }

            $builder
                ->add('validation', ChoiceType::class, [
                    'mapped'    => false,
                    'choices'   => $tabChoices
                ])
            ;
        }

        if ($event->getAuthor() === null and $this->security->getUser() === null){
            $builder
                ->add('MailAnonymous', EmailType::class, [
                    'label'     => "Adresse mail",
                    'required'  => false,
                    'attr'      => [
                        'placeholder' => "Pour modifier votre évènement ultérieurement, indiquer votre adresse mail :",
                        'required'    => false
                    ]
                ])
            ;
        }

        if (
            $event->getIsPublished() !== true and
            $this->security->getUser()
        ){
            $builder
                ->add('submitDraft', SubmitType::class, [
                    'label' => "Enregistrer et publier ultérieurement",
                    'attr' => ['class' => "submit-draft btn-secondary btn-block"]
                ])
            ;
                /*
                ->add('isPublished', ChoiceType::class, [
                    'label'     => "Publication de votre évènement",
                    'choices'   => [
                        "Évènement public (Publié sur l'agenda)"                 => true,
                        "Enregistré l'évènement pour une publication ultérieure" => false
                    ]
                ])
                */
        }

        if ($event->getIsPublished() ){$textButton = "Modifier votre évènement";}
        else {$textButton = "Publier votre évènement";}

        $builder
            ->add('submitPublished', SubmitType::class, [
                'label' => $textButton,
                'attr' => ['class' => "btn btn-primary btn-block"]
            ])
        ;

        if (
            $event->getIsFull() or
            $event->getIsCanceled() or
            $event->getIsSpam() or
            $event->getIsDuplicated() or
            $event->getIsMistake()
        ){
            $builder
                ->add('submitWithError', SubmitType::class, [
                    'attr' => ['class' => "submit-with-error btn-sm"]
                ])
            ;
        }

        if (
            $event->getIsPublished() and
            $event->getIsValidated() !== true and
            $event->getIsSpam() !== true and
            $event->getIsMistake() !== true and
            $event->getIsDuplicated() !== true and
            $this->security->getUser() and
            in_array('ROLE_ADMIN',$this->security->getUser()->getRoles())
        ){
            $builder
                ->add('submitWithValidation', SubmitType::class, [
                    'label' => "Enregistrer et valider l'évènement",
                    'attr'  => ['class' => "btn-secondary btn-block mt-4 mb-0 "]
                ])
            ;
        }

        $builder

            ->add(
                'coverImageFile',
                FileType::class, [
                    'label'         => "Ajouter une image",
                    'mapped'        => false,
                    'required'      => false,
                    'constraints'   => $imageConstraints,
                    'attr'          => [
                        'placeholder'   => "Ajouter une image",
                        'accept'        => "image/*",
                        'onchange'      => "localPicChange()"
                    ]
                ]
            )

            /*
            ->add(
                'coverImage',
                TextType::class,
                $this->getConfiguration("Titre","Ajouter une image", [
                    'class' => "blocAnnonceImage"
                ]
                )
            )
            */

            ->add(
                'title',
                TextType::class, [
                    'label' => "Titre*",
                    'required' => true,
                    'attr' => [
                        'placeholder' => "Titre uniquement"
                    ]
                ]
            )

            ->add(
                'dateStart',
                DateType::class, [
                    'label' => "Date*",
                    'required' => true,
                    'widget'    => 'single_text',
                    'html5'     => false,
                    'format'    => 'dd/MM/yyyy',
                    'attr' => [
                        'placeholder' => "JJ/MM/AAAA"
                    ]
                ]
            )

            ->add(
                'dateEnd',
                DateType::class,
                $this->getConfiguration("jusqu'au","JJ/MM/AAAA", [
                    'widget'    => 'single_text',
                    'html5'     => false,
                    'required'  => false,
                    'format'    => 'dd/MM/yyyy'
                ],false)
            )

            ->add(
                'precisionDate',
                ChoiceType::class, [
                    'label' => "Conditions d'ouverture",
                    'choices'  => [
                        "Préciser les jours d'ouverture"            => null,
                        "Ouvert tous les jours"                     => 'ALL_DAYS',
                        "Ouverture régulière (Préciser les jours)"  => 'DAYS_OPENING',
                        "Ouverture périodique (Préciser les dates)" => 'DATES_OPENING'
                        //"Ouvert les jours d'ouverture du lieu"      => 'SAME_OF_PLACE'
                    ]
                ]
            )

            ->add(
                'choiceOfDays',
                ChoiceType::class, [
                'multiple'  => true,
                'required'  => false,
                'choice_attr' => ['Un ou plusieurs jours ?*' => ['disabled'=>'']],
                'choices'   => [
                    'les lundis'     => 'Monday',
                    'les mardis'     => 'Tuesday',
                    'les mercredis'  => 'Wednesday',
                    'les jeudis'     => 'Thursday',
                    'les vendredis'  => 'Friday',
                    'les samedis'    => 'Saturday',
                    'les dimanches'  => 'Sunday'
                ]
            ])

            ->add(
                'dateMultiple',
                TextType::class, [
                    'required'  => false,
                    'attr'      => [
                        'placeholder'   => "Préciser les jours d'ouverture",
                        'class'         => 'multidate'
                    ]
                ]
            )

            ->add(
                'timeStart',
                TimeType::class,
                $this->getConfiguration("Horaire","HH:MM", [
                    'required'  => false,
                    'widget'    => 'single_text'
                ],false)
            )

            ->add(
                'timeEnd',
                TimeType::class,
                $this->getConfiguration("jusqu'à","HH:MM", [
                    'required'  => false,
                    'widget'    => 'single_text'
                ],false)
            )

            ->add(
                'place',
                Select2EntityType::class, [
                    'label'         => 'Lieu*',
                    'placeholder'   => 'Choisir un lieu',
                    'remote_route'  => 'tetranz_place_search',
                    //'transformer' => $this->transformer->transform(Place::class),
                    'class'         => Place::class,
                    'language'      => 'fr',
                    'minimum_input_length' => 3,
                    'primary_key'   => 'id',
                    'text_property' => 'title',
                    'required'      => true,
                    'allow_clear'   => true,
                    'allow_add'     => [
                        'enabled'           => true,
                        'new_tag_text'      => " (Hors annuaire)",
                        'tag_separators'    => '[]'
                    ],
                    'page_limit'    => 10,
                    'delay'         => 750
                ]
            )

            ->add('rooms', EntityType::class, [
                'class' => Room::class,
                'placeholder' => '',
            ])
/*
            ->add('places', ChoiceType::class, [
                'mapped'    => false,
                'choices' => $options['places'],
                'expanded' => true,
                'multiple' => false,
            ])

            ->add(
                'rooms',
                EntityType::class,
                $this->getConfiguration("Salle","...", [
                    'class' => Room::class,
                    'choice_label' => 'name',
                    'placeholder' => 'Choisir une salle...',
                    'required' => false
                ],false)
            )
*/

            ->add(
                'address',
                TextType::class, [
                    'mapped'    => false,
                    'required'  => false,
                    'label'     => "Adresse",
                    'attr'      => [
                        'placeholder' => "Préciser l'adresse (facultatif)"
                    ]
                ]
            )

            ->add(
                'city',
                Select2EntityType::class, [
                    'label'                 => 'Ville',
                    'placeholder'           => "Préciser la ville",
                    'remote_route'          => 'tetranz_city_search',
                    'language'              => 'fr',
                    'minimum_input_length'  => 3,
                    'class'                 => City::class,
                    'primary_key'           => 'id',
                    'text_property'         => 'name',
                    'allow_clear'           => true,
                    'page_limit'            => 10,
                    'delay'                 => 500,
                    'cache'                 => true,
                    'cache_timeout'         => 600000
                ]
            )

            //->add('placeTemporary', HiddenType::class)

            ->add(
                'rubric',
                EntityType::class, [
                    'label'     => "Rubrique*",
                    'class'         => Rubric::class,
                    'choice_label'  => 'name',
                    'placeholder'   => 'Choisir une rubrique',
                    'required'  => true
                ]
            )

            ->add(
                'category',
                Select2EntityType::class, [
                    //'placeholder' => 'Préciser une ou plusieurs catégories',
                    'placeholder'   => '  ...',
                    'remote_route'  => 'tetranz_category_search',
                    'multiple'      => true,
                    'language'      => 'fr',
                    'minimum_input_length' => 1,
                    'class'         => Category::class,
                    'primary_key'   => 'id',
                    'text_property' => 'name',
                    'attr'          => [
                        'data-maximum-selection-length' => 4
                    ],
                    'allow_add' => [
                        'enabled'           => true,
                        'new_tag_text'      => ' (NOUVEAU)',
                        'new_tag_prefix'    => '__',
                        'tag_separators'    => '[",", ""]'
                    ],
                    'page_limit'    => 10,
                    'delay'         => 750
                ]
            )

            ->add(
                'description',
                CKEditorType::class, [
                    'label' => "Description*",
                    'config'=> [
                        'entities'          => false,
                        'basicEntities'     => false,
                        'entities_greek'    => false,
                        'entities_latin'    => false,
                        'toolbar'           => 'toolbar_basic',
                        'language'          => 'fr',
                        'placeholder'       => 'Description',
                    ]

                ]
                //TextareaType::class,
                //$this->getConfiguration("Description*","Taper un description")
            )

            /*
            ->add(
                'description',
                TextareaType::class, [
                    'label' => "Description*",
                    'attr' => [
                        'placeholder' => "Taper un description",
                        "style" => "height:250px;"
                    ]
                ]
            )
            */

            ->add(
                'precisionPrice',
                ChoiceType::class, [
                    'label'     => 'Plein tarif',
                    'choices'   => [
                        'Plein tarif non communiqué'=> NULL,
                        'Entrée libre'              => "FREE",
                        'Prix libre et conscient'   => "AWARE",
                        'Payant'                    => "PAYING"
                        /*'Plus de 20 Euros' => "MORE"*/
                    ]
                ]
            )

            ->add('price', NumberType::class, [
                'required'  => false,
                'label'     => '€',
                'attr'      => [
                    'placeholder' => "Prix min. sur place"
                ]
            ])

            ->add('more', CheckboxType::class, [
                'label'     => "Je ne connais pas exactement le tarif sur place, mais c'est plus de 20 Euros !",
                'required'  => false,
                'mapped'    => false,
                //'data'      => true
            ])

            ->add(
                'videos',
                CollectionType::class,
                [
                    'entry_type' => VideoType::class,
                    'allow_add' => true,
                    'allow_delete' => true
                ]
            )

            /*
            ->add(
                'images',
                CollectionType::class,
                [
                    'entry_type' => ImageType::class,
                    'allow_add' => true,
                    'allow_delete' => true
                ]
            )
            */

            ->add(
                'prices',
                CollectionType::class,
                [
                    'entry_type' => PriceType::class,
                    'allow_add' => true,
                    'allow_delete' => true
                ]
            )

            ->add(
                'tickets',
                CollectionType::class,
                [
                    'entry_type' => TicketType::class,
                    'allow_add' => true,
                    'allow_delete' => true
                ]
            )

            ->add(
                'organizer',
                Select2EntityType::class, [
                    'label'             => 'Organisateur',
                    'placeholder'       => 'Organisé par',
                    'remote_route'      => 'tetranz_organizer_search',
                    'language'          => 'fr',
                    'minimum_input_length' => 3,
                    'class'             => Organizer::class,
                    'primary_key'       => 'id',
                    'text_property'     => 'name',
                    'allow_clear'       => true,
                    'allow_add'         => [
                        'enabled'           => true,
                        'new_tag_text'      => ' (NOUVEAU)',
                        'tag_separators'    => '[]'
                    ],
                    'page_limit'        => 10,
                    'delay'             => 750
                ]
            )

            ->add(
                'artists',
                Select2EntityType::class, [
                    'label'             => 'Artistes',
                    'placeholder'       => '   Préciser les artistes',
                    'remote_route'      => 'tetranz_artist_search',
                    'multiple'          => true,
                    'language'          => 'fr',
                    'minimum_input_length' => 2,
                    'class'             => Artist::class,
                    'primary_key'       => 'id',
                    'text_property'     => 'name',
                    'page_limit'        => 10,
                    'delay'             => 750,
                    'attr'              => [
                        'data-maximum-selection-length' => 10
                    ],
                    'allow_add' => [
                        'enabled'           => true,
                        'new_tag_text'      => ' (NOUVEAU)',
                        'new_tag_prefix'    => '__',
                        'tag_separators'    => '[",", ""]'
                    ]
                ]
            )

            ->add(
                'festival',
                Select2EntityType::class, [
                    'label'             => 'Festival',
                    'placeholder'       => "Dans le cadre d'un festival",
                    'remote_route'      => 'tetranz_festival_search',
                    'language'          => 'fr',
                    'minimum_input_length' => 3,
                    'class'             => Festival::class,
                    'primary_key'       => 'id',
                    'text_property'     => 'title',
                    'allow_clear'       => true,
                    'page_limit'        => 10,
                    'delay'             => 750,
                    'allow_add'         => [
                        'enabled'           => true,
                        'new_tag_text'      => ' (NOUVEAU)',
                        'tag_separators'    => '[]'
                    ]
                ]
            )

            ->add(
                'links',
                CollectionType::class,
                [
                    'allow_add'     => true,
                    'allow_delete'  => true
                ]
            )

            /*
            ->add(
                'conditionOfUse',
                CheckboxType::class, [
                    'label' => "J'accepte les conditions d'utilisation",
                    'mapped' => false,
                    //'required' => false,
                ]
            )
            */
        ;

        $formModifier = function (FormInterface $form, Place $place = null) {
            $rooms = null === $place ? [] : $place->getRooms();

            $form->add('rooms', EntityType::class, [
                'class' => Room::class,
                'required' => false,
                'choice_label' => 'name',
                'placeholder' => 'Préciser la salle',
                'choices' => $rooms,
            ]);
        };

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $fevent) use ($formModifier) {
                // this would be your entity, i.e. SportMeetup
                $data = $fevent->getData();

                $formModifier($fevent->getForm(), $data->getPlace());
            }
        );

        /*
        $builder->get('place')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $fevent) use ($formModifier) {
                // It's important here to fetch $event->getForm()->getData(), as
                // $event->getData() will get you the client data (that is, the ID)
                $place = $fevent->getForm()->getData();

                // since we've added the listener to the child, we'll have to pass on
                // the parent to the callback functions!
                $formModifier($fevent->getForm()->getParent(), $place);
            }
        );
        */
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Event::class,
            //'places' => [],
        ]);
    }
}
