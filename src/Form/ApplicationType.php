<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;

class ApplicationType extends AbstractType {

    /**
     * Configuration de base d'un champ
     *
     * @param string $label
     * @param string $placeholder
     * @param array $options
     * @return array
     */
    protected function getConfiguration ($label, $placeholder, $options = [], $required=false) {
        return array_merge([
            'label' => $label,
            'required' => $required,
            'attr' => [
                'placeholder' => $placeholder,
                'required' => $required
                //'lang' => 'fr'
            ]
        ], $options);
    }

}