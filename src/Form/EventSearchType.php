<?php

namespace App\Form;

use App\Entity\City;
use App\Entity\Place;
use App\Entity\Rubric;
use App\Entity\EventSearch;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class EventSearchType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('prix', IntegerType::class, [
                'label' => "Prix",
                'required' => false,
                'attr' => [
                    'placeholder' => 'Tarif maximum'
                ]
            ])
            
            ->add('titre', TextType::class, [
                'label' => "Titre",
                'required' => false,
                'attr' => [
                    'placeholder' => 'Titre'
                ]
            ])   
            /*
            ->add(
                'searchRubric', 
                EntityType::class,
                $this->getConfiguration("Rubrique","Choisir une rubrique", [
                    'class' => Rubric::class,
                    'choice_label' => 'name',
                    'placeholder' => 'Choisir une rubrique...'
                ])
            )  
            */

            /**/
            ->add(
                'rubrique', 
                EntityType::class, [
                    'label' => 'Type',
                    'required' => false,
                    'class' => Rubric::class,
                    'choice_label' => 'name',
                    'choice_value' => 'slug',
                    'placeholder' => 'Choisir une rubrique'
                ]
            ) 
            
            ->add(
                'lieu', 
                Select2EntityType::class, [
                    'placeholder' => 'Chercher par lieu',
                    'remote_route' => 'place_by_slug_search',
                    'class' => Place::class,
                    'language' => 'fr',
                    'minimum_input_length' => 3,
                    'primary_key' => 'slug',
                    'text_property' => 'title',
                    'allow_clear' => true,
                    'allow_add' => [
                        'enabled' => true,
                        'new_tag_text' => " (Rechercher)",
                        'tag_separators' => '[]'
                    ],
                    'page_limit' => 10,
                    'delay' => 500,

                ]
            )

            ->add(
                'ville', 
                Select2EntityType::class, [
                    'label' => 'Ville',
                    'remote_route' => 'city_by_slug_search',
                    'primary_key' => 'slug',
                    'text_property' => 'name',
                    'language' => 'fr',
                    'minimum_input_length' => 3,
                    'placeholder' => 'Choisir une ville',
                    'class' => City::class,
                    'allow_clear' => true,
                    'page_limit' => 10,
                    'delay' => 500,
                    //'cache' => true,
                    //'cache_timeout' => 600000, // if 'cache' is true
                ]
            )

            ->add('date', DateType::class, [
                'label' => 'Date',
                'widget' => 'single_text',
                'required' => false,
                'attr' => [
                    
                    'placeholder' => 'Date'
                ]
            ])   

        
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => EventSearch::class,
            'method' => 'get',
            'csrf_protection' => false
        ]);
    }
    
    /**
    * @return bool
    */
    public function getBlockPrefix()
    {
        return '';
    }
}
