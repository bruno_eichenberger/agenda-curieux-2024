<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ClaimType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'explanation',
                TextareaType::class, [
                    'label' => "Explication",
                    'required' => false,
                    'attr' => [
                        'required' => false,
                        'placeholder' => "Taper un description"
                    ]
                ]
            )

            ->add(
                'response',
                ChoiceType::class, [
                    'label' => "Votre décision",
                    'choices'  => [
                        '...' => NULL,
                        'Accepté' => TRUE,
                        'Refusé (Donnez une explication' => FALSE
                    ]
                ]
            )

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
