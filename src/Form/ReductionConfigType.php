<?php

namespace App\Form;

use App\Entity\Place;
use App\Entity\Festival;
use App\Entity\Organizer;
use App\Entity\Reduction;
use App\Entity\Subdomain;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class ReductionConfigType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add(
                'title', 
                TextType::class, [
                    'label' => "Titre"
                ]
            )

            ->add(
                'description', 
                TextareaType::class
            )

            ->add(
                'priceReduced', 
                TextType::class, [
                    'label' => "Tarif réduit"
                ]
            )

            ->add(
                'subdomains',
                Select2EntityType::class, [
                    'label' => "Les sites concernés",
                    'placeholder' => ' Préciser un ou plusieurs sites',
                    'remote_route' => 'tetranz_subdomain_search',
                    'multiple' => true,
                    'language' => 'fr',
                    'minimum_input_length' => 2,
                    'class' => Subdomain::class,
                    'primary_key' => 'id',
                    'text_property' => 'name'
                ]
            )

            ->add(
                'places',
                Select2EntityType::class, [
                    'label' => "Les lieux partenaires à tarif réduit",
                    'placeholder' => ' Préciser un ou plusieurs lieux',
                    'remote_route' => 'tetranz_place_search',
                    'multiple' => true,
                    'language' => 'fr',
                    'minimum_input_length' => 2,
                    'class' => Place::class,
                    'primary_key' => 'id',
                    'text_property' => 'title'
                ]
            )

            ->add(
                'freePlaces',
                Select2EntityType::class, [
                    'label' => "Les lieux partenaires à entrée libre",
                    'placeholder' => ' Préciser un ou plusieurs lieux',
                    'remote_route' => 'tetranz_place_search',
                    'multiple' => true,
                    'language' => 'fr',
                    'minimum_input_length' => 2,
                    'class' => Place::class,
                    'primary_key' => 'id',
                    'text_property' => 'title'
                ]
            )

            ->add(
                'organizers',
                Select2EntityType::class, [
                    'label' => "Les organisateurs partenaires à tarif réduit",
                    'placeholder' => ' Préciser un ou plusieurs organisateurs',
                    'remote_route' => 'tetranz_organizer_search',
                    'multiple' => true,
                    'language' => 'fr',
                    'minimum_input_length' => 2,
                    'class' => Organizer::class,
                    'primary_key' => 'id',
                    'text_property' => 'name'
                ]
            )

            ->add(
                'festivals',
                Select2EntityType::class, [
                    'label' => "Les festivals partenaires à tarif réduit",
                    'placeholder' => ' Préciser un ou plusieurs festivals',
                    'remote_route' => 'tetranz_festival_search',
                    'multiple' => true,
                    'language' => 'fr',
                    'minimum_input_length' => 2,
                    'class' => Festival::class,
                    'primary_key' => 'id',
                    'text_property' => 'title'
                ]
            )
            
            ->add(
                'submit',
                SubmitType::class, [
                    'label' => "Modifier",
                    'attr' => [
                        'class' =>"btn btn-primary btn-block mt-5"
                    ]
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Reduction::class,
        ]);
    }
}
