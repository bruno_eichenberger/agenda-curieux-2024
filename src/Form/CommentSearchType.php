<?php

namespace App\Form;

use App\Entity\Comment;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CommentSearchType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            /*
            ->add('validation', ChoiceType::class, [
                'label'     => "Utilisateur",
                'mapped'    => false,
                'choices'   => [
                    "A vérifier"    => 'verification',
                    "Déjà vérifié"  => 'checked',
                    "Tous"          => 'all'
                ]
            ])
            */

            ->add('user', ChoiceType::class, [
                'label'     => "Entité",
                'mapped'    => false,
                'choices'   => [
                    "Tout le monde"      => 'all',
                    "Anonyme"   => 'anonymous',
                    "Connecté"  => 'connected',
                    //"Certifié"  => 'certified',
                    //"Bénévole"  => 'volunteer'
                ]
            ])

            ->add('entity', ChoiceType::class, [
                'label'     => "Par type",
                'mapped'    => false,
                'choices'   => [
                    "Partout"           => 'all',
                    "Par évènement"     => 'event',
                    "Par lieu"          => 'place',
                    //"Par festival"      => 'festival',
                    //"Par artiste"       => 'artist',
                    //"Par organisateur"  => 'organizer',
                ]
            ])

            ->add('order', ChoiceType::class, [
                'label'     => "Ordre",
                'mapped'    => false,
                'choices'   => [
                    "Les plus récents"     => 'new',
                    "Les plus anciens"    => 'old'
                ],
                'attr'      => [
                    'class' => 'px-2'
                ]
            ])

            ->add('content', TextType::class, [
                'label' => "Contenant",
                'required' => false,
                'attr' => [
                    'placeholder' => 'Par texte'
                ]
            ])

            /*
            ->add('submit', SubmitType::class, [
                'attr' => ['class' => "btn btn-primary btn-block"]
            ])
            */
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Comment::class,
            'method' => 'get',
            'csrf_protection' => false
        ]);
    }

    /**
    * @return bool
    */
    public function getBlockPrefix()
    {
        return '';
    }
}
