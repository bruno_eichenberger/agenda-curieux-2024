<?php

namespace App\Form;

use App\Entity\Ticket;
use App\Entity\Ticketing;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class TicketType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('url', UrlType::class, [
            'attr' => [
                'placeholder' => "Lien vers une billetterie"
            ]
        ])
        ->add('price', NumberType::class, [
            'required'  => false,
            'label'     => "€",
            'attr'      => [
                'placeholder' => "Prix ..."
            ]
        ])
        
        ->add('information', TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => "Une précision ?"
            ]
        ])
        
        ->add('ticketing', EntityType::class, [
            'required' => false,
            'class' => Ticketing::class,
            'choice_label' => 'name',
            'attr' => [
                'placeholder' => "Source (facultatif)",
                //'disabled' => 'disabled'
            ]
        ])
            //->add('price')
            //->add('event')
            //->add('ticketing')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ticket::class,
        ]);
    }
}
