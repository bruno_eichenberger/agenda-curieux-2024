<?php

namespace App\Form;

use App\Entity\City;
use App\Entity\Event;
use App\Entity\Place;
use App\Entity\Rubric;
use App\Form\ApplicationType;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class EventLightType extends ApplicationType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add(
                'title', 
                TextType::class, [
                    'required'  => false,
                    'label'     => 'Titre',
                    'attr'      => [
                        'placeholder' => "Title only"
                    ]
                ]
            )

            ->add(
                'dateStart', 
                DateType::class, [
                    'required'  => true,
                    'label'     => 'Date de début*',
                    'attr'      => [
                        'placeholder' => "JJ/MM/AAAA",
                        'widget'    => 'single_text',
                        'html5'     => false,
                        'format'    => 'dd/MM/yyyy'
                    ]
                ]
            )

            ->add(
                'dateEnd', 
                DateType::class, 
                $this->getConfiguration('Date de fin',"JJ/MM/AAAA", [
                    'widget'    => 'single_text',
                    'html5'     => false,
                    'required'  => false,
                    'format'    => 'dd/MM/yyyy'
                ],false)
            )

            ->add(
                'place', 
                Select2EntityType::class, [
                    'label'         => 'Lieu*',
                    'placeholder'   => 'Choisir un lieu',
                    'remote_route'  => 'tetranz_place_search',
                    //'transformer' => $this->transformer->transform(Place::class),
                    'class'         => Place::class,
                    'language'      => 'fr',
                    'minimum_input_length' => 3,
                    'primary_key'   => 'id',
                    'text_property' => 'title',
                    'required'      => true,
                    'allow_clear'   => true,
                    'allow_add'     => [
                        'enabled'           => true,
                        'new_tag_text'      => " (Hors annuaire)",
                        'tag_separators'    => '[]'
                    ],
                    'page_limit'    => 10,
                    'delay'         => 750
                ]
            )

            ->add(
                'city', 
                Select2EntityType::class, [
                    'label'                 => 'Ville',
                    'placeholder'           => "Préciser la ville, si le lieu est (Hors annuaire)",
                    'remote_route'          => 'tetranz_city_search',
                    'language'              => 'fr',
                    'minimum_input_length'  => 3,
                    'class'                 => City::class,
                    'primary_key'           => 'id',
                    'text_property'         => 'name',
                    'allow_clear'           => true,
                    'page_limit'            => 10,
                    'delay'                 => 500,
                    'cache'                 => true,
                    'cache_timeout'         => 600000
                ]
            )

            ->add(
                'rubric',
                ChoiceType::class, [
                    'mapped'    => false,
                    'label'     => "Rubrique*",
                    'choices'   => [
                        "Concert"           => 'concert',
                        "Spectacle"         => 'spectacle',
                        "Soirée"            => 'soiree',
                        "Exposition"        => 'exposition',
                        "Cinéma"            => 'cinema',
                        "Action citoyenne"  => 'action-citoyenne',
                        "Sport"             => 'sport',
                        "Stage"             => 'stage',
                        "Autre"             => 'autre'
                    ]
                ]
            )

            /*
            ->add(
                'rubric', 
                EntityType::class, [
                    'required'      => true,
                    'label'         => "Rubrique*", 
                    'class'         => Rubric::class,
                    'choice_label'  => 'name',
                    'attr' => [
                        'placeholder' => "Choisir une rubrique"
                    ]
                ]
            )
            */
            
            ->add(
                'description',
                TextareaType::class, [
                    'label' => "Description*",
                    'attr' => [
                        'placeholder' => "Taper un description",
                        "style" => "height:250px;"
                    ]
                ]
            )

            ->add('price', NumberType::class, [
                'required'  => false,
                'label'     => 'Plein tarif',
                'attr'      => [
                    'placeholder' => "Prix min. sur place"
                ]
            ])
            
            ->add('submitPublished', SubmitType::class, [
                'label' => "Publier votre évènement",
                'attr' => ['class' => "btn btn-primary btn-block mt-5"]
            ])
            
            /**/
        ;

        $event = $options['data'];

        if ($event->getAuthor() === null and $this->security->getUser() === null){
            $builder 
                ->add('MailAnonymous', EmailType::class, [
                    'label'     => "Adresse mail",
                    'required'  => false,
                    'attr'      => [
                        'placeholder' => "Pour modifier votre évènement ultérieurement, indiquer votre adresse mail :",
                        'required'    => false
                    ]
                ])
            ;
        }

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Event::class,
        ]);
    }
}
