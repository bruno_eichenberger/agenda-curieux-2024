<?php

namespace App\Form;

use App\Entity\City;
use App\Entity\Placekind;
use App\Entity\PlaceSearch;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class PlaceSearchType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom', TextType::class, [
                'required' => false,
                'label' => 'Nom',
                'attr' => [
                    'placeholder' => 'Indiquer un nom...',
                ]
            ])   

            ->add(
                'type', 
                EntityType::class, [
                    'required' => false,
                    'class' => Placekind::class,
                    'label' => 'Type',
                    'choice_label' => 'name',
                    'choice_value' => 'slug',
                    'placeholder' => 'Préciser un type...'
                ]
            ) 

            ->add(
                'ville', 
                Select2EntityType::class, [
                    'remote_route' => 'city_by_slug_search',
                    'language' => 'fr',
                    'minimum_input_length' => 3,
                    'placeholder' => 'Préciser une ville...',
                    'class' => City::class,
                    'allow_clear' => true,
                    //'transformer' => 'Form\DataTransformer\CityEntitiesToPropertyTransformer',
                    'label' => 'Ville',
                    'primary_key' => 'slug',
                    'text_property' => 'name',
                    'cache' => true,
                    'cache_timeout' => 60000, // if 'cache' is true

                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => PlaceSearch::class,
            'method' => 'get',
            'csrf_protection' => false
        ]);
    }

    /**
    * @return bool
    */
    public function getBlockPrefix()
    {
        return '';
    }
}
