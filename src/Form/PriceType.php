<?php

namespace App\Form;

use App\Entity\Prices;
use App\Entity\Category;
use App\Entity\Reduction;
use App\Form\ApplicationType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class PriceType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add(
                'price',
                NumberType::class, [
                    'label' => "€",
                    'attr' => [
                        'placeholder' => "Prix ...",
                        'min' => "0",
                        'max' => "1000"
                    ]
                ]
            )

            ->add(
                'reductions',
                EntityType::class, [
                    'multiple'  => true,
                    'expanded'  => true,
                    'class'     => Reduction::class,
                    'choice_label'  => 'title',
                    'placeholder'   => 'Choisir une réduction',
                ]
            )

            /*
            ->add(
                'reduction', 
                EntityType::class,
                $this->getConfiguration("Réduction","Choisir une réduction", [
                    'class' => Reduction::class,
                    'choice_label' => 'title',
                    'placeholder' => 'Choisir une réduction'
                ])
            )
            */
            //->add('reduction')
            //->add('event')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Prices::class,
        ]);
    }
}
