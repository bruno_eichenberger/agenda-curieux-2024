<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Place;
use App\Entity\Artist;
use App\Entity\Organizer;
use Symfony\Component\Form\AbstractType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class AccountType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'avatarFile',
                FileType::class, [
                    'label' => "Photo de profil",
                    'mapped' => false,
                    'required' => false,
                    'attr' => [
                        'placeholder' => "Ajouter une image",
                        'accept' => "image/*",
                        'onchange' => "localPicChange()"
                    ],
                    'constraints' => [
                        new Image([
                            'maxSize' => '5M'
                        ])
                    ],
                    //'constraints' => $imageConstraints
                ]
            )
            ->add(
                'firstName',
                TextType::class, [
                    'label' => "Prénom",
                    'attr' => [
                        'placeholder' => "Votre prénom"
                    ]
                ]
            )
            ->add(
                'lastName',
                TextType::class, [
                    'label' => "Nom",
                    'attr' => [
                        'placeholder' => "Votre nom"
                    ]
                ]
            )
            ->add(
                'introduction',
                TextareaType::class, [
                    'label' => "En 2 mots, qui êtes-vous ?",
                    'required' => false,
                    'attr' => [
                        'placeholder' => "Une petit phrase de présentation"
                    ]
                ]
            )
            /*
            ->add(
                'presentation',
                TextareaType::class, [
                    'label' => "Pouvez-vous nous en dire plus...",
                    'required' => false,
                    'attr' => [
                        'placeholder' => "Cette description apparaîtra sur votre profil"
                    ]
                ]
            )
            */
            ->add(
                'presentation',
                CKEditorType::class, [
                    'config' => [
                        'entities' => false,
                        'basicEntities' => false,
                        'entities_greek' => false,
                        'entities_latin' => false,
                        'toolbar' => 'toolbar_basic',
                        'language' => 'fr',
                        'placeholder' => 'Description',
                        'label' => "Pouvez-vous nous en dire plus...",
                        'required' => false,
                        'attr' => [
                            'placeholder' => "Cette description apparaîtra sur votre profil"
                        ]
                    ]

                ]
                //TextareaType::class, 
                //$this->getConfiguration("Description*","Taper un description",['required' => false],false)
            )
            ->add(
                'placeAffirmers',
                Select2EntityType::class, [
                    'label' => "Responsable d'un ou plusieurs lieux",
                    'placeholder' => 'Responsable, chargée de communication, bénévole...',
                    'remote_route' => 'tetranz_place_search',
                    'multiple' => true,
                    'language' => 'fr',
                    'minimum_input_length' => 2,
                    'class' => Place::class,
                    'primary_key' => 'id',
                    'text_property' => 'title',
                    /*'attr' => [
                        'data-maximum-selection-length' => 4,
                        ''
                    ],
                    
                    'allow_add' => [
                        'enabled' => true,
                        'new_tag_text' => ' (NOUVEAU)',
                        'new_tag_prefix' => '__',
                        'tag_separators' => '[",", ""]'
                    ]
                    */
                ]
            )
            ->add(
                'organizerAffirmers',
                Select2EntityType::class, [
                    'label' => "Responsable d'une association ou autres collectifs",
                    'placeholder' => 'Association, organisateur, collectif, collectivité...',
                    'remote_route' => 'tetranz_organizer_search',
                    'multiple' => true,
                    'language' => 'fr',
                    'minimum_input_length' => 1,
                    'class' => Organizer::class,
                    'primary_key' => 'id',
                    'text_property' => 'name',
                    'allow_add' => [
                        'enabled' => true,
                        'new_tag_text' => ' (NOUVEAU)',
                        'new_tag_prefix' => '__',
                        'tag_separators' => '[",", ""]'
                    ]
                ]
            )
            ->add(
                'artistAffirmers',
                Select2EntityType::class, [
                    'label' => "Je suis artiste, responsable ou membre d'un groupe d'artiste",
                    'placeholder' => "Musicien, comédien, artiste, groupe... ",
                    'remote_route' => 'tetranz_artist_search',
                    'multiple' => true,
                    'language' => 'fr',
                    'minimum_input_length' => 1,
                    'class' => Artist::class,
                    'primary_key' => 'id',
                    'text_property' => 'name',
                    'allow_add' => [
                        'enabled' => true,
                        'new_tag_text' => ' (NOUVEAU)',
                        'new_tag_prefix' => '__',
                        'tag_separators' => '[",", ""]'
                    ]
                ]
            )
            /*
            ->add(
                'isVisible',
                ChoiceType::class, [
                    'label' => "Affichage de mon profil",
                    'choices'  => [
                        "Public (Afficher mon profil sur mes publications)" => true,
                        "Privée (Cacher mon profil sur mes publications)"   => false
                    ]
                ]
            )
            */
            ->add(
                'isVisible',
                CheckboxType::class, [
                    'label'     => "Afficher mon profil sur mes publications",
                    'required'  => false
                ]
            )

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
