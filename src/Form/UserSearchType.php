<?php

namespace App\Form;

use App\Entity\UserSearch;
use App\Form\ApplicationType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class UserSearchType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        
            ->add('nom', TextType::class, [
                'label' => "Nom",
                'required' => false,
                'attr' => [
                    'placeholder' => "Nom de l'utilisateur"
                ]
            ])

            ->add('email', TextType::class, [
                'label' => "Email",
                'required' => false,
                'attr' => [
                    'placeholder' => "Email de l'utilisateur"
                ]
            ])
            /*
            ->add(
                'submit',
                SubmitType::class, [
                    'label' => "Rechercher",
                    'attr' => [
                        'class' =>"btn btn-primary btn-block mt-5"
                    ]
                ]
            )
            */
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => UserSearch::class,
            'method' => 'get',
            'csrf_protection' => false
        ]);
    }

    /**
    * @return bool
    */
    public function getBlockPrefix()
    {
        return '';
    }
}
