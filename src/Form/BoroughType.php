<?php

namespace App\Form;

use App\Entity\City;
use App\Entity\Borough;
use App\Entity\Reduction;
use App\Entity\Department;
use App\Form\ApplicationType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class BoroughType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
            $builder

            ->add(
                'name', 
                TextType::class, 
                $this->getConfiguration("Nom de l'arrondissement","Taper le nom d'un arrondissement")
            )

            ->add(
                'chiefTown', 
                TextType::class, 
                $this->getConfiguration("Nom du chef-lieu","Taper le nom du chef-lieu")
            )

            ->add(
                'newCities', 
                Select2EntityType::class, [
                    'mapped'                => false,
                    'label'                 => 'Les villes',
                    'placeholder'           => "Ajouter ...",
                    'remote_route'          => 'tetranz_city_search',
                    'language'              => 'fr',
                    'multiple'              => true,
                    'class'                 => City::class,
                    'primary_key'           => 'id',
                    'text_property'         => 'name'
                ]
            )

            ->add(
                'latitude', 
                NumberType::class, [
                    'label'     => "Latitude",
                    'required'  => false,
                    'attr'      => [
                        'placeholder' => "Latitude",
                    ]
                ]
            )
            
            ->add(
                'longitude',
                NumberType::class, [
                    'label'     => "Longitude",
                    'required'  => false,
                    'attr'      => [
                        'placeholder' => "Longitude",
                    ]
                ] 
            )

            ->add(
                'zoom', 
                NumberType::class, [
                    'label'     => "Zoom",
                    'required'  => false,
                    'attr'      => [
                        'placeholder' => "Indiquer un chiffre entre 10 et 20",
                    ]
                ]
            )

            ->add(
                'resident', 
                TextType::class, 
                $this->getConfiguration("Nom des habitants","Taper le nom des habitants (au masculin pluriel)")
            )
            /*
            ->add(
                'slogan', 
                TextType::class, [
                    'label'     => "Slogan",
                    'required'  => false,
                    'attr'      => [
                        'placeholder' => "Agenda culturel et participatif",
                    ]
                ]
            )
            */
            ->add(
                'description', 
                TextareaType::class, [
                    'label'     => "Description",
                    'required'  => false,
                    'attr'      => [
                        'placeholder' => "En quelques mots...",
                    ]
                ]
            )
            /*
            ->add(
                'color',
                ChoiceType::class, [
                    'label'     => 'Choisir une couleur',
                    'choices'   => [
                        'Par défaut'   => NULL,
                        'Orange'        => "ORANGE",
                        'Rouge'         => "RED",
                        'Bleu'          => "BLUE",
                        'Vert'          => "GREEN",
                        'Turquoise'     => "TURQUOISE"
                    ]
                ]
            )

            ->add(
                'department', 
                EntityType::class,
                $this->getConfiguration("Département","Associer à un département", [
                    'class' => Department::class,
                    'choice_label' => 'name'
                ])
            )
            */

            ->add(
                'reduction', 
                Select2EntityType::class, [
                    'label'                 => 'Les réductions',
                    'placeholder'           => "Préciser les réductions",
                    'remote_route'          => 'tetranz_reduction_search',
                    'language'              => 'fr',
                    'multiple'              => true,
                    'class'                 => Reduction::class,
                    'primary_key'           => 'id',
                    'text_property'         => 'title',
                    'allow_clear'           => true,
                    'allow_add' => [
                        'enabled' => true,
                        'new_tag_text' => ' (NOUVEAU)',
                        'new_tag_prefix' => '__',
                        'tag_separators' => '[",", ""]'
                    ]
                ]
            )

        ;
        
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Borough::class,
        ]);
    }
}