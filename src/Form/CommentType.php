<?php

namespace App\Form;

use App\Entity\Comment;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Beelab\Recaptcha2Bundle\Form\Type\RecaptchaType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Beelab\Recaptcha2Bundle\Validator\Constraints\Recaptcha2;

class CommentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('createdAt')
            //->add('rating')
            //->add('event')
            //->add('author')
            ->add(
                'content',
                TextareaType::class, [
                    //'label' => "Explication",
                    //'required' => false,
                    'attr' => [
                        //'required' => false,
                        'placeholder' => "Laisser un commentaire"
                    ]
                ]
            )

            ->add(
                'isAlerted',
                ChoiceType::class, [
                    'choices'  => [
                        'Publier un commentaire' => FALSE,
                        "Signaler une erreur (Votre commentaire sera supprimé dès que l'erreur sera traitée)" => TRUE
                    ]
                ]
            )
            ->add('captcha', RecaptchaType::class, [
                // You can use RecaptchaSubmitType
                // "groups" option is not mandatory
                'constraints' => new Recaptcha2(),
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Comment::class,
        ]);
    }
}
