<?php

namespace App\Form;

use App\Entity\City;
use App\Entity\Place;
use App\Entity\Rubric;
use App\Entity\TicketSearch;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class TicketSearchType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('searchTitle', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'placeholder' => 'Titre'
                ]
            ])

            ->add(
                'searchPlace', 
                Select2EntityType::class, [
                    'remote_route' => 'place_by_slug_search',
                    'primary_key' => 'slug',
                    'text_property' => 'title',
                    'language' => 'fr',
                    'minimum_input_length' => 3,
                    'placeholder' => 'Choisir un lieu',
                    'class' => Place::class,
                    'label' => false,
                    'allow_clear' => true,
                    'allow_add' => [
                        'enabled' => true,
                        'new_tag_text' => " (Rechercher)",
                        'tag_separators' => '[]'
                    ]
                ]
            )

            ->add(
                'searchCity', 
                Select2EntityType::class, [
                    'remote_route' => 'city_by_slug_search',
                    'primary_key' => 'slug',
                    'text_property' => 'name',
                    'language' => 'fr',
                    'minimum_input_length' => 3,
                    'placeholder' => 'Choisir une ville',
                    'class' => City::class,
                    'label' => false,
                    'allow_clear' => true,
                    'allow_add' => [
                        'enabled' => true,
                        'new_tag_text' => " (Rechercher)",
                        'tag_separators' => '[]'
                    ]
                ]
            )

            ->add('searchDate', DateType::class, [
                'widget' => 'single_text',
                'required' => false,
                'label' => false,
                'attr' => [
                    
                    'placeholder' => 'Date'
                ]
            ])   
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TicketSearch::class,
            'method' => 'get',
            'csrf_protection' => false
        ]);
    }

    /**
    * @return bool
    */
    public function getBlockPrefix()
    {
        return '';
    }
}
