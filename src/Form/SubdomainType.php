<?php

namespace App\Form;

use App\Entity\City;
use App\Entity\User;
use App\Entity\Borough;
use App\Entity\Reduction;
use App\Entity\Subdomain;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class SubdomainType extends AbstractType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $subdomain = $options['data'];

        $builder
            ->add(
                'name', 
                TextType::class, [
                    'label'     => "Nom du site",
                    'attr'      => [
                        'placeholder' => "Le site des Curieux",
                    ]
                ]
            )

            ->add(
                'mainBorough', 
                Select2EntityType::class, [
                    'label'                 => "L'arrondissement principal",
                    'placeholder'           => "Ajouter ...",
                    'remote_route'          => 'tetranz_borough_search',
                    'language'              => 'fr',
                    'class'                 => Borough::class,
                    'primary_key'           => 'id',
                    'text_property'         => 'name'
                ]
            )
        ;

        if (
            $this->security->getUser() and
            in_array('ROLE_SUPERADMIN',$this->security->getUser()->getRoles())
        ){
            $builder->add(
                'keyInProvider', 
                TextType::class, [
                    'required'  => false,
                    'mapped'    => false,
                    'label'     => "Clé dans Google Analytics",
                    'attr'      => [
                        'placeholder' => "Chiffre",
                    ]
                ]
            );
        }

        if ($subdomain == new Subdomain){
            $builder->add(
                'slug', 
                TextType::class, [
                    'label'     => "Adresse Web",
                    'attr'      => [
                        'placeholder' => "www",
                    ]
                ]
            );
        } else {
            $builder
                ->add(
                    'boroughs', 
                    Select2EntityType::class, [
                        'label'                 => 'Les arrondissements secondaires',
                        'placeholder'           => "Ajouter ...",
                        'remote_route'          => 'tetranz_borough_search',
                        'language'              => 'fr',
                        'multiple'              => true,
                        'class'                 => Borough::class,
                        'primary_key'           => 'id',
                        'text_property'         => 'name'
                    ]
                )
                
                ->add(
                    'arounds', 
                    Select2EntityType::class, [
                        'label'                 => 'Les autres sites associés dans le menu',
                        'placeholder'           => "Ajouter ...",
                        'remote_route'          => 'tetranz_subdomain_search',
                        'language'              => 'fr',
                        'multiple'              => true,
                        'class'                 => Subdomain::class,
                        'primary_key'           => 'id',
                        'text_property'         => 'name'
                    ]
                )

                ->add(
                    'description', 
                    TextType::class, [
                        'mapped'    => false,
                        'label'     => "Description",
                        'required'  => false,
                        'attr'      => [
                            'placeholder' => "Personnalisée la description",
                        ]
                    ]
                )

                ->add(
                    'keywords', 
                    TextType::class, [
                        'mapped'    => false,
                        'label'     => "Mots-clés",
                        'required'  => false,
                        'attr'      => [
                            'placeholder' => "Personnalisée les mots-clés",
                        ]
                    ]
                )

                ->add(
                    'slogan', 
                    TextType::class, [
                        'label'     => "Slogan",
                        'required'  => false,
                        'attr'      => [
                            'placeholder' => "Agenda culturel et participatif",
                        ]
                    ]
                )

                ->add(
                    'volunteers', 
                    Select2EntityType::class, [
                        'label'                 => 'Les bénévoles',
                        'placeholder'           => "Ajouter ...",
                        'remote_route'          => 'tetranz_user_search',
                        'language'              => 'fr',
                        'multiple'              => true,
                        'class'                 => User::class,
                        'primary_key'           => 'id',
                        'text_property'         => 'fullname'
                    ]
                )

                ->add(
                    'legal', 
                    TextareaType::class, [
                        'label'     => "Conditions personnalisées",
                        'required'  => false,
                        'attr'      => [
                            'placeholder' => "Indiquer vos propres conditions d'utilisation",
                        ]
                    ]
                )

                ->add(
                    'color',
                    ChoiceType::class, [
                        'label'     => 'Choisir une couleur',
                        'choices'   => [
                            'Par défaut'   => NULL,
                            'Orange'        => "ORANGE",
                            'Rouge'         => "RED",
                            'Bleu'          => "BLUE",
                            'Vert'          => "GREEN",
                            'Turquoise'     => "TURQUOISE"
                        ]
                    ]
                )

                ->add(
                    'reductions', 
                    Select2EntityType::class, [
                        'label'                 => 'Les réductions',
                        'placeholder'           => "Préciser les réductions",
                        'remote_route'          => 'tetranz_reduction_search',
                        'language'              => 'fr',
                        'multiple'              => true,
                        'class'                 => Reduction::class,
                        'primary_key'           => 'id',
                        'text_property'         => 'title',
                        'allow_clear'           => true,
                        'allow_add' => [
                            'enabled' => true,
                            'new_tag_text' => ' (NOUVEAU)',
                            'new_tag_prefix' => '__',
                            'tag_separators' => '[",", ""]'
                        ]
                    ]
                )
                
                ->add(
                    'resident_singular_masculine', 
                    TextType::class, [
                        'mapped'    => false,
                        'label'     => "Singulier masculin",
                        'required'  => false,
                        'attr'      => [
                            'placeholder' => "habitant"
                        ]
                    ]
                )

                ->add(
                    'resident_singular_feminine', 
                    TextType::class, [
                        'mapped'    => false,
                        'label'     => "Singulier féminin",
                        'required'  => false,
                        'attr'      => [
                            'placeholder' => "habitante",
                        ]
                    ]
                )

                ->add(
                    'resident_plural_masculine', 
                    TextType::class, [
                        'mapped'    => false,
                        'label'     => "Pluriel masculin",
                        'required'  => false,
                        'attr'      => [
                            'placeholder' => "habitants",
                        ]
                    ]
                )

                ->add(
                    'resident_plural_feminine', 
                    TextType::class, [
                        'mapped'    => false,
                        'label'     => "Pluriel féminin",
                        'required'  => false,
                        'attr'      => [
                            'placeholder' => "habitantes",
                        ]
                    ]
                )

                ->add(
                    'facebook', 
                    TextType::class, [
                        'required'  => false,
                        'mapped'    => false,
                        'label'     => "Lien Facebook",
                        'attr'      => [
                            'placeholder' => "https://www.facebook.com/...",
                        ]
                    ]
                )

                ->add(
                    'instagram', 
                    TextType::class, [
                        'required'  => false,
                        'mapped'    => false,
                        'label'     => "Lien Instagram",
                        'attr'      => [
                            'placeholder' => "https://www.instagram.com/...",
                        ]
                    ]
                )

                ->add(
                    'youtube', 
                    TextType::class, [
                        'required'  => false,
                        'mapped'    => false,
                        'label'     => "Lien Youtube",
                        'attr'      => [
                            'placeholder' => "https://www.youtube.com/...",
                        ]
                    ]
                )

                ->add(
                    'twitter', 
                    TextType::class, [
                        'required'  => false,
                        'mapped'    => false,
                        'label'     => "Lien Twitter",
                        'attr'      => [
                            'placeholder' => "https://www.twitter.com/...",
                        ]
                    ]
                )
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Subdomain::class,
        ]);
    }
}
