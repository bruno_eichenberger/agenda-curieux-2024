<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Place;
use App\Entity\Artist;
use App\Entity\Organizer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class AccountEditorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'placeEditors',
                Select2EntityType::class, [
                    'label' => "Gestion des lieux",
                    'placeholder' => 'Préciser un ou plusieurs lieux',
                    'remote_route' => 'tetranz_place_search',
                    'multiple' => true,
                    'language' => 'fr',
                    'minimum_input_length' => 2,
                    'class' => Place::class,
                    'primary_key' => 'id',
                    'text_property' => 'title',
                    /*'attr' => [
                        'data-maximum-selection-length' => 4,
                        ''
                    ],
                    
                    'allow_add' => [
                        'enabled' => true,
                        'new_tag_text' => ' (NOUVEAU)',
                        'new_tag_prefix' => '__',
                        'tag_separators' => '[",", ""]'
                    ]
                    */
                ]
            )
            ->add(
                'organizerEditors',
                Select2EntityType::class, [
                    'label' => "Gestion des associations",
                    'placeholder' => 'Préciser une ou plusieurs associations ou autres organisateurs',
                    'remote_route' => 'tetranz_organizer_search',
                    'multiple' => true,
                    'language' => 'fr',
                    'minimum_input_length' => 1,
                    'class' => Organizer::class,
                    'primary_key' => 'id',
                    'text_property' => 'name',
                    'allow_add' => [
                        'enabled' => true,
                        'new_tag_text' => ' (NOUVEAU)',
                        'new_tag_prefix' => '__',
                        'tag_separators' => '[",", ""]'
                    ]
                ]
            )
            ->add(
                'artistEditors',
                Select2EntityType::class, [
                    'label' => "Gestion des artistes",
                    'placeholder' => "Nom de l'artiste ou du groupe",
                    'remote_route' => 'tetranz_artist_search',
                    'multiple' => true,
                    'language' => 'fr',
                    'minimum_input_length' => 1,
                    'class' => Artist::class,
                    'primary_key' => 'id',
                    'text_property' => 'name',
                    'allow_add' => [
                        'enabled' => true,
                        'new_tag_text' => ' (NOUVEAU)',
                        'new_tag_prefix' => '__',
                        'tag_separators' => '[",", ""]'
                    ]
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
