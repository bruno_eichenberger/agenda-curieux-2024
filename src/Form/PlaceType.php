<?php

namespace App\Form;

use App\Entity\City;
use App\Entity\User;
use App\Entity\Place;
use App\Form\RoomType;
use App\Entity\District;
use App\Entity\Placetag;
use App\Entity\Placekind;
use App\Form\ApplicationType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Security\Core\Security;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class PlaceType extends ApplicationType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            /*
            ->add(
                'title',
                TextType::class,
                $this->getConfiguration("Titre","Taper un titre")
            )
            */
            ->add(
                'pronoun',
                ChoiceType::class, // IntegerType pour un entier
                $this->getConfiguration("Pronom","...", [
                    'choices'  => [
                        '...' => NULL,
                        'Le' => 'Le',
                        'La' => 'La',
                        'L’' => "L'",
                        'Les' => 'Les',
                        'Aux' => 'Aux',
                        'Au' => 'Au',
                        'The' => 'The',
                        'Des' => 'Des',
                        'Du' => 'Du'
                        ]
                ])
            )

            ->add(
                'name',
                TextType::class,
                $this->getConfiguration("Nom du lieu*","le nom du lieu", [], true)
            )

            ->add(
                'number',
                TextType::class,
                $this->getConfiguration("№","Numéro")
            )

            ->add(
                'address',
                TextType::class,
                $this->getConfiguration("Adresse*","Rue, boulevard, impasse...", [], true)
            )

            ->add(
                'city',
                Select2EntityType::class, [
                    'label' => 'Ville',
                    'remote_route' => 'tetranz_city_search',
                    'primary_key' => 'id',
                    'text_property' => 'name',
                    'language' => 'fr',
                    'minimum_input_length' => 3,
                    'placeholder' => 'Choisir une ville',
                    'class' => City::class,
                    'required' => true
                ]
            )

            /*
            ->add(
                'district',
                EntityType::class,
                $this->getConfiguration("Quartier","...", [
                    'class' => District::class,
                    'choice_label' => 'name',
                    'placeholder' => 'Choisir un quartier...',
                    'required' => false
                ],false)
            )

            ->add(
                'city',
                EntityType::class,
                $this->getConfiguration("Ville","Choisir une ville", [
                    'class' => City::class,
                    'choice_label' => 'name',
                    'placeholder' => 'Choisir une ville...'
                ])
            )
            */

            ->add(
                'rooms',
                CollectionType::class,
                [
                    'entry_type' => RoomType::class,
                    'allow_add' => true,
                    'allow_delete' => true
                ]
            )

            ->add(
                'placekindprimary',
                EntityType::class,
                $this->getConfiguration("Type de lieu","Choisir le type principal", [
                    'class' => Placekind::class,
                    'choice_label' => 'name',
                    'placeholder' => 'Choisir un type de lieu...',
                    'required' => false
                ],false)
            )

            ->add(
                'placekindsecondary',
                EntityType::class,
                $this->getConfiguration("Second type","Choisir le type secondaire", [
                    'class' => Placekind::class,
                    'choice_label' => 'name',
                    'placeholder' => 'Choisir un type de lieu...',
                    'required' => false
                ],false)
            )

            ->add(
                'placekindtertiary',
                EntityType::class,
                $this->getConfiguration("Troisième type","Précisez un troisième type", [
                    'class' => Placekind::class,
                    'choice_label' => 'name',
                    'placeholder' => 'Choisir un type de lieu...',
                    'required' => false
                ],false)
            )

            ->add(
                'placetags',
                Select2EntityType::class, [
                    'label' => "Les services",
                    'placeholder' => 'Préciser un ou plusieurs services (Ex: billard, baby-foot, happy-hours...)',
                    'remote_route' => 'tetranz_placetag_search',
                    'multiple' => true,
                    'language' => 'fr',
                    'minimum_input_length' => 1,
                    'class' => Placetag::class,
                    'primary_key' => 'id',
                    'text_property' => 'name',
                    'attr' => [
                        'data-maximum-selection-length' => 4
                    ],
                    'allow_add' => [
                        'enabled' => true,
                        'new_tag_text' => ' (NOUVEAU)',
                        'new_tag_prefix' => '__',
                        'tag_separators' => '[",", ""]'
                    ]
                ]
            )

            /*
            ->add(
                'placekinds',
                CollectionType::class,
                [
                    'entry_type' => PlacekindType::class,
                    'allow_add' => true,
                    'allow_delete' => true
                ]
            )
*/
            ->add(
                'coverImageFile',
                FileType::class, [
                    'label' => "Ajouter une image",
                    'mapped' => false,
                    'required' => false,
                    'attr' => [
                        'placeholder' => "Ajouter une image",
                        'accept' => "image/*",
                        'onchange' => "localPicChangePlace()"
                    ],
                    /*
                    'constraints' => [
                        new CoverImage()
                    ]
                    'constraints' => $imageConstraints
                    */
                ]
            )

            /*
            ->add(
                'coverImage',
                TextType::class,
                $this->getConfiguration("Image","Ajouter une image", [
                    'required' => false
                ])
            )
            */

            ->add(
                'description',
                CKEditorType::class, [
                    'config' => [
                        'entities' => false,
                        'basicEntities' => false,
                        'entities_greek' => false,
                        'entities_latin' => false,
                        'toolbar' => 'toolbar_basic',
                        'language' => 'fr',
                        'placeholder' => 'Description',
                        'required' => false
                    ]

                ]
                //TextareaType::class,
                //$this->getConfiguration("Description*","Taper un description",['required' => false],false)
            )

            ->add(
                'email',
                EmailType::class,
                $this->getConfiguration("Email","Indiquer le mail du lieu",['required' => false],false)
            )

            ->add(
                'phone',
                TelType::class,
                $this->getConfiguration("Numéro de téléphone","Indiquer le téléphone du lieu",['required' => false],false)
            )

            ->add(
                'opinion',
                TextareaType::class,
                $this->getConfiguration("avis du Curieux","Notre opinion",['required' => false],false)
            )

            ->add(
                'parking',
                TextareaType::class,
                $this->getConfiguration("Venir en voiture","Parking pour se garer, la sortie d'autoroute à prendre...",['required' => false],false)
            )

            ->add(
                'links',
                CollectionType::class,
                [
                    'allow_add' => true,
                    'allow_delete' => true
                ]
            )

            ->add(
                'multiTitles',
                CollectionType::class,
                [
                    'allow_add' => true,
                    'allow_delete' => true
                ]
            )
        ;

        if (
            $this->security->getUser() and
            in_array('ROLE_ADMIN',$this->security->getUser()->getRoles())
        ){
            $builder
                ->add(
                    'editors',
                    Select2EntityType::class, [
                        'label' => "Gestion des éditeurs",
                        'placeholder' => 'Préciser un ou plusieurs éditeurs',
                        'remote_route' => 'tetranz_user_search',
                        'multiple' => true,
                        'language' => 'fr',
                        'minimum_input_length' => 2,
                        'class' => User::class,
                        'primary_key' => 'id',
                        'text_property' => 'fullName',
                    ]
                )
                /*
                ->add(
                    'affirmers',
                    Select2EntityType::class, [
                        'label' => "Les utilisateurs ayant affirmés être responsable du lieu",
                        'placeholder' => 'Préciser un ou plusieurs éditeurs',
                        'remote_route' => 'tetranz_user_search',
                        'multiple' => true,
                        'language' => 'fr',
                        'minimum_input_length' => 2,
                        'class' => User::class,
                        'primary_key' => 'id',
                        'text_property' => 'fullName',
                    ]
                )*/
                ->add(
                    'author',
                    Select2EntityType::class, [
                        'label' => "L'auteur",
                        'placeholder' => 'Préciser un ou plusieurs éditeurs',
                        'remote_route' => 'tetranz_user_search',
                        'language' => 'fr',
                        'class' => User::class,
                        'primary_key' => 'id',
                        'text_property' => 'fullName',
                    ]
                )
            ;
        }

        $builder->get('city')->addEventListener(
            FormEvents::POST_SET_DATA,
            function(FormEvent $fevent){
                $form = $fevent->getForm();
                if($form->getData()){
                    $form->getParent()->add('district', EntityType::class,[
                        'label' => 'Quartier',
                        'class' => District::class,
                        'choice_label' => 'name',
                        'placeholder' => 'Choisir un quartier...',
                        'required' => false,
                        'choices' => $form->getData()->getDistrict()
                    ]);
                }


            }
        );

        /*
        $builder
            ->add('title')
            ->add('description')
            ->add('coverImage')
            ->add('slug')
            ->add('latitude')
            ->add('longitude')
            ->add('pronoun')
            ->add('name')
            ->add('number')
            ->add('address')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('city')
            ->add('district')
        ;
        */
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Place::class,
        ]);
    }
}
