<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PricesRepository")
 */
class Prices
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2)
     * @Assert\Type(type="float", message = "La valeur {{ value }} doit être de type {{ type }}")
     * @Assert\Range(
     *      min = 0,
     *      max = 1000,
     *      notInRangeMessage = "Le tarif réduit doit être positif et inférieur à 1000"
     * )
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity=Reduction::class, inversedBy="prices")
     */
    private $reduction;

    /**
     * @ORM\ManyToOne(targetEntity=Event::class, inversedBy="prices")
     * @ORM\JoinColumn(nullable=false)
     */
    private $event;

    /**
     * @ORM\ManyToMany(targetEntity=Reduction::class)
     */
    private $reductions;

    public function __construct()
    {
        $this->reductions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getReduction(): ?Reduction
    {
        return $this->reduction;
    }

    public function setReduction(?Reduction $reduction): self
    {
        $this->reduction = $reduction;

        return $this;
    }

    public function getEvent(): ?Event
    {
        return $this->event;
    }

    public function setEvent(?Event $event): self
    {
        $this->event = $event;

        return $this;
    }

    /**
     * @return Collection|Reduction[]
     */
    public function getReductions(): Collection
    {
        return $this->reductions;
    }

    public function addReduction(Reduction $reduction): self
    {
        if (!$this->reductions->contains($reduction)) {
            $this->reductions[] = $reduction;
        }

        return $this;
    }

    public function removeReduction(Reduction $reduction): self
    {
        $this->reductions->removeElement($reduction);

        return $this;
    }
}
