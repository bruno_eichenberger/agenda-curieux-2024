<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class EventSearch {

    private $titre;
    
    private $rubrique;

    private $lieu;

    private $ville;

    /**
     * @Assert\Date
     * @var string A "Y-m-d" formatted value
     */
    protected $date;

    /**
     * @Assert\Range(min=0, minMessage="Le tarif doit être supérieur ou égal à 0 !")
     * @var int|null
     */
    private $prix;





    /**
     * Get the value of price
     *
     * @return  int|null
     */ 
    public function getPrix(): ?int
    {
        return $this->prix;
    }

    /**
     * Set the value of maxPrice
     *
     * @param  int|null  $prix
     * @return  self
     */ 
    public function setPrix(int $prix): EventSearch
    {
        $this->prix = $prix;
        return $this;
    }


    /**
     * Get the value of rubrique
     */ 
    public function getRubrique()
    {
        return $this->rubrique;
    }

    /**
     * Set the value of rubrique
     *
     * @param $rubrique
     * @return void
     */ 
    public function setRubrique($rubrique): EventSearch
    {
        $this->rubrique = $rubrique;
        return $this;
    }

    /**
     * Get the value of lieu
     */ 
    public function getLieu()
    {
        return $this->lieu;
    }

    /**
     * Set the value of lieu
     *
     * @param $lieu
     * @return void
     */ 
    public function setLieu($lieu): EventSearch
    {
        $this->lieu = $lieu;
        return $this;
    }

    /**
     * Get the value of ville
     */ 
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set the value of ville
     *
     * @param $ville
     * @return void
     */ 
    public function setVille($ville): EventSearch
    {
        $this->ville = $ville;
        return $this;
    }


    /**
     * Get the value of titre
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set the value of titre
     *
     * @param $titre
     * @return void
     */
    public function setTitre($titre): EventSearch
    {
        $this->titre = $titre;
        return $this;
    }


    /**
     * Get the value of date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set the value of Date
     *
     * @param string $date
     * @return void
     */
    public function setDate($date): EventSearch
    {
        $this->date = $date;
        return $this;
    }


}