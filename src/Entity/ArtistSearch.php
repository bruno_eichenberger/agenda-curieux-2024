<?php

namespace App\Entity;

class ArtistSearch {

    private $nom;

    //private $genre;

    private $ville;


    /**
     * Get the value of nom
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set the value of nom
     *
     * @param $nom
     * @return void
     */
    public function setNom($nom): ArtistSearch
    {
        $this->nom = $nom;
        return $this;
    }

    /*
     * Get the value of rubrique
     * 
    public function getRubrique()
    {
        return $this->rubrique;
    }

    /*
     * Set the value of rubrique
     *
     * @param $rubrique
     * @return void
     *
    public function setRubrique($rubrique): ArtistSearch
    {
        $this->rubrique = $rubrique;
        return $this;
    }
    */
    /**
     * Get the value of ville
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set the value of ville
     *
     * @param $ville
     * @return void
     */
    public function setVille($ville): ArtistSearch
    {
        $this->ville = $ville;
        return $this;
    }

}