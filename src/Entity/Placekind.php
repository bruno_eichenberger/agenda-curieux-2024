<?php

namespace App\Entity;

use Cocur\Slugify\Slugify;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity(repositoryClass="App\Repository\PlacekindRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Placekind
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $plural;

/*
     * @ORM\ManyToMany(targetEntity=Place::class, inversedBy="placekinds")
     
    private $place;
*/
    /**
     * @ORM\OneToMany(targetEntity=Place::class, mappedBy="placekindPrimary")
     */
    private $placekindPrimary;

    /**
     * @ORM\OneToMany(targetEntity=Place::class, mappedBy="placekindSecondary")
     */
    private $placekindSecondary;

    /**
     * @ORM\OneToMany(targetEntity=Place::class, mappedBy="placekindTertiary")
     */
    private $placekindTertiary;

    public function __construct()
    {
        //$this->place = new ArrayCollection();
        $this->placekindPrimary = new ArrayCollection();
        $this->placekindSecondary = new ArrayCollection();
        $this->placekindTertiary = new ArrayCollection();
    }

    /**
     * Permet d'initialiser le slug
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * 
     * @return void
     */
    public function initializeSlug(){
        if(empty($this->slug)){
            $slugify = new Slugify();
            $this->slug = $slugify->slugify($this->name);
        }
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getPlural(): ?string
    {
        return $this->plural;
    }

    public function setPlural(string $plural): self
    {
        $this->plural = $plural;

        return $this;
    }

    /*
     * @return Collection|Place[]
     
    public function getPlace(): Collection
    {
        return $this->place;
    }

    public function addPlace(Place $place): self
    {
        if (!$this->place->contains($place)) {
            $this->place[] = $place;
        }

        return $this;
    }

    public function removePlace(Place $place): self
    {
        if ($this->place->contains($place)) {
            $this->place->removeElement($place);
        }

        return $this;
    }
*/
    /**
     * @return Collection|Place[]
     */
    public function getPlacekindPrimary(): Collection
    {
        return $this->placekindPrimary;
    }

    /**
     * @return Collection|Place[]
     */
    public function getPlacekindSecondary(): Collection
    {
        return $this->placekindSecondary;
    }

    /**
     * @return Collection|Place[]
     */
    public function getPlacekindTertiary(): Collection
    {
        return $this->placekindTertiary;
    }

    public function addPlacekindTertiary(Place $placekindTertiary): self
    {
        if (!$this->placekindTertiary->contains($placekindTertiary)) {
            $this->placekindTertiary[] = $placekindTertiary;
            $placekindTertiary->setPlacekindTertiary($this);
        }

        return $this;
    }

    public function removePlacekindTertiary(Place $placekindTertiary): self
    {
        if ($this->placekindTertiary->contains($placekindTertiary)) {
            $this->placekindTertiary->removeElement($placekindTertiary);
            // set the owning side to null (unless already changed)
            if ($placekindTertiary->getPlacekindTertiary() === $this) {
                $placekindTertiary->setPlacekindTertiary(null);
            }
        }

        return $this;
    }
}
