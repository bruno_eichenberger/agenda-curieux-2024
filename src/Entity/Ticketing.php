<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TicketingRepository")
 */
class Ticketing
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $website;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $embed;

    /**
     * @ORM\Column(type="smallint")
     */
    private $setting;

    /**
     * @ORM\OneToMany(targetEntity=Ticket::class, mappedBy="ticketing", orphanRemoval=true)
     */
    private $ticket;

    /**
     * @ORM\OneToMany(targetEntity=TicketResource::class, mappedBy="ticketing", orphanRemoval=true)
     */
    private $resources;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $source;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $linked;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $domain;

    /**
     * @ORM\Column(type="simple_array", nullable=true)
     */
    private $redirections = [];

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isActivated;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isEmbeddable;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isOnlyResponsive;

    public function __construct()
    {
        $this->ticket = new ArrayCollection();
        $this->resources = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getEmbed(): ?string
    {
        return $this->embed;
    }

    public function setEmbed(?string $embed): self
    {
        $this->embed = $embed;

        return $this;
    }

    public function getSetting(): ?int
    {
        return $this->setting;
    }

    public function setSetting(int $setting): self
    {
        $this->setting = $setting;

        return $this;
    }

    /**
     * @return Collection|Ticket[]
     */
    public function getTicket(): Collection
    {
        return $this->ticket;
    }

    public function addTicket(Ticket $ticket): self
    {
        if (!$this->ticket->contains($ticket)) {
            $this->ticket[] = $ticket;
            $ticket->setPlatformTicket($this);
        }

        return $this;
    }

    public function removeTicket(Ticket $ticket): self
    {
        if ($this->ticket->contains($ticket)) {
            $this->ticket->removeElement($ticket);
            // set the owning side to null (unless already changed)
            if ($ticket->getPlatformTicket() === $this) {
                $ticket->setPlatformTicket(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TicketResource[]
     */
    public function getResources(): Collection
    {
        return $this->resources;
    }

    public function addResource(TicketResource $resource): self
    {
        if (!$this->resources->contains($resource)) {
            $this->resources[] = $resource;
            $resource->setTicketing($this);
        }

        return $this;
    }

    public function removeResource(TicketResource $resource): self
    {
        if ($this->resources->contains($resource)) {
            $this->resources->removeElement($resource);
            // set the owning side to null (unless already changed)
            if ($resource->getTicketing() === $this) {
                $resource->setTicketing(null);
            }
        }

        return $this;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }

    public function setSource(?string $source): self
    {
        $this->source = $source;

        return $this;
    }

    public function getLinked(): ?string
    {
        return $this->linked;
    }

    public function setLinked(?string $linked): self
    {
        $this->linked = $linked;

        return $this;
    }

    public function getDomain(): ?string
    {
        return $this->domain;
    }

    public function setDomain(string $domain): self
    {
        $this->domain = $domain;

        return $this;
    }

    public function getRedirections(): ?array
    {
        return $this->redirections;
    }

    public function setRedirections(?array $redirections): self
    {
        $this->redirections = $redirections;

        return $this;
    }

    public function getIsActivated(): ?bool
    {
        return $this->isActivated;
    }

    public function setIsActivated(?bool $isActivated): self
    {
        $this->isActivated = $isActivated;

        return $this;
    }

    public function getIsEmbeddable(): ?bool
    {
        return $this->isEmbeddable;
    }

    public function setIsEmbeddable(?bool $isEmbeddable): self
    {
        $this->isEmbeddable = $isEmbeddable;

        return $this;
    }

    public function getIsOnlyResponsive(): ?bool
    {
        return $this->isOnlyResponsive;
    }

    public function setIsOnlyResponsive(?bool $isOnlyResponsive): self
    {
        $this->isOnlyResponsive = $isOnlyResponsive;

        return $this;
    }
}
