<?php

namespace App\Entity;

use Cocur\Slugify\Slugify;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArtistRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Artist
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $coverImage;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=Video::class, mappedBy="artist")
     */
    private $videos;

    /**
     * @ORM\ManyToOne(targetEntity=Artistkind::class, inversedBy="artists")
     */
    private $artistkind;

    /**
     * @ORM\ManyToMany(targetEntity=Event::class, inversedBy="artists")
     */
    private $events;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, mappedBy="artistEditors")
     */
    private $editors;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="artistAffirmers")
     */
    private $affirmers;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="artistAuthors")
     */
    private $author;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="artist")
     */
    private $comments;

    public function __construct()
    {
        $this->videos = new ArrayCollection();
        $this->events = new ArrayCollection();
        $this->editors = new ArrayCollection();
        $this->affirmers = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    /**
     * Permet d'initialiser le slug
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * 
     * @return void
     */
    public function initializeSlug(){
        if(empty($this->slug)){
            $slugify = new Slugify();
            $this->slug = $slugify->slugify($this->name);
        }
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        if (
            mb_strtolower($name) == $name or
            mb_strtoupper($name) == $name
        ){
            $name = str_replace('-','- ',$name);
            $name = ucwords(mb_strtolower($name));
            $name = str_replace('- ','-',$name);
            $name = str_replace(' And ',' and ',$name);
            $name = str_replace(' Et ',' et ',$name);
        }

        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getCoverImage(): ?string
    {
        return $this->coverImage;
    }

    public function setCoverImage(?string $coverImage): self
    {
        $this->coverImage = $coverImage;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|video[]
     */
    public function getVideos(): Collection
    {
        return $this->videos;
    }

    public function addVideo(video $video): self
    {
        if (!$this->videos->contains($video)) {
            $this->videos[] = $video;
            $video->setArtist($this);
        }

        return $this;
    }

    public function removeVideo(video $video): self
    {
        if ($this->videos->contains($video)) {
            $this->videos->removeElement($video);
            // set the owning side to null (unless already changed)
            if ($video->getArtist() === $this) {
                $video->setArtist(null);
            }
        }

        return $this;
    }

    public function getArtistkind(): ?Artistkind
    {
        return $this->artistkind;
    }

    public function setArtistkind(?Artistkind $artistkind): self
    {
        $this->artistkind = $artistkind;

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->contains($event)) {
            $this->events->removeElement($event);
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getEditors(): Collection
    {
        return $this->editors;
    }

    public function addEditor(User $editor): self
    {
        if (!$this->editors->contains($editor)) {
            $this->editors[] = $editor;
            $editor->addArtistEditor($this);
        }

        return $this;
    }

    public function removeEditor(User $editor): self
    {
        if ($this->editors->contains($editor)) {
            $this->editors->removeElement($editor);
            $editor->removeArtistEditor($this);
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getAffirmers(): Collection
    {
        return $this->affirmers;
    }

    public function addAffirmer(User $affirmer): self
    {
        if (!$this->affirmers->contains($affirmer)) {
            $this->affirmers[] = $affirmer;
        }

        return $this;
    }

    public function removeAffirmer(User $affirmer): self
    {
        if ($this->affirmers->contains($affirmer)) {
            $this->affirmers->removeElement($affirmer);
        }

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setArtist($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getArtist() === $this) {
                $comment->setArtist(null);
            }
        }

        return $this;
    }
}
