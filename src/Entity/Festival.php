<?php

namespace App\Entity;

use Cocur\Slugify\Slugify;
use App\Service\UploaderHelper;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FestivalRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Festival
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $coverImage;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=Event::class, mappedBy="festival")
     */
    private $events;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, mappedBy="festivalEditors")
     */
    private $editors;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="festivalAffirmers")
     */
    private $affirmers;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="festivalAuthors")
     */
    private $author;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="festival")
     */
    private $comments;

    /**
     * @ORM\ManyToMany(targetEntity=Reduction::class, mappedBy="festivals")
     */
    private $reductions;

    public function __construct()
    {
        $this->events = new ArrayCollection();
        $this->editors = new ArrayCollection();
        $this->affirmers = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->reductions = new ArrayCollection();
    }


    /**
     * Permet d'initialiser le slug
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * 
     * @return void
     */
    public function initializeSlug(){
        if(empty($this->slug)){
            $slugify = new Slugify();
            $this->slug = $slugify->slugify($this->title);
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        if (
            mb_strtolower($title) == $title or
            mb_strtoupper($title) == $title
        ){
            $title = str_replace('-','- ',$title);
            $title = ucwords(mb_strtolower($title));
            $title = str_replace('- ','-',$title);
            $title = str_replace(' Et ',' et ', $title);
        }

        $this->title = $title;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getCoverImage(): ?string
    {
        return $this->coverImage;
    }

    public function setCoverImage(?string $coverImage): self
    {
        $this->coverImage = $coverImage;

        return $this;
    }

    public function getCoverImagePath()
    {
        return 'img/'.UploaderHelper::FESTIVAL_IMAGE.'/'.$this->getCoverImage();
    }

    public function getEventImage()
    {
        return 'img/'.UploaderHelper::EVENT_IMAGE.'/'.$this->getEventImage();
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->setFestival($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->contains($event)) {
            $this->events->removeElement($event);
            // set the owning side to null (unless already changed)
            if ($event->getFestival() === $this) {
                $event->setFestival(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getEditors(): Collection
    {
        return $this->editors;
    }

    public function addEditor(User $editor): self
    {
        if (!$this->editors->contains($editor)) {
            $this->editors[] = $editor;
            $editor->addFestivalEditor($this);
        }

        return $this;
    }

    public function removeEditor(User $editor): self
    {
        if ($this->editors->contains($editor)) {
            $this->editors->removeElement($editor);
            $editor->removeFestivalEditor($this);
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getAffirmers(): Collection
    {
        return $this->affirmers;
    }

    public function addAffirmer(User $affirmer): self
    {
        if (!$this->affirmers->contains($affirmer)) {
            $this->affirmers[] = $affirmer;
        }

        return $this;
    }

    public function removeAffirmer(User $affirmer): self
    {
        if ($this->affirmers->contains($affirmer)) {
            $this->affirmers->removeElement($affirmer);
        }

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setFestival($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getFestival() === $this) {
                $comment->setFestival(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Reduction[]
     */
    public function getReductions(): Collection
    {
        return $this->reductions;
    }

    public function addReduction(Reduction $reduction): self
    {
        if (!$this->reductions->contains($reduction)) {
            $this->reductions[] = $reduction;
            $reduction->addFestival($this);
        }

        return $this;
    }

    public function removeReduction(Reduction $reduction): self
    {
        if ($this->reductions->removeElement($reduction)) {
            $reduction->removeFestival($this);
        }

        return $this;
    }
}
