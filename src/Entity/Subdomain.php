<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\SubdomainRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=SubdomainRepository::class)
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity(
 *  fields={"slug"},
 *  message="Ce sous-domaine existe déjà !"
 * )
 */
class Subdomain
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $slogan;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="subdomains")
     */
    private $manager;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="supports")
     */
    private $volunteers;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isCreated;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isActivated;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToMany(targetEntity=Subdomain::class)
     */
    private $arounds;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $color;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $data = [];

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $legal;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $resident = [];

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $wallpaper;

    /**
     * @ORM\ManyToMany(targetEntity=Borough::class, inversedBy="subdomains")
     */
    private $boroughs;

    /**
     * @ORM\OneToOne(targetEntity=Borough::class, inversedBy="mainSubdomain", cascade={"persist", "remove"})
     */
    private $mainBorough;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $statistical = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $network = [];

    /**
     * @ORM\ManyToMany(targetEntity=Reduction::class, inversedBy="subdomains")
     */
    private $reductions;

    public function __construct()
    {
        $this->boroughs = new ArrayCollection();
        $this->volunteers = new ArrayCollection();
        $this->arounds = new ArrayCollection();
        $this->reductions = new ArrayCollection();
    }

    /**
     * Permet d'initialiser la date de création
     * 
     * @ORM\PrePersist
     * @ORM\PreUpdate
    */
    public function updatedTimestamps(): void
    {   
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getSlogan(): ?string
    {
        return $this->slogan;
    }

    public function setSlogan(?string $slogan): self
    {
        $this->slogan = $slogan;

        return $this;
    }

    public function getManager(): ?User
    {
        return $this->manager;
    }

    public function setManager(?User $manager): self
    {
        $this->manager = $manager;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getVolunteers(): Collection
    {
        return $this->volunteers;
    }

    public function addVolunteer(User $volunteer): self
    {
        if (!$this->volunteers->contains($volunteer)) {
            $this->volunteers[] = $volunteer;
        }

        return $this;
    }

    public function removeVolunteer(User $volunteer): self
    {
        if ($this->volunteers->contains($volunteer)) {
            $this->volunteers->removeElement($volunteer);
        }

        return $this;
    }

    public function getIsCreated(): ?bool
    {
        return $this->isCreated;
    }

    public function setIsCreated(?bool $isCreated): self
    {
        $this->isCreated = $isCreated;

        return $this;
    }

    public function getIsActivated(): ?bool
    {
        return $this->isActivated;
    }

    public function setIsActivated(?bool $isActivated): self
    {
        $this->isActivated = $isActivated;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getArounds(): Collection
    {
        return $this->arounds;
    }

    public function addAround(self $around): self
    {
        if (!$this->arounds->contains($around)) {
            $this->arounds[] = $around;
        }

        return $this;
    }

    public function removeAround(self $around): self
    {
        if ($this->arounds->contains($around)) {
            $this->arounds->removeElement($around);
        }

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getData(): ?array
    {
        return $this->data;
    }

    public function setData(?array $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getLegal(): ?string
    {
        return $this->legal;
    }

    public function setLegal(?string $legal): self
    {
        $this->legal = $legal;

        return $this;
    }

    public function getResident(): ?array
    {
        return $this->resident;
    }

    public function setResident(?array $resident): self
    {
        $this->resident = $resident;

        return $this;
    }

    public function getWallpaper(): ?string
    {
        return $this->wallpaper;
    }

    public function setWallpaper(?string $wallpaper): self
    {
        $this->wallpaper = $wallpaper;

        return $this;
    }

    /**
     * @return Collection|Borough[]
     */
    public function getBoroughs(): Collection
    {
        return $this->boroughs;
    }

    public function addBorough(Borough $borough): self
    {
        if (!$this->boroughs->contains($borough)) {
            $this->boroughs[] = $borough;
        }

        return $this;
    }

    public function removeBorough(Borough $borough): self
    {
        if ($this->boroughs->contains($borough)) {
            $this->boroughs->removeElement($borough);
        }

        return $this;
    }

    public function getMainBorough(): ?Borough
    {
        return $this->mainBorough;
    }

    public function setMainBorough(?Borough $mainBorough): self
    {
        $this->mainBorough = $mainBorough;

        return $this;
    }

    public function getStatistical(): ?array
    {
        return $this->statistical;
    }

    public function setStatistical(?array $statistical): self
    {
        $this->statistical = $statistical;

        return $this;
    }

    public function getNetwork(): ?array
    {
        return $this->network;
    }

    public function setNetwork(?array $network): self
    {
        $this->network = $network;

        return $this;
    }

    /**
     * @return Collection|Reduction[]
     */
    public function getReductions(): Collection
    {
        return $this->reductions;
    }

    public function addReduction(Reduction $reduction): self
    {
        if (!$this->reductions->contains($reduction)) {
            $this->reductions[] = $reduction;
        }

        return $this;
    }

    public function removeReduction(Reduction $reduction): self
    {
        if ($this->reductions->contains($reduction)) {
            $this->reductions->removeElement($reduction);
        }

        return $this;
    }

}
