<?php

namespace App\Entity;

use Cocur\Slugify\Slugify;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrganizerRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Organizer
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=Organizerkind::class, inversedBy="organizers")
     * @ORM\JoinColumn(nullable=true)
     */
    private $organizerkind;

    /**
     * @ORM\OneToMany(targetEntity=Event::class, mappedBy="organizer")
     */
    private $events;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, mappedBy="organizerEditors")
     */
    private $editors;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="organizerAffirmers")
     */
    private $affirmers;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="organizerAuthors")
     */
    private $author;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="organizer")
     */
    private $comments;

    /**
     * @ORM\ManyToMany(targetEntity=Reduction::class, mappedBy="organizers")
     */
    private $reductions;


    public function __construct()
    {
        $this->events = new ArrayCollection();
        $this->editors = new ArrayCollection();
        $this->affirmers = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->reductions = new ArrayCollection();
    }
    
    /**
     * Permet d'initialiser le slug
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * 
     * @return void
     */
    public function initializeSlug(){
        if(empty($this->slug)){
            $slugify = new Slugify();
            $this->slug = $slugify->slugify($this->name);
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        if (
            mb_strtolower($name) == $name or
            mb_strtoupper($name) == $name
        ){
            $name = str_replace('-','- ',$name);
            $name = ucwords(mb_strtolower($name));
            $name = str_replace('- ','-',$name);
            $name = str_replace(' And ',' and ',$name);
            $name = str_replace(' Et ',' et ',$name);
        }

        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getOrganizerkind(): ?Organizerkind
    {
        return $this->organizerkind;
    }

    public function setOrganizerkind(?Organizerkind $organizerkind): self
    {
        $this->organizerkind = $organizerkind;

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->setOrganizer($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->contains($event)) {
            $this->events->removeElement($event);
            // set the owning side to null (unless already changed)
            if ($event->getOrganizer() === $this) {
                $event->setOrganizer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getEditors(): Collection
    {
        return $this->editors;
    }

    public function addEditor(User $editor): self
    {
        if (!$this->editors->contains($editor)) {
            $this->editors[] = $editor;
            $editor->addOrganizerEditor($this);
        }

        return $this;
    }

    public function removeEditor(User $editor): self
    {
        if ($this->editors->contains($editor)) {
            $this->editors->removeElement($editor);
            $editor->removeOrganizerEditor($this);
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getAffirmers(): Collection
    {
        return $this->affirmers;
    }

    public function addAffirmer(User $affirmer): self
    {
        if (!$this->affirmers->contains($affirmer)) {
            $this->affirmers[] = $affirmer;
        }

        return $this;
    }

    public function removeAffirmer(User $affirmer): self
    {
        if ($this->affirmers->contains($affirmer)) {
            $this->affirmers->removeElement($affirmer);
        }

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setOrganizer($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getOrganizer() === $this) {
                $comment->setOrganizer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Reduction[]
     */
    public function getReductions(): Collection
    {
        return $this->reductions;
    }

    public function addReduction(Reduction $reduction): self
    {
        if (!$this->reductions->contains($reduction)) {
            $this->reductions[] = $reduction;
            $reduction->addOrganizer($this);
        }

        return $this;
    }

    public function removeReduction(Reduction $reduction): self
    {
        if ($this->reductions->removeElement($reduction)) {
            $reduction->removeOrganizer($this);
        }

        return $this;
    }

}
