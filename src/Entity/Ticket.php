<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TicketRepository")
 */
class Ticket
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Url(message = "L'adresse de la billetterie '{{ value }}' n'est pas valide")
     */
    private $url;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2, nullable=true)
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity=Event::class, inversedBy="tickets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $event;

    /**
     * @ORM\ManyToOne(targetEntity=Ticketing::class, inversedBy="ticket")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ticketing;

    /**
     * @ORM\OneToOne(targetEntity=TicketResource::class, mappedBy="ticket", cascade={"persist", "remove"})
     */
    private $resource;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $information;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $urlEmbed;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getEvent(): ?Event
    {
        return $this->event;
    }

    public function setEvent(?Event $event): self
    {
        $this->event = $event;

        return $this;
    }

    public function getTicketing(): ?Ticketing
    {
        return $this->ticketing;
    }

    public function setTicketing(?Ticketing $ticketing): self
    {
        $this->ticketing = $ticketing;

        return $this;
    }

    public function getResource(): ?TicketResource
    {
        return $this->resource;
    }

    public function setResource(?TicketResource $resource): self
    {
        $this->resource = $resource;

        // set (or unset) the owning side of the relation if necessary
        $newTicket = null === $resource ? null : $this;
        if ($resource->getTicket() !== $newTicket) {
            $resource->setTicket($newTicket);
        }

        return $this;
    }

    public function getInformation(): ?string
    {
        return $this->information;
    }

    public function setInformation(?string $information): self
    {
        $this->information = $information;

        return $this;
    }

    public function getUrlEmbed(): ?string
    {
        return $this->urlEmbed;
    }

    public function setUrlEmbed(?string $urlEmbed): self
    {
        $this->urlEmbed = $urlEmbed;

        return $this;
    }
}
