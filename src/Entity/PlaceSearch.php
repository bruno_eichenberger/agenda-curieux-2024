<?php

namespace App\Entity;

class PlaceSearch {

    private $nom;

    private $type;

    private $ville;


    /**
     * Get the value of nom
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set the value of nom
     *
     * @param $nom
     * @return void
     */
    public function setNom($nom): PlaceSearch
    {
        $this->nom = $nom;
        return $this;
    }

    /**
     * Get the value of type
     */ 
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set the value of type
     *
     * @param $type
     * @return void
     */ 
    public function setType($type): PlaceSearch
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Get the value of ville
     */ 
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set the value of ville
     *
     * @param $ville
     * @return void
     */ 
    public function setVille($ville): PlaceSearch
    {
        $this->ville = $ville;
        return $this;
    }

}