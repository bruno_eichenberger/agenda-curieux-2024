<?php

namespace App\Entity;

use Cocur\Slugify\Slugify;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReductionRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Reduction
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity=Prices::class, mappedBy="reduction", orphanRemoval=true)
     */
    private $prices;

    /**
     * @ORM\ManyToMany(targetEntity=Borough::class, mappedBy="reduction")
     */
    private $boroughs;

    /**
     * @ORM\ManyToMany(targetEntity=Subdomain::class, mappedBy="reductions")
     */
    private $subdomains;

    /**
     * @ORM\ManyToMany(targetEntity=Place::class, inversedBy="reductions")
     */
    private $places;

    /**
     * @ORM\ManyToMany(targetEntity=Organizer::class, inversedBy="reductions")
     */
    private $organizers;

    /**
     * @ORM\ManyToMany(targetEntity=Festival::class, inversedBy="reductions")
     */
    private $festivals;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isAlwaysNecessary;

    /**
     * @ORM\ManyToMany(targetEntity=Place::class, mappedBy="freeReductions")
     */
    private $freePlaces;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $priceReduced;

    public function __construct()
    {
        $this->prices = new ArrayCollection();
        $this->boroughs = new ArrayCollection();
        $this->subdomains = new ArrayCollection();
        $this->places = new ArrayCollection();
        $this->organizers = new ArrayCollection();
        $this->festivals = new ArrayCollection();
        $this->freePlaces = new ArrayCollection();
    }

    /**
     * Permet d'initialiser le slug
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * 
     * @return void
     */
    public function initializeSlug(){
        if(empty($this->slug)){
            $slugify = new Slugify();
            $this->slug = $slugify->slugify($this->title);
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection|Prices[]
     */
    public function getPrices(): Collection
    {
        return $this->prices;
    }

    public function addPrice(Prices $price): self
    {
        if (!$this->prices->contains($price)) {
            $this->prices[] = $price;
            $price->setReduction($this);
        }

        return $this;
    }

    public function removePrice(Prices $price): self
    {
        if ($this->prices->contains($price)) {
            $this->prices->removeElement($price);
            // set the owning side to null (unless already changed)
            if ($price->getReduction() === $this) {
                $price->setReduction(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Borough[]
     */
    public function getBoroughs(): Collection
    {
        return $this->boroughs;
    }

    public function addBorough(Borough $borough): self
    {
        if (!$this->boroughs->contains($borough)) {
            $this->boroughs[] = $borough;
            $borough->addReduction($this);
        }

        return $this;
    }

    public function removeBorough(Borough $borough): self
    {
        if ($this->boroughs->contains($borough)) {
            $this->boroughs->removeElement($borough);
            $borough->removeReduction($this);
        }

        return $this;
    }

    /**
     * @return Collection|Subdomain[]
     */
    public function getSubdomains(): Collection
    {
        return $this->subdomains;
    }

    public function addSubdomain(Subdomain $subdomain): self
    {
        if (!$this->subdomains->contains($subdomain)) {
            $this->subdomains[] = $subdomain;
            $subdomain->addReduction($this);
        }

        return $this;
    }

    public function removeSubdomain(Subdomain $subdomain): self
    {
        if ($this->subdomains->contains($subdomain)) {
            $this->subdomains->removeElement($subdomain);
            $subdomain->removeReduction($this);
        }

        return $this;
    }

    /**
     * @return Collection|Place[]
     */
    public function getPlaces(): Collection
    {
        return $this->places;
    }

    public function addPlace(Place $place): self
    {
        if (!$this->places->contains($place)) {
            $this->places[] = $place;
        }

        return $this;
    }

    public function removePlace(Place $place): self
    {
        $this->places->removeElement($place);

        return $this;
    }

    /**
     * @return Collection|Organizer[]
     */
    public function getOrganizers(): Collection
    {
        return $this->organizers;
    }

    public function addOrganizer(Organizer $organizer): self
    {
        if (!$this->organizers->contains($organizer)) {
            $this->organizers[] = $organizer;
        }

        return $this;
    }

    public function removeOrganizer(Organizer $organizer): self
    {
        $this->organizers->removeElement($organizer);

        return $this;
    }

    /**
     * @return Collection|Festival[]
     */
    public function getFestivals(): Collection
    {
        return $this->festivals;
    }

    public function addFestival(Festival $festival): self
    {
        if (!$this->festivals->contains($festival)) {
            $this->festivals[] = $festival;
        }

        return $this;
    }

    public function removeFestival(Festival $festival): self
    {
        $this->festivals->removeElement($festival);

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getIsAlwaysNecessary(): ?bool
    {
        return $this->isAlwaysNecessary;
    }

    public function setIsAlwaysNecessary(bool $isAlwaysNecessary): self
    {
        $this->isAlwaysNecessary = $isAlwaysNecessary;

        return $this;
    }

    /**
     * @return Collection|Place[]
     */
    public function getFreePlaces(): Collection
    {
        return $this->freePlaces;
    }

    public function addFreePlace(Place $freePlace): self
    {
        if (!$this->freePlaces->contains($freePlace)) {
            $this->freePlaces[] = $freePlace;
            $freePlace->addFreeReduction($this);
        }

        return $this;
    }

    public function removeFreePlace(Place $freePlace): self
    {
        if ($this->freePlaces->removeElement($freePlace)) {
            $freePlace->removeFreeReduction($this);
        }

        return $this;
    }

    public function getPriceReduced(): ?float
    {
        return $this->priceReduced;
    }

    public function setPriceReduced(?float $priceReduced): self
    {
        $this->priceReduced = $priceReduced;

        return $this;
    }
    
}
