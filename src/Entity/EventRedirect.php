<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class EventRedirect {

    private $searchRubric;

    private $searchPlace;

    private $searchCity;

    /**
     * @Assert\Date
     * @var string A "Y-m-d" formatted value
     */
    protected $searchDate;

    private $searchTitle;

    /**
     * Get the value of searchRubric
     */ 
    public function getSearchRubric()
    {
        return $this->searchRubric;
    }

    /**
     * Set the value of searchRubric
     *
     * @param $searchRubric
     * @return void
     */ 
    public function setSearchRubric($searchRubric): EventRedirect
    {
        $this->searchRubric = $searchRubric;
        return $this;
    }

    /**
     * Get the value of searchPlace
     */ 
    public function getSearchPlace()
    {
        return $this->searchPlace;
    }

    /**
     * Set the value of searchPlace
     *
     * @param $searchPlace
     * @return void
     */ 
    public function setSearchPlace($searchPlace): EventRedirect
    {
        $this->searchPlace = $searchPlace;
        return $this;
    }

    /**
     * Get the value of searchCity
     */ 
    public function getSearchCity()
    {
        return $this->searchCity;
    }

    /**
     * Set the value of searchCity
     *
     * @param $searchCity
     * @return void
     */ 
    public function setSearchCity($searchCity): EventRedirect
    {
        $this->searchCity = $searchCity;
        return $this;
    }


    /**
     * Get the value of searchTitle
     */
    public function getSearchTitle()
    {
        return $this->searchTitle;
    }

    /**
     * Set the value of searchTitle
     *
     * @param $searchTitle
     * @return void
     */
    public function setSearchTitle($searchTitle): EventRedirect
    {
        $this->searchTitle = $searchTitle;
        return $this;
    }


    /**
     * Get the value of searchDate
     */
    public function getSearchDate()
    {
        return $this->searchDate;
    }

    /**
     * Set the value of searchDate
     *
     * @param string $searchDate
     * @return void
     */
    public function setSearchDate($searchDate): EventRedirect
    {
        $this->searchDate = $searchDate;
        return $this;
    }


}