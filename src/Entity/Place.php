<?php

namespace App\Entity;

use App\Entity\User;
use DateTimeInterface;
use Cocur\Slugify\Slugify;
use App\Service\UploaderHelper;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlaceRepository")
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity(
 *  fields={"slug"},
 *  message="Ce lieu est déjà enregistré... Un autre lieu existe avec le même nom et dans la même ville"
 * )
 */
class Place
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $coverImage;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity=Event::class, mappedBy="place")
     */
    private $events;

    /**
     * @ORM\ManyToOne(targetEntity=City::class, inversedBy="place")
     * @ORM\JoinColumn(nullable=false)
     */
    private $city;

    /**
     * @ORM\ManyToOne(targetEntity=District::class, inversedBy="place")
     */
    private $district;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=7, nullable=true)
     */
    private $latitude;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=7, nullable=true)
     */
    private $longitude;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $pronoun;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity=Placekind::class, inversedBy="placekindPrimary")
     */
    private $placekindPrimary;

    /**
     * @ORM\ManyToOne(targetEntity=Placekind::class, inversedBy="placekindSecondary")
     */
    private $placekindSecondary;

    /**
     * @ORM\ManyToOne(targetEntity=Placekind::class, inversedBy="placekindTertiary")
     */
    private $placekindTertiary;

    /**
     * @ORM\ManyToOne(targetEntity=Postcode::class, inversedBy="places")
     */
    private $postcode;

    /**
     * @ORM\ManyToMany(targetEntity=Placetag::class, mappedBy="places")
     */
    private $placetags;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $oldserial;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Oldurl;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $opening = [];

    /**
     * @ORM\Column(type="simple_array", nullable=true)
     */
    private $links = [];

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mapUrl;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $streetUrl;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ip;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $transport = [];

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $dataLink;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $opinion;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $summary;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $wifi;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $lastService;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $parking;

    /**
     * @ORM\OneToMany(targetEntity=Claim::class, mappedBy="place")
     */
    private $claims;

    /**
     * @ORM\OneToMany(targetEntity=PlaceLike::class, mappedBy="place", orphanRemoval=true)
     */
    private $likes;

    /**
     * @ORM\OneToMany(targetEntity=Room::class, mappedBy="place", orphanRemoval=true)
     */
    private $rooms;

    /**
     * @ORM\OneToMany(targetEntity=TicketResource::class, mappedBy="place")
     */
    private $resources;

    /**
     * @ORM\Column(type="simple_array", nullable=true)
     */
    private $multiTitles = [];

    /**
     * @ORM\ManyToMany(targetEntity=User::class, mappedBy="placeEditors")
     */
    private $editors;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="placeAffirmers")
     */
    private $affirmers;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="placeAuthors")
     */
    private $author;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="place")
     */
    private $comments;

    /**
     * @ORM\ManyToMany(targetEntity=Reduction::class, mappedBy="places")
     */
    private $reductions;

    /**
     * @ORM\ManyToMany(targetEntity=Reduction::class, inversedBy="freePlaces")
     */
    private $freeReductions;

    //private $countOfEvents;


    public function __construct()
    {
        $this->events = new ArrayCollection();
        $this->placetags = new ArrayCollection();
        $this->claims = new ArrayCollection();
        $this->likes = new ArrayCollection();
        $this->rooms = new ArrayCollection();
        $this->resources = new ArrayCollection();
        $this->editors = new ArrayCollection();
        $this->affirmers = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->reductions = new ArrayCollection();
        $this->freeReductions = new ArrayCollection();
    }

    /**
     * Permet d'initialiser le slug
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * 
     * @return void
     */
    public function initializeSlug(){
        if(empty($this->slug)){
            $slugify = new Slugify();
            $this->slug = $slugify->slugify($this->title.'-'.$this->city->getName());
        }
    }
    
    /**
     * Permet d'initialiser la date de création et de mise à jour
     * 
     * @ORM\PrePersist
     * @ORM\PreUpdate
    */
    public function updatedTimestamps(): void
    {
        $this->setUpdatedAt(new \DateTime('now'));    
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCoverImage(): ?string
    {
        return $this->coverImage;
    }

    public function setCoverImage(?string $coverImage): self
    {
        $this->coverImage = $coverImage;

        return $this;
    }

    public function getCoverImagePath()
    {
        return 'img/'.UploaderHelper::PLACE_IMAGE.'/'.$this->getCoverImage();
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->setPlace($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->contains($event)) {
            $this->events->removeElement($event);
            // set the owning side to null (unless already changed)
            if ($event->getPlace() === $this) {
                $event->setPlace(null);
            }
        }

        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getDistrict(): ?District
    {
        return $this->district;
    }

    public function setDistrict(?District $district): self
    {
        $this->district = $district;

        return $this;
    }

    public function getLatitude()
    {
        return $this->latitude;
    }

    public function setLatitude($latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude()
    {
        return $this->longitude;
    }

    public function setLongitude($longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getPronoun(): ?string
    {
        return $this->pronoun;
    }

    public function setPronoun(?string $pronoun): self
    {
        $this->pronoun = $pronoun;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(?string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Permet de fusionner le numéro avec l'adresse
     * 
     */
    public function getPostalAddress(): ?string
    {
        return $this->number .' '. $this->address;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getPlacekindPrimary(): ?Placekind
    {
        return $this->placekindPrimary;
    }

    public function setPlacekindPrimary(?Placekind $placekindPrimary): self
    {
        $this->placekindPrimary = $placekindPrimary;

        return $this;
    }

    public function getPlacekindSecondary(): ?Placekind
    {
        return $this->placekindSecondary;
    }

    public function setPlacekindSecondary(?Placekind $placekindSecondary): self
    {
        $this->placekindSecondary = $placekindSecondary;

        return $this;
    }

    public function getPlacekindTertiary(): ?Placekind
    {
        return $this->placekindTertiary;
    }

    public function setPlacekindTertiary(?Placekind $placekindTertiary): self
    {
        $this->placekindTertiary = $placekindTertiary;

        return $this;
    }

    public function getPostcode(): ?Postcode
    {
        return $this->postcode;
    }

    public function setPostcode(?Postcode $postcode): self
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * @return Collection|Placetag[]
     */
    public function getPlacetags(): Collection
    {
        return $this->placetags;
    }

    public function addPlacetag(Placetag $placetag): self
    {
        if (!$this->placetags->contains($placetag)) {
            $this->placetags[] = $placetag;
            $placetag->addPlace($this);
        }

        return $this;
    }

    public function removePlacetag(Placetag $placetag): self
    {
        if ($this->placetags->contains($placetag)) {
            $this->placetags->removeElement($placetag);
            $placetag->removePlace($this);
        }

        return $this;
    }

    public function getOldserial(): ?int
    {
        return $this->oldserial;
    }

    public function setOldserial(?int $oldserial): self
    {
        $this->oldserial = $oldserial;

        return $this;
    }

    public function getOldurl(): ?string
    {
        return $this->Oldurl;
    }

    public function setOldurl(?string $Oldurl): self
    {
        $this->Oldurl = $Oldurl;

        return $this;
    }

    public function getOpening(): ?array
    {
        if ($this->opening){
            return $this->opening;
        } 
        else {
            return [
                'days' => [
                    'monday'    => ['open' => null,'close' => null],
                    'tuesday'   => ['open' => null,'close' => null],
                    'wednesday' => ['open' => null,'close' => null],
                    'thursday'  => ['open' => null,'close' => null],
                    'friday'    => ['open' => null,'close' => null],
                    'saturday'  => ['open' => null,'close' => null],
                    'sunday'    => ['open' => null,'close' => null]
                ],
                'update' => date('Y-m-d'),
                'onlyIfEvent' => false
            ];
        }
    }

    public function setOpening(?array $opening): self
    {
        $this->opening = $opening;

        return $this;
    }

    public function getLinks(): ?array
    {
        return $this->links;
    }

    public function setLinks(?array $links): self
    {
        $this->links = $links;

        return $this;
    }

    public function getMapUrl(): ?string
    {
        return $this->mapUrl;
    }

    public function setMapUrl(?string $mapUrl): self
    {
        $this->mapUrl = $mapUrl;

        return $this;
    }

    public function getStreetUrl(): ?string
    {
        return $this->streetUrl;
    }

    public function setStreetUrl(?string $streetUrl): self
    {
        $this->streetUrl = $streetUrl;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(?string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function getTransport(): ?array
    {
        if ($this->transport){
            return $this->transport;
        } else {
            return [
                'primary'   => ['service' => null, 'line' => null, 'stop' => null],
                'secondary' => ['service' => null, 'line' => null, 'stop' => null],
                'tertiary'  => ['service' => null, 'line' => null, 'stop' => null]
            ];
        }
    }

    public function setTransport(?array $transport): self
    {
        $this->transport = $transport;

        return $this;
    }

    public function getDataLink(): ?string
    {
        return $this->dataLink;
    }

    public function setDataLink(?string $dataLink): self
    {
        $this->dataLink = $dataLink;

        return $this;
    }

    public function getOpinion(): ?string
    {
        return $this->opinion;
    }

    public function setOpinion(?string $opinion): self
    {
        $this->opinion = $opinion;

        return $this;
    }

    public function getSummary(): ?string
    {
        return $this->summary;
    }

    public function setSummary(?string $summary): self
    {
        $this->summary = $summary;

        return $this;
    }

    public function getWifi(): ?string
    {
        return $this->wifi;
    }

    public function setWifi(?string $wifi): self
    {
        $this->wifi = $wifi;

        return $this;
    }

    public function getLastService(): ?\DateTimeInterface
    {
        return $this->lastService;
    }

    public function setLastService(?\DateTimeInterface $lastService): self
    {
        $this->lastService = $lastService;

        return $this;
    }

    public function getParking(): ?string
    {
        return $this->parking;
    }

    public function setParking(?string $parking): self
    {
        $this->parking = $parking;

        return $this;
    }

    /**
     * @return Collection|Claim[]
     */
    public function getClaims(): Collection
    {
        return $this->claims;
    }

    public function addClaim(Claim $claim): self
    {
        if (!$this->claims->contains($claim)) {
            $this->claims[] = $claim;
            $claim->setPlace($this);
        }

        return $this;
    }

    public function removeClaim(Claim $claim): self
    {
        if ($this->claims->contains($claim)) {
            $this->claims->removeElement($claim);
            // set the owning side to null (unless already changed)
            if ($claim->getPlace() === $this) {
                $claim->setPlace(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PlaceLike[]
     */
    public function getLikes(): Collection
    {
        return $this->likes;
    }

    public function addLike(PlaceLike $like): self
    {
        if (!$this->likes->contains($like)) {
            $this->likes[] = $like;
            $like->setPlace($this);
        }

        return $this;
    }

    public function removeLike(PlaceLike $like): self
    {
        if ($this->likes->contains($like)) {
            $this->likes->removeElement($like);
            // set the owning side to null (unless already changed)
            if ($like->getPlace() === $this) {
                $like->setPlace(null);
            }
        }

        return $this;
    }

    /**
     * Permet de savoir si le lieu est liké par un utilisateur
     * 
     * @param User $user
     * @return boolean
     */
    public function isLikedByUser(User $user) : bool
    {
        foreach ($this->likes as $like) {
            if ($like->getUser() === $user) return true;
        }
        return false;
    }

    /**
     * @return Collection|Room[]
     */
    public function getRooms(): Collection
    {
        return $this->rooms;
    }

    public function addRoom(Room $room): self
    {
        if (!$this->rooms->contains($room)) {
            $this->rooms[] = $room;
            $room->setPlace($this);
        }

        return $this;
    }

    public function removeRoom(Room $room): self
    {
        //dd($room);
        if ($this->rooms->contains($room)) {
            $this->rooms->removeElement($room);
            // set the owning side to null (unless already changed)
            if ($room->getPlace() === $this) {
                $room->setPlace(null);
            }
        }

        return $this;
    }

    /*
    public function getCountOfEvents(): ?int
    {
        return $this->countOfEvents;
    }

    public function CountOfEvents(?int $oldserial): self
    {
        $this->countOfEvents = $countOfEvents;

        return $this;
    }
    */

    /**
     * @return Collection|TicketResource[]
     */
    public function getResources(): Collection
    {
        return $this->resources;
    }

    public function addResource(TicketResource $resource): self
    {
        if (!$this->resources->contains($resource)) {
            $this->resources[] = $resource;
            $resource->setPlace($this);
        }

        return $this;
    }

    public function removeResource(TicketResource $resource): self
    {
        if ($this->resources->contains($resource)) {
            $this->resources->removeElement($resource);
            // set the owning side to null (unless already changed)
            if ($resource->getPlace() === $this) {
                $resource->setPlace(null);
            }
        }

        return $this;
    }

    public function getMultiTitles(): ?array
    {
        return $this->multiTitles;
    }

    public function setMultiTitles(?array $multiTitles): self
    {
        $this->multiTitles = $multiTitles;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getEditors(): Collection
    {
        return $this->editors;
    }

    public function addEditor(User $editor): self
    {
        if (!$this->editors->contains($editor)) {
            $this->editors[] = $editor;
            $editor->addPlaceEditor($this);
        }

        return $this;
    }

    public function removeEditor(User $editor): self
    {
        if ($this->editors->contains($editor)) {
            $this->editors->removeElement($editor);
            $editor->removePlaceEditor($this);
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getAffirmers(): Collection
    {
        return $this->affirmers;
    }

    public function addAffirmer(User $affirmer): self
    {
        if (!$this->affirmers->contains($affirmer)) {
            $this->affirmers[] = $affirmer;
        }

        return $this;
    }

    public function removeAffirmer(User $affirmer): self
    {
        if ($this->affirmers->contains($affirmer)) {
            $this->affirmers->removeElement($affirmer);
        }

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setPlace($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getPlace() === $this) {
                $comment->setPlace(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Reduction[]
     */
    public function getReductions(): Collection
    {
        return $this->reductions;
    }

    public function addReduction(Reduction $reduction): self
    {
        if (!$this->reductions->contains($reduction)) {
            $this->reductions[] = $reduction;
            $reduction->addPlace($this);
        }

        return $this;
    }

    public function removeReduction(Reduction $reduction): self
    {
        if ($this->reductions->removeElement($reduction)) {
            $reduction->removePlace($this);
        }

        return $this;
    }

    /**
     * @return Collection|reduction[]
     */
    public function getFreeReductions(): Collection
    {
        return $this->freeReductions;
    }

    public function addFreeReduction(reduction $freeReduction): self
    {
        if (!$this->freeReductions->contains($freeReduction)) {
            $this->freeReductions[] = $freeReduction;
        }

        return $this;
    }

    public function removeFreeReduction(reduction $freeReduction): self
    {
        $this->freeReductions->removeElement($freeReduction);

        return $this;
    }

}
