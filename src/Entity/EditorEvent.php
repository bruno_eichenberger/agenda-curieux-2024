<?php

namespace App\Entity;

use App\Repository\EditorEventRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EditorEventRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
class EditorEvent
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="editors")
     * @ORM\JoinColumn(nullable=false)
     */
    private $editor;

    /**
     * @ORM\ManyToOne(targetEntity=Event::class, inversedBy="editors")
     * @ORM\JoinColumn(nullable=false)
     */
    private $event;

    /**
     * @ORM\Column(type="datetime")
     */
    private $authorizedAt;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="authorizers")
     */
    private $authorizedBy;

    /**
     * Permet d'initialiser la date d'autorisation
     * 
     * @ORM\PrePersist
     * @ORM\PreUpdate
    */
    public function updatedTimestamps(): void
    {
        if ($this->authorizedAt === null) {
            $this->setAuthorizedAt(new \DateTime('now'));
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEditor(): ?User
    {
        return $this->editor;
    }

    public function setEditor(?User $editor): self
    {
        $this->editor = $editor;

        return $this;
    }

    public function getEvent(): ?Event
    {
        return $this->event;
    }

    public function setEvent(?Event $event): self
    {
        $this->event = $event;

        return $this;
    }

    public function getAuthorizedAt(): ?\DateTimeInterface
    {
        return $this->authorizedAt;
    }

    public function setAuthorizedAt(\DateTimeInterface $authorizedAt): self
    {
        $this->authorizedAt = $authorizedAt;

        return $this;
    }

    public function getAuthorizedBy(): ?User
    {
        return $this->authorizedBy;
    }

    public function setAuthorizedBy(?User $authorizedBy): self
    {
        $this->authorizedBy = $authorizedBy;

        return $this;
    }

}
