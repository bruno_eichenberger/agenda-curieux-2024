<?php

namespace App\Entity;

use Cocur\Slugify\Slugify;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BoroughRepository")
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity(
 *  fields={"slug"},
 *  message="Un autre arrondissement possède déjà ce titre, merci de le modifier"
 * )
 */
class Borough
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $resident;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $chiefTown;

    /**
     * @ORM\OneToMany(targetEntity=City::class, mappedBy="borough")
     */
    private $city;

    /**
     * @ORM\ManyToOne(targetEntity=Department::class, inversedBy="borough")
     * @ORM\JoinColumn(nullable=false)
     */
    private $department;

    /**
     * @ORM\ManyToMany(targetEntity=Reduction::class, inversedBy="boroughs")
     */
    private $reduction;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=7, nullable=true)
     */
    private $latitude;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=7, nullable=true)
     */
    private $longitude;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $zoom;

    /**
     * @ORM\ManyToMany(targetEntity=Subdomain::class, mappedBy="boroughs")
     */
    private $subdomains;

    /**
     * @ORM\OneToOne(targetEntity=Subdomain::class, mappedBy="mainBorough", cascade={"persist", "remove"})
     */
    private $mainSubdomain;

    public function __construct()
    {
        $this->city = new ArrayCollection();
        $this->reduction = new ArrayCollection();
        $this->subdomains = new ArrayCollection();
    }

    /**
     * Permet d'initialiser le slug
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * 
     * @return void
     */
    public function initializeSlug(){
        if(empty($this->slug)){
            $slugify = new Slugify();
            $this->slug = $slugify->slugify($this->chiefTown);
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getResident(): ?string
    {
        return $this->resident;
    }

    public function setResident(?string $resident): self
    {
        $this->resident = $resident;

        return $this;
    }

    public function getChiefTown(): ?string
    {
        return $this->chiefTown;
    }

    public function setChiefTown(string $chiefTown): self
    {
        $this->chiefTown = $chiefTown;

        return $this;
    }

    /**
     * @return Collection|City[]
     */
    public function getCity(): Collection
    {
        return $this->city;
    }

    public function addCity(City $city): self
    {
        if (!$this->city->contains($city)) {
            $this->city[] = $city;
            $city->setBorough($this);
        }

        return $this;
    }

    public function removeCity(City $city): self
    {
        if ($this->city->contains($city)) {
            $this->city->removeElement($city);
            // set the owning side to null (unless already changed)
            if ($city->getBorough() === $this) {
                $city->setBorough(null);
            }
        }

        return $this;
    }

    public function getDepartment(): ?Department
    {
        return $this->department;
    }

    public function setDepartment(?Department $department): self
    {
        $this->department = $department;

        return $this;
    }

    /**
     * @return Collection|Reduction[]
     */
    public function getReduction(): Collection
    {
        return $this->reduction;
    }

    public function addReduction(Reduction $reduction): self
    {
        if (!$this->reduction->contains($reduction)) {
            $this->reduction[] = $reduction;
        }

        return $this;
    }

    public function removeReduction(Reduction $reduction): self
    {
        if ($this->reduction->contains($reduction)) {
            $this->reduction->removeElement($reduction);
        }

        return $this;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function setLatitude(?string $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function setLongitude(?string $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getZoom(): ?int
    {
        return $this->zoom;
    }

    public function setZoom(?int $zoom): self
    {
        $this->zoom = $zoom;

        return $this;
    }

    /**
     * @return Collection|Subdomain[]
     */
    public function getSubdomains(): Collection
    {
        return $this->subdomains;
    }

    public function addSubdomain(Subdomain $subdomain): self
    {
        if (!$this->subdomains->contains($subdomain)) {
            $this->subdomains[] = $subdomain;
            $subdomain->addBorough($this);
        }

        return $this;
    }

    public function removeSubdomain(Subdomain $subdomain): self
    {
        if ($this->subdomains->contains($subdomain)) {
            $this->subdomains->removeElement($subdomain);
            $subdomain->removeBorough($this);
        }

        return $this;
    }

    public function getMainSubdomain(): ?Subdomain
    {
        return $this->mainSubdomain;
    }

    public function setMainSubdomain(?Subdomain $mainSubdomain): self
    {
        $this->mainSubdomain = $mainSubdomain;

        // set (or unset) the owning side of the relation if necessary
        $newMainBorough = null === $mainSubdomain ? null : $this;
        if ($mainSubdomain->getMainBorough() !== $newMainBorough) {
            $mainSubdomain->setMainBorough($newMainBorough);
        }

        return $this;
    }
}