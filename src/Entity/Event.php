<?php

namespace App\Entity;

use finfo;
use DateTime;
use App\Entity\User;
use Cocur\Slugify\Slugify;
use App\Service\UploaderHelper;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/*
 * @UniqueEntity(
 *  fields={"title"},
 *  message="Un autre évènement possède déjà ce titre, merci de le modifier"
 * )
*/

//itemOperations={"GET"={"path"="/event/{id}"}}
/**
 * @ORM\Entity(repositoryClass="App\Repository\EventRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Event
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Veuillez indiquer le titre")
     * @Assert\Length(min=3, max=120, minMessage="Le titre doit faire plus de 2 caractères !", maxMessage="Le titre doit faire maximum 120 caractères !")
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $price;

    //private $textPrice;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="Vous devez ajouter une description !")
     * @Assert\Length(min=10, minMessage="La description doit faire plus de 10 caractères !")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $coverImage;

    /**
     * @ORM\OneToMany(targetEntity=Image::class, mappedBy="event", orphanRemoval=true)
     */
    private $images;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="event")
     */
    private $comments;

    //@ApiSubresource
    /**
     * @ORM\ManyToOne(targetEntity=Place::class, inversedBy="events")
     * 
     */
    private $place;

    /**
     * @ORM\ManyToOne(targetEntity=Festival::class, inversedBy="events")
     */
    private $festival;

    /**
     * @ORM\OneToMany(targetEntity=Video::class, mappedBy="event", orphanRemoval=true)
     */
    private $videos;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /* @Assert/Date(message="La date doit être au format YYYY-MM-DD") */

    /**
     * @ORM\Column(type="date")
     * @Assert\NotBlank(message="Veuillez indiquer la date de début")
     */
    private $dateStart;

    /**
     * @ORM\Column(type="date")
     * @Assert\GreaterThanOrEqual(propertyPath="dateStart", message="Veuillez indiquer une date de fin supérieur ou égal à la date de début")
     */
    private $dateEnd;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $timeStart;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $timeEnd;

    /**
     * @ORM\ManyToOne(targetEntity=Rubric::class, inversedBy="events")
     * @ORM\JoinColumn(nullable=false)
     */
    private $rubric;

    /**
     * @ORM\ManyToMany(targetEntity=Category::class, inversedBy="events")
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity=City::class, inversedBy="event")
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $placeTemporary;

    /**
     * @ORM\OneToMany(targetEntity=Prices::class, mappedBy="event", orphanRemoval=true)
     * @Assert\Valid()
     */
    private $prices;

    /**
     * @ORM\OneToMany(targetEntity=Ticket::class, mappedBy="event", orphanRemoval=true)
     * @Assert\Valid()
     */
    private $tickets;

    /**
     * @ORM\ManyToOne(targetEntity=Organizer::class, inversedBy="events")
     */
    private $organizer;

    /**
     * @ORM\ManyToMany(targetEntity=Artist::class, mappedBy="events")
     */
    private $artists;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $oldserial;

    /**
     * @ORM\OneToMany(targetEntity=Claim::class, mappedBy="event")
     */
    private $claims;

    /**
     * @ORM\OneToMany(targetEntity=EventLike::class, mappedBy="event", orphanRemoval=true)
     */
    private $eventLikes;

    private $sameDateTime;

    /**
     * @ORM\ManyToOne(targetEntity=Room::class, inversedBy="events")
     */
    private $rooms;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mailAnonymous;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ipAnonymous;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $secretAnonymous;

    /**
     * @ORM\Column(type="simple_array", nullable=true)
     */
    private $dateMultiple = [];

    private $dateText = [
        "primary" => null,
        "secondary" => null
    ];

    /**
     * @ORM\Column(type="simple_array", nullable=true)
     */
    private $links = [];

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $timeOpen;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, mappedBy="eventEditors")
     */
    private $editors;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="eventAuthors")
     */
    private $author;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isValidated;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isCanceled;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isPublished;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isSpam;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isMistake;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isDuplicated;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isFull;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isPostponed;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $precisionDate;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $precisionPrice;

    /**
     * @ORM\Column(type="simple_array", nullable=true)
     */
    private $choiceOfDays = [];

    /*
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $validatedAt;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="eventsVerified")
     */
    private $verifiedBy;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $VerifiedAt;

    public function __construct()
    {
        $this->images = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->videos = new ArrayCollection();
        $this->category = new ArrayCollection();
        $this->prices = new ArrayCollection();
        $this->tickets = new ArrayCollection();
        $this->artists = new ArrayCollection();
        $this->claims = new ArrayCollection();
        $this->eventLikes = new ArrayCollection();
        $this->editors = new ArrayCollection();
    }

    /**
     * Permet d'initialiser le slug
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * 
     * @return void
     */
    public function initializeSlug(){
        if(empty($this->slug)){
            $slugify = new Slugify();
            $this->slug = $slugify->slugify($this->title);
        }
    }

    /**
     * Permet d'initialiser la date de fin
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * 
     * @return void
     */
    public function initializeDate()
    {
        if ($this->dateEnd === null && $this->dateStart !== null ) {
            $this->dateEnd = $this->dateStart;
        }
    }

    /**
     * Permet d'initialiser la date de création et de mise à jour
     * 
     * @ORM\PrePersist
     * @ORM\PreUpdate
    */
    public function updatedTimestamps(): void
    {
        $this->setUpdatedAt(new \DateTime('now'));    
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /*
    public function getTextPrice(): ?float
    {
        return $this->textPrice;
    }
    */
    
    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        //$description = html_entity_decode($description);
        //$description = htmlspecialchars_decode($description);
        //$description = htmlspecialchars($description, ENT_COMPAT,'ISO-8859-15', true);
        //dd($description);
        $this->description = $description;

        return $this;
    }

    public function setCoverImageByUrl($url, $slug)
    {
        $file = file_get_contents($url);
        $file_info = new finfo(FILEINFO_MIME);
        $mime_type = $file_info->buffer($file);
        if($mime_type == "image/jpeg; charset=binary"){$extension = "jpg";} else {$extension = "png";}
        $nameOfFile = date("Y").'/'.date("m").'/'.uniqid().'-'.$slug. '.' .$extension;
        $destination = 'img/'.UploaderHelper::EVENT_IMAGE. '/' .$nameOfFile;
        file_put_contents($destination, $file);

        $this->coverImage = $nameOfFile;

    }

    public function getCoverImage(): ?string
    {
        return $this->coverImage;
    }

    public function setCoverImage(?string $coverImage): self
    {
        $this->coverImage = $coverImage;

        return $this;
    }

    public function getCoverImagePath()
    {
        return 'img/'.UploaderHelper::EVENT_IMAGE.'/'.$this->getCoverImage();
    }

    /**
     * @return Collection|Image[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Image $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setEvent($this);
        }

        return $this;
    }

    public function removeImage(Image $image): self
    {
        if ($this->images->contains($image)) {
            $this->images->removeElement($image);
            // set the owning side to null (unless already changed)
            if ($image->getEvent() === $this) {
                $image->setEvent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setEvent($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getEvent() === $this) {
                $comment->setEvent(null);
            }
        }

        return $this;
    }

    public function getPlace(): ?Place
    {
        return $this->place;
    }

    public function setPlace(?Place $place): self
    {
        $this->place = $place;

        return $this;
    }

    public function getFestival(): ?Festival
    {
        return $this->festival;
    }

    public function setFestival(?Festival $festival): self
    {
        $this->festival = $festival;

        return $this;
    }

    /**
     * @return Collection|Video[]
     */
    public function getVideos(): Collection
    {
        return $this->videos;
    }

    public function addVideo(Video $video): self
    {
        if (!$this->videos->contains($video)) {
            $this->videos[] = $video;
            $video->setEvent($this);
        }

        return $this;
    }

    public function removeVideo(Video $video): self
    {
        if ($this->videos->contains($video)) {
            $this->videos->removeElement($video);
            // set the owning side to null (unless already changed)
            if ($video->getEvent() === $this) {
                $video->setEvent(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDateStart(): ?\DateTimeInterface
    {
        return $this->dateStart;
    }

    public function setDateStart(\DateTimeInterface $dateStart): self
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    public function getDateEnd(): ?\DateTimeInterface
    {
        return $this->dateEnd;
    }

    public function setDateEnd(\DateTimeInterface $dateEnd = NULL): self
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    public function getTimeStart(): ?\DateTimeInterface
    {
        return $this->timeStart;
    }

    public function setTimeStart(?\DateTimeInterface $timeStart): self
    {
        $this->timeStart = $timeStart;

        return $this;
    }

    public function getTimeEnd(): ?\DateTimeInterface
    {
        return $this->timeEnd;
    }

    public function setTimeEnd(?\DateTimeInterface $timeEnd): self
    {
        $this->timeEnd = $timeEnd;

        return $this;
    }

    public function getRubric(): ?Rubric
    {
        return $this->rubric;
    }

    public function setRubric(?Rubric $rubric): self
    {
        $this->rubric = $rubric;

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategory(): Collection
    {
        return $this->category;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->category->contains($category)) {
            $this->category[] = $category;
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->category->contains($category)) {
            $this->category->removeElement($category);
        }

        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getPlaceTemporary(): ?string
    {
        return $this->placeTemporary;
    }

    public function setPlaceTemporary(?string $placeTemporary): self
    {
        if (
            mb_strtolower($placeTemporary) == $placeTemporary or
            mb_strtoupper($placeTemporary) == $placeTemporary
        ){
            $placeTemporary = ucwords(mb_strtolower($placeTemporary));
        }

        $this->placeTemporary = $placeTemporary;

        return $this;
    }

    /**
     * @return Collection|Prices[]
     */
    public function getPrices(): Collection
    {
        return $this->prices;
    }

    public function addPrice(Prices $price): self
    {
        if (!$this->prices->contains($price)) {
            $this->prices[] = $price;
            $price->setEvent($this);
        }

        return $this;
    }

    public function removePrice(Prices $price): self
    {
        if ($this->prices->contains($price)) {
            $this->prices->removeElement($price);
            // set the owning side to null (unless already changed)
            if ($price->getEvent() === $this) {
                $price->setEvent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Ticket[]
     */
    public function getTickets(): Collection
    {
        return $this->tickets;
    }

    public function addTicket(Ticket $ticket): self
    {
        if (!$this->tickets->contains($ticket)) {
            $this->tickets[] = $ticket;
            $ticket->setEvent($this);
        }

        return $this;
    }

    public function removeTicket(Ticket $ticket): self
    {
        if ($this->tickets->contains($ticket)) {
            $this->tickets->removeElement($ticket);
            // set the owning side to null (unless already changed)
            if ($ticket->getEvent() === $this) {
                $ticket->setEvent(null);
            }
        }

        return $this;
    }

    public function getOrganizer(): ?Organizer
    {
        return $this->organizer;
    }

    public function setOrganizer(?Organizer $organizer): self
    {
        $this->organizer = $organizer;

        return $this;
    }

    /**
     * @return Collection|Artist[]
     */
    public function getArtists(): Collection
    {
        return $this->artists;
    }

    public function addArtist(Artist $artist): self
    {
        if (!$this->artists->contains($artist)) {
            $this->artists[] = $artist;
            $artist->addEvent($this);
        }

        return $this;
    }

    public function removeArtist(Artist $artist): self
    {
        if ($this->artists->contains($artist)) {
            $this->artists->removeElement($artist);
            $artist->removeEvent($this);
        }

        return $this;
    }

    public function getOldserial(): ?string
    {
        return $this->oldserial;
    }

    public function setOldserial(?string $oldserial): self
    {
        $this->oldserial = $oldserial;

        return $this;
    }

    /**
     * @return Collection|Claim[]
     */
    public function getClaims(): Collection
    {
        return $this->claims;
    }

    public function addClaim(Claim $claim): self
    {
        if (!$this->claims->contains($claim)) {
            $this->claims[] = $claim;
            $claim->setEvent($this);
        }

        return $this;
    }

    public function removeClaim(Claim $claim): self
    {
        if ($this->claims->contains($claim)) {
            $this->claims->removeElement($claim);
            // set the owning side to null (unless already changed)
            if ($claim->getEvent() === $this) {
                $claim->setEvent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EventLike[]
     */
    public function getEventLikes(): Collection
    {
        return $this->eventLikes;
    }

    public function addEventLike(EventLike $eventLike): self
    {
        if (!$this->eventLikes->contains($eventLike)) {
            $this->eventLikes[] = $eventLike;
            $eventLike->setEvent($this);
        }

        return $this;
    }

    public function removeEventLike(EventLike $eventLike): self
    {
        if ($this->eventLikes->contains($eventLike)) {
            $this->eventLikes->removeElement($eventLike);
            // set the owning side to null (unless already changed)
            if ($eventLike->getEvent() === $this) {
                $eventLike->setEvent(null);
            }
        }

        return $this;
    }

    /**
     * Permet de savoir si l'évènement est liké par un utilisateur
     * 
     * @param User $user
     * @return boolean
     */
    public function isLikedByUser(User $user) : bool
    {
        foreach ($this->eventLikes as $like) {
            if ($like->getUser() === $user) return true;
        }
        return false;
    }


    /**
     * Permet de savoir comment afficher
     */
    public function getSameDateTime(): ?bool
    {
        return $this->sameDateTime;
    }

    public function setSameDateTime(?bool $sameDateTime): self
    {
        $this->sameDateTime = $sameDateTime;

        return $this;
    }

    public function getRooms(): ?Room
    {
        return $this->rooms;
    }

    public function setRooms(?Room $rooms): self
    {
        $this->rooms = $rooms;

        return $this;
    }

    public function getMailAnonymous(): ?string
    {
        return $this->mailAnonymous;
    }

    public function setMailAnonymous(?string $mailAnonymous): self
    {
        $this->mailAnonymous = $mailAnonymous;

        return $this;
    }

    public function getIpAnonymous(): ?string
    {
        return $this->ipAnonymous;
    }

    public function setIpAnonymous(?string $ipAnonymous): self
    {
        $this->ipAnonymous = $ipAnonymous;

        return $this;
    }

    public function getSecretAnonymous(): ?string
    {
        return $this->secretAnonymous;
    }

    public function setSecretAnonymous(?string $secretAnonymous): self
    {
        $this->secretAnonymous = $secretAnonymous;

        return $this;
    }

    public function getDateMultipleArray(): ?array
    {
        return $this->dateMultiple;
    }

    public function getDateMultiple(): ?string
    {
        $newTabDate = [];
        foreach($this->dateMultiple as $day){
            $newTabDate[] = DateTime::createFromFormat('Y-m-d', $day)->format('d/m/Y');
        }

        return implode( ', ', $newTabDate);
    }

    public function setDateMultiple(?string $dateMultiple): self
    {
        $newTabDate = [];
        $dateBegin = new DateTime($this->dateStart->format('Y-m-d'));

        /*
        if ($this->precisionDate == "ALL_DAYS"){
            $nextDay = $dateBegin->modify('+1 day');
            for($i = 1; $nextDay < $this->dateEnd; $i++){
                $newTabDate[] = $nextDay->format('Y-m-d');
                $nextDay = $dateBegin->modify('+1 day');
            }
        }*/
        if ($this->precisionDate == "DAYS_OPENING"){
            //dd($this->choiceOfDays);
            $nextDay = $dateBegin->modify('+1 day');
            for($i = 1; $nextDay < $this->dateEnd; $i++){
                //dd($nextDay->format('l'));
                if (in_array($nextDay->format('l'),$this->choiceOfDays)){
                    $newTabDate[] = $nextDay->format('Y-m-d');
                }
                $nextDay = $dateBegin->modify('+1 day');
            }
        }
        elseif ($this->precisionDate == "DATES_OPENING") {
            $tabDateMultiple = explode(", ",$dateMultiple);
            foreach($tabDateMultiple as $day){
                $daySearch = DateTime::createFromFormat('d/m/Y', $day);
                if ($this->dateStart < $daySearch and $this->dateEnd > $daySearch){
                    $newTabDate[] = $daySearch->format('Y-m-d');
                }
            }
        }

        $this->dateMultiple = $newTabDate;

        return $this;
    }

    public function setDateTimeMultiple(?string $dateMultiple): self
    {
        $tabDateMultiple = explode(", ",$dateMultiple);
        $newTabDate = [];
        foreach($tabDateMultiple as $day){
            $daySearch = DateTime::createFromFormat('d/m/Y', $day);
            if ($this->dateStart < $daySearch and $this->dateEnd > $daySearch){$newTabDate[] = $daySearch->format('Y-m-d');}
        }

        $this->dateMultiple = $newTabDate;

        return $this;
    }

    public function setDateTimeMultipleArray(?array $dateMultiple): self
    {
        $newTabDate = [];
        foreach($dateMultiple as $day){
            $daySearch = DateTime::createFromFormat('Y-m-d H:i', $day)->format('Y-m-d');
            if ($this->dateStart->format('Y-m-d') < $daySearch and $this->dateEnd->format('Y-m-d') > $daySearch){$newTabDate[] = $daySearch;}
        }
        $this->dateMultiple = $newTabDate;

        return $this;
    }

    public function getTimeOpen(): ?string
    {
        return $this->timeOpen;
    }

    public function setTimeOpen(?string $timeOpen): self
    {
        $this->timeOpen = $timeOpen;

        return $this;
    }

    public function getLinks(): ?array
    {
        return $this->links;
    }

    public function setLinks(?array $links): self
    {
        $this->links = $links;

        return $this;
    }
    
    /**
     * @return Collection|User[]
     */
    public function getEditors(): Collection
    {
        return $this->editors;
    }

    public function addEditor(User $editor): self
    {
        if (!$this->editors->contains($editor)) {
            $this->editors[] = $editor;
            $editor->addEventEditor($this);
        }

        return $this;
    }

    public function removeEditor(User $editor): self
    {
        if ($this->editors->contains($editor)) {
            $this->editors->removeElement($editor);
            $editor->removeEventEditor($this);
        }

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getIsValidated(): ?bool
    {
        return $this->isValidated;
    }

    public function setIsValidated(?bool $isValidated): self
    {
        $this->isValidated = $isValidated;

        return $this;
    }

    public function getIsCanceled(): ?bool
    {
        return $this->isCanceled;
    }

    public function setIsCanceled(?bool $isCanceled): self
    {
        $this->isCanceled = $isCanceled;

        return $this;
    }

    public function getIsPublished(): ?bool
    {
        return $this->isPublished;
    }

    public function setIsPublished(?bool $isPublished): self
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    public function getIsSpam(): ?bool
    {
        return $this->isSpam;
    }

    public function setIsSpam(?bool $isSpam): self
    {
        $this->isSpam = $isSpam;

        return $this;
    }

    public function getIsMistake(): ?bool
    {
        return $this->isMistake;
    }

    public function setIsMistake(?bool $isMistake): self
    {
        $this->isMistake = $isMistake;

        return $this;
    }

    public function getIsDuplicated(): ?bool
    {
        return $this->isDuplicated;
    }

    public function setIsDuplicated(?bool $isDuplicated): self
    {
        $this->isDuplicated = $isDuplicated;

        return $this;
    }

    public function getIsFull(): ?bool
    {
        return $this->isFull;
    }

    public function setIsFull(?bool $isFull): self
    {
        $this->isFull = $isFull;

        return $this;
    }

    public function getIsPostponed(): ?bool
    {
        return $this->isPostponed;
    }

    public function setIsPostponed(?bool $isPostponed): self
    {
        $this->isPostponed = $isPostponed;

        return $this;
    }

    public function getPrecisionDate(): ?string
    {
        return $this->precisionDate;
    }

    public function setPrecisionDate(?string $precisionDate): self
    {
        $this->precisionDate = $precisionDate;

        return $this;
    }

    public function getPrecisionPrice(): ?string
    {
        return $this->precisionPrice;
    }

    public function setPrecisionPrice(?string $precisionPrice): self
    {
        $this->precisionPrice = $precisionPrice;

        return $this;
    }

    public function getChoiceOfDays(): ?array
    {
        return $this->choiceOfDays;
    }

    public function setChoiceOfDays(?array $choiceOfDays): self
    {
        $this->choiceOfDays = $choiceOfDays;

        return $this;
    }

    public function getDateText(): ?array
    {
        return $this->dateText;
    }
    
    public function setDateText(?array $dateText): self
    {
        $this->dateText = $dateText;

        return $this;
    }

/*
    /**
     * @return Array
     * /
    public function getDateText(): array
    {
        $week = ["dimanche","lundi","mardi","mercredi","jeudi","vendredi","samedi","dimanche"];
        $month = ["décembre","janvier","février","mars","avril","mai","juin","juillet","août","septembre","octobre","novembre","décembre"];

        $text = ['primary' => null, 'secondary' => null];

        if ($this->timeStart and $this->timeEnd){
            $timePrecision = " de ";
            if ($this->timeStart->format('i') == '00'){
                $timePrecision .= $this->timeStart->format('H\\h');
            } else {
                $timePrecision .= $this->timeStart->format('H\\hi');
            }
            $timePrecision .= " à ";
            if ($this->timeEnd->format('i') == '00'){
                $timePrecision .= $this->timeEnd->format('H\\h');
            } else {
                $timePrecision .= $this->timeEnd->format('H\\hi');
            }
        } 
        elseif ($this->timeStart){
            $timePrecision = " à partir de ";
            if ($this->timeStart->format('i') == '00'){
                $timePrecision .= $this->timeStart->format('H\\h');
            } else {
                $timePrecision .= $this->timeStart->format('H\\hi');
            }
        } 
        elseif ($this->timeEnd){
            $timePrecision = " jusqu'à ";
            if ($this->timeEnd->format('i') == '00'){
                $timePrecision .= $this->timeEnd->format('H\\h');
            } else {
                $timePrecision .= $this->timeEnd->format('H\\hi');
            }
        }
        else {$timePrecision = "";}

        // Sur une journée
        if ($this->dateStart == $this->dateEnd){
            if ($this->dateStart == new DateTime('today')){
                $text['primary'] = "Aujourd'hui le ";
            } elseif ($this->dateStart == new DateTime('tomorrow')) {
                $text['primary'] = "Demain, le ";
            } else {
                $text['primary'] = "Le ";
            }
            $text['primary'] .= $week[$this->dateStart->format('w')] ." ";
            $text['primary'] .= $this->dateStart->format('j') ." ";
            $text['primary'] .= $month[$this->dateStart->format('n')] ." ";
            $text['primary'] .= $this->dateStart->format('Y');
            $text['primary'] .= $timePrecision;
        }

        // Si dates périodiques
        elseif ($this->precisionDate == 'DATES_OPENING' and $this->dateMultiple){

            $tabFullDates = $this->dateMultiple;
            $tabFullDates[] = $this->dateStart->format('Y-m-d');
            $tabFullDates[] = $this->dateEnd->format('Y-m-d');
            sort($tabFullDates);

            $listOfDate = [];

            foreach($tabFullDates as $d){
                $day = new DateTime($d);
                if ($day->format('Y-m-d') >= date('Y-m-d')){
                    $textDate = $week[$day->format('w')] ." ";
                    $textDate .= $day->format('j') ." ";
                    $textDate .= $month[$day->format('n')] ." ";
                    $textDate .= $day->format('Y');
                    if ($text['primary'] == []){
                        if ($day->format('Y-m-d') == date('Y-m-d')){
                            $text['primary'] .= "À partir d'aujourd'hui, le ". $textDate;
                        } else {
                            $text['primary'] .= "Prochaine date le ". $textDate;
                        }
                    }
                    $tabOfDates[] = "<li>". ucfirst($textDate) ."</li>";
                }
            }
            $text['primary'] .= $timePrecision;
            $text['secondary'] .= "<p class='mt-3'><u>Les ". count($tabOfDates) ." dates à venir :</u><ul>". implode($tabOfDates) ."</ul></p>";
        }

        // Sur plusieurs jours
        else {

            if ($this->precisionDate == 'ALL_DAYS'){
                $textPrecision = "Ouvert tous les jours";
            }
            elseif ($this->precisionDate == 'DAYS_OPENING'){
                
                $tabDays[1] = in_array('Monday', $this->choiceOfDays);
                $tabDays[2] = in_array('Tuesday', $this->choiceOfDays);
                $tabDays[3] = in_array('Wednesday', $this->choiceOfDays);
                $tabDays[4] = in_array('Thursday', $this->choiceOfDays);
                $tabDays[5] = in_array('Friday', $this->choiceOfDays);
                $tabDays[6] = in_array('Saturday', $this->choiceOfDays);
                $tabDays[7] = in_array('Sunday', $this->choiceOfDays);

                $i = 0;
                $period = [];
                $lastValue = false;
                foreach($tabDays as $key => $value){
                    if ($lastValue and $lastValue != $value){$i++;}
                    if ($value){$period[$i][] = $key;}
                    $lastValue = $value;
                }

                if (count($period) == 1){
                    if (count($period[0]) == 1){
                        $textPrecision = "Tous les ". $week[$period[0][0]] ."s";
                    }
                    elseif (count($period[0]) == 2){
                        $textPrecision = "Les ". $week[$period[0][0]] ."s et ". $week[$period[0][1]] ."s";
                    }
                    else {
                        $textPrecision = "Du ". $week[$period[0][0]] ." au ". $week[end($period[0])];
                    }
                }
                elseif (count($period) >= 2){

                    foreach($tabDays as $day => $bol){
                        if ($bol){
                            $daysByOrder[] = $day;
                        }
                    }

                    $textPrecision = "Tous les";
                    $k = 1;
                    $count = count($daysByOrder);

                    foreach($daysByOrder as $day){
                        if ($count == $k) {$textPrecision .= " et";}
                        elseif ($k != 1 and $count != $k ){$textPrecision .= ",";}
                        
                        if ($day == 1){$textPrecision .= " lundis";}
                        elseif ($day == 2){$textPrecision .= " mardis";}
                        elseif ($day == 3){$textPrecision .= " mercredis";}
                        elseif ($day == 4){$textPrecision .= " jeudis";}
                        elseif ($day == 5){$textPrecision .= " vendredis";}
                        elseif ($day == 6){$textPrecision .= " samedis";}
                        elseif ($day == 7){$textPrecision .= " dimanches";}
                        $k++;
                    }
                }

            }

            if (isset($textPrecision) and
                $this->getDateEnd == new DateTime('today')){
                $text['primary'] = "Termine aujourd'hui".$timePrecision.", commencé le ";
                $text['primary'] .= $week[$this->dateStart->format("w")]." ";
                $text['primary'] .= $this->dateStart->format('j')." ";
                $text['primary'] .= $month[$this->dateStart->format('n')]." ";
                $text['primary'] .= $this->dateStart->format('Y');
            }
            elseif (
                isset($textPrecision) and
                $this->dateStart->format('Y-m-d') <= date('Y-m-d') and 
                $this->dateEnd->format('Y-m-d') >= date('Y-m-d')
            ){
                $text['primary'] = $textPrecision."".$timePrecision;
                $text['primary'] .= " jusqu'au ";
                $text['primary'] .= $week[$this->dateEnd->format("w")]." ";
                $text['primary'] .= $this->dateEnd->format('j')." ";
                $text['primary'] .= $month[$this->dateEnd->format('n')]." ";
                $text['primary'] .= $this->dateEnd->format('Y');
                
                if ($this->dateStart == new DateTime('today')){
                    $text['secondary'] .= "Commence aujourd'hui";
                } else {
                    $text['secondary'] .= "Commencé le ";
                    $text['secondary'] .= $week[$this->dateStart->format("w")]." ";
                    $text['secondary'] .= $this->dateStart->format('j')." ";
                    $text['secondary'] .= $month[$this->dateStart->format('n')]." ";
                    $text['secondary'] .= $this->dateStart->format('Y');
                }
            } 
            else {
                $text['primary'] = "Du ";
                $text['primary'] .= $week[$this->dateStart->format("w")]." ";
                $text['primary'] .= $this->dateStart->format('j')." ";
                if ($this->dateStart->format('Y') != $this->dateEnd->format('Y')){
                    $text['primary'] .= $month[$this->dateStart->format('n')]." ";
                    $text['primary'] .= $this->dateStart->format('Y')." ";
                } elseif ($this->dateStart->format('n') != $this->dateEnd->format('n')){
                    $text['primary'] .= $month[$this->dateStart->format('n')]." ";
                }
                $text['primary'] .= "au ";
                $text['primary'] .= $week[$this->dateEnd->format("w")]." ";
                $text['primary'] .= $this->dateEnd->format('j')." ";
                $text['primary'] .= $month[$this->dateEnd->format('n')]." ";
                $text['primary'] .= $this->dateEnd->format('Y');
                if (isset($textPrecision)){$text['primary'] .= " - ". $textPrecision;}
                $text['primary'] .= $timePrecision;
            }
        }

        return $text;

/*
{% if event.timeStart and event.timeEnd %}
    <span>de
    {% if event.timeStart|date('i') == '00' %}
        {{ event.timeStart|date('H\\h') }} 
    {% else %}
        {{ event.timeStart|date('H\\hi') }} 
    {% endif %}
    à
    {% if event.timeEnd|date('i') == '00' %}
        {{ event.timeEnd|date('H\\h') }}
    {% else %}
        {{ event.timeEnd|date('H\\hi') }} 
    {% endif %}
    </span>
{% elseif event.timeStart %}
    <span>à partir de
    {% if event.timeStart|date('i') == '00' %}
        {{ event.timeStart|date('H\\h') }} 
    {% else %}
        {{ event.timeStart|date('H\\hi') }} 
    {% endif %}
    </span>
{% elseif event.timeEnd %}
    <span>jusqu'à
    {% if event.timeEnd|date('i') == '00' %}
        {{ event.timeEnd|date('H\\h') }} 
    {% else %}
        {{ event.timeEnd|date('H\\hi') }} 
    {% endif %}
    </span>
{% endif %}



    }

*/

    public function getValidatedAt(): ?\DateTimeInterface
    {
        return $this->validatedAt;
    }

    public function setValidatedAt(?\DateTimeInterface $validatedAt): self
    {
        $this->validatedAt = $validatedAt;

        return $this;
    }

    public function getVerifiedBy(): ?User
    {
        return $this->verifiedBy;
    }

    public function setVerifiedBy(?User $verifiedBy): self
    {
        $this->verifiedBy = $verifiedBy;

        return $this;
    }

    public function getVerifiedAt(): ?\DateTimeInterface
    {
        return $this->VerifiedAt;
    }

    public function setVerifiedAt(?\DateTimeInterface $VerifiedAt): self
    {
        $this->VerifiedAt = $VerifiedAt;

        return $this;
    }
}
