<?php

namespace App\Entity;

use Cocur\Slugify\Slugify;
use App\Service\UploaderHelper;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(
 *  fields={"email"},
 *  message="Un utilisateur utilise déjà cette adresse email"
 * )  
 */
class User implements  UserInterface, PasswordAuthenticatedUserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Veuillez indiquer le prénom")
     * @Assert\Length(min=3, max=255, minMessage="Le prénom doit faire plus de 2 caractères !", maxMessage="Le prénom doit faire maximum 255 caractères !")
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Veuillez indiquer le nom")
     * @Assert\Length(min=3, max=255, minMessage="Le nom de famille doit faire plus de 2 caractères !", maxMessage="Le nom de famille doit faire maximum 255 caractères !")
     */
    private $lastName;

    private $fullName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Email(message="Veuillez indiquer une adresse email valide")
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $avatar;

    /**
     * @Assert\Regex(
     * pattern = "/^(?=.*\d)(?!.*(.)\1{2}).*[a-z]/m",
     * match=true,
     * message="Votre mot de passe doit comporter au moins huit caractères, dont une lettre et un chiffre.")
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $hash;

    /**
     * @Assert\EqualTo(propertyPath="hash", message="Vous n'avez pas confirmé correctement votre mot de passe")
     */
    public $passwordConfirm;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=250, maxMessage="Votre intro doit faire maximum 250 caractères !")
     */
    private $introduction;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $presentation;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\ManyToMany(targetEntity=Role::class, mappedBy="users")
     */
    private $userRoles;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="author")
     */
    private $comments;

    /**
     * @ORM\OneToMany(targetEntity=Claim::class, mappedBy="owner", orphanRemoval=true)
     */
    private $claims;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $token;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $passwordRequestedAt;

    /**
     * @ORM\OneToMany(targetEntity=EventLike::class, mappedBy="user", orphanRemoval=true)
     */
    private $eventLikes;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $last_connection_at;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $level;

    /**
     * @ORM\OneToMany(targetEntity=PlaceLike::class, mappedBy="user", orphanRemoval=true)
     */
    private $placeLikes;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $facebookId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $googleId;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isVisible;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isActivated;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isChecked;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isVolunteer;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $registrationAt;

    /**
     * @ORM\Column(type="simple_array", nullable=true)
     */
    private $multiIp = [];


    /**
     * @ORM\ManyToMany(targetEntity=Artist::class, inversedBy="editors")
     */
    private $artistEditors;

    /**
     * @ORM\ManyToMany(targetEntity=Organizer::class, inversedBy="editors")
     */
    private $organizerEditors;

    /**
     * @ORM\ManyToMany(targetEntity=Event::class, inversedBy="editors")
     */
    private $eventEditors;

    /**
     * @ORM\ManyToMany(targetEntity=Place::class, inversedBy="editors")
     */
    private $placeEditors;

    /**
     * @ORM\ManyToMany(targetEntity=Festival::class, inversedBy="editors")
     */
    private $festivalEditors;


    /**
     * @ORM\ManyToMany(targetEntity=Artist::class, mappedBy="affirmers")
     */
    private $artistAffirmers;

    /**
     * @ORM\ManyToMany(targetEntity=Organizer::class, mappedBy="affirmers")
     */
    private $organizerAffirmers;

    /**
     * @ORM\ManyToMany(targetEntity=Place::class, mappedBy="affirmers")
     */
    private $placeAffirmers;

    /**
     * @ORM\ManyToMany(targetEntity=Festival::class, mappedBy="affirmers")
     */
    private $festivalAffirmers;



    /**
     * @ORM\OneToMany(targetEntity=Artist::class, mappedBy="author")
     */
    private $artistAuthors;

    /**
     * @ORM\OneToMany(targetEntity=Organizer::class, mappedBy="author")
     */
    private $organizerAuthors;

    /**
     * @ORM\OneToMany(targetEntity=Event::class, mappedBy="author")
     */
    private $eventAuthors;

    /**
     * @ORM\OneToMany(targetEntity=Place::class, mappedBy="author")
     */
    private $placeAuthors;

    /**
     * @ORM\OneToMany(targetEntity=Festival::class, mappedBy="author")
     */
    private $festivalAuthors;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="moderator")
     */
    private $moderateComments;

    /**
     * @ORM\OneToMany(targetEntity=Subdomain::class, mappedBy="manager")
     */
    private $subdomains;

    /**
     * @ORM\ManyToMany(targetEntity=Subdomain::class, mappedBy="volunteers")
     */
    private $supports;

    /**
     * @ORM\OneToMany(targetEntity=Event::class, mappedBy="verifiedBy")
     */
    private $eventsVerified;

    public function __construct()
    {
        $this->userRoles = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->claims = new ArrayCollection();
        $this->eventLikes = new ArrayCollection();
        $this->likes = new ArrayCollection();
        $this->placeLikes = new ArrayCollection();
        $this->artistEditors = new ArrayCollection();
        $this->organizerEditors = new ArrayCollection();
        $this->eventEditors = new ArrayCollection();
        $this->placeEditors = new ArrayCollection();
        $this->festivalEditors = new ArrayCollection();
        $this->artistAffirmers = new ArrayCollection();
        $this->organizerAffirmers = new ArrayCollection();
        $this->placeAffirmers = new ArrayCollection();
        $this->festivalAffirmers = new ArrayCollection();
        $this->festivalAuthors = new ArrayCollection();
        $this->artistAuthors = new ArrayCollection();
        $this->organiserAuthors = new ArrayCollection();
        $this->organizerAuthors = new ArrayCollection();
        $this->placeAuthors = new ArrayCollection();
        $this->eventAuthor = new ArrayCollection();
        $this->moderateComments = new ArrayCollection();
        $this->subdomains = new ArrayCollection();
        $this->supports = new ArrayCollection();
        $this->eventsVerified = new ArrayCollection();
    }
    
    public function getFullName() {
        return "{$this->firstName} {$this->lastName}";
    }

    /**
     * Permet d'initialiser le slug
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * 
     * @return void
     */
    public function initializeSlug(){
        if(empty($this->slug)){
            $slugify = new Slugify();
            $this->slug = $slugify->slugify($this->firstName . ' ' . $this->lastName);
        }
    }

    /**
     * Permet d'initialiser la date de création du compte
     * 
     * @ORM\PrePersist
     * @ORM\PreUpdate
    */
    public function updatedTimestamps(): void
    {   
        if ($this->getRegistrationAt() === null) {
            $this->setRegistrationAt(new \DateTime('now'));
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function setAvatar(?string $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function getAvatarPath()
    {
        return 'img/'.UploaderHelper::USER_IMAGE.'/'.$this->getAvatar();
    }

    public function getPassword(): ?string
    {
        return $this->hash;
    }

    public function getHash(): ?string
    {
        return $this->hash;
    }

    public function setHash(string $hash): self
    {
        $this->hash = $hash;

        return $this;
    }

    public function getIntroduction(): ?string
    {
        return $this->introduction;
    }

    public function setIntroduction(?string $introduction): self
    {
        $this->introduction = $introduction;

        return $this;
    }

    public function getPresentation(): ?string
    {
        return $this->presentation;
    }

    public function setPresentation(?string $presentation): self
    {
        $this->presentation = $presentation;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        //$roles = $this->roles;


        $roles = $this->userRoles->map(function($role){
            return $role->getTitle();
        })->toArray();

        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        //dump($roles);
        //die();
        //return ['ROLE_USER'];

        return $roles;
        //return array_unique($roles);
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * The public representation of the user (e.g. a username, an email address, etc.)
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }


    /**
     * @deprecated since Symfony 5.3
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }
    
     /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection|Role[]
     */
    public function getUserRoles(): Collection
    {
        return $this->userRoles;
    }

    public function addUserRole(Role $userRole): self
    {
        if (!$this->userRoles->contains($userRole)) {
            $this->userRoles[] = $userRole;
            $userRole->addUser($this);
        }

        return $this;
    }

    public function removeUserRole(Role $userRole): self
    {
        if ($this->userRoles->contains($userRole)) {
            $this->userRoles->removeElement($userRole);
            $userRole->removeUser($this);
        }

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setAuthor($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getAuthor() === $this) {
                $comment->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Claim[]
     */
    public function getClaims(): Collection
    {
        return $this->claims;
    }

    public function addClaim(Claim $claim): self
    {
        if (!$this->claims->contains($claim)) {
            $this->claims[] = $claim;
            $claim->setOwner($this);
        }

        return $this;
    }

    public function removeClaim(Claim $claim): self
    {
        if ($this->claims->contains($claim)) {
            $this->claims->removeElement($claim);
            // set the owning side to null (unless already changed)
            if ($claim->getOwner() === $this) {
                $claim->setOwner(null);
            }
        }

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getPasswordRequestedAt(): ?\DateTimeInterface
    {
        return $this->passwordRequestedAt;
    }

    public function setPasswordRequestedAt(?\DateTimeInterface $passwordRequestedAt): self
    {
        $this->passwordRequestedAt = $passwordRequestedAt;

        return $this;
    }

    /**
     * @return Collection|EventLike[]
     */
    public function getEventLikes(): Collection
    {
        return $this->eventLikes;
    }

    public function addEventLike(EventLike $eventLike): self
    {
        if (!$this->eventLikes->contains($eventLike)) {
            $this->eventLikes[] = $eventLike;
            $eventLike->setUser($this);
        }

        return $this;
    }

    public function removeEventLike(EventLike $eventLike): self
    {
        if ($this->eventLikes->contains($eventLike)) {
            $this->eventLikes->removeElement($eventLike);
            // set the owning side to null (unless already changed)
            if ($eventLike->getUser() === $this) {
                $eventLike->setUser(null);
            }
        }

        return $this;
    }

    public function getLastConnectionAt(): ?\DateTimeInterface
    {
        return $this->last_connection_at;
    }

    public function setLastConnectionAt(?\DateTimeInterface $last_connection_at): self
    {
        $this->last_connection_at = $last_connection_at;

        return $this;
    }

    public function getLevel(): ?bool
    {
        return $this->level;
    }

    public function setLevel(?bool $level): self
    {
        $this->level = $level;

        return $this;
    }

    /**
     * @return Collection|PlaceLike[]
     */
    public function getPlaceLikes(): Collection
    {
        return $this->placeLikes;
    }

    public function addPlaceLike(PlaceLike $placeLike): self
    {
        if (!$this->placeLikes->contains($placeLike)) {
            $this->placeLikes[] = $placeLike;
            $placeLike->setUser($this);
        }

        return $this;
    }

    public function removePlaceLike(PlaceLike $placeLike): self
    {
        if ($this->placeLikes->contains($placeLike)) {
            $this->placeLikes->removeElement($placeLike);
            // set the owning side to null (unless already changed)
            if ($placeLike->getUser() === $this) {
                $placeLike->setUser(null);
            }
        }

        return $this;
    }

    public function getFacebookId(): ?string
    {
        return $this->facebookId;
    }

    public function setFacebookId(string $facebookId): self
    {
        $this->facebookId = $facebookId;

        return $this;
    }

    public function getGoogleId(): ?string
    {
        return $this->googleId;
    }

    public function setGoogleId(?string $googleId): self
    {
        $this->googleId = $googleId;

        return $this;
    }

    public function getIsVisible(): ?bool
    {
        return $this->isVisible;
    }

    public function setIsVisible(?bool $isVisible): self
    {
        $this->isVisible = $isVisible;

        return $this;
    }

    public function getIsActivated(): ?bool
    {
        return $this->isActivated;
    }

    public function setIsActivated(?bool $isActivated): self
    {
        $this->isActivated = $isActivated;

        return $this;
    }

    public function getIsChecked(): ?bool
    {
        return $this->isChecked;
    }

    public function setIsChecked(?bool $isChecked): self
    {
        $this->isChecked = $isChecked;

        return $this;
    }

    public function getIsVolunteer(): ?bool
    {
        return $this->isVolunteer;
    }

    public function setIsVolunteer(?bool $isVolunteer): self
    {
        $this->isVolunteer = $isVolunteer;

        return $this;
    }

    public function getRegistrationAt(): ?\DateTimeInterface
    {
        return $this->registrationAt;
    }

    public function setRegistrationAt(?\DateTimeInterface $registrationAt): self
    {
        $this->registrationAt = $registrationAt;

        return $this;
    }

    public function getMultiIp(): ?array
    {
        return $this->multiIp;
    }

    public function setMultiIp(?array $multiIp): self
    {
        $this->multiIp = $multiIp;

        return $this;
    }

    /**
     * @return Collection|Artist[]
     */
    public function getArtistEditors(): Collection
    {
        return $this->artistEditors;
    }

    public function addArtistEditor(Artist $artistEditor): self
    {
        if (!$this->artistEditors->contains($artistEditor)) {
            $this->artistEditors[] = $artistEditor;
        }

        return $this;
    }

    public function removeArtistEditor(Artist $artistEditor): self
    {
        if ($this->artistEditors->contains($artistEditor)) {
            $this->artistEditors->removeElement($artistEditor);
        }

        return $this;
    }

    /**
     * @return Collection|Organizer[]
     */
    public function getOrganizerEditors(): Collection
    {
        return $this->organizerEditors;
    }

    public function addOrganizerEditor(Organizer $organizerEditor): self
    {
        if (!$this->organizerEditors->contains($organizerEditor)) {
            $this->organizerEditors[] = $organizerEditor;
        }

        return $this;
    }

    public function removeOrganizerEditor(Organizer $organizerEditor): self
    {
        if ($this->organizerEditors->contains($organizerEditor)) {
            $this->organizerEditors->removeElement($organizerEditor);
        }

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEventEditors(): Collection
    {
        return $this->eventEditors;
    }

    public function addEventEditor(Event $eventEditor): self
    {
        if (!$this->eventEditors->contains($eventEditor)) {
            $this->eventEditors[] = $eventEditor;
        }

        return $this;
    }

    public function removeEventEditor(Event $eventEditor): self
    {
        if ($this->eventEditors->contains($eventEditor)) {
            $this->eventEditors->removeElement($eventEditor);
        }

        return $this;
    }

    /**
     * @return Collection|Place[]
     */
    public function getPlaceEditors(): Collection
    {
        return $this->placeEditors;
    }

    public function addPlaceEditor(Place $placeEditor): self
    {
        if (!$this->placeEditors->contains($placeEditor)) {
            $this->placeEditors[] = $placeEditor;
        }

        return $this;
    }

    public function removePlaceEditor(Place $placeEditor): self
    {
        if ($this->placeEditors->contains($placeEditor)) {
            $this->placeEditors->removeElement($placeEditor);
        }

        return $this;
    }

    /**
     * @return Collection|Festival[]
     */
    public function getFestivalEditors(): Collection
    {
        return $this->festivalEditors;
    }

    public function addFestivalEditor(Festival $festivalEditor): self
    {
        if (!$this->festivalEditors->contains($festivalEditor)) {
            $this->festivalEditors[] = $festivalEditor;
        }

        return $this;
    }

    public function removeFestivalEditor(Festival $festivalEditor): self
    {
        if ($this->festivalEditors->contains($festivalEditor)) {
            $this->festivalEditors->removeElement($festivalEditor);
        }

        return $this;
    }

    /**
     * @return Collection|Artist[]
     */
    public function getArtistAffirmers(): Collection
    {
        return $this->artistAffirmers;
    }

    public function addArtistAffirmer(Artist $artistAffirmer): self
    {
        if (!$this->artistAffirmers->contains($artistAffirmer)) {
            $this->artistAffirmers[] = $artistAffirmer;
            $artistAffirmer->addAffirmer($this);
        }

        return $this;
    }

    public function removeArtistAffirmer(Artist $artistAffirmer): self
    {
        if ($this->artistAffirmers->contains($artistAffirmer)) {
            $this->artistAffirmers->removeElement($artistAffirmer);
            $artistAffirmer->removeAffirmer($this);
        }

        return $this;
    }

    /**
     * @return Collection|Organizer[]
     */
    public function getOrganizerAffirmers(): Collection
    {
        return $this->organizerAffirmers;
    }

    public function addOrganizerAffirmer(Organizer $organizerAffirmer): self
    {
        if (!$this->organizerAffirmers->contains($organizerAffirmer)) {
            $this->organizerAffirmers[] = $organizerAffirmer;
            $organizerAffirmer->addAffirmer($this);
        }

        return $this;
    }

    public function removeOrganizerAffirmer(Organizer $organizerAffirmer): self
    {
        if ($this->organizerAffirmers->contains($organizerAffirmer)) {
            $this->organizerAffirmers->removeElement($organizerAffirmer);
            $organizerAffirmer->removeAffirmer($this);
        }

        return $this;
    }

    /**
     * @return Collection|Place[]
     */
    public function getPlaceAffirmers(): Collection
    {
        return $this->placeAffirmers;
    }

    public function addPlaceAffirmer(Place $placeAffirmer): self
    {
        if (!$this->placeAffirmers->contains($placeAffirmer)) {
            $this->placeAffirmers[] = $placeAffirmer;
            $placeAffirmer->addAffirmer($this);
        }

        return $this;
    }

    public function removePlaceAffirmer(Place $placeAffirmer): self
    {
        if ($this->placeAffirmers->contains($placeAffirmer)) {
            $this->placeAffirmers->removeElement($placeAffirmer);
            $placeAffirmer->removeAffirmer($this);
        }

        return $this;
    }

    /**
     * @return Collection|Festival[]
     */
    public function getFestivalAffirmers(): Collection
    {
        return $this->festivalAffirmers;
    }

    public function addFestivalAffirmer(Festival $festivalAffirmer): self
    {
        if (!$this->festivalAffirmers->contains($festivalAffirmer)) {
            $this->festivalAffirmers[] = $festivalAffirmer;
            $festivalAffirmer->addAffirmer($this);
        }

        return $this;
    }

    public function removeFestivalAffirmer(Festival $festivalAffirmer): self
    {
        if ($this->festivalAffirmers->contains($festivalAffirmer)) {
            $this->festivalAffirmers->removeElement($festivalAffirmer);
            $festivalAffirmer->removeAffirmer($this);
        }

        return $this;
    }

    /**
     * @return Collection|Artist[]
     */
    public function getArtistAuthors(): Collection
    {
        return $this->artistAuthors;
    }

    public function addArtistAuthor(Artist $artistAuthor): self
    {
        if (!$this->artistAuthors->contains($artistAuthor)) {
            $this->artistAuthors[] = $artistAuthor;
            $artistAuthor->setAuthor($this);
        }

        return $this;
    }

    public function removeArtistAuthor(Artist $artistAuthor): self
    {
        if ($this->artistAuthors->contains($artistAuthor)) {
            $this->artistAuthors->removeElement($artistAuthor);
            // set the owning side to null (unless already changed)
            if ($artistAuthor->getAuthor() === $this) {
                $artistAuthor->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Organizer[]
     */
    public function getOrganizerAuthors(): Collection
    {
        return $this->organizerAuthors;
    }

    public function addOrganizerAuthor(Organizer $organizerAuthor): self
    {
        if (!$this->organizerAuthors->contains($organizerAuthor)) {
            $this->organizerAuthors[] = $organizerAuthor;
            $organizerAuthor->setAuthor($this);
        }

        return $this;
    }

    public function removeOrganizerAuthor(Organizer $organizerAuthor): self
    {
        if ($this->organizerAuthors->contains($organizerAuthor)) {
            $this->organizerAuthors->removeElement($organizerAuthor);
            // set the owning side to null (unless already changed)
            if ($organizerAuthor->getAuthor() === $this) {
                $organizerAuthor->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEventAuthors(): Collection
    {
        return $this->eventAuthors;
    }

    public function addEventAuthor(Event $eventAuthor): self
    {
        if (!$this->eventAuthors->contains($eventAuthor)) {
            $this->eventAuthors[] = $eventAuthor;
            $eventAuthor->setAuthor($this);
        }

        return $this;
    }

    public function removeEventAuthor(Event $eventAuthor): self
    {
        if ($this->eventAuthors->contains($eventAuthor)) {
            $this->eventAuthors->removeElement($eventAuthor);
            // set the owning side to null (unless already changed)
            if ($eventAuthor->getAuthor() === $this) {
                $eventAuthor->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Place[]
     */
    public function getPlaceAuthors(): Collection
    {
        return $this->placeAuthors;
    }

    public function addPlaceAuthor(Place $placeAuthor): self
    {
        if (!$this->placeAuthors->contains($placeAuthor)) {
            $this->placeAuthors[] = $placeAuthor;
            $placeAuthor->setAuthor($this);
        }

        return $this;
    }

    public function removePlaceAuthor(Place $placeAuthor): self
    {
        if ($this->placeAuthors->contains($placeAuthor)) {
            $this->placeAuthors->removeElement($placeAuthor);
            // set the owning side to null (unless already changed)
            if ($placeAuthor->getAuthor() === $this) {
                $placeAuthor->setAuthor(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection|Festival[]
     */
    public function getFestivalAuthors(): Collection
    {
        return $this->festivalAuthors;
    }

    public function addFestivalAuthor(Festival $festivalAuthor): self
    {
        if (!$this->festivalAuthors->contains($festivalAuthor)) {
            $this->festivalAuthors[] = $festivalAuthor;
            $festivalAuthor->setAuthor($this);
        }

        return $this;
    }

    public function removeFestivalAuthor(Festival $festivalAuthor): self
    {
        if ($this->festivalAuthors->contains($festivalAuthor)) {
            $this->festivalAuthors->removeElement($festivalAuthor);
            // set the owning side to null (unless already changed)
            if ($festivalAuthor->getAuthor() === $this) {
                $festivalAuthor->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getModerateComments(): Collection
    {
        return $this->moderateComments;
    }

    public function addModerateComment(Comment $moderateComment): self
    {
        if (!$this->moderateComments->contains($moderateComment)) {
            $this->moderateComments[] = $moderateComment;
            $moderateComment->setModerator($this);
        }

        return $this;
    }

    public function removeModerateComment(Comment $moderateComment): self
    {
        if ($this->moderateComments->contains($moderateComment)) {
            $this->moderateComments->removeElement($moderateComment);
            // set the owning side to null (unless already changed)
            if ($moderateComment->getModerator() === $this) {
                $moderateComment->setModerator(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Subdomain[]
     */
    public function getSubdomains(): Collection
    {
        return $this->subdomains;
    }

    public function addSubdomain(Subdomain $subdomain): self
    {
        if (!$this->subdomains->contains($subdomain)) {
            $this->subdomains[] = $subdomain;
            $subdomain->setManager($this);
        }

        return $this;
    }

    public function removeSubdomain(Subdomain $subdomain): self
    {
        if ($this->subdomains->contains($subdomain)) {
            $this->subdomains->removeElement($subdomain);
            // set the owning side to null (unless already changed)
            if ($subdomain->getManager() === $this) {
                $subdomain->setManager(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Subdomain[]
     */
    public function getSupports(): Collection
    {
        return $this->supports;
    }

    public function addSupport(Subdomain $support): self
    {
        if (!$this->supports->contains($support)) {
            $this->supports[] = $support;
            $support->addVolunteer($this);
        }

        return $this;
    }

    public function removeSupport(Subdomain $support): self
    {
        if ($this->supports->contains($support)) {
            $this->supports->removeElement($support);
            $support->removeVolunteer($this);
        }

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEventsVerified(): Collection
    {
        return $this->eventsVerified;
    }

    public function addEventsVerified(Event $eventsVerified): self
    {
        if (!$this->eventsVerified->contains($eventsVerified)) {
            $this->eventsVerified[] = $eventsVerified;
            $eventsVerified->setVerifiedBy($this);
        }

        return $this;
    }

    public function removeEventsVerified(Event $eventsVerified): self
    {
        if ($this->eventsVerified->removeElement($eventsVerified)) {
            // set the owning side to null (unless already changed)
            if ($eventsVerified->getVerifiedBy() === $this) {
                $eventsVerified->setVerifiedBy(null);
            }
        }

        return $this;
    }

}
