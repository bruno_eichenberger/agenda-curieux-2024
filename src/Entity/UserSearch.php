<?php

namespace App\Entity;

class UserSearch {

    private $nom;

    private $email;

    /**
     * Get the value of nom
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set the value of nom
     *
     * @param $nom
     * @return void
     */
    public function setNom($nom): UserSearch
    {
        $this->nom = $nom;
        return $this;
    }

    /**
     * Get the value of email
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @param $email
     * @return void
     */ 
    public function setEmail($email): UserSearch
    {
        $this->email = $email;
        return $this;
    }

}