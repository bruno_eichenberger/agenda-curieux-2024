<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\City;
use App\Entity\Role;
use App\Entity\User;
use App\Entity\Event;
use App\Entity\Image;
use App\Entity\Place;
use App\Entity\Video;
use App\Entity\Artist;
use App\Entity\Prices;
use App\Entity\Region;
use App\Entity\Rubric;
use App\Entity\Ticket;
use App\Entity\Borough;
use App\Entity\Comment;
use App\Entity\Country;
use App\Entity\Category;
use App\Entity\District;
use App\Entity\Festival;
use App\Entity\Postcode;
use App\Entity\Placekind;
use App\Entity\Reduction;
use App\Entity\Subdomain;
use App\Entity\Ticketing;
use App\Entity\Department;
use App\Entity\Videostore;
use Cocur\Slugify\Slugify;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private $encoder;
    
    public function __construct(UserPasswordHasherInterface $hasher){
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager)
    {

        $faker = Factory::create('fr_FR');
        $slugify = new Slugify();


        $adminRole = new Role();
        $adminRole->setTitle('ROLE_ADMIN');
        $manager->persist($adminRole);

        $superRole = new Role();
        $superRole->setTitle('ROLE_SUPERADMIN');
        $manager->persist($superRole);

        // Utilisateur admin
        $users = [];
        
        $adminUser = new User();
        $adminUser->setFirstName('Bruno')
                  ->setLastName('Eichenberger')
                  ->setEmail('bruno@curieux.net')
                  ->setIntroduction($faker->sentence())
                  ->setPresentation( '<p>' . join('</p><p>' , $faker->paragraphs(3)) . '</p>')
                  ->setHash($this->hasher->hashPassword($adminUser, 'password'))
                  ->setAvatar('https://cdn.iconscout.com/icon/free/png-256/avatar-375-456327.png')
                  ->addUserRole($adminRole)
                  ->addUserRole($superRole)
                  ->setIsActivated(true)
                  ->setIsChecked(true)
                  ->setIsVisible(true);
        $manager->persist($adminUser);
        $users[] = $adminUser;


        //$users = [];
        $adminUser = new User();
        $adminUser->setFirstName('Filou')
                  ->setLastName('Curieux')
                  ->setEmail('filou@curieux.net')
                  ->setIntroduction($faker->sentence())
                  ->setPresentation( '<p>' . join('</p><p>' , $faker->paragraphs(3)) . '</p>')
                  ->setHash($this->hasher->hashPassword($adminUser, 'password'))
                  ->setAvatar('https://cdn.iconscout.com/icon/free/png-256/avatar-375-456327.png')
                  ->addUserRole($adminRole)
                  ->setIsActivated(true)
                  ->setIsChecked(true)
                  ->setIsVisible(true);
        $manager->persist($adminUser);
        $users[] = $adminUser;

        //$users = [];
        $adminUser = new User();
        $adminUser->setFirstName('David')
                  ->setLastName('Curieux')
                  ->setEmail('david@curieux.net')
                  ->setIntroduction($faker->sentence())
                  ->setPresentation( '<p>' . join('</p><p>' , $faker->paragraphs(3)) . '</p>')
                  ->setHash($this->hasher->hashPassword($adminUser, 'password'))
                  ->setAvatar('https://banner2.kisspng.com/20180615/rtc/kisspng-avatar-user-profile-male-logo-profile-icon-5b238cb002ed52.870627731529056432012.jpg')
                  ->addUserRole($adminRole)
                  ->setIsActivated(true)
                  ->setIsChecked(true)
                  ->setIsVisible(true);
        $manager->persist($adminUser);
        $users[] = $adminUser;


        // Utilisateurs
        $genres = ['male','female'];

        for ($i = 1; $i <= 10; $i++){
            $user = new User();

            $genre = $faker->randomElement($genres);

            $avatar = 'https://randomuser.me/api/portraits/';
            $avatarId = $faker->numberBetween(1, 99) . '.jpg';

            $avatar .= ($genre == "male" ? 'men/' : 'women/') . $avatarId;

            $hash = $this->hasher->hashPassword($user, 'password');

            $user->setFirstName($faker->firstname($genre))
                 ->setLastName($faker->lastname)
                 ->setEmail($faker->email)
                 ->setIntroduction($faker->sentence())
                 ->setPresentation( '<p>' . join('</p><p>' , $faker->paragraphs(3)) . '</p>')
                 ->setHash($hash)
                 ->setAvatar($avatar);

            $manager->persist($user);
            $users[] = $user;
        }

        // Pays de base
        $countrys = [];
        $country = new Country();
        $country->setName('France')
                ->setCode('FR');
        $manager->persist($country);
        $countrys[] = $country;

        // Régions
        //$url = "https://geo.api.gouv.fr/regions";
        $url = "public/json/regions.json";
        $json = file_get_contents($url);
        $json_data = json_decode($json, true);
        
        foreach ($json_data as $data) {

            $region = new Region();

            $region ->setName($data["nom"])
                    ->setCountry($countrys[0]);

            $manager->persist($region);

            $regions[] = $region;
            $code_region[$data["code"]] = $region;
        }
        
        // Avancement
        //echo "/";

        // Départements
        //$url = "https://geo.api.gouv.fr/departements";
        $url = "public/json/departements.json";
        $json = file_get_contents($url);
        $json_data = json_decode($json, true);

        foreach ($json_data as $data) {

            $department = new Department();

            $department ->setName($data["nom"])
                        ->setCode($data["code"])
                        ->setRegion($code_region[$data["codeRegion"]]);

            $manager->persist($department);
            $departments[$data["nom"]] = $department;
            $code_department[$data["code"]] = $department;
        }

        //echo "/";

        // Arrondissements

        /*
        $laposte = [];
        
        if (($handle = fopen("public/json/laposte_gps.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {

                $name = "*".mb_strtolower($data[1]);
                $name = str_replace(" st "," saint ",$name);
                $name = str_replace(" ste "," sainte ",$name);
                $name = str_replace("*st ","*saint ",$name);
                $name = str_replace("*ste ","*sainte ",$name);
                $name = str_replace("*","",$name);

                $explode = explode(" ", $name);
                if (is_numeric(end($explode))){
                    $arrondissement = end($explode);
                    $name = str_replace(" ".$arrondissement, "", $name);
                } else {
                    $arrondissement = null;
                }

                $name = $slugify->slugify($name);
                $name = str_replace("chateau-chinon-ville","chateau-chinon",$name);

                $numDep = substr($data[0], 0, 2);
                if (is_numeric($numDep) and $numDep > 96){
                    $numDep = substr($data[0], 0, 3);
                }

                $laposte[$numDep][$name][] = [
                    'gps' => $data[6],
                    'arrondissement' => $arrondissement,
                    'code' => [
                        'insee'         => $data[0],
                        'postal'        => $data[2]
                    ]
                ];
                
            }
            fclose($handle);
        }
        */

        $url = "public/json/borough.json";
        //$url = "../agenda-curieux/public/json/borough.json";
        $json = file_get_contents($url);
        $json_data = json_decode($json, true);

        foreach ($json_data as $data) {

            $borough = new Borough();

            if ($data["habitants"] != ""){
                $borough->setResident($data["habitants"]);
            }

            $borough->setName($data["name"])
                    ->setChiefTown($data["chieftown"])
                    ->setDepartment($code_department[$data["codeDepartement"]]);

            $chieftown = $data["chieftown"];
            $chieftown = str_replace("ü", "u", $chieftown);
            $chieftown = $slugify->slugify($chieftown);
            $chieftown = str_replace("-et-isson","",$chieftown);
            $chieftown = str_replace("-pres-munster","",$chieftown);

            if (isset($laposte[$data["codeDepartement"]][$chieftown])){
                $explodeGps = explode(",",$laposte[$data["codeDepartement"]][$chieftown]['0']['gps']);
                $borough->setLatitude($explodeGps[0])
                        ->setLongitude($explodeGps[1]);
            }

            $manager->persist($borough);
            $boroughs[] = $borough;
            $boroughByChieftown[$data["chieftown"]] = $borough;

            $borough_by_dep = [];
            $bigger_borough = [];
            if (isset($borough_by_dep[$data["codeDepartement"]])){
                if ($data["population"] > $bigger_borough[$data["codeDepartement"]] ){
                    $borough_by_dep[$data["codeDepartement"]] = $borough;
                    $bigger_borough[$data["codeDepartement"]] = $data["population"];
                }
            } else {
                $borough_by_dep[$data["codeDepartement"]] = $borough;
                $bigger_borough[$data["codeDepartement"]] = $data["population"];
            }
            
            
        }

        //echo "/";

        /*
        // Région de base
        $regions = [];
        $region = new Region();
        $region->setName('Grand-Est')
               ->setCountry($countrys[0]);
        $manager->persist($region);
        $regions[] = $region;

        // Département de base
        $departments = [];
        $department = new Department();
        $department->setName('Bas-Rhin')
                   ->setCode('67')
                   ->setRegion($regions[0]);
        $manager->persist($department);
        $departments[] = $department;

        // Arrondissement de base
        $boroughs = [];
        $borough = new Borough();
        $borough->setName('Strasbourg')
                ->setResident('strasbourgeois')
                ->setChiefTown('Strasbourg')
                ->setDepartment($departments[0]);
        $manager->persist($borough);
        $boroughs[] = $borough;

        */

        // Ville de base
        $citys = [];
        $city = new City();
        $description = '<p>' . join('</p><p>' , $faker->paragraphs(5)) . '</p>';
        $coatOfArms = $faker->imageUrl(500,500);
        $city->setName('Strasbourg')
             ->setDescription($description)
             ->setCoatOfArms($coatOfArms)
             ->setBorough($boroughByChieftown["Strasbourg"])
             ->setDepartment($departments["Bas-Rhin"]);
        $manager->persist($city);
        $citys[] = $city;

        // Code postaux de base
        $postcode = new Postcode();
        $postcode->setCode('67000')
                 ->setCity($citys[0]);
        $manager->persist($postcode);

        $postcode = new Postcode();
        $postcode->setCode('67100')
                 ->setCity($citys[0]);
        $manager->persist($postcode);

        $postcode = new Postcode();
        $postcode->setCode('67200')
                 ->setCity($citys[0]);
        $manager->persist($postcode);



        //echo "/--";


        // Artists

        $artists = [];
        for ($i = 1; $i <= 10; $i++){

            $artist = new Artist();

            $name = ucfirst($faker->word());
            $coverImage = $faker->imageUrl(500,1000);
            $description = '<p>' . join('</p><p>' , $faker->paragraphs(5)) . '</p>';

            $artist ->setName($name)
                    ->setCoverImage($coverImage)
                    ->setDescription($description);

            $manager->persist($artist);
            $artists[] = $artist;
        }

        // Pays
        for ($i = 1; $i <= 100; $i++){

            $country = new Country();

            $name = $faker->country();
            $code = $faker->countryCode();

            $country->setName($name)
                    ->setCode($code);

            $manager->persist($country);
            $countrys[] = $country;
        }

        //echo "/";
/*
        // Régions
        for ($i = 1; $i <= 100; $i++){

            $region = new Region();

            $name = $faker->region();
            
            if (mt_rand(0,1)){
                $country = $countrys[0];
            } else {
                $country = $countrys[mt_rand(1, count($countrys) - 1)];
            }

            $region ->setName($name)
                    ->setCountry($country);

            $manager->persist($region);
            $regions[] = $region;
        }

        // Départements
        for ($i = 1; $i <= 100; $i++){

            $department = new Department();

            $departmentTab = $faker->department();
            foreach ($departmentTab as $k => $v) {
                $code = $k;
                $name = $v;
            }

            if (mt_rand(0,1)){
                $region = $regions[0];
            } else {
                $region = $regions[mt_rand(1, count($regions) - 1)];
            }

            $department ->setName($name)
                        ->setCode($code)
                        ->setRegion($region);

            $manager->persist($department);
            $departments[] = $department;
        }

        // Arrondissements
        for ($i = 1; $i <= 10; $i++){

            $borough = new Borough();

            $name = $faker->city();
            $resident = $name . "eois";
            $chiefTown = $name;

            if (mt_rand(0,1)){
                $department = $departments[0];
            } else {
                $department = $departments[mt_rand(1, count($departments) - 1)];
            }

            $borough->setName($name)
                    ->setResident($resident)
                    ->setChiefTown($chiefTown)
                    ->setDepartment($department);

            $manager->persist($borough);
            $boroughs[] = $borough;
        }
*/
        // Villes
        for ($i = 1; $i <= 30; $i++){

            $city = new City();

            $name = $faker->city();
            $description = '<p>' . join('</p><p>' , $faker->paragraphs(5)) . '</p>';
            $coatOfArms = $faker->imageUrl(500,500);
            if (mt_rand(0,1)){
                $borough = $boroughByChieftown["Strasbourg"];
            } else {
                $borough = $boroughs[mt_rand(1, count($boroughs) - 1)];
            }

            $city->setName($name)
                 ->setDescription($description)
                 ->setCoatOfArms($coatOfArms)
                 ->setBorough($borough)
                 ->setDepartment($departments["Bas-Rhin"]);

            $manager->persist($city);
            $citys[] = $city;
        }
 
        //echo "/";

        // Codes postaux
        for ($i = 1; $i <= 50; $i++){

            $postcode = new Postcode();

            $city = $citys[mt_rand(1, count($citys) - 1)];
            $code = $faker->postcode();

            $postcode->setCode($code)
                     ->setCity($city);

            $manager->persist($postcode);
            $postcodes['$city'][] = $postcode;
        }
 
        // Quartiers
        $districts = [];
        for ($i = 1; $i <= 30; $i++){

            $district = new District();

            $name = $faker->words(2);
            $name = $name[0].' '.$name[1];
            $name = ucwords($name);
            $description = '<p>' . join('</p><p>' , $faker->paragraphs(5)) . '</p>';

            if (mt_rand(0,1)){
                $city = $citys[0];
            } else {
                $city = $citys[mt_rand(1, count($citys) - 1)];
            }

            $district->setName($name)
                     ->setDescription($description)
                     ->setCity($city);

            $manager->persist($district);
            $districts[] = $district;
        }

        // Types de lieu
        $types = [
            'Bar' => 'Les bars',
            'Salle de concert' => 'Les salles de concert',
            'Boîte de nuit' => 'Les boîtes de nuit',
            'Théâtre' => 'Les théâtres',
            'Salle d’exposition' => 'Les salles d’exposition',
            'Manifestation' => 'Les manifestations citoyennes',
            'Equipement sportif' => 'Les équipements sportifs'
        ];
        $placekinds = [];

        foreach ($types as $k => $v) {

            $name = $k;
            $plural = $v;

            $placekind = new Placekind();

            $placekind  ->setName($name)
                        ->setPlural($plural);

            $manager->persist($placekind);
            $placekinds[] = $placekind;
        }

        // Lieux
        
        $places = [];
        $pronouns = ['Le','La','Les','Aux','Au','The','L’'];
            
        for ($i = 1; $i <= 100; $i++){

            $place = new Place();

            $name = ucfirst($faker->word());
            if (mt_rand(0,1)){
                $pronoun = $pronouns[mt_rand(0, count($pronouns) - 1)];
                if ($pronoun == 'L’'){$separator = '';} else {$separator = ' ';}
                $title = $pronoun .''. $separator .''. $name;
            } else {
                $pronoun = NULL;
                $title = $name;
            }
            
            $coverSite = "https://picsum.photos";
            if (mt_rand(0,1)){
                //$coverImage = $faker->imageUrl(1000,350);
                $widthImage = mt_rand(800,1000);
                $heightImage = mt_rand(300,500);
            } elseif (mt_rand(0,1)){
                //$coverImage = $faker->imageUrl(1000,1200);
                $widthImage = mt_rand(800,1200);
                $heightImage = mt_rand(1200,1400);
            } else {
                //$coverImage = $faker->imageUrl(500,1000);
                $widthImage = mt_rand(400,600);
                $heightImage = mt_rand(1100,1200);
            }
            $coverImage = $coverSite . '/' . $widthImage . '/' . $heightImage;


            $description = '<p>' . join('</p><p>' , $faker->paragraphs(5)) . '</p>';

            if (mt_rand(0,1)){
                $city = $citys[0];
            } else {
                $city = $citys[mt_rand(1, count($citys) - 1)];
            }

            if (mt_rand(0,1)){$district = $districts[mt_rand(0, count($districts) - 1)];} else {$district = NULL;}

            $latitude = $faker->latitude($min = 41, $max = 51);
            $longitude = $faker->longitude($min = -2, $max = 8);
            $number = $faker->buildingNumber();
            $address = $faker->streetName();

            if (mt_rand(0,1)){$placekindPrimary = $placekinds[mt_rand(0, count($placekinds)-1)];} else {$placekindPrimary = NULL;}
            if (mt_rand(0,1) && $placekindPrimary != NULL){$placekindSecondary = $placekinds[mt_rand(0, count($placekinds)-1)];} else {$placekindSecondary = NULL;}
            if (mt_rand(0,1) && $placekindSecondary != NULL){$placekindTertiary = $placekinds[mt_rand(0, count($placekinds)-1)];} else {$placekindTertiary = NULL;}

            $code = $postcodes['$city'][mt_rand(1, count($postcodes['$city']) - 1)];

            $place  ->setTitle($title)
                    ->setCoverImage($coverImage)
                    ->setDescription($description)
                    ->setCity($city)
                    //->setDistrict($district)
                    ->setLatitude($latitude)
                    ->setLongitude($longitude)
                    ->setPronoun($pronoun)
                    ->setName($name)
                    ->setNumber($number)
                    ->setAddress($address)
                    //->setPlacekindPrimary($placekindPrimary)
                    //->setPlacekindSecondary($placekindSecondary)
                    //->setPlacekindTertiary($placekindTertiary)
                    //->setPostcode($code);
            ;
            // Types d'un lieu
            /*
            for ($j = 1; $j <= mt_rand(0,3); $j++){
                $placekind = $placekinds[mt_rand(0, count($placekinds) - 1)];
                $place  ->addPlacekind($placekind);
                
                $manager->persist($place);
            }
            */

            $manager->persist($place);
            $places[] = $place;
        }

        // Festivals
        $festivals = [];

        for ($i = 1; $i <= 10; $i++){

            $festival = new Festival();

            $title = $faker->words(2);
            $title = $title[0].' '.$title[1];
            //$slug = $slugify->slugify($title);
            //$coverImage = $faker->imageUrl(1000,350);
            $coverSite = "https://picsum.photos";
            if (mt_rand(0,1)){
                //$coverImage = $faker->imageUrl(1000,350);
                $widthImage = mt_rand(800,1000);
                $heightImage = mt_rand(300,500);
            } elseif (mt_rand(0,1)){
                //$coverImage = $faker->imageUrl(1000,1200);
                $widthImage = mt_rand(800,1200);
                $heightImage = mt_rand(1200,1400);
            } else {
                //$coverImage = $faker->imageUrl(500,1000);
                $widthImage = mt_rand(400,600);
                $heightImage = mt_rand(1100,1200);
            }
            $coverImage = $coverSite . '/' . $widthImage . '/' . $heightImage;
            $description = '<p>' . join('</p><p>' , $faker->paragraphs(5)) . '</p>';
            $festival->setTitle($title)
                //->setSlug($slug)
                ->setCoverImage($coverImage)
                ->setDescription($description);

            $manager->persist($festival);
            $festivals[] = $festival;
        }
 
        // Rubriques
        $rubrics = [
            'Concert' => [
                'single' => 'concert',
                'plural' => 'concerts',
                'color'  => 'red'
            ],
            'Spectacle' => [
                'single' => 'spectacle',
                'plural' => 'spectacles',
                'color'  => 'black'
            ],
            'Soirée' => [
                'single' => 'soirée',
                'plural' => 'soirées',
                'color'  => 'blue'
            ],
            'Exposition' => [
                'single' => 'exposition',
                'plural' => 'expositions',
                'color'  => 'yellow'
            ],
            'Cinéma' => [
                'single' => 'projection cinématographique',
                'plural' => 'projections cinématographiques',
                'color'  => 'grey'
            ],
            'Action citoyenne' => [
                'single' => 'activité citoyenne',
                'plural' => 'activités citoyennes',
                'color'  => 'orange'
            ],
            'Sport' => [
                'single' => 'évènement sportif',
                'plural' => 'évènements sportifs',
                'color'  => 'green'
            ],
            'Stage' => [
                'single' => 'stage',
                'plural' => 'stages et ateliers',
                'color'  => 'pink'
            ],
            'Autre' => [
                'single' => 'évènement inclassable',
                'plural' => 'évènements inclassables',
                'color'  => 'white'
            ]
        ];
        $tabRubrics = [];
        
        foreach ($rubrics as $keyRubric => $valueRubric) {

            $rubric = new Rubric();

            $rubric ->setName($keyRubric)
                    ->setSingle($valueRubric['single'])
                    ->setPlural($valueRubric['plural'])
                    ->setColor($valueRubric['color']);

            $manager->persist($rubric);
            $tabRubrics[] = $rubric;

            $rubric_by_name[$slugify->slugify($keyRubric)] = $rubric;
        }

        // Categories
        $tabCategories = [
            "années 70" => [],
            "années 80" => ["soiree"],
            "années 90" => [],
            "années 2000" => [],
            "antiquité" => ["exposition"],
            "architecture" => ["exposition"],
            "art contemporain" => ["exposition"],
            "art de rue" => ["exposition"],
            "associatif" => ["action-citoyenne","stage","autre"],
            "atelier" => ["stage"],
            "athlétisme" => ["sport"],
            "avant-première" => ["cinema"],
            "braderie" => ["autre"],
            "blues" => ["concert"],
            "braderie" => ["autre"],
            "cabaret" => ["spectacle"],
            "chanson francaise" => ["concert"],
            "chorale" => ["concert"],
            "cirque" => ["spectacle"],
            "clubbing" => ["soiree"],
            "conférence" => ['action-citoyenne'],
            "comédie" => ["spectacle"],
            "court-métrage" => ["cinema"],
            "dancehall" => ["soiree"],
            "danse" => ["spectacle","stage"],
            "danse classique" => [],
            "danse contemporaine" => [],
            "danse de salon" => [],
            "danse sportive" => [],
            "danse traditionnelle" => [],
            "danse urbaine" => [],
            "déambulation" => ["action-citoyenne"],
            "débat" => ["cinema","action-citoyenne"],
            "dédicace" => ["autre"],
            "dessin" => ["exposition"],
            "disco" => ["soiree"],
            "documentaire" => ["cinema"],
            "drum and bass" => ["soiree"],
            "dubstep" => ["soiree"],
            "duo" => [],
            "électro" => ["soiree"],
            "engagé" => ["action-citoyenne"],
            "environnement" => ["action-citoyenne"],
            "football" => ["sport"],
            "foire" => ["autre"],
            "folk" => ["concert"],
            "funk" => ["concert"],
            "garden party" => ["soiree"],
            "geek" => ["cinema","autre"],
            "groove" => ["concert"],
            "handball" => ["sport"],
            "happy hours" => ["soiree"],
            "hip-hop" => ["concert"],
            "house" => ["soiree"],
            "humour" => ["spectacle"],
            "improvisation" => ["spectacle"],
            "initiation" => ["stage"],
            "jam session" => ["concert"],
            "jazz" => ["concert"],
            "jeune public" => ["concert","spectacle","cinema","autre"],
            "jeux" => ["autre"],
            "karaoké" => ["soiree"],
            "lecture" => ["autre"],
            "loisirs" => ["autre"],
            "manifestation" => ["action-citoyenne"],
            "marché" => ["autre"],
            "master class" => ["stage"],
            "musique électronique" => ["concert"],
            "musique classique" => ["concert"],
            "musique du monde" => ["concert"],
            "métal" => ["concert"],
            "natation" => ["sport"],
            "one man show" => ["spectacle"],
            "opéra" => ["spectacle"],
            "orchestre" => ["concert"],
            "peinture" => ["exposition"],
            "percussion" => [],
            "performance" => ["spectacle"],
            "photographie" => ["exposition"],
            "piano" => [],
            "politique" => ['action-citoyenne'],
            "pop" => ["concert"],
            "porte-ouverte" => ["autre"],
            "poésie" => ["spectacle"],
            "projection" => ["cinema"],
            "punk" => ["concert"],
            "quatuor" => [],
            "quintette" => [],
            "ragga jungle" => ["soiree"],
            "rap" => ["concert"],
            "rassemblement" => ["action-citoyenne"],
            "reggae" => ["concert"],
            "reggae dub" => ["soiree"],
            "rencontre" => ["action-citoyenne"],
            "réunion publique" => ["action-citoyenne"],
            "RnB" => ["concert"],
            "rock'n'roll" => ["concert"],
            "rugby" => ["sport"],
            "salon" => ["autre"],
            "salsa" => [],
            "sculpture" => ["exposition"],
            "showcase" => ["concert"],
            "slam" => ["concert"],
            "social" => ["action-citoyenne"],
            "soirée à thème" => ["soiree"],
            "soirée étudiante" => ["soiree"],
            "solo" => [],
            "soutien" => ["soiree"],
            "sport de combat" => ["sport"],
            "tango" => [],
            "techno" => ["soiree"],
            "tennis" => ["sport"],
            "théâtre" => ["spectacle"],
            "tourisme" => ["autre"],
            "trio" => [],
            "vernissage" => ["exposition"],
            "volleyball" => ["sport"],
            "workshop" => ["stage"],
            "yoga" => ["stage"],
            "zumba" => ["stage"],
        ];

        foreach ($tabCategories as $name => $arrayRubric) {

            $category = new Category();

            $category->setName($name);

            foreach ($arrayRubric as $rub) {
                $rubric = $rubric_by_name[$rub];
                $rubric->addCategory($category);
                $manager->persist($rubric);
            }

            $manager->persist($category);
            $categorys[] = $category;

            $category_by_name[$slugify->slugify($name)] = $category;
        }
                
        // Réductions
        $reductions = ['Carte culture' => false, 'Carte Atout Voir' => false, 'Tarif jeune public' => true];
        $tabReductions = [];

        foreach ($reductions as $title => $isNecessary) {

            $reduction = new Reduction();

            $reduction  ->setTitle($title)
                        ->setIsAlwaysNecessary($isNecessary);

            // Arrondissement par reduction
             for ($j = 1; $j <= mt_rand(8,10); $j++){
                $borough = $boroughs[mt_rand(0, count($boroughs) - 1)];
                $reduction->addBorough($borough);
                
                $manager->persist($reduction);
            }


            $manager->persist($reduction);
            $tabReductions[] = $reduction;
        }

        // Plateforme Vidéos
        $tab = [
            'Youtube' => ['https://www.youtube.com' => 'https://www.youtube.com/embed/'],
            'Vimeo' => ['https://vimeo.com/fr' => 'https://player.vimeo.com/video/'],
            'Dailymotion' => ['https://www.dailymotion.com' => 'https://www.dailymotion.com/embed/video/']
        ];
        $videostores = [];

        foreach ($tab as $k => $v) {
            $videostore = new Videostore();

            $name = $k;
            foreach ($v as $x => $y) {
                $website = $x;
                $embed = $y;
            }
            $videostore  ->setName($name)
                            ->setWebsite($website)
                            ->setEmbed($embed);

            $manager->persist($videostore);
            $videostores[] = $videostore;
        }

        // Billetteries
        $tab = [
            'Fnac'          => [
                'website'   => 'https://www.fnac.com',
                'domain'    => 'fnac.com',
                'source'    => 'https://productdata.awin.com/datafeed/download/apikey/83b88e251508aaadf5877111cb1c5a32/language/fr/fid/23455/columns/aw_product_id,product_name,search_price,currency,alternate_image,aw_deep_link,custom_1,custom_2,custom_3,custom_4,custom_5,custom_6,Tickets%3Avenue_address,Tickets%3Alongitude,Tickets%3Alatitude,product_short_description,description,Tickets%3Agenre,merchant_product_id,merchant_image_url,merchant_category/format/xml-tree/compression/zip/adultcontent/1/',
                'linked'    => 'fnac.xml',
                'embed'     => null,
                'redirect'  => ['awin.com','awin1.com'],
                'embeddable'=> false,
                'responsive'=> false
            ],
            'Digitick'      => [
                'website'   => 'https://www.digitick.com',
                'domain'    => 'digitick.com',
                'source'    => 'https://productdata.awin.com/datafeed/download/apikey/83b88e251508aaadf5877111cb1c5a32/language/fr/fid/22739/columns/aw_deep_link,product_name,aw_product_id,merchant_product_id,merchant_image_url,description,merchant_category,search_price,merchant_name,merchant_id,category_name,aw_image_url,currency,store_price,delivery_cost,merchant_deep_link,last_updated,display_price,data_feed_id,in_stock,is_for_sale,merchant_thumb_url,Tickets%3Aevent_date,Tickets%3Aevent_name,Tickets%3Avenue_name,Tickets%3Avenue_address,Tickets%3Aavailable_from,Tickets%3Agenre,Tickets%3Amin_price,Tickets%3Alongitude,Tickets%3Alatitude,Tickets%3Aevent_location_address,Tickets%3Aevent_location_city,Tickets%3Aevent_location_country,Tickets%3Aevent_duration,Tickets%3Amax_price,pre_order,web_offer,valid_to/format/xml-tree/compression/zip/adultcontent/1/',
                'linked'    => 'digitick.xml',
                'embed'     => null,
                'redirect'  => ['awin.com','awin1.com'],
                'embeddable'=> false,
                'responsive'=> false
            ],
            "Carrefour"     => [
                'website'   => 'https://www.spectacles.carrefour.fr',
                'domain'    => 'carrefour.fr',
                'source'    => "https://productdata.awin.com/datafeed/download/apikey/83b88e251508aaadf5877111cb1c5a32/language/fr/fid/29961/columns/product_name,product_short_description,description,merchant_image_url,search_price,currency,aw_thumb_url,alternate_image_three,Tickets%3Agenre,merchant_product_id,aw_product_id,merchant_category,merchant_deep_link,aw_deep_link/format/xml-tree/compression/zip/adultcontent/1/",
                'linked'    => 'carrefour.xml',
                'embed'     => null,
                'redirect'  => ['awin.com','awin1.com'],
                'embeddable'=> false,
                'responsive'=> false
            ],
            'Ticketmaster'  => [
                'website'   => 'https://www.ticketmaster.fr',
                'domain'    => 'ticketmaster.fr',
                'source'    => 'https://app.ticketmaster.com/discovery/v2/events?apikey=7VDvqykz9BxdfkVyb7eVxESribUANcpU&locale=fr-FR&countryCode=FR', // &city=strasbourg&size=50&page=1
                'linked'    => null,
                'embed'     => null,
                'redirect'  => ['ticketmaster.com', 'tm7516.net'],
                'embeddable'=> false,
                'responsive'=> false
            ],
            "Ticketac"      => [
                'website'   => 'https://www.ticketac.com',
                'domain'    => 'ticketac.com',
                'source'    => 'http://adxml.publicidees.com/xml.php?progid=2986&partid=51257',
                'linked'    => 'ticketac.xml',
                'embed'     => null,
                'redirect'  => ['publicidees.com'],
                'embeddable'=> false,
                'responsive'=> false
            ],
            "Datatourisme"  => [
                'website'   => 'https://www.datatourisme.gouv.fr/',
                'domain'    => 'gouv.fr',
                'source'    => 'https://diffuseur.datatourisme.gouv.fr/webservice/ce859609d795085d9dc6400859cdaef4/3d884177-7469-44a1-a3f4-f64f1597c46a',
                'linked'    => 'gouv.xml',
                'embed'     => null,
                'redirect'  => ['gouv.fr'],
                'embeddable'=> false,
                'responsive'=> false
            ],
            "YuTicket"=> [
                'website'   => 'https://www.yuticket.com/',
                'domain'    => 'yuticket.com',
                'source'    => null,
                'linked'    => null,
                'embed'     => 'https://www.yuticket.com/iframe/ticketing/',
                'redirect'  => null,
                'embeddable'=> true,
                'responsive'=> false
            ],
            "TicketTailor"=> [
                'website'   => 'https://www.tickettailor.com/',
                'domain'    => 'tickettailor.com',
                'source'    => null,
                'linked'    => null,
                'embed'     => '',
                'redirect'  => ['https://buytickets.at'],
                'embeddable'=> false,
                'responsive'=> false
            ],
            "YesGoLive"=> [
                'website'   => 'https://www.yesgolive.com/',
                'domain'    => 'yesgolive.com',
                'source'    => null,
                'linked'    => null,
                'embed'     => 'https://www.yesgolive.com/w/',
                'redirect'  => null,
                'embeddable'=> true,
                'responsive'=> false
            ],
            "BilletWeb"=> [
                'website'   => 'https://www.billetweb.fr/',
                'domain'    => 'billetweb.fr',
                'source'    => null,
                'linked'    => null,
                'embed'     => 'https://www.billetweb.fr/shop.php?event=',
                'redirect'  => null,
                'embeddable'=> true,
                'responsive'=> false
            ],
            "YurPlan"=> [
                'website'   => 'https://yurplan.com/',
                'domain'    => 'yurplan.com',
                'source'    => null,
                'linked'    => null,
                'embed'     => null, //'/tickets/widget?from=widget&default_culture=fr
                'redirect'  => null,
                'embeddable'=> true,
                'responsive'=> false
            ],
            "EventBrite"=> [
                'website'   => 'https://www.eventbrite.fr/',
                'domain'    => 'eventbrite.fr',
                'source'    => null,
                'linked'    => null,
                'embed'     => null,
                'redirect'  => null,
                'embeddable'=> true,
                'responsive'=> false
            ],
            "HelloAsso"=> [
                'website'   => 'https://www.helloasso.com/',
                'domain'    => 'helloasso.com',
                'source'    => null,
                'linked'    => null,
                'embed'     => null,
                'redirect'  => null,
                'embeddable'=> true,
                'responsive'=> false
            ],
            "Weezevent"=> [
                'website'   => 'https://www.weezevent.com/',
                'domain'    => 'weezevent.com',
                'source'    => null,
                'linked'    => null,
                'embed'     => null,
                'redirect'  => null,
                'embeddable'=> true,
                'responsive'=> false
            ],
            "Google"=> [
                'website'   => 'https://docs.google.com/',
                'domain'    => 'google.com',
                'source'    => null,
                'linked'    => null,
                'embed'     => null,
                'redirect'  => ['https://goo.gl'],
                'embeddable'=> true,
                'responsive'=> true
            ]
        ];

        /*
        $tab = [
            'Fnac' => ['https://www.fnac.com' => null],
            'Ticketmaster' => ['https://www.ticketmaster.fr/' => null],
            'Digitick' => ['https://www.dailymotion.com' => 'https://www.dailymotion.com/embed/video/']
        ];
        */
        $ticketings = [];

        foreach ($tab as $name => $data) {
            $ticketing = new Ticketing();

            $ticketing  ->setName($name)
                        ->setWebsite($data['website'])
                        ->setDomain($data['domain'])
                        ->setSource($data['source'])
                        ->setLinked($data['linked'])
                        ->setEmbed($data['embed'])
                        ->setRedirections($data['redirect'])
                        ->setIsEmbeddable($data['embeddable'])
                        ->setisOnlyResponsive($data['responsive'])
                        ->setSetting(1)
                        ->setIsActivated(true);

            $manager->persist($ticketing);
            $ticketings[] = $ticketing;
        }


        $subdomains = [
            'Strasbourg'    => "Strasbourg Curieux",
            'Nancy'         => "Nancy Curieux",
            'Metz'          => "Metz Curieux"
        ];

        foreach($subdomains as $key => $value){
            $subdomain = new Subdomain();
            $subdomain  ->setName($value)
                        ->setSlug($slugify->slugify($key))
                        ->setMainBorough($boroughByChieftown[$key])
                        ->addBorough($boroughByChieftown[$key]);

            $subdomain->setResident([
                'singular' => [
                    'masculine' => null,
                    'feminine' => null
                ],
                'plural' => [
                    'masculine' => null,
                    'feminine' => null
                ]
            ]);

            $subdomain->setData([
                'description'   => null,
                'keywords'      => null
            ]);

            $subdomain->setStatistical([
                'provider'  => null,
                'key'       => null
            ]);

            $subdomain->setNetwork([
                'facebook'  => null,
                'instagram' => null,
                'twitter'   => null,
                'youtube'   => null
            ]);

            $manager->persist($subdomain);
        }


        // Evènements
        for ($i = 1; $i <= 500; $i++){

            $event = new Event();

            $title = $faker->sentence();
            //$slug = $slugify->slugify($title);

            //$coverImage = $faker->imageUrl(1000,350);
            

            $description = '<p>' . join('</p><p>' , $faker->paragraphs(5)) . '</p>';

            $coverSite = "https://picsum.photos";
            if (mt_rand(0,1)){
                //$coverImage = $faker->imageUrl(1000,350);
                $widthImage = mt_rand(800,1000);
                $heightImage = mt_rand(300,500);
            } elseif (mt_rand(0,1)){
                //$coverImage = $faker->imageUrl(1000,1200);
                $widthImage = mt_rand(800,1200);
                $heightImage = mt_rand(1200,1400);
            } else {
                //$coverImage = $faker->imageUrl(500,1000);
                $widthImage = mt_rand(400,600);
                $heightImage = mt_rand(1100,1200);
            }
            $coverImage = $coverSite . '/' . $widthImage . '/' . $heightImage;
            
            if (mt_rand(0,1)){
                $user = null;
            } elseif (mt_rand(0,1)){
                $user = $users[0];
            } else {
                $user = $users[mt_rand(0, count($users) - 1)];
            }
            
            /* Debug
            if (rand(0,1)){
                $place = $places[mt_rand(0, count($places) - 1)];
                $city = NULL;
                $placeTemporary = NULL;
            } else {
                $place = NULL;
                $city = $citys[mt_rand(0, count($citys) - 1)];
                $placeTemporary = $faker->word();
            }
            */
             
            $city = $citys[mt_rand(0, count($citys) - 1)];
            $placeTemporary = $faker->word();

            if (rand(0,1)){$festival = $festivals[mt_rand(0, count($festivals) - 1)];} else {$festival = NULL;}
           
            $dateStart = $faker->dateTimeBetween('-5 days','+10 days');
            
            $duration = mt_rand(0,3);
            $dateEnd = (clone $dateStart)->modify("+$duration days");

            if (rand(0,1)){
                $hourStart = mt_rand(10, 24);
                if (rand(0,1)){$minuteStart="00";}
                elseif (rand(0,1)){$minuteStart="45";}
                else {$minuteStart="30";}
                $timeStart = $hourStart.":".$minuteStart.":00";
                $timeStart = \DateTime::createFromFormat('H:i:s', $timeStart);
                if (rand(0,1)){
                    $duration = mt_rand(1,6);
                    $hourEnd = $hourStart + $duration;
                    if ($hourEnd>24){$hourEnd=$hourEnd-24;}
                    $timeEnd = $hourEnd.":00:00";
                    $timeEnd = \DateTime::createFromFormat('H:i:s', $timeEnd);
                } else {
                    $timeEnd = NULL;
                }
            } else {
                $timeStart = NULL;
                $timeEnd = NULL;
            }
            
            
            if (rand(0,1)) {
                $price = mt_rand(0,19);
                if (rand(0,1)){$price .= ".50";}
            } elseif (rand(0,1)) {
                $price = 0;
            } elseif (rand(0,1)) {
                $price = 0.01;
            } elseif (rand(0,1)) {
                $price = 22;
            } elseif (rand(0,1)) {
                $price = 21;
            } else {
                $price = 20;
            }

            $rubric = $tabRubrics[mt_rand(0, count($tabRubrics) - 1)];

            // Association de catégories avec un évènement
            for ($j = 1; $j <= mt_rand(0,3); $j++){
                $event->addCategory($categorys[mt_rand(0, count($categorys) - 1)]);
            }

            $event->setTitle($title)
                //->setSlug($slug)
                ->setDateStart($dateStart)
                ->setDateEnd($dateEnd)
                ->setTimeStart($timeStart)
                ->setTimeEnd($timeEnd)
                ->setCoverImage($coverImage)
                ->setDescription($description)
                ->setPrice($price)
                //->setAuthor($user)
                //->setPlace($place)
                ->setFestival($festival)
                ->setRubric($rubric)
                ->setCity($city)
                ->setPlaceTemporary($placeTemporary)
                ->setIsPublished(true)
                ->setIsValidated(true);


            // Billets
            if ($price > 6 && $price < 22){
                for ($j = 1; $j <= mt_rand(0,3); $j++){
                    $tickets = new Ticket();

                    $url = $faker->url();
                    if ($price == 21) {$price = mt_rand(21,60);}
                    elseif ($price > 6 && $price < 21) {$price = $price - 1.80;}
                    $ticketing = $ticketings[mt_rand(0, count($ticketings) - 1)];

                    $tickets ->setUrl($url)
                            ->setPrice($price)
                            ->setEvent($event)
                            ->setTicketing($ticketing);
                    
                    $manager->persist($tickets);
                }
            }

            // Tarifs réduits d'un évènement
            if ($price > 6 && $price < 22){
                for ($j = 1; $j <= mt_rand(0,4); $j++){
                    $prices = new Prices();

                    if ($price == 21) {
                        $price = mt_rand(21,60);
                    }
                    elseif ($price > 6 && $price < 21) {
                        $price = $price - 1.80;
                    }
                    $reduction = $tabReductions[mt_rand(0, count($tabReductions) - 1)];

                    $prices ->setPrice($price)
                            ->setEvent($event)
                            ->setReduction($reduction);
                    
                    $manager->persist($prices);
                }
            }

            // Images d'un évènement
            for ($j = 1; $j <= mt_rand(2,5); $j++){
                $image = new Image();

                $image  ->setUrl($faker->imageUrl())
                        ->setCaption($faker->sentence())
                        ->setEvent($event);
                
                $manager->persist($image);
            }

            // Vidéos d'un évènement
            $youtubeIds = ['c10SJto6oqk','Leienp-spqc','BtyHYIpykN0','k489OfT29BE','zp9Da20n0_k','W-gmIG41dGA','3egagRvougI','xZx3LDukUdA','IILAfcHbe8U','c10SJto6oqk','39y8RWATD04','Dab4EENTW5I','cb8rr6gHeUQ','Jb4uXBJgrys','gwQ2EuN7lpc'];
            
            for ($j = 1; $j <= mt_rand(0,4); $j++){
                $video = new Video();

                $videoUrl = 'https://www.youtube.com/watch?v=';
                $videoUrl .= $youtubeIds[mt_rand(0, count($youtubeIds) - 1)];
                $videostore = $videostores[mt_rand(0, count($videostores) - 1)];

                $video  ->setUrl($videoUrl)
                        ->setCaption($faker->sentence())
                        ->setEvent($event)
                        ->setVideostore($videostore);
                
                $manager->persist($video);
            }

            // Commentaires d'un évènement
            for ($j = 1; $j <= mt_rand(0,5); $j++){
                $comment = new Comment();

                $user2 = $users[mt_rand(0, count($users) - 1)];

                $comment ->setContent($faker->paragraph())
                         ->setRating(mt_rand(1,5))
                         ->setAuthor($user2)
                         ->setEvent($event);

                $manager->persist($comment);
            }
            
            $manager->persist($event);
        }
        $manager->flush();

        echo "La base de donnée a été remplie via les fixtures !\n";
    }
}

