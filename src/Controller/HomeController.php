<?php

namespace App\Controller;

use App\Service\Website;
use App\Repository\EventRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    public function __construct(Website $website)
    {
        $this->website = $website->getArray();
        $this->subdomain = $website->getArray()['address']['subdomain'];
    }

    /**
     * @Route("/", name="homepage")
     */
    public function home(EventRepository $repoEvent, Request $request){

        // Action temporaire
        return $this->redirectToRoute('event_index');

        $today = date('Y-m-d', strtotime('now'));
        $events = $repoEvent->findEventsForMenu($today, $this->subdomain);
        
        return $this->render('home.html.twig', [
                'page'      => 'home',
                'events'    => $events,
                'website'   => $this->website,
                'directory' => NULL
            ]
        );
    }


}