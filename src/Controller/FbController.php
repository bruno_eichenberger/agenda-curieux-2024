<?php

namespace App\Controller;

use App\Service\Website;
use App\Service\ApiFacebook;
use Facebook\Exceptions\FacebookSDKException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Facebook\Exceptions\FacebookResponseException;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FbController extends AbstractController
{
    private $session;

    public function __construct(ApiFacebook $facebook)
    {
        $this->facebook = $facebook;
    }
    
    /**
     * @Route("/fb", name="fb")
     */
    public function index(): Response
    {        
        return new Response('<a href="' . $this->facebook->getLoginUrl() . '">Connexion avec Facebook!</a>');
        /*
        return $this->render('fb/index.html.twig', [
            'controller_name' => 'FbController',
        ]);
        */
    }

    /**
     * @Route("/login-callback", name="log_fb")
     */
    public function callback(RequestStack $requestStack): Response
    {
        $helper = $this->facebook->getObject()->getRedirectLoginHelper();
        
        try {
            $accessToken = $helper->getAccessToken();
        } 
        catch(FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
        
        if (isset($accessToken)) {
            // Logged in!
            //$_SESSION['facebook_access_token'] = (string) $accessToken;
            $requestStack->getSession()->set('facebook_access_token',(string) $accessToken);
            
            return $this->redirectToRoute('api_fb');
            // Now you can redirect to another page and use the
            // access token from $_SESSION['facebook_access_token']b
        }

        return new Response("Connexion n'est pas établie avec Facebook!");
    }

    /**
     * @Route("/api-fb", name="api_fb")
     */
    public function apifb(Website $website): Response
    {
        return $this->render('fb/index.html.twig', [
            'website'   => $website->getArray(),
            'events'    => $this->facebook->getEventsFB()['events'],
            'pagination'=> $this->facebook->getEventsFB()['pagination']
        ]);
        //return new Response('Connexion établie avec Facebook! Token : '. $_SESSION['facebook_access_token']);
    }

}