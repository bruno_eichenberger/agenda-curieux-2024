<?php

namespace App\Controller;

use App\Entity\City;
use App\Entity\Borough;
use App\Entity\Postcode;
use App\Service\Website;
use App\Entity\Department;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TransfertController extends AbstractController
{

    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager, Website $website, ManagerRegistry $doctrine)
    {
        $this->entityManager = $entityManager;
        $this->website = $website->getArray();
        $this->subdomain = $website->getArray()['address']['subdomain'];
        $this->doctrine = $doctrine;
    }


    function get_distance_m($lat1, $lng1, $lat2, $lng2) {
        $earth_radius = 6378137;   // Terre = sphère de 6378km de rayon
        $rlo1 = deg2rad($lng1);
        $rla1 = deg2rad($lat1);
        $rlo2 = deg2rad($lng2);
        $rla2 = deg2rad($lat2);
        $dlo = ($rlo2 - $rlo1) / 2;
        $dla = ($rla2 - $rla1) / 2;
        $a = (sin($dla) * sin($dla)) + cos($rla1) * cos($rla2) * (sin($dlo) * sin($dlo));
        $d = 2 * atan2(sqrt($a), sqrt(1 - $a));
        return ($earth_radius * $d);
    }

    /**
     * @Route("/transfert", name="transfert_index")
     * @Security("is_granted('ROLE_ADMIN')", message="Vous n'avez pas le droit d'accéder à cette page'")
     * 
     * @return Response
     */
    public function transfert(Request $request)
    {
        return new Response(
            '<html><body>
            <a href="/importation-des-villes-francaises" >Importations des villes francaises</a><br><br>
            <a href="/gps-arrondissement" >Transfert des GPS pour les arrondissements</a><br><br>
            </body></html>'
        );
    }

    /**
     * @Route("/importation-des-villes-francaises/", name="transfert_city")
     * @Security("is_granted('ROLE_ADMIN')", message="Vous n'avez pas le droit d'accéder à cette page'")
     * 
     * @return Response
     */
    public function transfertCity(Request $request)
    {
        
        set_time_limit(100);
        ini_set("memory_limit", "-1");
        $slugify = new Slugify();
        $laposte = [];
        
        if (($handle = fopen("json/laposte_gps.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {

                //if ($data[6] != ""){
                    $name = "*".$data[1];
                    $name = str_replace(" st "," saint ",mb_strtolower($name));
                    $name = str_replace(" ste "," sainte ",$name);
                    $name = str_replace("*st ","*saint ",mb_strtolower($name));
                    $name = str_replace("*ste ","*sainte ",$name);
                    $name = str_replace("*","",$name);

                    $explode = explode(" ", $name);
                    if (is_numeric(end($explode))){
                        $arrondissement = end($explode);
                        $name = str_replace(" ".$arrondissement, "", $name);
                    } else {
                        $arrondissement = null;
                    }

                    $name = $slugify->slugify($name);
                    $numDep = substr($data[0], 0, 2);
                    if (is_numeric($numDep) and $numDep > 96){
                        $numDep = substr($data[0], 0, 3);
                    }

                    // correction
                    $name = str_replace("beaujeu-saint-vallier-pierrejux-quitteur","beaujeu-saint-vallier-pierrejux-et-quitteur",$name);
                    $name = str_replace("roche-linotte-et-sorans-cordiers","roche-sur-linotte-et-sorans-les-cordiers",$name);
                    $name = str_replace("la-villeneuve-bellenoye-la-maize","la-villeneuve-bellenoye-et-la-maize",$name);
                    $name = str_replace("saint-laurent-bains-laval-d-aurelle","saint-laurent-les-bains-laval-d-aurelle",$name);
                    $name = str_replace("montigny-mornay-villeneuve-vingeanne","montigny-mornay-villeneuve-sur-vingeanne",$name);
                    $name = str_replace("saint-remy-en-bouzemont-saint-genest-isson","saint-remy-en-bouzemont-saint-genest-et-isson",$name);
                    $name = str_replace("","",$name);
                    /*
                    if ($data[0] == "07262"){
                        dd($name);
                    }
                    */

                    $laposte[$numDep][$name][] = [
                        'gps' => $data[6],
                        'arrondissement' => $arrondissement,
                        'code' => [
                            'insee'         => $data[0],
                            'postal'        => $data[2]
                        ]
                    ];

                    // Doublon pour debug certain nom aléatoire

                    $name = "*".$data[5];
                    $name = str_replace(" st "," saint ",mb_strtolower($name));
                    $name = str_replace(" ste "," sainte ",$name);
                    $name = str_replace("*st ","*saint ",mb_strtolower($name));
                    $name = str_replace("*ste ","*sainte ",$name);
                    $name = str_replace("*","",$name);

                    $explode = explode(" ", $name);
                    if (is_numeric(end($explode))){
                        $arrondissement = end($explode);
                        $name = str_replace(" ".$arrondissement, "", $name);
                    } else {
                        $arrondissement = null;
                    }

                    $name = $slugify->slugify($name);

                    $laposte[$numDep][$name][] = [
                        'gps' => $data[6],
                        'arrondissement' => $arrondissement,
                        'code' => [
                            'insee'         => $data[0],
                            'postal'        => $data[2]
                        ]
                    ];
                //}
            }
            fclose($handle);
        }

        //dd($laposte['67']['strasbourg']);

        $page = $request->query->getInt('page');

        $departments = $this->doctrine->getRepository(Department::class)->findAll();
        $department = $departments[$page];
        $codeDep = $department->getCode();

        $cities = $this->doctrine->getRepository(City::class)->findBy(['department' => $department->getId()]);
        $array_cities = [];
        foreach ($cities as $city){
            $cityByName[$city->getName()] = $city;
            $array_cities[] = $city->getName();
        }

        $boroughs = $this->doctrine->getRepository(Borough::class)->findAll();
        foreach ($boroughs as $borough){
            $borough_by_chieftown[$borough->getChiefTown()] = $borough;
            $boroughs_by_dep[$borough->getDepartment()->getCode()][] = $borough;
        }
        

        $url = "https://geo.api.gouv.fr/communes?codeDepartement=".$codeDep."&fields=nom,code,codesPostaux,codeDepartement";
        $json = file_get_contents($url);
        $json_data = json_decode($json, true);

        //dd($json_data);

        foreach ($json_data as $data) {
            
            $name = $data['nom'];

            if ($codeDep == "04" and $name == "Corbières"){
                $name = "Corbières-en-Provence";
            }
            if ($codeDep == "05" and $name == "Dévoluy"){
                $name = "Le Dévoluy";
            }
            if ($codeDep == "16"){
                if ($name == "Bors (Canton de Tude-et-Lavalette)"){
                    $name = "Bors-de-Montmoreau";
                }
                if ($name == "Bors (Canton de Charente-Sud)"){
                    $name = "Bors-de-Baignes";
                }
                if ($name == "Exideuil"){
                    $name = "Exideuil-sur-Vienne";
                }
            }
            if ($codeDep == "21"){
                if ($name == "Collonges-lès-Premières" or $name = "Premières"){
                    $name = "Collonges-et-Premières";
                }
                if ($name == "Crimolois" or $name == "Neuilly-lès-Dijon"){
                    $name = "Neuilly-Crimolois";
                }
            }
            if ($codeDep == "30" and $name == "Saint-Christol-lès-Alès"){
                $name = "Saint-Christol-lez-Alès";
            }
            if ($codeDep == "35" and $name == "Saint-Père"){
                $name = "Saint-Père-Marc-en-Poulet";
            }
            if ($codeDep == "45"){
                if ($name == "Courcelles"){
                    $name = "Courcelles-le-Roi";
                }
                if ($name == "Saint-Loup-de-Gonois"){
                    $name = "La Selle-sur-le-Bied";
                }
            }
            if ($codeDep == "51" and $name == "Breuil"){
                $name = "Breuil-sur-Vesle";
            }
            if ($codeDep == "57" and $name == "Varize-Vaudoncourt"){
                $name = "Varize";
            }
            if ($codeDep == "59" and $name == "Aix"){
                $name = "Aix-en-Pévèle";
            }
            if ($codeDep == "64" and $name == "Castillon (Canton d'Arthez-de-Béarn)"){
                $name = "Castillon-d'Arthez";
            }
            if ($codeDep == "64" and $name == "Castillon (Canton de Lembeye)"){
                $name = "Castillon-de-Lembeye";
            }
            
            if ($codeDep == "70" and $name == "Saint-Remy"){
                $name = "Saint-Rémy-en-Comté";
            }
            if ($codeDep == "79" and $name == "Champdeniers-Saint-Denis"){
                $name = "Champdeniers";
            }
            if ($codeDep == "84" and $name == "Castellet"){
                $name = "Castellet-en-Luberon";
            }
            if ($codeDep == "93" and $name == "Saint-Ouen"){
                $name = "Saint-Ouen-sur-Seine";
            }
            if ($codeDep == "95" and $name == "Herblay"){
                $name = "Herblay-sur-Seine";
            }

            $slug = str_replace("ü","u",$name);
            $slug = $slugify->slugify($slug);

            // Si la base est vide ou si la ville est déjà enregistrée
            if ($array_cities and in_array($name,$array_cities)){
                $city = $cityByName[$name];
            } else {
                $city = new City();
            }

            $city   ->setName($name)
                    ->setDepartment($department);

            if (isset($laposte[$codeDep][$slug]) and $laposte[$codeDep][$slug]['0']['gps'] != ""){

                $explodeGps = explode(",",$laposte[$codeDep][$slug]['0']['gps']);
                $city   ->setLatitude($explodeGps[0])
                        ->setLongitude($explodeGps[1]);
                
                /*     
                if ($data["codeDepartement"] == "88" && $explodeGps[1] < 6.08){
                    $city->setBorough($borough_by_chieftown["Vittel"]);
                }
                elseif ($data["codeDepartement"] == "88" && $explodeGps[1] > 6.08){
                    $city->setBorough($borough_by_chieftown["Épinal"]);
                }
                elseif ($data["codeDepartement"] == "35"){
                    $city->setBorough($borough_by_chieftown["Rennes"]);
                }
                else {
                    
                }
                */

                $distance = [];
                foreach ($boroughs_by_dep[$codeDep] as $borough){
                    $distance[$this->get_distance_m($borough->getLatitude(), $borough->getLongitude(), $city->getLatitude(), $city->getLongitude())] = $borough;
                }
                ksort($distance);

                $city->setBorough(reset($distance));
                
                /*
                if ($city->getName() == "Marseille"){
                    $city->setBorough($borough_by_chieftown[$data['nom']]);
                }
                */

                $this->entityManager->persist($city);

                foreach ($data["codesPostaux"] as $code) {
                
                    $postcode = new Postcode();
                    $postcode   ->setCode($code)
                                ->setCity($city);

                    $this->entityManager->persist($postcode);
                }

                

            } else {
                //dd($codeDep.' '.$slug.' '.$name);
            }

            $this->entityManager->flush();

            // Ajouter le nouveau lieu dans le compteur
            if ($array_cities == [] or in_array($name,$array_cities) == false){
                $cities[] = $city;
            }
            
            /*if (isset($borough_by_chieftown[$chieftown])){
                $city->setBorough($borough_by_chieftown[$chieftown]);
            } else{
                $city->setBorough($borough_by_chieftown["Strasbourg"]);
            }
            */

            
            
            /*
            if ($data["codeDepartement"] == "68" && $explodeGps[0]>47.927316){
                $city->setBorough($borough_by_chieftown["Colmar"]);
            } 
            
            else {
                $city->setBorough($borough_by_dep[$data["codeDepartement"]]);
            }
            */
            

        }

        if ($page >= count($departments)-1){
            return new Response(
                '<html><body>
                Transfert des villes réussi<br><br>                
                </body></html>'
            );
        }
        elseif (is_int($page/15) && $page != "0"){
            //$page++;
            $pageNext = $page+1;
            return new Response(
                '<html><body>
                    Page '.$page.' - Importation des villes en cours<br><br>

                    '.count($cities).' villes en base de données dans le département '.$department->getName().'<br><br>
                    
                    <a href="?page='.$pageNext.'">Continuer le processus</a>
                </body></html>'
            );
        }
        else {
            $page++;
            return $this->redirect('?page='.$page);
        }
        
        //}
    }


    /**
     * @Route("/gps-arrondissement/", name="gps-borough")
     */
    public function gps_borough()
    {
        $laposte = [];
        $slugify = new Slugify();

        if (($handle = fopen("json/laposte_gps.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
    
                $name = "*".mb_strtolower($data[1]);
                $name = str_replace(" st "," saint ",$name);
                $name = str_replace(" ste "," sainte ",$name);
                $name = str_replace("*st ","*saint ",$name);
                $name = str_replace("*ste ","*sainte ",$name);
                $name = str_replace("*","",$name);
    
                $explode = explode(" ", $name);
                if (is_numeric(end($explode))){
                    $arrondissement = end($explode);
                    $name = str_replace(" ".$arrondissement, "", $name);
                } else {
                    $arrondissement = null;
                }
    
                $name = $slugify->slugify($name);
                $name = str_replace("chateau-chinon-ville","chateau-chinon",$name);
    
                $numDep = substr($data[0], 0, 2);
                if (is_numeric($numDep) and $numDep > 96){
                    $numDep = substr($data[0], 0, 3);
                }
    
                $laposte[$numDep][$name][] = [
                    'gps' => $data[6],
                    'arrondissement' => $arrondissement,
                    'code' => [
                        'insee'         => $data[0],
                        'postal'        => $data[2]
                    ]
                ];
                
            }
            fclose($handle);
        }

        $boroughs = $this->doctrine->getRepository(Borough::class)->findAll();
        foreach ($boroughs as $borough){
            $borough_by_chieftown[$borough->getChiefTown()] = $borough;
        }

        foreach ($boroughs as $borough) {

            $slug = $borough->getSlug();
            $codeDep = $borough->getDepartment()->getCode();

            if (isset($laposte[$codeDep][$slug])){
                $explodeGps = explode(",",$laposte[$codeDep][$slug]['0']['gps']);
                $borough->setLatitude($explodeGps[0])
                        ->setLongitude($explodeGps[1]);
            } else {
                dd($slug);
            }

            $this->entityManager->persist($borough);
            
        }
        $this->entityManager->flush();

        return new Response(
            '<html><body>
            Ajout des coordonnées GPS réussi !<br><br>                
            </body></html>'
        );

    }

    /**
     * @Route("/test-gps/", name="test-gps")
     */
    public function transfertGPS()
    {
        $row = 1;
        if (($handle = fopen("json/laposte_gps.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                if (isset($data[0]) && isset($data[1])){
                    $tabGPS = explode(";",$data[0]);
                    $id = $tabGPS[4]." ".$tabGPS[0];
                    $gps_by_city[$id] = $tabGPS[6].",".$data[1];
                    //echo "<p> ".$tabGPS[4]." ---- ".$tabGPS[6].",".$data[1]."<br /></p>\n";

                }
                //if ($num>1)
                //
                
            }
                //$gps_by_city[] = 
            fclose($handle);
        }

        dd($gps_by_city);

        return new Response(
            '<html><body>
                Test ok<br>
            </body></html>'
        );
            
    }

}
