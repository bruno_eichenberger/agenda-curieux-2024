<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Event;
use App\Service\Sender;
use App\Service\Website;
use App\Form\AccountType;
use App\Entity\PasswordUpdate;
use App\Form\RegistrationType;
use App\Service\UploaderHelper;
use App\Form\PasswordForgotType;
use App\Form\PasswordUpdateType;
use Symfony\Component\Form\FormError;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class AccountController extends AbstractController
{   
    public function __construct(Website $website, ManagerRegistry $doctrine)
    {
        $this->website = $website->getArray();
        $this->doctrine = $doctrine;
    }
    
    /**
     * Permet d'afficher et de gérer le formulaire de connexion
     * 
     * @Route("/connexion", name="account_login")
     * 
     * @return Response
     */
    public function login(Request $request, AuthenticationUtils $utils)
    {
        $error = $utils->getLastAuthenticationError();
        $username = $utils->getLastUsername();

        if ($error && $error->getMessage() != "Bad credentials."){
            $message = $error->getMessage();
        } else {
            $message = null;
        }

        //$session = new Session();
        //dd($session->get('token'));

        /*
        if ($this->getUser()){
            return $this->redirectToRoute('event_index');
        }
        */

        return $this->render('account/login.html.twig', [
            'website'   => $this->website,
            'page'      => 'account',
            'hasError'  => $error !== null,
            'message'   => $message,
            'username'  => $username,
        ]);
    }

    /**
     * Permet de se deconnecter
     * 
     * @Route("/deconnexion", name="account_logout")
     * 
     * @return void
     */
    public function logout() { 
        // Rien !!
    }


    /**
     * Permet d'afficher le formulaire d'inscription
     * 
     * @Route("/inscription", name="account_register")
     * 
     * @return Response
     */
    public function register(Request $request, Sender $sender, EntityManagerInterface $manager, UserPasswordHasherInterface $passwordHasher, TokenGeneratorInterface $tokenGenerator) {
        $user = new User();

        $form = $this->createForm(RegistrationType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $hash = $passwordHasher->hashPassword($user, $user->getPassword());
            $user->setHash($hash);
            $user->setToken($tokenGenerator->generateToken());
            //$user->setPasswordRequestedAt(new \Datetime());

            $user->setFirstName(ucfirst($user->getFirstName()));
            $user->setLastName(ucfirst($user->getLastName()));

            $manager->persist($user);
            $manager->flush();

            // on utilise le service Sender
            $bodyMail = $sender->createBodyMail('emails/signup.html.twig', [
                'user' => $user
            ]);
            $sender->sendMessage('bonjour@curieux.net', $user->getEmail(), 'Activation de votre compte sur Curieux.net', $bodyMail);

            $this->addFlash('success', "Votre compte a bien été créé !<br>Un mail vous a été envoyé afin que vous puissiez confirmer votre adresse email.");

            return $this->redirectToRoute('account_login');
        }
        
        return $this->render('account/registration.html.twig', [
            'page' => 'account',
            'form' => $form->createView(),
            'website'   => $this->website
        ]);
    }

    /**
     * Activation du compte (Via un lien recu par mail)
     * 
     * @Route("/activation-du-compte/{token}", name="account_activation")
     */
    public function activation(User $user, $token, EntityManagerInterface $manager, Request $request)
    {
        //$user = $this->doctrine->getRepository(User::class)->findOneBy(['token' => $token]);

        //dd($user);

        // interdit l'accès à la page si:
        // - le token associé au membre est null
        // - le token enregistré en base et le token présent dans l'url ne sont pas égaux
        if ($user->getToken() === null || $token !== $user->getToken())
        {
            throw new AccessDeniedHttpException();
        }

        // réinitialisation du token à null pour qu'il ne soit plus réutilisable
        $user->setToken(null);
        //$user->setPasswordRequestedAt(null);

        // ??? Si problème, suppression du token de session
        // $session = new Session();
        // $session->set('token', null);

        // activation du compte
        //$user->setLevel("0");
        $user->setIsActivated(true);

        $events = $this->doctrine->getRepository(Event::class)->findBy(['mailAnonymous' => $user->getEmail()]);
        foreach ($events as $event){
            $user->addEventAuthor($event);
        }

        $user->setMultiIp([$request->getClientIp()]);

        $manager->persist($user);
        $manager->flush();

        $this->addFlash('success', "Votre compte est activé");

        return $this->redirectToRoute('account_login');
        
    }



    /**
     * Formulaire de modification de profil
     *
     * @Route("/compte/mon-profil", name="account_profile")
     * @IsGranted("ROLE_USER")
     * 
     * @return Response
     */
    public function profile(Request $request, EntityManagerInterface $manager, UploaderHelper $uploaderHelper){
        $user = $this->getUser();

        //dd($list_access);
/*
        echo "<br>Les demandes d'accès : ";
        foreach ($user->getClaims() as $claim){
            //if($claim->getPlace() !== null){echo $claim->getPlace()->getId();}
            if(array_search($claim->getPlace()->getId(),$list_access)){
                echo "Problème !" . $claim->getPlace()->getTitle();
            }
        }
*/
        $form = $this->createForm(AccountType::class, $user);

        //dd($form->getData()->getEditors());

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){


            /** @var UploadedFile $uploadedFile */
            //$uploadedFile = $request->files->get( 'image' );
            $uploadedFile = $form['avatarFile']->getData();



            if ($uploadedFile){
                $newFilename = $uploaderHelper->uploadUserImage($uploadedFile);
                $user->setAvatar($newFilename);
            }

            /*
            $list_access = [];
            foreach ($form->getData()->getPlaces() as $place){
                //echo $place->getId();
                $list_access[] = $place->getId();
            }
            

            foreach ($form->getData()->getPlaceEditors() as $place){
                //if(array_search($place->getId(),$list_access) === false){
                if($form->getData()->getPlaces()->contains($place->getId()) === false){
                    $this->addFlash(
                        'danger',
                        "Vous venez de renoncer aux droits de modifier ".$place->getTitle()
                    );
                    $user->removePlaceEditor($place);
                }
            }
*/
            foreach($user->getArtistAffirmers() as $artist){
                $artist->addAffirmer($user);
                $manager->persist($artist);
            }

            foreach($user->getOrganizerAffirmers() as $organizer){
                $organizer->addAffirmer($user);
                $manager->persist($organizer);
            }

            $manager->persist($user);
            $manager->flush();
            
            $this->addFlash(
                'success',
                "Vos données du profil ont été mis à jour avec succès"
            );

            return $this->redirect($request->getUri());

        }

        $claims = [];
        foreach($user->getClaims() as $claim){
            if($claim->getPlace() and $claim->getResponse() === NULL){
               $claims[] = $claim->getPlace(); 
            }
        }
        
        return $this->render('account/profile.html.twig', [
            'page'      => 'account',
            'form'      => $form->createView(),
            'claims'    => $claims,
            'website'   => $this->website
        ]);
    }

    /**
     * Permet de modifier le mot de passe
     *
     * @Route("/compte/nouveau-mot-de-passe", name="account_password")
     * @IsGranted("ROLE_USER")
     * 
     * @return Response
     */
    public function updatePassword(Request $request, UserPasswordHasherInterface $passwordHasher, EntityManagerInterface $manager){

        $passwordUpdate = new PasswordUpdate();

        $user = $this->getUser();

        $form = $this->createForm(PasswordUpdateType::class, $passwordUpdate);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            // Vérifier que l'ancien mot de passe correspond au hash en BDD
            if(!password_verify($passwordUpdate->getOldPassword(), $user->getHash())){
                // Gérer l'erreur
                $form->get('oldPassword')->addError(new FormError("Le mot de passe que vous avez tapé n'est pas votre mot de passe actuel !"));
            } else {
                $newPassword = $passwordUpdate->getNewPassword();
                $hash = $passwordHasher->hashPassword($user, $user->getPassword());

                $user->setHash($hash);

                $manager->persist($user);
                $manager->flush();

                $this->addFlash(
                    'success',
                    "Vos données du profil ont été mis à jour avec succès"
                );

            return $this->redirectToRoute('home');

            }

        }
        
        return $this->render('account/password.html.twig', [
            'form'      => $form->createView(),
            'page'      => 'account',
            'website'   => $this->website
        ]);
    }

    /**
     * Permet d'afficher le profil de l'utilisateur connecté
     *
     * @Route("/compte", name="account_index")
     * @IsGranted("ROLE_USER")
     * 
     * @return Response
     */
    public function myAccount() {

        return $this->render('user/index.html.twig', [
            'page'      => 'account',
            'user'      => $this->getUser(),
            'directory' => '',
            'website'   => $this->website
        ]);
    }

    /**
     * Permet d'afficher le formulaire de mot de passe oublié
     * 
     * @Route("/mot-de-passe-oublie", name="account_forgot_password")
     * 
     * @return Response
     */
    public function forgotPassword(Request $request, Sender $sender, TokenGeneratorInterface $tokenGenerator)
    {
        // création d'un formulaire "à la volée", afin que l'internaute puisse renseigner son mail
        $form = $this->createFormBuilder()
            ->add(
                'email',
                EmailType::class, [
                    'attr' => [
                        'placeholder' => 'Indiquer votre adresse email'
                    ],
                    'constraints' => [
                        new NotBlank()
                    ]
                ]
            )
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $email = $form->getData('email');

            $em = $this->doctrine->getManager();

            // voir l'épisode 2 de cette série pour retrouver la méthode loadUserByUsername:
            // $user = $em->getRepository(User::class)->loadUserByUsername($form->getData()['email']);
            
            $user = $em->getRepository(User::class)->findOneBy(['email' => $email]);

            // aucun email associé à ce compte.
            if (!$user) {
                $request->getSession()->getFlashBag()->add('warning', "Cet email n'existe pas.");
                return $this->redirectToRoute("account_forgot_password");
            } 

            // création du token
            $user->setToken($tokenGenerator->generateToken());
            // enregistrement de la date de création du token
            $user->setPasswordRequestedAt(new \Datetime());
            $em->flush();
            
            // on utilise le service Sender
            $bodyMail = $sender->createBodyMail('emails/forgot-password.html.twig', [
                'user' => $user
            ]);
            $sender->sendMessage('bonjour@curieux.net', $user->getEmail(), 'Renouvellement du mot de passe sur Curieux.net', $bodyMail);
            
            $request->getSession()->getFlashBag()->add('success', "Un mail a été envoyé afin que vous puissiez renouveller votre mot de passe. Le lien que vous recevrez sera valide pendant 24h.");

            return $this->redirectToRoute("account_login");
        }
        
        return $this->render('account/forgot-password.html.twig', [
            'page'      => 'account',
            'form'      => $form->createView(),
            'website'   => $this->website
        ]);

    }

    /**
     * @Route("/reinitialisation-du-mot-de-passe/{token}", name="account_resetting")
     */
    public function resetting(User $user, $token, Request $request, UserPasswordHasherInterface $passwordHasher, EntityManagerInterface $manager)
    {
        // interdit l'accès à la page si:
        // le token associé au membre est null
        // le token enregistré en base et le token présent dans l'url ne sont pas égaux
        // le token date de plus de 24 heures
        if ($user->getToken() === null || $token !== $user->getToken() || !$this->isRequestInTime($user->getPasswordRequestedAt()))
        {
            throw new AccessDeniedHttpException();
        }

        $form = $this->createForm(PasswordForgotType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $hash = $passwordHasher->hashPassword($user, $user->getPassword());

            $user->setHash($hash);

            // réinitialisation du token à null pour qu'il ne soit plus réutilisable
            $user->setToken(null);
            $user->setPasswordRequestedAt(null);

            $manager->persist($user);
            $manager->flush();

            $this->addFlash('success', "Votre mot de passe a été renouvelé.");

            return $this->redirectToRoute('account_login');

        }
        
        return $this->render('account/password-resetting.html.twig', [
            'website'   => $this->website,
            'page'      => 'account',
            'form'      => $form->createView()
        ]);
        
    }



    // si supérieur à 24 heures, retourne false
    private function isRequestInTime(\Datetime $passwordRequestedAt = null)
    {
        if ($passwordRequestedAt === null)
        {
            return false;        
        }
        
        $now = new \DateTime();
        $interval = $now->getTimestamp() - $passwordRequestedAt->getTimestamp();

        $daySeconds = 60 * 60 * 24;
        $response = $interval > $daySeconds ? false : $reponse = true;
        return $response;
    }
}
