<?php

namespace App\Controller;

use DateTime;
use App\Entity\Claim;
use App\Entity\Place;
use App\Entity\Comment;
use App\Form\ClaimType;
use App\Service\Website;
use App\Form\BoroughType;
use App\Form\SubdomainType;
use App\Form\CommentSearchType;
use App\Repository\ClaimRepository;
use App\Repository\EventRepository;
use App\Repository\PlaceRepository;
use App\Repository\ArtistRepository;
use App\Repository\BoroughRepository;
use App\Repository\CommentRepository;
use App\Repository\FestivalRepository;
use App\Repository\OrganizerRepository;
use App\Repository\SubdomainRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/verification")
 */
class VerificationController extends AbstractController
{    
    public function __construct(Website $website)
    {
        $this->website = $website->getArray();
        $this->subdomain = $website->getArray()['address']['subdomain'];
    }
    
    /**
     * Validation des évènements
     * 
     * @Route("/publication/evenement", name="events_validator")
     * @Security("is_granted('ROLE_USER') and (user.getIsVolunteer() or is_granted('ROLE_ADMIN'))", message="Vous n'avez pas le droit d'accéder à cette page")
     * @return Response
     */
    public function eventsValidation(EventRepository $repo, PaginatorInterface $paginator, Request $request)
    {
        $events = $paginator->paginate($repo->findForVerification($this->subdomain, $request->get("menu")), $request->query->getInt('page', 1), 50 /*limite par page*/);

        return $this->render('validation/event.html.twig', [
            'events'            => $events,
            'menu'              => $request->get("menu"),
            'website'           => $this->website
        ]);
    }

    /**
     * Validation des lieux
     * 
     * @Route("/publication/lieu", name="places_validator")
     * @Security("is_granted('ROLE_USER') and (user.getIsVolunteer() or is_granted('ROLE_ADMIN'))", message="Vous n'avez pas le droit d'accéder à cette page")
     * @return Response
     */
    public function placesValidation(PlaceRepository $repo, PaginatorInterface $paginator, Request $request)
    { 
        $places = $paginator->paginate($repo->findForVerification($this->subdomain, $request), $request->query->getInt('page', 1), 50 /*limite par page*/);
        
        return $this->render('validation/place.html.twig', [
            'places'            => $places,
            'menu'              => $request->get("filtre"),
            'website'           => $this->website
        ]);
    }

    /**
     * Validation des festivals
     * 
     * @Route("/publication/festival", name="festivals_validator")
     * @Security("is_granted('ROLE_USER') and (user.getIsVolunteer() or is_granted('ROLE_ADMIN'))", message="Vous n'avez pas le droit d'accéder à cette page")
     * @return Response
     */
    public function festivalsValidation(FestivalRepository $repo, PaginatorInterface $paginator, Request $request)
    {
        $festivals = $paginator->paginate($repo->findForVerification($this->subdomain, $request), $request->query->getInt('page', 1), 50 /*limite par page*/);
        
        //dd($festivals->getItems());
        return $this->render('validation/festival.html.twig', [
            'festivals'         => $festivals,
            'menu'              => $request->get("filtre"),
            'website'           => $this->website
        ]);
    }

    /**
     * Validation des artistes
     * 
     * @Route("/publication/artiste", name="artists_validator")
     * @Security("is_granted('ROLE_USER') and (user.getIsVolunteer() or is_granted('ROLE_ADMIN'))", message="Vous n'avez pas le droit d'accéder à cette page")
     * @return Response
     */
    public function artistsValidation(ArtistRepository $repo, PaginatorInterface $paginator, Request $request)
    {
        $artists = $paginator->paginate($repo->findForVerification($this->subdomain, $request), $request->query->getInt('page', 1), 50 /*limite par page*/);
        
        //dd($artists->getItems());
        return $this->render('validation/artist.html.twig', [
            'artists'           => $artists,
            'menu'              => $request->get("filtre"),
            'website'           => $this->website
        ]);
    }

    /**
     * Validation des organisateurs
     * 
     * @Route("/publication/organisateur", name="organizers_validator")
     * @Security("is_granted('ROLE_USER') and (user.getIsVolunteer() or is_granted('ROLE_ADMIN'))", message="Vous n'avez pas le droit d'accéder à cette page")
     * @return Response
     */
    public function organizersValidation(OrganizerRepository $repo, PaginatorInterface $paginator, Request $request)
    {
        $organizers = $paginator->paginate($repo->findForVerification($this->subdomain, $request), $request->query->getInt('page', 1), 50 /*limite par page*/);
        
        //dd($organizers->getItems());
        return $this->render('validation/organizer.html.twig', [
            'organizers'         => $organizers,
            'menu'              => $request->get("filtre"),
            'website'           => $this->website
        ]);
    }

    /**
     * Validation des messages
     * 
     * @Route("/message", name="messages_verification")
     * @Route("/commentaire", name="comments_verification")
     * @Route("/alerte", name="alerts_verification")
     * @Security("is_granted('ROLE_USER') and (user.getIsVolunteer() or is_granted('ROLE_ADMIN'))", message="Vous n'avez pas le droit d'accéder à cette page")
     * @return Response
     */
    public function messagesValidation(CommentRepository $repo, PaginatorInterface $paginator, Request $request)
    { 
        $search = new Comment;
        $form = $this->createForm(CommentSearchType::class,$search);
        $form->handleRequest($request);

        $query = $repo->findForVerification($this->subdomain, $request);
        
        $comments = $paginator->paginate($query, $request->query->getInt('page', 1), 50 /*limite par page*/);
        
        if ($request->get('_route') == "comments_verification"){$menu = "commentaire";}
        elseif ($request->get('_route') == "alerts_verification"){$menu = "alerte";}
        else {$menu = NULL;}

        return $this->render('validation/comment.html.twig', [
            'comments'  => $comments,
            'form'      => $form->createView(),
            'menu'      => $menu,
            'website'   => $this->website
        ]);
    }


    /**
     * Permet de supprimer un commentaire
     * 
     * @Route("/message/{id}/supprimer", name="comment_delete")
     * @Security("is_granted('ROLE_USER') and (user.getIsVolunteer() or is_granted('ROLE_ADMIN'))", message="Vous n'avez pas le droit d'accéder à cette page")
     * 
     * @param Comment $comment
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function deleteComment(Comment $comment, EntityManagerInterface $manager)
    {
        $manager->remove($comment);
        $manager->flush();

        $this->addFlash(
            'success',
            "Votre commentaire a bien été supprimé !"
        );

        return $this->redirectToRoute('messages_verification');
    }

     /**
     * Afficher les réclamations
     * 
     * @Route("/reclamation", name="claims_verification")
     * @Security("is_granted('ROLE_ADMIN')", message="Vous n'avez pas accès aux réclamations")
     * 
     */
    public function claim(ClaimRepository $repo, PaginatorInterface $paginator, Request $request)
    {
        $query = $repo->findForVerification($this->subdomain, $request);
        
        $claims = $paginator->paginate($query, $request->query->getInt('page', 1), 50 /*limite par page*/);
    
        return $this->render('validation/claim.html.twig', [
            'website'   => $this->website,
            'filtre'    => $request->get("filtre"),
            'claims'    => $claims
        ]);
    }

    /**
     * Editer une réclamation
     * 
     * @Route("/reclamation/{id}", name="claim_edit")
     * @Security("is_granted('ROLE_ADMIN')", message="Vous n'avez pas le droit de traité cette réclamation")
     * 
     * @param Claim $claim
     * @param EntityManagerInterface $manager
     */
    public function editClaim(Claim $claim, Request $request, EntityManagerInterface $manager)
    {
        $form = $this->createForm(ClaimType::class, $claim);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            //Si la décision est positive, injecté l'utilisateur dans les modérateurs
            if($claim->getResponse()){

                $user = $claim->getOwner();

                if ($form->getData()->getResponse() == TRUE){
                    $claim->setResponse(TRUE);
                    if ($claim->getEvent()){
                        $user->addEventEditor($claim->getEvent());
                    }
                    elseif ($claim->getPlace()){
                        $user->addPlaceEditor($claim->getPlace());
                    }
                    $manager->persist($user);
                }
                else {
                    if ($form->getData()->getResponse() == FALSE){
                        $claim->setResponse(FALSE);
                    }
                    if ($claim->getEvent()){
                        $user->removeEventEditor($claim->getEvent());
                    }
                    elseif ($claim->getPlace()){
                        $user->removePlaceEditor($claim->getPlace());
                    }
                    $manager->persist($user);
                }



                if ($claim->getPlace()){
                    $user->addPlaceEditor($claim->getPlace());
                } 
                elseif ($claim->getEvent()){
                    $user->addEventEditor($claim->getEvent());
                    /*
                    $editor = new EditorEvent;
                    $editor ->setEditor($claim->getOwner())
                            ->setEvent($claim->getEvent())
                            ->setAuthorizedBy($this->getUser());
                            
                    $manager->persist($editor);
                    */

                }
            }

            $date = new DateTime();

            $claim->setDateResponse($date);

            $manager->persist($claim);
            $manager->flush();

            $this->addFlash(
                'success',
                "Les modifications ont été effectués !"
            );

            return $this->redirectToRoute('claims_verification');
        }

        
        
        return $this->render('validation/claim_edit.html.twig', [
            'website'   => $this->website,
            'form'      => $form->createView(),
            'claim'     => $claim
        ]);
    }

    /**
     * Accepter ou refuser une réclamation
     * 
     * @Route("/reclamation/{id}/accepter", name="claim_accept")
     * @Route("/reclamation/{id}/refuser", name="claim_refuse")
     * 
     * @Security("is_granted('ROLE_ADMIN')", message="Vous n'avez pas le droit de traiter cette réclamation")
     * 
     * @param Claim $claim
     * @param EntityManagerInterface $manager
     */
    public function buttonClaim(Claim $claim, Request $request, EntityManagerInterface $manager)
    {
        $user = $claim->getOwner();

        if ($request->get('_route') == "claim_accept"){
            $claim->setResponse(TRUE);
            if ($claim->getEvent()){
                $user->addEventEditor($claim->getEvent());
            }
            elseif ($claim->getPlace()){
                $user->addPlaceEditor($claim->getPlace());
            }
            $manager->persist($user);
        }
        elseif ($request->get('_route') == "claim_refuse"){
            $claim->setResponse(FALSE);
            if ($claim->getEvent()){
                $user->removeEventEditor($claim->getEvent());
            }
            elseif ($claim->getPlace()){
                $user->removePlaceEditor($claim->getPlace());
            }
            $manager->persist($user);
        }

        $date = new DateTime();

        $claim->setDateResponse($date);

        $manager->persist($claim);
        $manager->flush();

        return $this->redirectToRoute('claims_verification');
    }

    /**
     * Permet de supprimer un lieu par un administrateur
     * 
     * @Route("/publication/lieu/{id}/supprimer", name="place_delete_by_id")
     * @Security("is_granted('ROLE_ADMIN')", message="Vous n'avez pas le droit de supprimer ce lieu")
     * 
     * @param Place $place
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delete(Place $place, EntityManagerInterface $manager)
    {
        $manager->remove($place);
        $manager->flush();

        $this->addFlash(
            'success',
            "Le lieu <strong>{$place->getTitle()}</strong> a bien été supprimé !"
        );

        return $this->redirectToRoute('places_validator');
    }

}