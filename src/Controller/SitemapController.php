<?php

namespace App\Controller;

use App\Service\Website;
use App\Repository\EventRepository;
use App\Repository\RubricRepository;
use App\Repository\SubdomainRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SitemapController extends AbstractController
{

    /**
     * @Route("/sitemap", name="sitemap", defaults={"_format"="xml"})
     */
    public function index(Request $request, Website $website, RubricRepository $repo, EventRepository $repoEvent)
    {
        $hostname = $request->getSchemeAndHttpHost();

        $urls = [];

            $urls[] = ["loc" => $this->generateUrl('event_index')];
            $urls[] = ["loc" => $this->generateUrl('place_index')];
            $urls[] = ["loc" => $this->generateUrl('event_by_day', ["txtDay" => "aujourd-hui"])];
            $urls[] = ["loc" => $this->generateUrl('event_by_day', ["txtDay" => "demain"])];
            $urls[] = ["loc" => $this->generateUrl('event_by_day', ["txtDay" => "apres-demain"])];

        foreach ($repo->findAll() as $rubric){
            $urls[] = ["loc" => $this->generateUrl('events_by_rubric', ["slug" => $rubric->getSlug()])];
        }

        $subdomain = $website->getArray()['address']['subdomain'];
        if ($subdomain != "www"){
            $events = $repoEvent->findAllOfThisWeek($subdomain);
            foreach ($events as $event){
                $urls[] = [
                    "loc" => $this->generateUrl('event_show', ["slug" => $event->getSlug()]),
                    "lastmod" => $event->getUpdatedAt()->format('Y-m-d')
                ];
            }
        }
        

        $response = new Response(
            $this->renderView('xml/sitemap.xml.twig', [
                'urls' => $urls,
                'hostname' => $hostname
            ]), 200
        );

        $response->headers->set('Content-Type', 'text/xml');

        return $response;
    }


    
    /**
     * @Route("/sitemapindex", name="sitemapindex", defaults={"_format"="xml"})
     */
    public function indexOfSitemaps(SubdomainRepository $repoSubdomain)
    {
        $urls = [];

        $urls[] = "https://www.curieux.net/sitemap";

        foreach ($repoSubdomain->findAll() as $subdomain){
            $urls[] = "https://" .$subdomain->getSlug(). ".curieux.net/sitemap";
        }

        //dd($urls);

        $response = new Response(
            $this->renderView('xml/sitemapIndex.xml.twig', [
                'urls' => $urls
            ]), 200
        );

        $response->headers->set('Content-Type', 'text/xml');

        return $response;
    }
}
