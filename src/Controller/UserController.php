<?php

namespace App\Controller;

use App\Entity\User;
use App\Service\Website;
use App\Repository\UserRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UserController extends AbstractController
{
    public function __construct(Website $website)
    {
        $this->website = $website->getArray();
    }
    
    /**
     * @Route("/utilisateur/{slug}", name="user_show")
     */
    public function index(User $user)
    {
        return $this->render('user/index.html.twig', [
            'user'      => $user,
            'website'   => $this->website
        ]);
    }
}
