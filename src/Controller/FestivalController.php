<?php

namespace App\Controller;

use App\Entity\City;
use App\Entity\Rubric;
use App\Entity\Festival;
use App\Service\Website;
use App\Form\FestivalType;
use App\Entity\FestivalSearch;
use App\Service\UploaderHelper;
use App\Form\FestivalSearchType;
use App\Repository\EventRepository;
use App\Repository\RubricRepository;
use App\Repository\CategoryRepository;
use App\Repository\FestivalRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Contracts\Cache\ItemInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FestivalController extends AbstractController
{
    public function __construct(Website $website, ManagerRegistry $doctrine)
    {
        $this->website = $website->getArray();
        $this->subdomain = $website->getArray()['address']['subdomain'];
        $this->doctrine = $doctrine;
    }

    /**
     * Tous les festivals et recherche personnalisée
     * 
     * @Route("/festival", name="festival_index")
     */
    public function index(FestivalRepository $repo, PaginatorInterface $paginator, Request $request, CacheInterface $cache)
    {
        $search = new FestivalSearch;
        $form = $this->createForm(FestivalSearchType::class,$search);
        $form->handleRequest($request);

        if ($search == new FestivalSearch){
            $menu = ['MENU_CITY','MENU_RUBRIC','MENU_SEARCH'];
        } else {
            $menu = ['MENU_SEARCH'];
        }

        $festivals = $paginator->paginate($repo->findAllSearchByFilter($search,$this->subdomain),$request->query->getInt('page', 1), 20 /*limite par page*/);

        return $this->render('festival/index.html.twig', [
            'page'          => 'festival',
            'title'         => 'à venir',
            'festivals'     => $festivals,
            'website'       => $this->website,
            'menu_cities'   => $this->getCountsByCity($cache),
            'menu_rubrics'  => $this->getCountsByRubric($cache),
            'form'          => $form->createView(),
            'menu'          => $menu,
            'directory'     => "",
            'data'          => ""
        ]);
    }

    /**
     * Tous les festivals par rubrique
     * 
     * @Route("/festival/rubrique-{slug}", name="festivals_by_rubric")
     */
    public function search_by_rubric(FestivalRepository $repo, PaginatorInterface $paginator, Request $request, $slug, CacheInterface $cache)
    {        
        $rubric = $this->doctrine->getRepository(Rubric::class)->findOneBy(['slug' => $slug]);
        $afterText = "à venir";

        // Permet de préciser dans les festivals
        $rubrics = $this->doctrine->getRepository(Rubric::class)->findAll();

        $festivals = $paginator->paginate($repo->findByRubric($slug, $this->subdomain), $request->query->getInt('page', 1), 20 /*limite par page*/);

        $search = new FestivalSearch;
        $form = $this->createForm(FestivalSearchType::class,$search);
        $form->handleRequest($request);

        return $this->render('festival/index.html.twig', [
            'page'              => 'festival',
            'title'             => 'avec des '.$rubric->getPlural().' '.$afterText,
            'festivals'         => $festivals,
            'website'           => $this->website,
            'menu_cities'       => $this->getCountsByCity($cache),
            'menu_rubrics'      => $this->getCountsByRubric($cache),
            'form'              => $form->createView(),
            'menu'              => ['MENU_RUBRIC','MENU_SEARCH'],
            'directory'         => $slug,
            'data'              => $rubrics
        ]);
    }

    /**
     * Tous les festivals par ville
     * 
     * @Route("/festival/ville-de-{slug}", name="festivals_by_city")
     */
    public function search_by_city(FestivalRepository $repo, PaginatorInterface $paginator, Request $request, $slug, CacheInterface $cache)
    {
        $city = $this->doctrine->getRepository(City::class)->findOneBy(['slug' => $slug]);
        $afterText = "à venir";

        $festivals = $paginator->paginate($repo->findByCity($slug, $this->subdomain), $request->query->getInt('page', 1), 20 /*limite par page*/);

        $search = new FestivalSearch;
        $form = $this->createForm(FestivalSearchType::class,$search);
        $form->handleRequest($request);

        return $this->render('festival/index.html.twig', [
            'page'              => 'festival',
            'title'             => 'de '. $city->getName().' '.$afterText,
            'festivals'         => $festivals,
            'website'           => $this->website,
            'menu_cities'       => $this->getCountsByCity($cache),
            'menu_rubrics'      => $this->getCountsByRubric($cache),
            'form'              => $form->createView(),
            'menu'              => ['MENU_CITY','MENU_SEARCH'],
            'directory'         => $slug,
            'data'              => ""
        ]);
    }

    /**
     * Permet d'afficher un seul festival
     * 
     * @Route("/festival/{slug}", name="festival_show")
     * 
     * @return Response
     */
    public function show(Festival $festival, EventRepository $repo, PaginatorInterface $paginator, Request $request, $slug){
        
        //dd($repo->findByFestival($slug)->getResult());

        $data = [
            "places"            => null,
            "placesWithoutId"   => null,
            "artists"           => null,
            "organizers"        => null,
            "cities"            => null,
        ];

        foreach($repo->findByFestival($slug)->getResult() as $event){

            if ($event->getPlace()){
                $data['places'][$event->getPlace()->getId()] = $event->getPlace();
            } elseif ($event->getPlaceTemporary()){
                $data['placesWithoutId'][] = $event->getPlaceTemporary();
            }

            foreach($event->getArtists() as $artist){
                $data['artists'][$artist->getId()] = $artist;
            }

            if($event->getOrganizer()){
                $data['organizers'][$event->getOrganizer()->getId()] = $event->getOrganizer();
            }

            if($event->getCity()){
                $data['cities'][$event->getCity()->getId()] = $event->getCity();
            }
        }

        $events = $paginator->paginate($repo->findByFestival($slug),$request->query->getInt('page', 1), 10 );

        return $this->render('festival/show.html.twig', [
            'page'      => 'festival',
            'festival'  => $festival,
            'data'      => $data,
            'events'    => $events,
            'website'   => $this->website,
            'directory' => ''
        ]);
    }

    
    /**
     * Créer un festival
     * 
     * @Route("/ajouter-un-festival", name="festival_create")
     * @Security("is_granted('ROLE_USER')", message="Connectez-vous pour créer un festival")
     * @return Response
     */
    public function create(Request $request, EntityManagerInterface $manager, UploaderHelper $uploaderHelper){
        $festival = new Festival();

        $form = $this->createForm(FestivalType::class, $festival);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            /** @var UploadedFile $uploadedFile */
            $uploadedFile = $form['coverImageFile']->getData();

            if ($uploadedFile){
                $newFilename = $uploaderHelper->uploadFestivalImage($uploadedFile);
                $festival->setCoverImage($newFilename);
            }

            if ($this->getUser()){
                $festival->setAuthor($this->getUser());
            }

            $manager->persist($festival);
            $manager->flush();

            $this->addFlash(
                'success',
                "Le festival <strong>{$festival->getTitle()}</strong> a bien été enregistré !"
            );

            return $this->redirectToRoute('festival_show', [
                'slug' => $festival->getSlug()
            ]);
        }

        return $this->render('festival/new.html.twig', [
            
            'page'          => 'festival',
            'formFestival'  => $form->createView(),
            'website'       => $this->website,
        ]);
    }

    

    /**
     * Permet d'éditer un festival
     * 
     * @Route("/festival/{slug}/edit", name="festival_edit")
     * @Security("is_granted('ROLE_USER') and (user === festival.getAuthor() or festival.getEditors().contains(user) or is_granted('ROLE_ADMIN'))", message="Vous n'avez pas le droit de modifier ce festival")
     * @return Response
     */
    public function edit(Festival $festival, Request $request, EntityManagerInterface $manager, UploaderHelper $uploaderHelper){

        $form = $this->createForm(FestivalType::class, $festival);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

             /** @var UploadedFile $uploadedFile */
             $uploadedFile = $form['coverImageFile']->getData();

             if ($uploadedFile){
                 $newFilename = $uploaderHelper->uploadFestivalImage($uploadedFile);
                 $festival->setCoverImage($newFilename);
             }
 
            /*
            foreach($festival->getImages() as $image){
                $image->setFestival($festival);
                $manager->persist($image);
            }
            */

            $manager->persist($festival);
            $manager->flush();

            $this->addFlash(
                'success',
                "Les modifications ont été effectué !"
            );

            return $this->redirectToRoute('festival_show', [
                'slug' => $festival->getSlug()
            ]);
        }

        return $this->render('festival/edit.html.twig', [
            'page'          => 'festival',
            'formFestival'  => $form->createView(),
            'festival'      => $festival,
            'website'       => $this->website,
        ]);
    }

    /* @Security("is_granted('ROLE_USER') and festival.getEditors().contains(user) or is_granted('ROLE_ADMIN')", message="Vous n'avez pas le droit de supprimer ce festival")
    */

    /**
     * Permet de supprimer un festival
     * 
     * @Route("/festival/{slug}/supprimer", name="festival_delete")
     * @Security("is_granted('ROLE_ADMIN')", message="Vous n'avez pas le droit de supprimer un festival")
     * 
     * @param Event $event
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delete(Festival $festival, EntityManagerInterface $manager)
    {

        //$city = $this->doctrine->getRepository(City::class)->findOneBy(['slug' => $slug]);
        
        foreach($festival->getEvents() as $event)
        {
            $event->setFestival(null);
            $manager->persist($event);
        }

        $manager->remove($festival);
        $manager->flush();

        $this->addFlash(
            'success',
            "Le festival <strong>{$festival->getTitle()}</strong> a bien été supprimé !"
        );

        return $this->redirectToRoute('festival_index');
    }
    
    public function getCountsByCity($cache)
    {
        return $cache->get('menu_festival_by_city_'.$this->subdomain, function(ItemInterface $item){
           
            $item->expiresAfter(60*61);
        
            $today = date('Y-m-d');

            if ($this->subdomain){

                $query =   "SELECT c.name, c.slug, COUNT(DISTINCT f.id) as compteur
                            FROM festival f, event e, place p, city c, borough b
                            WHERE f.id = e.festival_id
                            AND c.id = p.city_id
                            AND (p.id = e.place_id OR c.id = e.city_id)
                            AND b.id = c.borough_id
                            AND b.slug = '".$this->subdomain."' ";
            } else {
                
                $query =   "SELECT c.name, c.slug, COUNT(DISTINCT f.id) as compteur
                            FROM festival f, event e, place p, city c
                            WHERE f.id = e.festival_id
                            AND c.id = p.city_id
                            AND (p.id = e.place_id OR c.id = e.city_id)";
            }

            $query.=   "AND e.date_end >= '". date('Y-m-d') ."'
                        GROUP BY c.id
                        ORDER BY compteur DESC
                        LIMIT 20";

            $em = $this->doctrine->getManager();
            $statement = $em->getConnection()->prepare($query)->execute();

            return $statement->fetchAll();
        });
    }

    public function getCountsByRubric($cache)
    {
        return $cache->get('menu_festival_by_rubric_'.$this->subdomain, function(ItemInterface $item){
           
            $item->expiresAfter(60*62);
        
            $today = date('Y-m-d');
            
            if ($this->subdomain){

                $query =   "SELECT r.name, r.slug, COUNT(DISTINCT f.id) as compteur
                            FROM festival f, event e, rubric r, place p, city c, borough b
                            WHERE f.id = e.festival_id
                            AND r.id = e.rubric_id
                            AND p.id = e.place_id
                            AND (c.id = p.city_id OR c.id = e.city_id)
                            AND b.id = c.borough_id
                            AND b.slug = '".$this->subdomain."' ";
            } else {
                
                $query =   "SELECT r.name, r.slug, COUNT(DISTINCT f.id) as compteur
                            FROM festival f, event e, rubric r
                            WHERE f.id = e.festival_id
                            AND r.id = e.rubric_id ";
            }

            $query.=   "AND e.date_end >= '$today'
                        GROUP BY r.name
                        ORDER BY compteur DESC
                        LIMIT 10";
            

            $em = $this->doctrine->getManager();
            $statement = $em->getConnection()->prepare($query)->execute();

            return $statement->fetchAll();
        });
    }

}
