<?php

namespace App\Controller;

use App\Repository\PricesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/tmp")
 * @Security("is_granted('ROLE_ADMIN')", message="Vous n'avez pas le droit de modifier cet évènement")
 */
class TmpController extends AbstractController
{
    /**
     * Permet de fusionner les réductions par prix
     * 
     * @Route("/transfert-reduction")
     * @Security("is_granted('ROLE_ADMIN')", message="Vous n'avez pas le droit de modifier cet évènement")
     */
    public function transfert_reduction(Request $request, EntityManagerInterface $manager, PricesRepository $repo): Response
    {
        $prices = $repo->findAll();
        $array = [];

        foreach($prices as $price){
            if ($price->getReduction() !== null){
                $array[$price->getEvent()->getId()][$price->getPrice()][] = $price;
            }
        }
        
        //dd($array);

        foreach ($array as $eventId => $data){
            foreach ($data as $number => $allPrices){

                for ($i = 0; $i < count($allPrices); $i++) {
                    $allPrices[0]->addReduction($allPrices[$i]->getReduction());
                    $allPrices[$i]->setReduction(null);
                    $manager->persist($allPrices[0]);
                    $manager->persist($allPrices[$i]);
                }
                /*
                foreach ($allPrices as $entityPrice){
                    dd($entityPrice);
                }
                */
            }

            //$price->addReduction($price->getReduction());
            $manager->persist($price);
        }
        $manager->flush();

        return new Response("C'est transférer ! - ". $i);
    }

    
    /**
     * Permet de supprimer les prix sans reduction à la volée
     * 
     * @Route("/supprimer-reduction")
     * @Security("is_granted('ROLE_ADMIN')", message="Vous n'avez pas le droit de modifier cet évènement")
     */
    public function delete_reduction(Request $request, EntityManagerInterface $manager, PricesRepository $repo): Response
    {
        $prices = $repo->findAll();
        $i = 0;
        foreach ($prices as $price){
            if(count($price->getReductions()) == "0"){
                $manager->remove($price);
                $i++;
            }
        }

        $manager->flush();

        return new Response("C'est supprimer ! - ". $i);
    }
}
