<?php

namespace App\Controller\Admin;

use App\Entity\Rubric;
use App\Form\RubricType;
use App\Service\Website;
use App\Service\Paginator;
use App\Repository\RubricRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminRubricController extends AbstractController
{
    public function __construct(Website $website)
    {
        $this->website = $website->getArray();
        $this->subdomain = $website->getArray()['address']['subdomain'];
    }
    
    /**
     * @Route("/admin/rubrique/{page<\d+>?1}", name="admin_rubric_index")
     */
    public function index(RubricRepository $repo, $page, Paginator $paginator, Request $request, EntityManagerInterface $manager)
    {
        $paginator  ->setEntityClass(Rubric::class)
                    ->setOrder(['id'=>'ASC'])
                    ->setPage($page);

        $rubric = new Rubric();

        $form = $this->createForm(RubricType::class, $rubric);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $manager->persist($rubric);
            $manager->flush();

            $this->addFlash(
                'success',
                "La rubrique <b>{$rubric->getName()}</b> a bien été enregistrée !"
            );

            return $this->redirectToRoute('admin_rubric_index');
        }
        
        return $this->render('admin/rubric/index.html.twig', [
            'website'   => $this->website,
            'paginator' => $paginator,
            'form'      => $form->createView()
        ]);
    }

    /**
     * Permet d'éditer une rubrique dans l'admin
     *
     * @Route("/admin/rubrique/{id}/editer", name="admin_rubric_edit")
     * 
     * @param Rubric $rubric
     * @return Response
     */
    public function edit(Rubric $rubric, Request $request, EntityManagerInterface $manager)
    {
        $form = $this->createForm(RubricType::class, $rubric);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $manager->persist($rubric);
            $manager->flush();

            $this->addFlash(
                'success',
                "<div class='row'>
                <div class='col'>
                La rubrique '{$rubric->getName()}' a bien été modifiée !<br>
                </div>
                <div class='col text-right'>
                    <a href='../' class='btn btn-primary'>Revenir sur la liste des rubriques</a>
                </div>
            </div>"
            );
        }
        
        return $this->render('admin/rubric/edit.html.twig', [
            'website'   => $this->website,
            'form'      => $form->createView(),
            'rubric'    => $rubric
        ]);
    }

    /**
     * Permet de supprimer une rubrique
     *
     * @Route("/admin/rubrique/{id}/supprimer", name="admin_rubric_delete")
     * 
     * @param Rubric $rubric
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delete(Rubric $rubric, EntityManagerInterface $manager){
        $manager->remove($rubric);
        $manager->flush();

        $this->addFlash(
            'success',
            "La rubrique <b>'{$rubric->getName()}'</b> a bien été supprimée !"
        );

        return $this->redirectToRoute('admin_rubric_index');
    }
}
