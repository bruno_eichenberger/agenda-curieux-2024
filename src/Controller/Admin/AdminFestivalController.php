<?php

namespace App\Controller\Admin;

use App\Entity\Festival;
use App\Service\Website;
use App\Form\FestivalType;
use App\Service\Paginator;
use App\Repository\FestivalRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminFestivalController extends AbstractController
{
    public function __construct(Website $website)
    {
        $this->website = $website->getArray();
        $this->subdomain = $website->getArray()['address']['subdomain'];
    }
    
    /**
     * @Route("/admin/festival/{page<\d+>?1}", name="admin_festival_index")
     */
    public function index(FestivalRepository $repo, $page, Paginator $paginator, Request $request, EntityManagerInterface $manager)
    {
        $paginator  ->setEntityClass(Festival::class)
                    ->setPage($page);

        $festival = new Festival();

        $form = $this->createForm(FestivalType::class, $festival);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $manager->persist($festival);
            $manager->flush();

            $this->addFlash(
                'success',
                "Le festival <b>{$festival->getTitle()}</b> a bien été enregistré !"
            );

            return $this->redirectToRoute('admin_festival_index');
        }
        
        return $this->render('admin/festival/index.html.twig', [
            'website'   => $this->website,
            'paginator' => $paginator,
            'form'      => $form->createView()
        ]);
    }

    /**
     * Permet d'éditer un festival dans l'admin
     *
     * @Route("/admin/festival/{id}/editer", name="admin_festival_edit")
     * 
     * @param Festival $festival
     * @return Response
     */
    public function edit(Festival $festival, Request $request, EntityManagerInterface $manager)
    {
        $form = $this->createForm(FestivalType::class, $festival);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $manager->persist($festival);
            $manager->flush();

            $this->addFlash(
                'success',
                "<div class='row'>
                <div class='col'>
                Le festival '{$festival->getTitle()}' a bien été modifié !<br>
                </div>
                <div class='col text-right'>
                    <a href='../' class='btn btn-primary'>Revenir sur la liste des festivals</a>
                </div>
            </div>"
            );
        }
        
        return $this->render('admin/festival/edit.html.twig', [
            'website'   => $this->website,
            'form'      => $form->createView(),
            'festival'  => $festival
        ]);
    }

    /**
     * Permet de supprimer une festival
     *
     * @Route("/admin/festival/{id}/supprimer", name="admin_festival_delete")
     * 
     * @param Festival $festival
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delete(Festival $festival, EntityManagerInterface $manager){
        $manager->remove($festival);
        $manager->flush();

        $this->addFlash(
            'success',
            "Le festival <b>'{$festival->getTitle()}'</b> a bien été supprimé !"
        );

        return $this->redirectToRoute('admin_festival_index');
    }
}
