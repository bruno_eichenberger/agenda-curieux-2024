<?php

namespace App\Controller\Admin;

use App\Entity\Comment;
use App\Service\Website;
use App\Service\Paginator;
use App\Form\AdminCommentType;
use App\Repository\CommentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminCommentController extends AbstractController
{
    public function __construct(Website $website)
    {
        $this->website = $website->getArray();
        $this->subdomain = $website->getArray()['address']['subdomain'];
    }
    
    /**
     * @Route("/admin/commentaires/{page<\d+>?1}", name="admin_comment_index")
     */
    public function index(CommentRepository $repo, $page, Paginator $paginator)
    {

        $paginator  ->setEntityClass(Comment::class)
                    //->setLimit(100)
                    //->setRoute('admin_comment_index')
                    ->setPage($page);
        
        return $this->render('admin/comment/index.html.twig', [
            'website'   => $this->website,
            //'comments' => $repo->findAll()
            'paginator' => $paginator
        ]);
    }

    
    /**
     * Permet de modifier un commentaire
     *
     * @Route("/admin/comment/{id}/edit", name="admin_comment_edit")
     * 
     * @param Comment $comment
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function edit(Comment $comment, Request $request, EntityManagerInterface $manager) {

        $form = $this->createForm(AdminCommentType::class, $comment);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $manager->persist($comment);
            $manager->flush();

            $this->addFlash(
                'success',
                "Le commentaire n '{$comment->getId()}' a bien été modifiée !"
            );
        }
        
        return $this->render('admin/comment/edit.html.twig', [
            'website'   => $this->website,
            'comment'   => $comment,
            'form'      => $form->createView()
        ]);
    }

    
    /**
     * Permet de supprimer un commentaire
     *
     * @Route("/admin/comment/{id}/delete", name="admin_comment_delete")
     * 
     * @param Comment $comment
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delete(Comment $comment, EntityManagerInterface $manager){
        $manager->remove($comment);
        $manager->flush();

        $this->addFlash(
            'success',
            "Le commentaire de <strong>'{$comment->getAuthor()->getFullName()}'</strong> dans l'évènement <strong>'{$comment->getEvent()->getTitle()}'</strong> a bien été supprimée !"
        );

        return $this->redirectToRoute('admin_comment_index');
    }
}
