<?php

namespace App\Controller\Admin;

use App\Entity\Event;
use App\Form\EventType;
use App\Service\Website;
use App\Entity\EventSearch;
use App\Form\EventSearchType;
use App\Repository\EventRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminEventController extends AbstractController
{
    public function __construct(Website $website)
    {
        $this->website = $website->getArray();
        $this->subdomain = $website->getArray()['address']['subdomain'];
    }
    
    /**
     * @Route("/admin/evenement/", name="admin_event_index")
     */
    public function index(EventRepository $repo, PaginatorInterface $paginator, Request $request)
    {
        //$test = $paginator->getInformation();
        //dump($test);

        $search = new EventSearch;
        $form = $this->createForm(EventSearchType::class,$search);
        $form->handleRequest($request);
        
        $events = $paginator->paginate($repo->findAllSearchQuery($search, $this->subdomain/*,'admin-event'*/),$request->query->getInt('page', 1), 20 /*limite par page*/);

        /*
        $paginator->setEntityClass(Event::class)
                  ->setLimit(20)
                  //->setRoute('admin_event_index')
                  ->setPage($page);
        */

        //$limit = 10;
        //$start = $page * $limit - $limit;
        //$total = count($repo->findAll());
        //$pages = ceil($total / $limit);

        
        return $this->render('admin/event/index.html.twig', [
            'website'   => $this->website,
            'form'      => $form->createView(),
            'events'    => $events
            /*
            'paginator' => $paginator
            'events' => $paginator->getData(),
            'pages' => $paginator->getPages(),
            'page' => $page
            */
        ]);
    }

    /**
     * Permet d'éditer un évènement dans l'admin
     *
     * @Route("/admin/evenement/{id}/edit", name="admin_event_edit")
     * 
     * @param Event $event
     * @return Response
     */
    public function edit(Event $event, Request $request, EntityManagerInterface $manager)
    {
        $form = $this->createForm(EventType::class, $event);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            /*
            foreach($event->getImages() as $image){
                $image->setEvent($event);
                $manager->persist($image);
            }
            */

            $manager->persist($event);
            $manager->flush();

            $this->addFlash(
                'success',
                "<div class='row'>
                    <div class='col'>
                    L'évènement '{$event->getTitle()}' a bien été modifié !<br>
                    </div>
                    <div class='col text-right'>
                        <a href='../' class='btn btn-primary'>Revenir sur la liste des évènements</a>
                    </div>
                </div>"
            );
        }
        
        return $this->render('admin/event/edit.html.twig', [
            'website'   => $this->website,
            'form'      => $form->createView(),
            'event'     => $event
        ]);
    }

    /**
     * Permet de supprimer un évènement
     *
     * @Route("/admin/evenement/{id}/delete", name="admin_event_delete")
     * 
     * @param Event $event
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delete(Event $event, EntityManagerInterface $manager){
        $manager->remove($event);
        $manager->flush();

        $this->addFlash(
            'success',
            "L'annonce <strong>'{$event->getTitle()}'</strong> a bien été supprimé !"
        );

        return $this->redirectToRoute('admin_event_index');
    }
}
