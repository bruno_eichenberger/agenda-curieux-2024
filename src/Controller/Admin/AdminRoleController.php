<?php

namespace App\Controller\Admin;

use App\Entity\Role;
use App\Form\RoleType;
use App\Service\Website;
use App\Service\Paginator;
use App\Repository\RoleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminRoleController extends AbstractController
{
    public function __construct(Website $website)
    {
        $this->website = $website->getArray();
        $this->subdomain = $website->getArray()['address']['subdomain'];
    }
    
    /**
     * @Route("/admin/role", name="admin_role_index")
     */
    public function index(RoleRepository $repo)
    {

        $roles = $repo->findAll();

        return $this->render('admin/role/index.html.twig', [
            'website'   => $this->website,
            'roles'     => $roles
        ]);
    }

    /**
     * Permet d'éditer un rôle dans l'admin
     *
     * @Route("/admin/role/{id}/editer", name="admin_role_edit")
     * 
     * @param Role $role
     * @return Response
     */
    public function edit(Role $role, Request $request, EntityManagerInterface $manager)
    {
        $form = $this->createForm(RoleType::class, $role);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $manager->persist($role);
            $manager->flush();

            $this->addFlash(
                'success',
                "<div class='row'>
                <div class='col'>
                La rubrique '{$role->getName()}' a bien été modifiée !<br>
                </div>
                <div class='col text-right'>
                    <a href='../' class='btn btn-primary'>Revenir sur la liste des rubriques</a>
                </div>
            </div>"
            );
        }
        
        return $this->render('admin/role/edit.html.twig', [
            'website'   => $this->website,
            'form'      => $form->createView(),
            'role'    => $role
        ]);
    }

    /**
     * Permet de supprimer une rubrique
     *
     * @Route("/admin/rubrique/{id}/supprimer", name="admin_role_delete")
     * 
     * @param Role $role
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delete(Role $role, EntityManagerInterface $manager){
        $manager->remove($role);
        $manager->flush();

        $this->addFlash(
            'success',
            "La rubrique <b>'{$role->getName()}'</b> a bien été supprimée !"
        );

        return $this->redirectToRoute('admin_role_index');
    }
}
