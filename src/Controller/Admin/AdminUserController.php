<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Entity\Claim;
use App\Form\RoleType;
use App\Form\ClaimType;
use App\Service\Website;
use App\Entity\UserSearch;
use App\Form\UserSearchType;
use App\Form\AccountEditorType;
use App\Service\UploaderHelper;
use App\Form\AccountAffirmerType;
use App\Repository\RoleRepository;
use App\Repository\UserRepository;
use App\Repository\ClaimRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminUserController extends AbstractController
{
    public function __construct(Website $website, ManagerRegistry $doctrine)
    {
        $this->website = $website->getArray();
        $this->subdomain = $website->getArray()['address']['subdomain'];
        $this->doctrine = $doctrine;
    }
    
    /**
     * @Route("/admin/utilisateur", name="admin_user_index")
     */
    public function index(UserRepository $repo, Request $request, PaginatorInterface $paginator)
    {
        $search = new UserSearch;
        $form = $this->createForm(UserSearchType::class,$search);
        $form->handleRequest($request);
    
        //dd($search);
        $users = $paginator->paginate($repo->findAllSearchByFilter($search),$request->query->getInt('page', 1), /*page number*/20 /*limite par page*/);

        //$users = $repo->findAll();

        return $this->render('admin/user/index.html.twig', [
            'website'   => $this->website,
            'form'      => $form->createView(),
            'users'     => $users
        ]);
    }

    /**
     * Formulaire de modification du profil et des droits
     *
     * @Route("/admin/utilisateur/{id}", name="admin_user_edit")
     * 
     * @return Response
     */
    public function profile(User $user, Request $request, EntityManagerInterface $manager, UploaderHelper $uploaderHelper, RoleRepository $repoRole){
        
        $formAffirm = $this->createForm(AccountAffirmerType::class, $user);
        $formAffirm->handleRequest($request);

        if ($formAffirm->isSubmitted() && $formAffirm->isValid()){

            foreach($user->getArtistAffirmers() as $artist){
                $artist->addAffirmer($user);
                $manager->persist($artist);
            }

            foreach($user->getOrganizerAffirmers() as $organizer){
                $organizer->addAffirmer($user);
                $manager->persist($organizer);
            }

            $manager->persist($user);
            $manager->flush();
            
            $this->addFlash(
                'success',
                "Les données du profil ont été mis à jour avec succès"
            );

            return $this->redirect($request->getUri());

        }

        $formEdit = $this->createForm(AccountEditorType::class, $user);
        $formEdit->handleRequest($request);
        
        //dd($form->getData()->getEditors());


        if ($formEdit->isSubmitted() && $formEdit->isValid()){

            foreach($user->getArtistEditors() as $artist){
                $artist->addEditor($user);
                $manager->persist($artist);
            }

            foreach($user->getOrganizerEditors() as $organizer){
                $organizer->addEditor($user);
                $manager->persist($organizer);
            }

            $manager->persist($user);
            $manager->flush();
            
            $this->addFlash(
                'success',
                "Les droits du profil ont été mis à jour avec succès"
            );

            return $this->redirect($request->getUri());

        }
        
        $claims = [];
        foreach($user->getClaims() as $claim){
            if($claim->getPlace()){
               $claims[] = $claim->getPlace(); 
            }
        }
        

        $formRole = $this->createForm(RoleType::class, $user);
        $formRole->handleRequest($request);

        if ($formRole->isSubmitted() && $formRole->isValid()){
            if($formRole->get("isAdministrator")->getData()){

                if (count($formRole->get("subdomains")->getData()) > 0){
                    $user->addUserRole($repoRole->findOneBy(['title' => "ROLE_ADMIN"]));
                    $manager->persist($user);
                    $manager->flush();
                } else {
                    $this->addFlash(
                        'danger',
                        "Vous devez associer au moins un sous-domaine pour modifier les droits d'administration"
                    );
                }
            }
        }

        return $this->render('admin/user/edit.html.twig', [
            'formAffirm'=> $formAffirm->createView(),
            'formEdit'  => $formEdit->createView(),
            'formRole'  => $formRole->createView(),
            'claims'    => $claims,
            'user'      => $user,
            'website'   => $this->website
        ]);
    }


    /**
     * Afficher les réclamations
     * 
     * @Route("/admin/reclamation", name="admin_claim_index")
     */
    public function claim(ClaimRepository $repo)
    {
        return $this->render('admin/claim/index.html.twig', [
            'website'   => $this->website,
            'claims'    => $repo->findAll()
        ]);
    }

    /**
     * Editer une réclamation
     * 
     * @Route("/admin/reclamation/{id}", name="admin_claim_edit")
     */
    public function edit(Claim $claim, Request $request, EntityManagerInterface $manager)
    {
        $form = $this->createForm(ClaimType::class, $claim);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            /*
            foreach($festival->getImages() as $image){
                $image->setFestival($festival);
                $manager->persist($image);
            }
            */
            // inutile car Injection dépendance
            //$manager = $this->doctrine->getManager();


            //dd($claim->getOwner());


            //Si la décision est positive, injecté l'utilisateur dans les modérateurs
            if($claim->getResponse()){
                /*
                $place = $this->doctrine->getRepository(Place::class)->findOneBy(['id' => $claim->getPlace()->getId()]);
                $user = $this->doctrine->getRepository(User::class)->findOneBy(['id' => $claim->getOwner()->getId()]);

                $place->addEditor($user);
                $manager->persist($place);
                */

                if ($claim->getPlace())
                {
                    $claim->getOwner()->addPlaceEditor($claim->getPlace());
                } 
                elseif ($claim->getEvent())
                {
                    $claim->getOwner()->addEventEditor($claim->getEvent());
                    /*
                    $editor = new EditorEvent;
                    $editor ->setEditor($claim->getOwner())
                            ->setEvent($claim->getEvent())
                            ->setAuthorizedBy($this->getUser());
                            
                    $manager->persist($editor);
                    */

                }
            }

            $manager->persist($claim);
            $manager->flush();

            $this->addFlash(
                'success',
                "Les modifications ont été effectués !"
            );

            return $this->redirectToRoute('admin_claim_index');
        }
        
        return $this->render('admin/claim/edit.html.twig', [
            'website'   => $this->website,
            'form'      => $form->createView(),
            'claim'     => $claim
        ]);
    }
}
