<?php

namespace App\Controller\Admin;

use App\Entity\Place;
use App\Form\PlaceType;
use App\Service\Website;
use App\Entity\PlaceSearch;
use App\Form\PlaceSearchType;
use App\Service\UploaderHelper;
use App\Repository\PlaceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminPlaceController extends AbstractController
{   
    public function __construct(Website $website)
    {
        $this->website = $website->getArray();
        $this->subdomain = $website->getArray()['address']['subdomain'];
    }

    /**
     * @Route("/admin/lieu/", name="admin_place_index")
     */
    public function index(PlaceRepository $repo, PaginatorInterface $paginator, Request $request)
    {
        /*
        $paginator  ->setEntityClass(Place::class)
                    ->setLimit(25)
                    ->setPage($page);
        */

        $search = new PlaceSearch;
        $form = $this->createForm(PlaceSearchType::class,$search);
        
        $form->handleRequest($request);

        $places = $paginator->paginate($repo->findAllSearchByFilter($search,$this->subdomain,'admin-place'),$request->query->getInt('page', 1), /*page number*/20 /*limite par page*/);

        return $this->render('admin/place/index.html.twig', [
            'website'   => $this->website,
            'form'      => $form->createView(),
            'places'    => $places
            //'places' => $repo->findBy([], ['id' => 'DESC']),
            //'paginator' => $paginator
        ]);
    }

    
    /**
     * Permet d'éditer un lieu dans l'admin
     * 
     * @Route("/admin/lieu/{id}/edit", name="admin_place_edit")
     * 
     * @param Place $place
     * @return Response
     */
    public function edit(Place $place, Request $request, EntityManagerInterface $manager, UploaderHelper $uploaderHelper)
    {
        $description = $place->getDescription();
        $description = str_replace("<br>","
",$description);
        $description = str_replace("<p>","",$description);
        $description = str_replace("</p>","",$description);
        $place->setDescription(trim($description, ", "));

        $form = $this->createForm(PlaceType::class, $place);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){


            /** @var UploadedFile $uploadedFile */
            $uploadedFile = $form['coverImageFile']->getData();

            if ($uploadedFile){
                $newFilename = $uploaderHelper->uploadPlaceImage($uploadedFile);
                $place->setCoverImage($newFilename);
            }

            // Obligation de remplir la date d'ouverture et de fermeture en même temps
            $days = $request->get('opening')['days'];
            foreach ($days as $day => $tab){
                foreach ($tab as $time){
                    if ($time == ""){$days[$day] = ['open' => null, 'close' => null];}
                }
            }

            // Vérifie s'il y une modification apportée aux horaires d'ouverture ou si validation par checkbox
            if ($days != $form->getData()->getOpening()['days'] || 
                isset($request->get('opening')['update']) ||
                isset($request->get('opening')['onlyIfEvent']) != $form->getData()->getOpening()['onlyIfEvent']
            ){
                $openingTab = ['days' => $days, 'update' => date('Y-m-d'), 'onlyIfEvent' => isset($request->get('opening')['onlyIfEvent']) ];
                $place->setOpening($openingTab);
            }

            // Obligation de remplir au moins le moyen de transport et l'arret (ligne facultatif)
            // Permet également de ramener au moyen principale si celui-ci est vide
            $transport = $request->get('transport');
            if ($transport['primary']['service'] == "" || $transport['primary']['stop'] == ""){
                if ($transport['secondary']['service'] == "" || $transport['secondary']['stop'] == ""){
                    if ($transport['tertiary']['service'] == "" || $transport['tertiary']['stop'] == ""){
                        $transport['primary'] = ['service' => null, 'line' => null, 'stop' => null];
                    } else {
                        $transport['primary'] = $transport['tertiary'];
                        $transport['tertiary'] = ['service' => null, 'line' => null, 'stop' => null];
                    }
                    $transport['primary'] = ['service' => null, 'line' => null, 'stop' => null];
                } else {
                    $transport['primary'] = $transport['secondary'];
                    $transport['secondary'] = ['service' => null, 'line' => null, 'stop' => null];
                }
            } elseif ($transport['primary']['line'] == ""){$transport['primary']['line'] == null;}

            if ($transport['secondary']['service'] == "" || $transport['secondary']['stop'] == ""){
                if ($transport['tertiary']['service'] == "" || $transport['tertiary']['stop'] == ""){
                    $transport['secondary'] = ['service' => null, 'line' => null, 'stop' => null];
                } else {
                    $transport['secondary'] = $transport['tertiary'];
                    $transport['tertiary'] = ['service' => null, 'line' => null, 'stop' => null];
                }
            } elseif ($transport['secondary']['line'] == ""){$transport['secondary']['line'] == null;}

            if ($transport['tertiary']['service'] == "" || $transport['tertiary']['stop'] == ""){
                $transport['tertiary'] = ['service' => null, 'line' => null, 'stop' => null];
            } elseif ($transport['tertiary']['line'] == ""){$transport['tertiary']['line'] == null;}

            $place->setTransport($transport);

            if ($form->getData()->getPronoun()){
                $place->setTitle($form->getData()->getPronoun().' '.$form->getData()->getName());
            } else {
                $place->setTitle($form->getData()->getName());
            }

            $description = $form->getData()->getDescription();
            $description = trim($description, ", ");
            $description = str_replace("

","</p><p>",$description);
            $description = str_replace("
","<br>",$description);
            $description = "<p>".$description."</p>";
            $place->setDescription($description);

            $manager->persist($place);
            $manager->flush();

            $this->addFlash(
                'success',
                "<div class='row'>
                    <div class='col'>
                    Le lieu '{$place->getTitle()}' a bien été modifié !<br>
                    </div>
                    <div class='col text-right'>
                        <a href='../' class='btn btn-primary'>Revenir sur la liste des lieu</a>
                    </div>
                </div>"
            );
        }

        /*
        
        if (isset($form->getData()->getTransport()['primary'])){
            $primary = $form->getData()->getTransport()['primary'];
            $tabTransport['primary'] = ['service' => $primary['service'], 'line' => $primary['line'], 'stop' => $primary['stop']];
        } else {
            $tabTransport['primary'] = ['service' => null, 'line' => null, 'stop' => null];
        }

        if (isset($form->getData()->getTransport()['secondary'])){
            $secondary = $form->getData()->getTransport()['secondary'];
            $tabTransport['secondary'] = ['service' => $secondary['service'], 'line' => $secondary['line'], 'stop' => $secondary['stop']];
        } else {
            $tabTransport['secondary'] = ['service' => null, 'line' => null, 'stop' => null];
        }
        
        if (isset($form->getData()->getTransport()['tertiary'])){
            $tertiary = $form->getData()->getTransport()['tertiary'];
            $tabTransport['tertiary'] = ['service' => $tertiary['service'], 'line' => $tertiary['line'], 'stop' => $tertiary['stop']];
        } else {
            $tabTransport['tertiary'] = ['service' => null, 'line' => null, 'stop' => null];
        }
        
        $form->getData()->setTransport($tabTransport);
        */


/*
        $primary = $form->getData()->getTransport()['primary'];
        $secondary = $form->getData()->getTransport()['secondary'];
        $tertiary = $form->getData()->getTransport()['tertiary'];

        $tabTransport['primary'] = ['service' => $primary['service'], 'line' => $primary['line'], 'stop' => $primary['stop']];
        $tabTransport['secondary'] = ['service' => $secondary['service'], 'line' =>  $secondary['line'], 'stop' =>  $secondary['stop']];
        $tabTransport['tertiary'] = ['service' => $tertiary['service'], 'line' => $tertiary['line'], 'stop' => $tertiary['stop']];

        $form->getData()->setTransport($tabTransport);
*/

        return $this->render('admin/place/edit.html.twig', [
            'website'       => $this->website,
            'formPlace'     => $form->createView(),
            'formOpening'   => $form->getData()->getOpening(),
            'formTransport' => $form->getData()->getTransport(),
            'place'         => $place
        ]);
    }

    /**
     * Permet de supprimer un lieu
     *
     * @Route("/admin/place/{id}/delete", name="admin_place_delete")
     * 
     * @param Place $place
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delete(Place $place, EntityManagerInterface $manager){
        $manager->remove($place);
        $manager->flush();

        $this->addFlash(
            'success',
            "L'annonce <strong>'{$place->getTitle()}'</strong> a bien été supprimé !"
        );

        return $this->redirectToRoute('admin_place_index');
    }
}
