<?php

namespace App\Controller\Admin;

use App\Entity\District;
use App\Service\Website;
use App\Form\DistrictType;
use App\Service\Paginator;
use App\Repository\DistrictRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminDistrictController extends AbstractController
{
    public function __construct(Website $website)
    {
        $this->website = $website->getArray();
        $this->subdomain = $website->getArray()['address']['subdomain'];
    }
    
    /**
     * @Route("/admin/quartier/{page<\d+>?1}", name="admin_district_index")
     */
    public function index(DistrictRepository $repo, $page, Paginator $paginator, Request $request, EntityManagerInterface $manager)
    {
        $paginator  ->setEntityClass(District::class)
                    ->setPage($page);

        $district = new District();

        $form = $this->createForm(DistrictType::class, $district);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $manager->persist($district);
            $manager->flush();

            $this->addFlash(
                'success',
                "Le quartier <b>{$district->getName()}</b> a bien été enregistré !"
            );

            return $this->redirectToRoute('admin_district_index');
        }
        
        return $this->render('admin/district/index.html.twig', [
            'website'   => $this->website,
            'paginator' => $paginator,
            'form'      => $form->createView()
        ]);
    }

    /**
     * Permet d'éditer un quartier dans l'admin
     *
     * @Route("/admin/quartier/{id}/editer", name="admin_district_edit")
     * 
     * @param District $district
     * @return Response
     */
    public function edit(District $district, Request $request, EntityManagerInterface $manager)
    {
        $form = $this->createForm(DistrictType::class, $district);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $manager->persist($district);
            $manager->flush();

            $this->addFlash(
                'success',
                "<div class='row'>
                <div class='col'>
                Le quartier '{$district->getName()}' a bien été modifié !<br>
                </div>
                <div class='col text-right'>
                    <a href='../' class='btn btn-primary'>Revenir sur la liste des quartiers</a>
                </div>
            </div>"
            );
        }
        
        return $this->render('admin/district/edit.html.twig', [
            'website'   => $this->website,
            'form'      => $form->createView(),
            'district'  => $district
        ]);
    }

    /**
     * Permet de supprimer une quartier
     *
     * @Route("/admin/quartier/{id}/supprimer", name="admin_district_delete")
     * 
     * @param District $district
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delete(District $district, EntityManagerInterface $manager){
        $manager->remove($district);
        $manager->flush();

        $this->addFlash(
            'success',
            "Le quartier <b>'{$district->getName()}'</b> a bien été supprimé !"
        );

        return $this->redirectToRoute('admin_district_index');
    }
}
