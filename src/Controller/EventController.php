<?php

namespace App\Controller;

use Date;
use DateTime;
use App\Entity\User;
use App\Entity\Claim;
use App\Entity\Event;
use App\Entity\Place;
use App\Entity\Rubric;
use App\Entity\Ticket;
//use Facebook\Facebook;
use App\Entity\Comment;
use App\Form\EventType;
use App\Service\Counts;
use App\Service\Sender;
use App\Entity\Category;
use App\Service\Website;
use App\Service\Widgets;
use App\Entity\EventLike;
use App\Entity\Reduction;
use App\Entity\Ticketing;
use App\Form\CommentType;
use App\Entity\Videostore;
use Cocur\Slugify\Slugify;
use App\Entity\EventSearch;
use App\Form\EventLightType;
//use App\Service\ApiFacebook;
use App\Entity\EventRedirect;
use App\Form\EventSearchType;
use App\Entity\TicketResource;
use App\Service\DataTreatment;
use App\Service\TitlesOfPages;
use App\Form\EventRedirectType;
use App\Service\UploaderHelper;
use App\Form\ReductionConfigType;
use App\Repository\EventRepository;
use App\Repository\EventLikeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Contracts\Cache\ItemInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Contracts\Cache\CacheInterface;
//use Facebook\Exceptions\FacebookSDKException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
//use Facebook\Exceptions\FacebookResponseException;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

/**
 * @Route("/agenda")
 */
class EventController extends AbstractController
{
    public function __construct(Website $website, ManagerRegistry $doctrine)
    {
        $this->website = $website->getArray();
        $this->subdomain = $website->getArray()['address']['subdomain'];
        $this->doctrine = $doctrine;
    }

    /**
     * Permet d'éditer les évènenemts à la volée
     *
     * @Route("/covid", name="event_covid")
     * @Security("is_granted('ROLE_ADMIN')", message="Vous n'avez pas le droit de modifier cet évènement")
     */
    public function covid(Request $request, EntityManagerInterface $manager, EventRepository $repo): Response
    {
        //$start = new DateTime();
        //$now = new DateTime();
        //$end = $now->modify('+4 week');

        $start = new DateTime("2020-12-15");
        $end = new DateTime("2021-01-04");

        $events = $repo->findForCanceled($start->format('Y-m-d'),$end->format('Y-m-d'));

        foreach ($events as $event){
            $event->setIsCanceled(true);
            $manager->persist($event);
        }
        $manager->flush();

        return $this->render('event/covid.html.twig', [
            'events'    => $events,
            'website'   => $this->website,
            'start'     => $start,
            'end'       => $end
        ]);
    }

    /*
     * Permet de récupérer les évènements FB
     *
     * @Route("/apifacebook", name="apifb")

    public function fb(): Response
    {
        $fb = new Facebook([
          'app_id'              => '528367174008732',
          'app_secret'          => '06db823468a39aab62caddacd569cf63',
          'graph_api_version'   => 'v5.7',
        ]);


        try {
            //$response = $fb->get('/me', 'EAAHgjBG3v5wBAJWQSMsGLGTNXyvMn0hmZAoI46xggdWG1VZAtg6xZAk5X5OCvo7Sr22N7lX4q7AjwFxuQq63053AMGNl72S8eJZAk5RftJaOnsClFcaw1nOKMr11z0hhklsKLOPehRNfm9MLZB2fWeToJHOyPkxj1pN8ffe56ShSZCJLl0PArSCZBZAZAHaCkh1qOQh8sNdQCBrgZBZAZCieFAZAfxaGSdHZA75ywZD');
            //$response = $fb->get('816369702339651', 'EAAHgjBG3v5wBAJWQSMsGLGTNXyvMn0hmZAoI46xggdWG1VZAtg6xZAk5X5OCvo7Sr22N7lX4q7AjwFxuQq63053AMGNl72S8eJZAk5RftJaOnsClFcaw1nOKMr11z0hhklsKLOPehRNfm9MLZB2fWeToJHOyPkxj1pN8ffe56ShSZCJLl0PArSCZBZAZAHaCkh1qOQh8sNdQCBrgZBZAZCieFAZAfxaGSdHZA75ywZD');
            //$response = $fb->get('me?fields=events', 'EAAHgjBG3v5wBAKW3rWe8lrvhFKuP8CQMZC42x1c6oVtEzUMvSCESLDH7vmsTDysyb4lUcZCyZAcsLG2UxyTSGSmZCR4ZCp4jUooKcNgsjydWoG4d96gCrbWFrvmWEFuWf78goYRxHoZCRQUkMSUB0Fm8iU2p4Geit6YXdpqHdYdf7YiAEDnxZAJ');
            //$response = $fb->get('me/events?fields=name,start_time,end_time,place,category,description,ticket_uri,picture,cover,is_canceled,is_draft,is_online,rsvp_status,live_videos', 'EAAHgjBG3v5wBAKW3rWe8lrvhFKuP8CQMZC42x1c6oVtEzUMvSCESLDH7vmsTDysyb4lUcZCyZAcsLG2UxyTSGSmZCR4ZCp4jUooKcNgsjydWoG4d96gCrbWFrvmWEFuWf78goYRxHoZCRQUkMSUB0Fm8iU2p4Geit6YXdpqHdYdf7YiAEDnxZAJ');
            $response = $fb->get('me/events?fields=name,place,cover,start_time,end_time,event_times,description,is_online', 'EAAHgjBG3v5wBANPGSmph1mx1yjI4v5XQa6NmRbBRHCSza9GShZAu7JITZAjGiwX0YPALh9HzIus6VF2AELZBIZChnCdU04jb5gw2tffsswP4cErEsZCQ8sFCM9ysW3EmqShW9ZCrZCltVQZBlZCQGSddZAjKj5EnWrdRKGh0HPpPGSUztlyWqQafivtm7v3T1WuDk4TQNXMcyg94l4ZAcZAf5ZB2VXRAE7VjeXY4ZD');


        } catch(FacebookResponseException $e) {
            // Returns Graph API errors when they occur
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(FacebookSDKException $e) {
            // Returns SDK errors when validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        //$me = $response->getGraphUser();
        $me = $response->getGraphEdge();

        //dd($me);
        //foreach($me["events"] as $eventFb){
        foreach($me as $eventFb){
            //dd($eventFb);
            $ticket = new TicketResource;
            $ticket->setTitle($eventFb["name"]);
            $ticket->setCoverImage($eventFb["cover"]["source"]);
            $ticket->setDescription($eventFb["description"]);
            $ticket->setDateStart($eventFb["start_time"]);
            $ticket->setDateEnd($eventFb["end_time"]);
            $ticket->setTimeStart($eventFb["start_time"]);
            $ticket->setTimeEnd($eventFb["end_time"]);
            $ticket->setPlaceResource([
                'name'      => $eventFb["place"]["name"],
                'city'      => $eventFb["place"]["location"]["city"],
                'country'   => $eventFb["place"]["location"]["country"],
                'latitude'  => $eventFb["place"]["location"]["latitude"],
                'longitude' => $eventFb["place"]["location"]["longitude"],
                'postal'    => $eventFb["place"]["location"]["zip"]
            ]);
            dd($ticket);

            //dd($eventFb["description"]);
        }

        return $this->render('event/facebook.html.twig', [

        ]);
    }
    */

    /**
     * @Route("/rss", name="flux_rss", defaults={"_format"="rss"})
     */
    public function feed(EventRepository $repo, CacheInterface $cache)
    {
        $nameOfCache = "rss_". $this->subdomain;

        $cache->delete($nameOfCache);
        $events = $cache->get($nameOfCache, function (ItemInterface $item) use ($repo)
        {
            $item->expiresAfter(3600);
            $date = new DateTime();
            //return $repo->findByDate($date->format('Y-m-d'), $this->subdomain);
            return $repo->findAllOfThisWeek($this->subdomain);
        });

        //dd($this->website);
        return $this->render('rss/index.html.twig', [
            'events'    => $events,
            'website'   => $this->website
        ]);
    }


    /**
     * Redirection vers billetterie externe
     *
     * @Route("/evenement/{slug}/billet-{id}", name="redirect_ticket")
     */
    public function redirect_ticket(Ticket $ticket): Response
    {
        /*if (is_numeric($id)){
        } else {
            return $this->redirectToRoute('event_index');
        }*/
            //$ticket = $this->doctrine->getRepository(Ticket::class)->findOneBy(['id' => $id]);

        if ($ticket){
            return $this->redirect($ticket->getUrl());
        }
        else {
            return $this->redirectToRoute('event_index');
        }
    }



    /**
     * Permet d'afficher une seule annonce
     *
     * @Route("/agenda/evenement/{slug}", name="event_show")
     *
     * @return Response
     */
    public function show(Event $event, Request $request, EntityManagerInterface $manager, TitlesOfPages $titles, EventRepository $repo, PaginatorInterface $paginator, Counts $counts){

        // rediriger apres connexion
        $request->getSession()->set('_security.main.target_path', $request->getUri());

        // function show($slug, EventRepository $repo){
        // Je récupère l'annonce qui correspond au slug
        //$event = $repo->findOneBySlug($slug);

            //dd($this->container->get('security.token_storage')->getToken());

        /*
        foreach($event->getEditors() as $editor){
            dd($editor->getEditor());
        }

        $search = new EventSearch;
        $form = $this->createForm(EventSearchType::class,$search);
        $form->handleRequest($request);
        */
        $postComment = new Comment;
        $formComment = $this->createForm(CommentType::class,$postComment);
        $formComment->handleRequest($request);

        if($formComment->isSubmitted() && $formComment->isValid())
        {
            $comment = $formComment->getData();

            $comment    ->setIsChecked(false)
                        ->setIsSpammed(false)
                        ->setEvent($event)
                        ->setAuthor($this->getUser());

            if ($this->getUser() === null){
                $comment->setIsAlerted(true);
            }

            $manager->persist($comment);
            $manager->flush();

            $this->addFlash(
                'success',
                "Votre commentaire a été publié !"
            );

            return $this->redirectToRoute('event_show', [
                'slug' => $event->getSlug()
            ]);
        }


        //$event->setDateText($this->dateSentence->getText($event));

        $rubric = $event->getRubric();
        $cache = new FilesystemAdapter();

        $nameOfCache = "events_by_rubric" .'_'. $this->subdomain .'_'. $rubric->getSlug() .'_'. $request->query->getInt('page', 1);
        //$cache->delete($nameOfCache);

        if($cache->getItem($nameOfCache)->get("value")){
            $data = $cache->getItem($nameOfCache)->get("value");
        } else {
            $data = null;
        }

        $menu['day'] = $this->getCountsByDayOfTheWeek($cache);
        $menu['rubric'] = $this->getRubrics($cache);
        $menu['category'] = $this->getCategories($cache);
        $menu['reduction'] = $this->getReductions($cache);
        //$menu['form'] = $form->createView();
        $menu['collapse'] = ['MENU_WEEK','MENU_SEARCH'];

        return $this->render('event/show.html.twig', [
            'page'              => 'event',
            'event'             => $event,
            'data'              => $data,
            'website'           => $this->website,
            'daysOfWeek'        => $this->getDaysOfTheWeek(),
            'formComment'       => $formComment->createView(),
            'menu'              => $menu
        ]);
    }

    /**
     * Recherche personnalisée avec plusieurs filtres
     *
     * @Route("/recherche/", name="search_by_filter")
     * @Route("/archive/", name="old_search_by_filter")
     */
    public function search(EventRepository $repo, PaginatorInterface $paginator, Request $request, CacheInterface $cache, TitlesOfPages $titles, Counts $counts): Response
    {
        $search = new EventSearch;
        $form = $this->createForm(EventSearchType::class,$search);
        $form->handleRequest($request);
        $widget['form'] = $form->createView();

        if (strpos($request->attributes->get('_route'),"old_") !== false){
            $period = "before";
        } else {
            $period = null;
        }

        $pagination = $paginator->paginate($repo->findAllSearchQuery($search, $this->subdomain, null, $period), $request->query->getInt('page', 1), 50 );

        $title = $titles->getTitlesSimple(
            $request->attributes->get("_route"),
            $counts->getCounts($repo, $request, null, $search),
            $search
        );

        $data = $this->createArrayOfData($pagination, $title);
        //dd($data);

        /*
        if ($request->attributes->get('_route') == "search_by_filter")
        {
            $events = $this->separator($paginator->paginate($repo->findAllSearchQuery($search, $this->subdomain), $request->query->getInt('page', 1), 50 ));
            $countOldEvents = $repo->findAllSearchQuery($search, $this->subdomain, false, true, true)->getSingleScalarResult();
            $title = $titles->getTitlesOfSearch($request, $events->getTotalItemCount(), $countOldEvents, "old_search_by_filter");
        }
        elseif ($request->attributes->get('_route') == "old_search_by_filter")
        {
            $events = $this->separator($paginator->paginate($repo->findAllSearchQuery($search, $this->subdomain,false, true), $request->query->getInt('page', 1), 50));
            $countIncomingEvents = $repo->findAllSearchQuery($search, $this->subdomain, false, false, true)->getSingleScalarResult();
            $title = $titles->getTitlesOfSearch($request, $events->getTotalItemCount(), $countIncomingEvents, "search_by_filter");
            if($ticket){
                return $this->redirect($ticket->getUrl());
            }
            else {
                return $this->redirectToRoute('event_index');
            }
        }
        */



        $menu['day'] = $this->getCountsByDayOfTheWeek($cache);
        $menu['rubric'] = $this->getRubrics($cache);
        $menu['category'] = $this->getCategories($cache);
        $menu['reduction'] = $this->getReductions($cache);
        //$menu['form'] = $form->createView();
        $menu['collapse'] = ['MENU_WEEK'];

        return $this->render('event/search-new.html.twig', [
            'page'              => 'event',
            'data'              => $data,
            'website'           => $this->website,
            'daysOfWeek'        => $this->getDaysOfTheWeek(),
            'title'             => $title,
            'menu'              => $menu,
            'widget'            => $widget,
        ]);
    }

    public function createArrayOfData($pagination, $title)
    {
        $data['title'] = $title['head'];

        foreach($pagination as $event){
            //$event->setDateText($this->dateSentence->getText($event));

            if (
                $event->getDateStart() <= new DateTime('today') and
                $event->getDateEnd() >= new DateTime('today')
            ){
                $events['today'][] = $event;
            } else {
                $events['otherday'][] = $event;
            }
        }

        if (isset($events['today'])){
            $data['list'][] = [
                'title' => $title['today'],
                'events' => $this->separator($events['today'])
            ];
            //dd($data['list']);
        }

        if (isset($events['otherday'])){
            $data['list'][] = [
                'title' => $title['text'],
                'events' => $this->separator($events['otherday'])
            ];
        }

        if ($title['button']){
            $data['button'] = $title['button'];
        }

        $data['pagination'] = $pagination;

        return $data;
    }

    public function nameOfCache($request)
    {

        $nameOfCache = $request->attributes->get('_route') ."_";
        $nameOfCache.= $this->subdomain ."_";
        $nameOfCache.= $request->attributes->get('slug') ."_";
        $nameOfCache.= $request->query->getInt('page', 1);

        return $nameOfCache;
    }

    /**
     * Recherche par rubrique
     *
     * @Route("/rubrique/{slug}", name="events_by_rubric")
     * @Route("/rubrique/{slug}/archive", name="old_events_by_rubric")
     */
    public function search_by_rubric(Rubric $entity, EventRepository $repo, PaginatorInterface $paginator, Request $request, CacheInterface $cache, TitlesOfPages $titles, Widgets $widgets, Counts $counts): Response
    {

        /*
        $search = new EventSearch;
        $form = $this->createForm(EventSearchType::class,$search);
        $form->handleRequest($request);
        */

        $nameOfCache = $this->nameOfCache($request);
        //$cache->delete($nameOfCache);

        $data = $cache->get($nameOfCache, function (ItemInterface $item) use ($repo, $paginator, $request, $titles, $counts, $entity)
        {
            $today = new DateTime();

            if ($today->format('H') == 23){
                $tomorrow = new DateTime('tomorrow');
                $item->expiresAt($tomorrow->setTime(0,0));
            } else {
                $item->expiresAfter(3600);
            }

            if ($request->attributes->get("_route") == "old_events_by_rubric"){
                $period = "before";
            } else {
                $period = null;
            }

            $pagination = $paginator->paginate($repo->findByRubric($request->attributes->get('slug'), $this->subdomain, $period), $request->query->getInt('page', 1), 10 );

            $title = $titles->getTitlesOfRubric(
                $request->attributes->get("_route"),
                $counts->getCounts($repo,$request),
                $entity
            );
            //dd($title);

            return $this->createArrayOfData($pagination, $title);
        });

        //dd($data);

        $menu['day'] = $this->getCountsByDayOfTheWeek($cache);
        $menu['rubric'] = $this->getRubrics($cache);
        $menu['category'] = $this->getCategories($cache);
        $menu['reduction'] = $this->getReductions($cache);
        //$menu['form'] = $form->createView();
        $menu['collapse'] = ['MENU_WEEK','MENU_SEARCH'];

        return $this->render('event/search-new.html.twig', [
            'page'          => 'event',
            'data'          => $data,
            'website'       => $this->website,
            'daysOfWeek'    => $this->getDaysOfTheWeek(),
            'menu'          => $menu,
            'widget'        => $widgets->getWidgets($cache,$request)
        ]);
    }

    /**
     * Recherche par catégorie
     *
     * @Route("/categorie/{slug}", name="events_by_category")
     * @Route("/categorie/{slug}/archive", name="old_events_by_category")
     */
    public function search_by_category(Category $entity, EventRepository $repo, PaginatorInterface $paginator, Request $request, CacheInterface $cache, TitlesOfPages $titles, Widgets $widgets, Counts $counts): Response
    {
        /*
        $search = new EventSearch;
        $form = $this->createForm(EventSearchType::class,$search);
        $form->handleRequest($request);
        */

        $nameOfCache = $this->nameOfCache($request);
        //$cache->delete($nameOfCache);

        $data = $cache->get($nameOfCache, function (ItemInterface $item) use ($repo, $paginator, $request, $titles, $counts, $entity)
        {
            $today = new DateTime();

            if ($today->format('H') == 23){
                $tomorrow = new DateTime('tomorrow');
                $item->expiresAt($tomorrow->setTime(0,0));
            } else {
                $item->expiresAfter(3600);
            }

            if (strpos($request->attributes->get('_route'),"old_") !== false){
                $period = "before";
            } else {
                $period = null;
            }

            $pagination = $paginator->paginate($repo->findByCategory($request->attributes->get('slug'), $this->subdomain, $period), $request->query->getInt('page', 1), 50 );

            $title = $titles->getTitlesSimple(
                $request->attributes->get("_route"),
                $counts->getCounts($repo,$request),
                $entity
            );

            return $this->createArrayOfData($pagination, $title);
        });

        //dd($data);

        $menu['day'] = $this->getCountsByDayOfTheWeek($cache);
        $menu['rubric'] = $this->getRubrics($cache);
        $menu['category'] = $this->getCategories($cache);
        $menu['reduction'] = $this->getReductions($cache);

        //$menu['form'] = $form->createView();
        $menu['collapse'] = ['MENU_RUBRIC','MENU_SEARCH'];

        return $this->render('event/search-new.html.twig', [
            'page'              => 'event',
            'data'              => $data,
            'website'           => $this->website,
            'daysOfWeek'        => $this->getDaysOfTheWeek(),
            'menu'              => $menu,
            'widget'            => $widgets->getWidgets($cache,$request)
        ]);
    }

    /**
     * Recherche par réduction
     *
     * @Route("/reduction/{slug}", name="events_by_reduction")
     * @Route("/reduction/{slug}/archive", name="old_events_by_reduction")
     */
    public function search_by_reduction(Reduction $entity, EventRepository $repo, PaginatorInterface $paginator, Request $request, CacheInterface $cache, TitlesOfPages $titles, Widgets $widgets, Counts $counts): Response
    {
        /*
        $search = new EventSearch;
        $form = $this->createForm(EventSearchType::class,$search);
        $form->handleRequest($request);
        */

        $nameOfCache = $this->nameOfCache($request);
        //$cache->delete($nameOfCache);

        $data = $cache->get($nameOfCache, function (ItemInterface $item) use ($repo, $paginator, $request, $titles, $counts, $entity)
        {
            $today = new DateTime();

            if ($today->format('H') == 23){
                $tomorrow = new DateTime('tomorrow');
                $item->expiresAt($tomorrow->setTime(0,0));
            } else {
                $item->expiresAfter(3600);
            }

            if (strpos($request->attributes->get('_route'),"old_") !== false){
                $period = "before";
            } else {
                $period = null;
            }

            $pagination = $paginator->paginate($repo->findByReduction($request->attributes->get('slug'), $this->subdomain, $period), $request->query->getInt('page', 1), 50 );

            $title = $titles->getTitlesSimple(
                $request->attributes->get("_route"),
                $counts->getCounts($repo,$request),
                $entity
            );

            return $this->createArrayOfData($pagination, $title);
        });

        $menu['day'] = $this->getCountsByDayOfTheWeek($cache);
        $menu['rubric'] = $this->getRubrics($cache);
        $menu['category'] = $this->getCategories($cache);
        $menu['reduction'] = $this->getReductions($cache);
        //$menu['form'] = $form->createView();
        $menu['collapse'] = ['MENU_RUBRIC','MENU_SEARCH'];

        return $this->render('event/search-new.html.twig', [
            'page'              => 'event',
            'data'              => $data,
            'website'           => $this->website,
            'daysOfWeek'        => $this->getDaysOfTheWeek(),
            'menu'              => $menu,
            'widget'            => $widgets->getWidgets($cache,$request)
        ]);
    }

    /**
     * Recherche par favoris
     *
     * @Security("is_granted('ROLE_USER')", message="Vous devez vous connecter pour voir vos évènements favoris")
     *
     * @Route("/mes-evenements-favoris", name="events_by_like")
     * @Route("/mes-evenements-favoris/archive", name="old_events_by_like")
     */
    public function search_by_like(EventRepository $repo, PaginatorInterface $paginator, Request $request, CacheInterface $cache, TitlesOfPages $titles, Counts $counts): Response
    {
        /*
        $search = new EventSearch;
        $form = $this->createForm(EventSearchType::class,$search);
        $form->handleRequest($request);
        */

        if (strpos($request->attributes->get('_route'),"old_") !== false){
            $period = "before";
        } else {
            $period = null;
        }

        $pagination = $paginator->paginate($repo->findByLike($this->getUser(), $period), $request->query->getInt('page', 1), 50 );

        $title = $titles->getTitlesSimple(
            $request->attributes->get("_route"),
            $counts->getCounts($repo, $request, $this->getUser()),
            $this->getUser()
        );

        $data = $this->createArrayOfData($pagination, $title);


        $menu['day'] = $this->getCountsByDayOfTheWeek($cache);
        $menu['rubric'] = $this->getRubrics($cache);
        $menu['category'] = $this->getCategories($cache);
        $menu['reduction'] = $this->getReductions($cache);
        //$menu['form'] = $form->createView();
        $menu['collapse'] = ['MENU_SEARCH'];

        return $this->render('event/search-new.html.twig', [
            'page'              => 'event',
            'data'              => $data,
            'website'           => $this->website,
            'daysOfWeek'        => $this->getDaysOfTheWeek(),
            'menu'              => $menu,
            'widget'            => [],
        ]);
    }

    /**
     * Configurer une réduction
     *
     * @Route("/reduction/{slug}/configuration", name="configuration_reduction")
     * @Security("is_granted('ROLE_ADMIN')", message="Vous n'avez pas le droit de modifier cette réduction")
     */
    public function config_reduction(Reduction $reduction, Request $request, EntityManagerInterface $manager): Response
    {
        //dd($reduction);
        // and reduction.getAuthor(user)

        $form = $this->createForm(ReductionConfigType::class, $reduction);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($reduction);
            $manager->flush();
        }

        return $this->render('validation/configuration-reduction.html.twig', [
            'form'          => $form->createView(),
            'website'       => $this->website,
        ]);
    }

    /**
     * Afficher les évènements par jour
     *
     * @Route("/calendrier/{date}", name="event_by_calendar")
     */
    public function calendar(EventRepository $repo, Request $request, $date, CacheInterface $cache, Widgets $widgets): Response
    {
        //$dateToday = new DateTime(date('Y-m-d'));
        //$date = date_create($dateTxt);
        //$interval = $dateToday->diff($date);

        $interval = date_diff(date_create(date('Y-m-d')), date_create($date));
        $nbDay = $interval->format('%d');

        if ($interval->invert){
            if($nbDay == 1){return $this->redirectToRoute('event_by_day', ['txtDay' => "hier" ]);}
            elseif($nbDay == 2){return $this->redirectToRoute('event_by_day', ['txtDay' => "avant-hier" ]);}
        } else {
            if($nbDay == "0"){return $this->redirectToRoute('event_by_day', ['txtDay' => "aujourd-hui" ]);}
            elseif($nbDay == 1){return $this->redirectToRoute('event_by_day', ['txtDay' => "demain" ]);}
            elseif($nbDay == 2){return $this->redirectToRoute('event_by_day', ['txtDay' => "apres-demain" ]);}
            elseif($nbDay == 3){return $this->redirectToRoute('event_by_day', ['txtDay' => "dans-3-jours" ]);}
            elseif($nbDay == 4){return $this->redirectToRoute('event_by_day', ['txtDay' => "dans-4-jours" ]);}
            elseif($nbDay == 5){return $this->redirectToRoute('event_by_day', ['txtDay' => "dans-5-jours" ]);}
            elseif($nbDay == 6){return $this->redirectToRoute('event_by_day', ['txtDay' => "dans-6-jours" ]);}
            elseif($nbDay == 7){return $this->redirectToRoute('event_by_day', ['txtDay' => "dans-une-semaine" ]);}
        }

        $dayOfWeek = ["dimanche","lundi","mardi","mercredi","jeudi","vendredi","samedi"];
        $month = ["décembre","janvier","février","mars","avril","mai","juin","juillet","août","septembre","octobre","novembre","décembre"];

        $nbWeek = date('w', strtotime($date));
        $theDay = date('d', strtotime($date));
        $nbMonth = date('n', strtotime($date));
        $theYear = date('Y', strtotime($date));

        $title = $dayOfWeek[$nbWeek].' '.$theDay.' '.$month[$nbMonth].' '.$theYear;

        /*
        $events = $this->separatorByTime($repo->findByDate($date, $this->subdomain));
        $eventsSecondary = $this->separatorByDate($repo->findByDateSecondary($date, $this->subdomain));
        */

        //dd($repo->findByDate($date, $this->subdomain));

        foreach($repo->findByDate($date, $this->subdomain) as $event)
        {
            //$event->setDateText($this->dateSentence->getText($event));

            if (
                $event->getDateStart() == new DateTime($date) or
                $event->getDateEnd() == new DateTime($date)
            ){
                $events['primary'][] = $event;
            } else {
                $events['secondary'][] = $event;
            }
        }

        if (isset($events['primary'])){
            $data['list'][] = [
                'title' => $title,
                'events' => $this->separator($events['primary'])
            ];
            //dd($data['list']);
        }

        if (isset($events['secondary'])){
            $data['list'][] = [
                'title' => "Les évènements sur plusieurs jours",
                'events' => $this->separator($events['secondary'])
            ];
        }

        //dd($data);

        $data['title'] = $title;
        $data['button'] = null;
        $data['pagination'] = null;

        /*
        $search = new EventSearch;
        $form = $this->createForm(EventSearchType::class,$search);
        $form->handleRequest($request);
        */

        $menu['day'] = $this->getCountsByDayOfTheWeek($cache);
        $menu['rubric'] = $this->getRubrics($cache);
        $menu['category'] = $this->getCategories($cache);
        $menu['reduction'] = $this->getReductions($cache);
        //$menu['form'] = $form->createView();
        $menu['collapse'] = ['MENU_RUBRIC','MENU_SEARCH'];

        return $this->renderForm('event/search-day.html.twig', [
            'page'              => 'event',
            'dateSearch'        => $date,
            'data'              => $data,
            'website'           => $this->website,
            'daysOfWeek'        => $this->getDaysOfTheWeek(),
            'title'             => $title,
            'menu'              => $menu,
            'widget'            => $widgets->getWidgets($cache,$request,$date)
        ]);

    }

    /**
     * Création d'un évènement simple
     *
     * @Route("/annoncer-simplement", name="event_create_light")
     */
    public function createlight(EventRepository $repo, Request $request, EntityManagerInterface $manager, UploaderHelper $uploaderHelper, TokenGeneratorInterface $tokenGenerator, DataTreatment $dataTreatment): Response
    {
        $event = new Event();

        $form = $this->createForm(EventLightType::class, $event);
        $form->handleRequest($request);

        if ($event->getDateEnd() === null and $event->getDateStart() and $event->getDateStart()->format("Y-m-d") < date("Y-m-d")){
            $this->addFlash(
                'danger',
                "Vous ne pouvez pas annoncer un évènement déjà terminé !"
            );
        }
        elseif ($form->getData()->getCity() == NULL && $form->getData()->getPlace() && $form->getData()->getPlace()->getId() == NULL && $form->getData()->getPlace()->getTitle() != NULL)
        {
            $this->addFlash(
                'danger',
                "Veuillez préciser la ville ! Vous pouvez également choisir ou créer un lieu dans l'annuaire !"
            );
        }
        elseif($form->isSubmitted() && $form->isValid())
        {
            $alreadyPublished = $repo->findAlreadyPublished($event->getDateStart(), $event->getDateEnd(), $event->getTitle(), $event->getPlace(), $event->getPlaceTemporary());

            if ($alreadyPublished != []){

                if ($this->getUser() and $this->getUser() == $event->getAuthor()){
                    $this->addFlash(
                        'danger',
                        "<div class='h2'>Vous avez déjà annoncé cet évènement!</div>
                        <a class='btn btn-primary mt-2' target='_blank' href='".$this->generateUrl('event_show', ['slug'=> $alreadyPublished[0]->getSlug()])."'>Voir l'évènement déjà annoncé</a>
                        <a class='btn btn-primary mt-2' target='_blank' href='".$this->generateUrl('event_edit', ['slug'=> $alreadyPublished[0]->getSlug()])."'>Modifier l'évènement déjà annoncé</a>"
                    );
                } elseif ($this->getUser()){
                    $this->addFlash(
                        'danger',
                        "<div class='h2'>Cet évènement est déjà annoncé !</div>
                        <a class='btn btn-primary mt-2' target='_blank' href='".$this->generateUrl('event_show', ['slug'=> $alreadyPublished[0]->getSlug()])."'>Voir l'évènement déjà annoncé</a>"
                    );
                } else {
                    $this->addFlash(
                        'danger',
                        "<div class='h2'>Cet évènement est déjà annoncé !</div>
                        Vous pouvez modifier cet évènement en créant un compte !<br>
                        IMPORTANT : Compléter votre profil afin que nous puissions vous identifier avant de vous accorder les droits d'accès.</br>
                        <a class='btn btn-primary mt-2' target='_blank' href='".$this->generateUrl('event_show', ['slug'=> $alreadyPublished[0]->getSlug()])."'>Voir l'évènement déjà annoncé</a>
                        <a class='btn btn-primary mt-2' target='_blank' href='".$this->generateUrl('event_claim', ['slug'=> $alreadyPublished[0]->getSlug()])."'>Demander le droit de modifier l'évènement déjà annoncé</a>"
                    );
                }

            } else {

                $slugify = new Slugify();
                $slug = $slugify->slugify($event->getTitle());

                $sameSlug = $this->doctrine->getRepository(Event::class)->findOneBy(['slug' => $slug]);
                if ($sameSlug){
                    for($i = 1; is_object($sameSlug); $i++){
                        $slugTMP = $slug."-".$i;
                        $sameSlug = $this->doctrine->getRepository(Event::class)->findOneBy(['slug' => $slugTMP]);
                    }
                    $event->setSlug($slugTMP);
                }

                if ($this->getUser()){

                    $event->setAuthor($this->getUser());
                    if ($this->getUser()->getIsChecked() or in_array('ROLE_ADMIN',$this->getUser()->getRoles())){
                        $event->setIsValidated(true);
                    }

                } else {
                    $event->setIsPublished(true);
                    $session = $request->getSession();

                    if ($session->get('token') === null){
                        $session->set('token', $tokenGenerator->generateToken());
                        //$session->set('token', random_int(1000000000,9999999999));
                    }

                    $event->setSecretAnonymous($session->get('token'));
                    $event->setIpAnonymous($request->getClientIp());

                    if ($event->getMailAnonymous()){
                        $event->setMailAnonymous($event->getMailAnonymous());
                    }
                }

                $data = $dataTreatment->event($form);

                $event = $data['event'];

                $manager->persist($event);
                $manager->flush();

                if ($event->getIsValidated() !== true) {
                    $this->addFlash(
                        'success',
                        "Votre évènement <strong>{$event->getTitle()}</strong> est enregistré et en attente de validation !"
                    );
                }

                return $this->redirectToRoute('event_show', ['slug' => $event->getSlug()]);
            }

        }

        return $this->renderForm('event/newlight.html.twig', [
            'page'      => 'event',
            'form'      => $form,
            'website'   => $this->website,
            'annonce'   => 'new'
        ]);
    }

    public function translateResourceToEvent($resource): Event
    {
        $event = new Event;

        if ($resource->getPlace()){
            $event->setPlace($resource->getPlace());
        } elseif ($resource->getPlaceResource()) {
            $placeTMP = new Place;
            $event->setPlace($placeTMP->setTitle($resource->getPlaceResource()['name']));
        }

        if ($resource->getCoverImage() != "https://www.fnacspectacles.com/static/uploads/images/defaultManifG.png")
        {
            $imageUrl = str_replace("/visuel/grand/","/visuel/600/",$resource->getCoverImage());
            $headers = @get_headers($imageUrl);

            if ($headers && strpos( $headers[0], '200')) {$event->setCoverImage($imageUrl);}
            else {$event->setCoverImage($resource->getCoverImage());}

            //dd(file_get_contents($resource->getCoverImage()));
        }

        $event  ->setTitle($resource->getTitle())
                ->setDateStart($resource->getDateStart())
                ->setTimeStart($resource->getTimeStart())
                ->setTimeEnd($resource->getTimeEnd())
                ->setCity($resource->getCity())
                ->setRubric($resource->getRubric())
                ;

        if ($resource->getDateStart() != $resource->getDateEnd()){
            $event ->setDateEnd($resource->getDateEnd());
        }

        if ($resource->getDescription()){
            $description = str_replace("! ","!</p><p>",$resource->getDescription());
            $description = str_replace(". ",".</p><p>",$description);
            $description = str_replace("; ",". ",$description);
            $description = str_replace("'","’",$description);
            $event ->setDescription($description);
        }

        if($resource->getMultiDateTime() != []){
            $event  ->setDateTimeMultipleArray($resource->getMultiDateTime());
                    //->setDescription($resource->getDescription() ."<p>". implode(", ", $resource->getMultiDateTime()) ."<p>");
        }

        $event->setPrecisionPrice('PAYING');

        if($resource->getPrice()){
            $event->setPrice(round($resource->getPrice() - 1.80));
            /*
            if ($resource->getPrice() < 10 ){
                $event  ->setPrice(round($resource->getPrice()));
            } elseif ($resource->getPrice() <= 21.80 ){
                $event->setPrice(round($resource->getPrice() - 1.80));
            } else {
                $event->setPrice(null);
            }*/
        }

        return $event;
    }

    /**
     * Création d'un évènement
     *
     * @Route("/annoncer-un-evenement", name="event_create")
     */
    public function create(EventRepository $repo, Request $request, EntityManagerInterface $manager, UploaderHelper $uploaderHelper, TokenGeneratorInterface $tokenGenerator, /*ApiFacebook $facebook,*/ DataTreatment $dataTreatment): Response
    {

        //dd($request->getClientIp());
        //dd($this->container->get('request_stack')->getCurrentRequest()->getClientIp());

        $event = new Event();

        if ($request->query->get('lieu')){
            $place = $this->doctrine->getRepository(Place::class)->findOneBy(['slug' => $request->query->get('lieu')]);
            $event  ->setPlace($place)
                    ->setCity($place->getCity());
        }

        /*
        if ($request->query->get('fb')){
            $eventsFb = $facebook->getEventsFB()['events'];
            foreach ($eventsFb as $eventFb){
                if ($eventFb->getIdResource() == $request->query->get('fb')){
                    //dd($eventFb);
                    $event = $this->translateResourceToEvent($eventFb);
                    //dd($event);
                }
            }
        }
        */

        if ($request->query->get('resource')){
            $resource = $this->doctrine->getRepository(TicketResource::class)->findOneBy(['id' => $request->query->get('resource')]);
            if ($resource){

                $event = $this->translateResourceToEvent($resource);

                //$tickets = $this->doctrine->getRepository(TicketResource::class)->findAllSameEvents($resource->getDateStart(), $resource->getDateEnd(), $resource->getTitle(), $resource->getPlace(), $resource->getPlaceResource()['name']);
                $tickets = $this->doctrine->getRepository(TicketResource::class)->findAllSameEvents($resource);

                //dd($tickets);

                foreach($tickets as $t){
                    $ticket = new Ticket;

                    $ticket ->setUrl($t->getLinkAffiliate())
                            ->setPrice($t->getPrice())
                            ->setTicketing($t->getTicketing())
                            ->setResource($t);

                    if ($t->getTitle() != $event->getTitle()){
                        $ticket->setInformation($t->getTitle());
                    }

                    //dd($ticket);

                    $event->addTicket($ticket);
                }
                //dd($event);
                //dd($ticket);
            }
        }

        $form = $this->createForm(EventType::class, $event);

        //dd($form->getData());

        $form->handleRequest($request);

        //dd($form->getData());
        //dd($event->getDateEnd());

        /*
        if ($form['dateEnd']->getData() == null){
            $event->setDateEnd($form['dateStart']);
        }
        */
        /*
        Création d'un formulaire temporaire
        $image = new Image();

        $image->setUrl('http://placehold.it/400x200')
              ->setCaption('Titre 1');

              $image2 = new Image();

              $image2->setUrl('http://placehold.it/400x200')
                    ->setCaption('Titre 2');

        $event->addImage($image)
              ->addImage($image2);
        */


        /*
        $i = 0;
        foreach($event->getCategory() as $category){
            if ($i < 4) {
                $category->addEvent($event);
                $manager->persist($category);
            } else {
                $event->removeCategory($category);
            }
            $i++;
        }

        $j = 0;
        foreach($event->getArtists() as $artist){
            if ($j < 10) {
                $artist->addEvent($event);
                $manager->persist($artist);
            } else {
                $event->removeArtist($artist);
            }
            $j++;
        }
        */

        $suggestion = $this->doctrine->getRepository(Rubric::class)->findAllByCategory()->getResult();
        $suggestions = ['categories' => $suggestion, 'places' => NULL, 'reductions' => NULL];

        if ($form->isSubmitted() && $form->isValid() && $dataTreatment->check($form))
        {
            //dd($event->getDescription());
            //dd($request->request->get('event')["description"]);

            $alreadyPublished = $repo->findAlreadyPublished($event->getDateStart(), $event->getDateEnd(), $event->getTitle(), $event->getPlace(), $event->getPlaceTemporary());

            if ($alreadyPublished != []){

                if ($this->getUser() and $this->getUser() == $event->getAuthor()){
                    $this->addFlash(
                        'danger',
                        "<div class='h2'>Vous avez déjà annoncé cet évènement!</div>
                        <a class='btn btn-primary mt-2' target='_blank' href='".$this->generateUrl('event_show', ['slug'=> $alreadyPublished[0]->getSlug()])."'>Voir l'évènement déjà annoncé</a>
                        <a class='btn btn-primary mt-2' target='_blank' href='".$this->generateUrl('event_edit', ['slug'=> $alreadyPublished[0]->getSlug()])."'>Modifier l'évènement déjà annoncé</a>"
                    );
                } elseif ($this->getUser()){
                    $this->addFlash(
                        'danger',
                        "<div class='h2'>Cet évènement est déjà annoncé !</div>
                        <a class='btn btn-primary mt-2' target='_blank' href='".$this->generateUrl('event_show', ['slug'=> $alreadyPublished[0]->getSlug()])."'>Voir l'évènement déjà annoncé</a>"
                    );
                } else {
                    $this->addFlash(
                        'danger',
                        "<div class='h2'>Cet évènement est déjà annoncé !</div>
                        Vous pouvez modifier cet évènement en créant un compte !<br>
                        IMPORTANT : Compléter votre profil afin que nous puissions vous identifier avant de vous accorder les droits d'accès.</br>
                        <a class='btn btn-primary mt-2' target='_blank' href='".$this->generateUrl('event_show', ['slug'=> $alreadyPublished[0]->getSlug()])."'>Voir l'évènement déjà annoncé</a>
                        <a class='btn btn-primary mt-2' target='_blank' href='".$this->generateUrl('event_claim', ['slug'=> $alreadyPublished[0]->getSlug()])."'>Demander le droit de modifier l'évènement déjà annoncé</a>"
                    );
                }

            } else {

                $slugify = new Slugify();
                $slug = $slugify->slugify($event->getTitle());

                $sameSlug = $this->doctrine->getRepository(Event::class)->findOneBy(['slug' => $slug]);
                if ($sameSlug){
                    for($i = 1; is_object($sameSlug); $i++){
                        $slugTMP = $slug."-".$i;
                        $sameSlug = $this->doctrine->getRepository(Event::class)->findOneBy(['slug' => $slugTMP]);
                    }
                    $event->setSlug($slugTMP);
                }

                if ($this->getUser()){

                    $event->setAuthor($this->getUser());
                    if ($this->getUser()->getIsChecked() or in_array('ROLE_ADMIN',$this->getUser()->getRoles())){
                        $event->setIsValidated(true);
                    }

                } else {
                    //$event->setIsValidated(false);
                    $event->setIsPublished(true);

                    //$session = new Session();
                    //$session->start();

                    //dump($session);
                    //dd($session->get('token'));

                    $session = $request->getSession();

                    //Injection un token dans la session si nécessaire
                    if ($session->get('token') === null){
                        $session->set('token', $tokenGenerator->generateToken());
                        //$session->set('token', random_int(1000000000,9999999999));
                    }

                    //Injection du token en base de donnée
                    $event->setSecretAnonymous($session->get('token'));
                    //$event->setSecretAnonymous($this->container->get('security.token_storage')->getToken()->getSecret());

                    $event->setIpAnonymous($request->getClientIp());

                    if ($event->getMailAnonymous()){
                        $event->setMailAnonymous($event->getMailAnonymous());
                    }
                }

                $data = $dataTreatment->event($form);

                $event = $data['event'];
                $suggestions['places'] = $data['places'];
                //dd($request->files->get('event')['images']);

                $manager->persist($event);
                $manager->flush();

                if ($event->getIsPublished() !== true){
                    $this->addFlash(
                        'success',
                        "Votre évènement <strong>{$event->getTitle()}</strong> a bien été enregistré en brouillon !"
                    );
                } elseif ($event->getIsValidated() !== true) {
                    $this->addFlash(
                        'success',
                        "Votre évènement <strong>{$event->getTitle()}</strong> est enregistré et en attente de validation !"
                    );
                } else {
                    $this->addFlash(
                        'success',
                        "Votre évènement <strong>{$event->getTitle()}</strong> a bien été publié !"
                    );
                }

                return $this->redirectToRoute('event_show', ['slug' => $event->getSlug()]);
            }

        }

        return $this->renderForm('event/new.html.twig', [
            'page'          => 'event',
            'form'          => $form,
            'website'       => $this->website,
            'suggestion'    => $suggestion,
            'suggestions'   => $suggestions,
            'annonce'       => 'new'
        ]);
    }

    //@Security("(request.getSession().get('token') and request.getSession().get('token') === event.getSecretAnonymous()) or (is_granted('ROLE_USER') and (user === event.getAuthor() or event.getEditors().contains(user) or is_granted('ROLE_ADMIN')))", message="Vous n'avez pas le droit de modifier cet évènement")

    /**
     * Permet d'éditer une annonce
     *
     * @Route("/evenement/{slug}/modification", name="event_edit")
     */
    public function edit(Event $event, Request $request, EntityManagerInterface $manager, UploaderHelper $uploaderHelper, DataTreatment $dataTreatment): Response
    {
        // vérification du droit d'accès (configurer dans App/Security/EventVoter)
        $this->denyAccessUnlessGranted("edit", $event);

        // Si aucun lieu associé, alors injecte objet place dans event avec placeTemporary dans Name
        if ($event->getPlace() == NULL && $event->getPlaceTemporary() != ""){
            $place = new Place();
            $place->setTitle($event->getPlaceTemporary());
            $event->setPlace($place);
        }

        $form = $this->createForm(EventType::class, $event);

        if ($event->getPrecisionPrice() == 'FREE' or $event->getPrecisionPrice() == 'AWARE'){
            $form['price']->setData(null);
        }
        elseif ($event->getPrecisionPrice() == 'MORE'){
            $form['precisionPrice']->setData('PAYING');
            $form['more']->setData(true);
        }

        $form->handleRequest($request);

        $suggestion = $this->doctrine->getRepository(Rubric::class)->findAllByCategory()->getResult();
        $suggestions = ['categories' => $suggestion, 'places' => NULL, 'reductions' => NULL];

        if($form->isSubmitted() && $form->isValid() && $dataTreatment->check($form))
        {
            $data = $dataTreatment->event($form);

            $event = $data['event'];
            $suggestions['places'] = $data['places'];

            $manager->persist($event);
            $manager->flush();

            $this->addFlash(
                'success',
                "Les modifications ont été effectuées !"
            );

            //if ($data['places'] == []){}
            return $this->redirectToRoute('event_show', [
                'slug' => $event->getSlug()
            ]);

        }

        return $this->render('event/edit.html.twig', [
            'page'          => 'event',
            'form'          => $form->createView(),
            'website'       => $this->website,
            'suggestion'    => $suggestion,
            'suggestions'   => $suggestions,
            'annonce'       => 'edit',
            'author'        => $event->getAuthor()
        ]);
    }

    /**
     * Afficher les évènements par jour
     *
     * @Route("/que-faire-{txtDay}", name="event_by_day")
     * @Route("/c-etait-{txtDay}", name="event_by_day_past")
     * @Route("/{txtDay}", name="event_others")
     */
    public function indexDay(EventRepository $repo, Request $request, $txtDay, CacheInterface $cache, Widgets $widgets)
    {
        $tabRedirectToIndex = ['rubrique','categorie','reduction'];

        // redirection des anciennes rubriques
        $tabRubric = ['concert' => 'concert', 'soiree' => 'soiree', 'theatre' => 'spectacle', 'exposition' => 'exposition', 'cinema' => 'cinema', 'manifestation' => 'action-citoyenne', 'reunion-publique' => 'action-citoyenne', 'musique-classique' => 'concert', 'autre' => 'autre'];

        if (in_array($txtDay,$tabRedirectToIndex)){
            return $this->redirectToRoute('event_index');
        }
        elseif (isset($tabRubric[$txtDay])){
            return $this->redirectToRoute('events_by_rubric', ['slug' => $tabRubric[$txtDay] ]);
        }

        //Exception ancienne rubrique Danse vers catégorie
        if ($txtDay == "danse"){
            return $this->redirectToRoute('events_by_category', ['slug' => 'danse' ]);
        }

        if ($txtDay == "evenement"){
            return $this->redirectToRoute('event_create');
        }

        /*
        $events = $this->separatorByTime($repo->findByDate($date, $this->subdomain));
        $eventsSecondary = $this->separatorByDate($repo->findByDateSecondary($date, $this->subdomain));
        */

        ///dd($repo->findByDate($date, $this->subdomain));
        $nameOfCache = $request->attributes->get('_route') ."_";
        $nameOfCache.= $this->subdomain ."_";
        $nameOfCache.= $txtDay;


        $textePrecision="";
        if ($txtDay=="avant-hier"){$nbDay=-2; $textePrecision="Avant-hier";}
        elseif ($txtDay=="hier"){$nbDay=-1; $textePrecision="Hier";}
        elseif ($txtDay=="aujourd-hui"){$nbDay=0; $textePrecision="Aujourd'hui, ce ";}
        elseif ($txtDay=="demain"){$nbDay=1; $textePrecision="Demain, le ";}
        elseif ($txtDay=="apres-demain"){$nbDay=2; $textePrecision="Après-demain, le ";}
        elseif ($txtDay=="dans-3-jours"){$nbDay=3;}
        elseif ($txtDay=="dans-4-jours"){$nbDay=4;}
        elseif ($txtDay=="dans-5-jours"){$nbDay=5;}
        elseif ($txtDay=="dans-6-jours"){$nbDay=6;}
        elseif ($txtDay=="dans-une-semaine"){$nbDay=7;}
        else {$nbDay=0;}

        $date = date('Y-m-d', strtotime($nbDay.'days'));

        $dayOfWeek = ["dimanche","lundi","mardi","mercredi","jeudi","vendredi","samedi"];
        $month = ["décembre","janvier","février","mars","avril","mai","juin","juillet","août","septembre","octobre","novembre","décembre"];

        $nbWeek = date('w', strtotime($nbDay.'days'));
        $theDay = date('d', strtotime($nbDay.'days'));
        $nbMonth = date('n', strtotime($nbDay.'days'));
        $theYear = date('Y', strtotime($nbDay.'days'));
        $title = $textePrecision.' '.$dayOfWeek[$nbWeek].' '.$theDay.' '.$month[$nbMonth].' '.$theYear;


        //dd($nameOfCache);
        //$cache->delete($nameOfCache);


        $data = $cache->get($nameOfCache, function (ItemInterface $item) use ($repo, $date, $title)
        {
            $data["list"] = [];

            $today = new DateTime();

            if ($today->format('H') == 23){
                $tomorrow = new DateTime('tomorrow');
                $item->expiresAt($tomorrow->setTime(0,0));
            } else {
                $item->expiresAfter(3600);
            }

            foreach($repo->findByDate($date, $this->subdomain) as $event)
            {
                //$event->setDateText($this->dateSentence->getText($event));

                if (
                    $event->getDateStart() == new DateTime($date) or
                    $event->getDateEnd() == new DateTime($date)
                ){
                    $events['primary'][] = $event;
                } else {
                    $events['secondary'][] = $event;
                }
            }

            if (isset($events['primary'])){
                $data['list'][] = [
                    'title' => $title,
                    'events' => $this->separator($events['primary'])
                ];
                //dd($data['list']);
            }

            if (isset($events['secondary'])){
                $data['list'][] = [
                    'title' => "Les évènements sur plusieurs jours",
                    'events' => $this->separator($events['secondary'])
                ];
            }

            return $data;
        });


        $data['title'] = $title;
        $data['button'] = null;
        $data['pagination'] = null;

        /*
        $search = new EventSearch;
        $form = $this->createForm(EventSearchType::class,$search);
        $form->handleRequest($request);
        */

        $menu['day'] = $this->getCountsByDayOfTheWeek($cache);
        $menu['rubric'] = $this->getRubrics($cache);
        $menu['category'] = $this->getCategories($cache);
        $menu['reduction'] = $this->getReductions($cache);
        //$menu['form'] = $form->createView();
        $menu['collapse'] = ['MENU_RUBRIC','MENU_SEARCH'];

        return $this->render('event/search-day.html.twig', [
            'page'              => 'event',
            'dateSearch'        => $date,
            'data'              => $data,
            'website'           => $this->website,
            'daysOfWeek'        => $this->getDaysOfTheWeek(),
            'title'             => $title,
            'menu'              => $menu,
            'widget'            => $widgets->getWidgets($cache,$request,$date)
        ]);
    }


    /**
     * Afficher les évènements de la semaine
     *
     * @Route("/", name="event_index")
     * @return Response
     */
    public function index(EventRepository $repo, Request $request, Widgets $widgets)
    {
        $redirect = new EventRedirect;
        $formRedirect = $this->createForm(EventRedirectType::class,$redirect);
        $formRedirect->handleRequest($request);

        /*
        $search = new EventSearch;
        dd($search);
        $form = $this->createForm(EventSearchType::class,$search);
        $form->handleRequest($request);
        */

        $cache = new FilesystemAdapter();

        $menu['day'] = $this->getCountsByDayOfTheWeek($cache);
        $menu['rubric'] = $this->getRubrics($cache);
        $menu['category'] = $this->getCategories($cache);
        $menu['reduction'] = $this->getReductions($cache);
        //dd($this->getReductions($cache));
        //$menu['form'] = $form->createView();
        $menu['form'] = [];
        $menu['collapse'] = [];
        //$menu['collapse'] = ['MENU_RUBRIC','MENU_CATEGORY','MENU_REDUCTION','MENU_SEARCH'];



        if ($this->subdomain){
            $nameOfCacheIndex = 'index_'.$this->subdomain;
        } else {
            $nameOfCacheIndex = 'index_all';
        }

        //$cache->delete($nameOfCacheIndex);

        $tabFinal = $cache->get($nameOfCacheIndex, function (ItemInterface $item) use ($repo, $menu) {

            $today = new DateTime();
            $tomorrow = new DateTime('tomorrow');

            if($today->format('H') == '23'){
                $item->expiresAt($tomorrow->setTime(0,0));
            } else {
                $item->expiresAfter(3600);
            }

            //$events = $this->separatorByTime($repo->findAllOfThisWeek($this->subdomain));
            $events = $repo->findAllOfThisWeek($this->subdomain);

            $tab['title'] = ["Aujourd'hui", "Demain", "Après-demain", "Dans 3 jours", "Dans 4 jours", "Dans 5 jours", "Dans 6 jours", "Dans 7 jours"];
            $tab['day'] = ["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"];
            $tab['month'] = ["décembre","janvier","février","mars","avril","mai","juin","juillet","août","septembre","octobre","novembre","décembre"];
            $tab['slug'] = ["aujourd-hui", "demain", "apres-demain", "dans-3-jours", "dans-4-jours", "dans-5-jours", "dans-6-jours", "dans-7-jours"];

            if (count($events) > 20){

                // Création du tableau vierge
                for($i = 0; $i < 7; $i++){

                    $dateOfDay = $menu['day'][$i]['date'];
                    $datetime = new DateTime();
                    $newDate = $datetime->createFromFormat('Y-m-d H:i', $dateOfDay." 00:00");

                    $textDate = $tab['day'][$newDate->format("w")] ." ". $newDate->format("j") ." ". $tab['month'][$newDate->format("n")] ." ". $newDate->format("Y");

                    if ($dateOfDay == $today->format('Y-m-d')){$anchor = "aujourd-hui";}
                    elseif ($dateOfDay == $tomorrow->format('Y-m-d')){$anchor = "demain";}
                    else {$anchor = $tab['day'][$newDate->format('N')];}

                    $tabEvent[$dateOfDay] = [
                        'title'     => $tab['title'][$i],
                        'date'      => $textDate,
                        'anchor'    => $anchor,
                        'link'      => $tab['slug'][$i],
                        'events'    => [
                            'primary'   => null,
                            'secondary' => null
                        ]
                    ];
                }
                //dd($tabEvent);

                foreach($events as $event){
                    //$event->setDateText($this->dateSentence->getText($event));

                    foreach($tabEvent as $day => $data){
                        if($event->getDateStart()->format("Y-m-d") == $day or $event->getDateEnd()->format("Y-m-d") == $day){
                            $tabEvent[$day]['events']['primary'][] = $event;
                        }
                        if($event->getDateStart()->format("Y-m-d") < $day and $event->getDateEnd()->format("Y-m-d") > $day){
                            $tabEvent[$day]['events']['secondary'][] = $event ;
                        }
                    }
                }

                $tabEvent[$day]['events']['primary'] = $this->separator($tabEvent[$day]['events']['secondary']);
                $tabEvent[$day]['events']['secondary'] = $this->separator($tabEvent[$day]['events']['secondary']);
            } else {

                $events = $repo->findAllOfThisMonth($this->subdomain);

                $now = new DateTime();
                $week = $now->modify('+1 week');

                $now = new DateTime();
                $month = $now->modify('+1 month');

                $tabEvent['today']['title'] = $tab['day'][$today->format("w")] ." ". $today->format("j") ." ". $tab['month'][$today->format("n")] ." ". $today->format("Y");
                $tabEvent['today']['events'] = null;
                $tabEvent['week']['events'] = null;
                $tabEvent['month']['events'] = null;

                foreach($events as $event){
                    //$event->setDateText($this->dateSentence->getText($event));
                    if (
                        $event->getDateStart()->format("Y-m-d") == $today->format("Y-m-d") or
                        $event->getDateEnd()->format("Y-m-d") == $today->format("Y-m-d"))
                    {
                        $tabEvent['today']['events'][] = $event;
                    }
                    elseif (
                        $event->getDateEnd()->format("Y-m-d") < $week->format("Y-m-d")
                    ){
                        $tabEvent['week']['events'][] = $event;
                    }
                    elseif (
                        $event->getDateEnd()->format("Y-m-d") < $month->format("Y-m-d")
                    ){
                        $tabEvent['month']['events'][] = $event;
                    }
                }
            }

            //dd($tabEvent);
            return $tabEvent;
        });

        //dd($this->website);

        // Si sous-domaine détecté
        if ($this->subdomain){

            // Si un slogan est enregistré
            if ($this->website['slogan']){
                $title['top'] = $this->website['slogan'];
            }
            /*
            elseif ($this->website['city']['resident']){
                $title['top'] = "Agenda culturel de la région ". $this->website['city']['resident']['singular']['feminine'];
            }
            */
            else {
                $title['top'] = "Agenda culturel et participatif";
            }

            /*
            if ($this->website['city']['resident']){
                $title['secondary'] = "à " . $this->website['city']['name'] . " et alentours";
            } else {
                $title['secondary'] = null;
            }
            */
            $title['secondary'] = null;
        } else {
            $title['top'] = "Agenda culturel et participatif";
        }

        //dd($menu);

        $widget = $widgets->getWidgets($cache,$request,date('Y-m-d'));

        $formHeader = $formRedirect->createView();


        //dd($formHeader);

        //dd($tabFinal);
        return $this->render('event/index.html.twig', [
            'page'                  => 'event',
            'tabevents'             => $tabFinal,
            //'map'                 => $map,
            //'nextEvents'          => $nextEvents,
            //'nextEventsSecondary' => $nextEventsSecondary,
            'website'               => $this->website,
            'daysOfWeek'            => $this->getDaysOfTheWeek(),
            'menu'                  => $menu,
            'title'                 => $title,
            'widget'                => $widget,
            'searchPrimary'         => $formHeader
        ]);
    }


    /**
     * Permet de valider un évènement
     *
     * @Route("/evenement/{slug}/valider-cet-evenement", name="event_validation")
     * @Security("is_granted('ROLE_USER') and (user.getIsVolunteer() or is_granted('ROLE_ADMIN'))", message="Vous n'avez pas le droit de valider cet évènement")
     */
    public function validate(Event $event, Sender $sender, EntityManagerInterface $manager)
    {
        // si il y a un auteur ou un email anonyme
        /*
        if ($event->getAuthor()){
            $bodyMail = $sender->createBodyMail('emails/event-published-by-user.html.twig', ['event' => $event]);
            $sender->sendMessage('bonjour@curieux.net', $event->getAuthor()->getEmail(), 'Votre évènement a été publié', $bodyMail);
        }
        elseif ($event->getMailAnonymous()){
            $bodyMail = $sender->createBodyMail('emails/event-published-by-anonymous.twig', ['event' => $event]);
            $sender->sendMessage('bonjour@curieux.net', $event->getMailAnonymous(), 'Votre évènement a été publié', $bodyMail);
        }
        */

        $event  ->setIsValidated(true)
                ->setVerifiedBy($this->getUser())
                ->setVerifiedAt(new DateTime())
        ;

        $manager->persist($event);
        $manager->flush();

        $this->addFlash(
            'success',
            "L'évènement <strong>{$event->getTitle()}</strong> a bien été validé !"
        );

        return $this->redirectToRoute('events_validator');
    }

    /**
     * Permet de signaler un évènement
     *
     * @Route("/evenement/{slug}/signaler-cet-evenement/{error}", name="event_signalisation")
     * @Security("is_granted('ROLE_USER') and (user.getIsVolunteer() or is_granted('ROLE_ADMIN'))", message="Vous n'avez pas le droit de signaler cet évènement")
     */
    public function signalisate(Event $event, EntityManagerInterface $manager, $error): Response
    {
        if ($error == "spam"){$event->setIsSpam(true);}
        elseif ($error == "doublon"){$event->setIsDuplicated(true);}
        elseif ($error == "erreur"){$event->setIsMistake(true);}

        $event->setIsValidated(false);
        $manager->persist($event);
        $manager->flush();

        $this->addFlash(
            'success',
            "L'évènement <strong>{$event->getTitle()}</strong> a bien été signalé comme {$error} !"
        );

        return $this->redirectToRoute('events_validator');
    }

    /**
     * Permet de supprimer un évènement
     *
     * @Route("/evenement/{slug}/supprimer-cet-evenement", name="event_delete")
     * @Security("(request.getSession().get('token') and request.getSession().get('token') === event.getSecretAnonymous()) or (is_granted('ROLE_USER') and (user === event.getAuthor() or is_granted('ROLE_ADMIN')))", message="Vous n'avez pas le droit de modifier cet évènement")
     */
    public function delete(Event $event, EntityManagerInterface $manager, Request $request): Response
    {
        if ($this->isCsrfTokenValid($event->getSlug(), $request->request->get('token')))
        {
            $manager->remove($event);
            $manager->flush();

            $this->addFlash(
                'success',
                "L'évènement <strong>{$event->getTitle()}</strong> a bien été supprimé !"
            );
        }

        return $this->redirectToRoute('event_index');
    }

    /**
     * Permet de supprimer un commentaire
     *
     * @Route("/supprimer-un-commentaire/{id}", name="event_comment_delete")
     * @Security("is_granted('ROLE_USER') and (user === comment.getAuthor() or is_granted('ROLE_ADMIN'))", message="Vous n'avez pas le droit de supprimer ce commentaire")
     */
    public function deleteComment(Comment $comment, EntityManagerInterface $manager): Response
    {
        $manager->remove($comment);
        $manager->flush();

        $this->addFlash(
            'success',
            "Votre commentaire a bien été supprimé !"
        );

        if ($comment->getEvent()){
            return $this->redirectToRoute('event_show', [
                'slug' => $comment->getEvent()->getSlug()
            ]);
        } elseif ($comment->getPlace()){
            return $this->redirectToRoute('place_show', [
                'slug' => $comment->getPlace()->getSlug()
            ]);
        } elseif ($comment->getFestival()){
            return $this->redirectToRoute('festival_show', [
                'slug' => $comment->getFestival()->getSlug()
            ]);
        } elseif ($comment->getArtist()){
            return $this->redirectToRoute('artist_show', [
                'slug' => $comment->getArtist()->getSlug()
            ]);
        } elseif ($comment->getOrganizer()){
            return $this->redirectToRoute('organizer_show', [
                'slug' => $comment->getOrganizer()->getSlug()
            ]);
        } else {
            return $this->redirectToRoute('event_index');
        }

    }

    /**
     * Permet de réclamer les droits de modification d'un évènement
     *
     * @Security("is_granted('ROLE_USER')", message="Il faut se connecter pour demander à modifier cet évènement")
     * @Route("/evenement/{slug}/demande-d-acces", name="event_claim")
     */
    public function claim(Event $event, EntityManagerInterface $manager, $slug): Response
    {

        $claim = new Claim;
        $user = $this->getUser();

        $claim  ->setEvent($event)
                //->setDate(new \DateTime('now'))
                ->setOwner($user)
                //->setExplanation("Veuillez compléter votre profil pour que nous puissions vous identifier correctement")
        ;

        if (
            // Si aucune publication de lieu ou d'évènement
            //$this->getUser()->getPlaces()->count() == "0" &&
            //$this->getUser()->getEvents()->count() == "0" &&

            // Si aucune précision sur son profil, Ex: membre d'une association ou d'un groupe de musique


            $user->getIntroduction() == NULL &&
            $user->getPresentation() == NULL &&
            $user->getPlaceEditors()->count() == "0" &&
            $user->getOrganizerEditors()->count() == "0" &&
            $user->getArtistEditors()->count() == "0"
        ){
            $claim->setExplanation("Veuillez compléter votre profil pour que nous puissions vous identifier correctement");

            $this->addFlash(
                'warning',
                "Compléter votre profil pour que nous puissions vous identifier, et ainsi vous autoriser à modifier les informations de l'évènement '{$event->getTitle()}' !<br>
                <a class='btn btn-primary btn-block mt-3' href='{$this->generateUrl('account_profile')}' >Améliorer mes informations personnelles</a>"
            );

            //return $this->redirectToRoute('event_show', ['slug' => $slug ]);
        }

        $manager->persist($claim);
        $manager->flush();

        $this->addFlash(
            'success',
            "Votre réclamation pour modifier les informations de l'évènement '{$event->getTitle()}' a été pris en compte !<br>
            Nous répondrons à votre demande dans les plus brefs délais"
        );

        return $this->redirectToRoute('event_show', ['slug' => $slug ]);

    }

    /**
     * Affichage d'une carte de test
     *
     * @Route("/leaflet", name="event_map")
     */
    public function carte()
    {
        $markers[] = ["layer3", "texte 1", 51.7538, -1.25609, "passage souris 1"];
        $markers[] = ["layer3", "texte 2", 51.752, -1.2577, "passage souris 2"];

        return $this->render('event/map.html.twig', [
            'page'          => 'place',
            'markers'       => $markers,
            'website'       => $this->website
        ]);
    }

    public function separator($events)
    {
        if ($events){
            $lastTime = "";
            $lastDateStart = "";
            $lastDateEnd = "";
            foreach ($events as $event){

                if ($event->getDateStart()->format('Y-m-d') == date('Y-m-d')){
                    if (isset($lastTime) and $event->getTimeStart() == $lastTime){
                        $event->setSameDateTime(true);
                    }
                } else {
                    if (
                        isset($lastDateStart) and isset($lastDateEnd) and
                        $event->getDateStart() == $lastDateStart and
                        $event->getDateEnd() == $lastDateEnd
                    ){
                        $event->setSameDateTime(true);
                    }
                }

                $lastDateStart = $event->getDateStart();
                $lastDateEnd = $event->getDateEnd();
                $lastTime = $event->getTimeStart();
            }
            return $events;
        }
        else {
            return null;
        }

    }

    public function separatorByTime($events)
    {
        $lastTime = "";
        foreach ($events as $event){
            if ($event->getTimeStart() == $lastTime){
                $event->setSameDateTime(true);
            }
            $lastTime = $event->getTimeStart();
        }
        return $events;
    }

    public function separatorByDate($events)
    {
        $lastDateStart = "";
        $lastDateEnd = "";
        foreach ($events as $event){
            if (
                isset($lastDateStart) and isset($lastDateEnd) and
                $event->getDateStart() == $lastDateStart and
                $event->getDateEnd() == $lastDateEnd
            ){
                $event->setSameDateTime(true);
            }
            $lastDateStart = $event->getDateStart();
            $lastDateEnd = $event->getDateEnd();
        }

        return $events;
    }

    function getDaysOfTheWeek()
    {
        $week = ["dimanche","lundi","mardi","mercredi","jeudi","vendredi","samedi","dimanche","lundi","mardi","mercredi","jeudi","vendredi","samedi"];
        $dayWeek = date('w');
        return [$week[$dayWeek], $week[$dayWeek+1], $week[$dayWeek+2], $week[$dayWeek+3], $week[$dayWeek+4], $week[$dayWeek+5], $week[$dayWeek+6], $week[$dayWeek+7]];
    }

    public function getCountsByDayOfTheWeek($cache)
    {
        return $cache->get('menu_events_by_days_'.$this->subdomain, function(ItemInterface $item){

            $item->expiresAfter(60*61);

            $today = date('Y-m-d');
            $dateOfNextWeek = date('Y-m-d', strtotime('+7 days'));

            if ($this->subdomain)
            {
                $query = "
                    SELECT e.date_start, e.date_end
                    FROM event e
                    LEFT JOIN place p ON p.id = e.place_id
                    LEFT JOIN city c ON c.id = p.city_id OR c.id = e.city_id
                    LEFT JOIN borough b ON b.id = c.borough_id
                    LEFT JOIN subdomain_borough sb ON sb.borough_id = c.borough_id
                    LEFT JOIN subdomain s ON s.id = sb.subdomain_id
                    WHERE s.slug = '".$this->subdomain."'
                    AND e.is_validated is true
                    AND e.date_start <= '$dateOfNextWeek'
                    AND e.date_end >= '$today' ";
                /*
                $query = "
                    SELECT *
                    FROM event e, place p, city c, borough b
                    WHERE p.id = e.place_id
                    AND (c.id = p.city_id OR c.id = e.city_id)
                    AND b.id = c.borough_id
                    AND b.slug = '".$this->subdomain."'
                    AND date_start <= '$dateOfNextWeek'
                    AND date_end >= '$today'
                    ORDER BY date_start";
                */
            }
            else
            {
                $query = "SELECT e.date_start, e.date_end FROM event e where e.date_start <= '$dateOfNextWeek' AND  e.date_end >= '$today' AND e.is_validated is true ";
            }

            $query.= "ORDER BY date_start";

            $em = $this->doctrine->getManager();
            $statement = $em->getConnection()->prepare($query);
            $events = $statement->execute()->fetchAll();

            $week = ["dimanche","lundi","mardi","mercredi","jeudi","vendredi","samedi","dimanche","lundi","mardi","mercredi","jeudi","vendredi","samedi"];
            $dayWeek = date('w');

            $day[0] = array('name'=>$week[$dayWeek],'date'=>date('Y-m-d', strtotime('+0 days')),'count'=>0);
            $day[1] = array('name'=>$week[$dayWeek+1],'date'=>date('Y-m-d', strtotime('+1 days')),'count'=>0);
            $day[2] = array('name'=>$week[$dayWeek+2],'date'=>date('Y-m-d', strtotime('+2 days')),'count'=>0);
            $day[3] = array('name'=>$week[$dayWeek+3],'date'=>date('Y-m-d', strtotime('+3 days')),'count'=>0);
            $day[4] = array('name'=>$week[$dayWeek+4],'date'=>date('Y-m-d', strtotime('+4 days')),'count'=>0);
            $day[5] = array('name'=>$week[$dayWeek+5],'date'=>date('Y-m-d', strtotime('+5 days')),'count'=>0);
            $day[6] = array('name'=>$week[$dayWeek+6],'date'=>date('Y-m-d', strtotime('+6 days')),'count'=>0);
            $day[7] = array('name'=>$week[$dayWeek+7],'date'=>date('Y-m-d', strtotime('+7 days')),'count'=>0);

            foreach($events as $key => $event) {
                for ($i=0; $i < count($day) ; $i++) {
                    if($event["date_start"] <= $day[$i]["date"] && $event["date_end"] >= $day[$i]["date"] ){
                        $day[$i]["count"]++;
                    }
                }
            }
            return $day;
        });

    }

    public function getRubrics($cache)
    {
        return $cache->get('menu_events_by_rubrics_'.$this->subdomain, function(ItemInterface $item){

            $item->expiresAfter(60*59);

            $today = date('Y-m-d');

            if ($this->subdomain){

                $query =   "SELECT r.name, r.slug, r.color, COUNT(e.id) as compteur
                            FROM event e
                            LEFT JOIN place p ON p.id = e.place_id
                            LEFT JOIN city c ON c.id = p.city_id OR c.id = e.city_id
                            LEFT JOIN borough b ON b.id = c.borough_id
                            LEFT JOIN subdomain_borough sb ON sb.borough_id = c.borough_id
                            LEFT JOIN subdomain s ON s.id = sb.subdomain_id
                            LEFT JOIN rubric r ON r.id = e.rubric_id
                            WHERE s.slug = '".$this->subdomain."' ";
                /*
                $query =   "SELECT r.name, r.slug, COUNT(*) as compteur
                            FROM rubric r, borough b, city c, event e
                            LEFT JOIN place p ON p.id = e.place_id
                            WHERE (c.id = p.city_id OR c.id = e.city_id)
                            AND r.id = e.rubric_id
                            AND b.id = c.borough_id
                            AND b.slug = '".$this->subdomain."' ";
                */
            } else {

                $query =   "SELECT r.name, r.slug, r.color, COUNT(e.id) as compteur
                            FROM event e, rubric r
                            WHERE r.id = e.rubric_id ";
            }

            $query.=   "AND e.date_end >= '$today'
                        GROUP BY r.name
                        ORDER BY compteur DESC";

            $em = $this->doctrine->getManager();
            $statement = $em->getConnection()->prepare($query);
            $rubrics = $statement->execute()->fetchAll();

            $tabfinal = [];
            foreach($rubrics as $rubric){
                if($rubric['slug'] == "autre"){
                    $rubricAutre = $rubric;
                } else {
                    $tabfinal[] = $rubric;
                }
            }
            if (isset($rubricAutre)){
                $tabfinal[] = $rubricAutre;
            }

            return $tabfinal;

        });
    }

    public function getCategories($cache)
    {
        return $cache->get('menu_events_by_categorys_'.$this->subdomain, function(ItemInterface $item){

            $item->expiresAfter(60*60);

            $today = date('Y-m-d');

            if ($this->subdomain){

                $query =   "SELECT ca.name, ca.slug, COUNT(e.id) as compteur
                            FROM event e
                            LEFT JOIN place p ON p.id = e.place_id
                            LEFT JOIN city c ON c.id = p.city_id OR c.id = e.city_id
                            LEFT JOIN borough b ON b.id = c.borough_id
                            LEFT JOIN subdomain_borough sb ON sb.borough_id = c.borough_id
                            LEFT JOIN subdomain s ON s.id = sb.subdomain_id
                            LEFT JOIN event_category ec ON e.id = ec.event_id
                            LEFT JOIN category ca ON ca.id = ec.category_id
                            WHERE s.slug = '".$this->subdomain."' ";

                /*
                $query =   "SELECT ca.name, ca.slug, COUNT(*) as compteur
                            FROM event_category ec, category ca, city c, borough b, event e
                            LEFT JOIN place p ON p.id = e.place_id
                            WHERE e.id = ec.event_id
                            AND ca.id = ec.category_id
                            AND (c.id = p.city_id OR c.id = e.city_id)
                            AND b.id = c.borough_id
                            AND b.slug = '".$this->subdomain."' ";
                */
            } else {

                $query =   "SELECT ca.name, ca.slug, COUNT(e.id) as compteur
                            FROM event e, event_category ec, category ca
                            WHERE e.id = ec.event_id
                            AND ca.id = ec.category_id ";
            }

            $query.=   "AND e.date_end >= '$today'
                        GROUP BY ca.name
                        ORDER BY compteur DESC
                        limit 20";

            $em = $this->doctrine->getManager();
            $statement = $em->getConnection()->prepare($query);
            $categories = $statement->execute()->fetchAll();

            $tabfinal = [];
            foreach($categories as $category){
                if($category['slug'] !== NULL){
                    $tabfinal[] = $category;
                }
            }

            return $tabfinal;
        });
    }

    public function getReductions($cache)
    {
        return $cache->get('menu_events_by_reductions_'.$this->subdomain, function(ItemInterface $item){

            $item->expiresAfter(60*60);

            $today = date('Y-m-d');

            if ($this->subdomain){

                $query =   "SELECT r.title, r.slug, COUNT(e.id) as compteur
                            FROM event e
                            LEFT JOIN place p ON p.id = e.place_id
                            LEFT JOIN city c ON c.id = p.city_id OR c.id = e.city_id
                            LEFT JOIN borough b ON b.id = c.borough_id
                            LEFT JOIN subdomain_borough sb ON sb.borough_id = c.borough_id
                            LEFT JOIN subdomain s ON s.id = sb.subdomain_id
                            LEFT JOIN prices pr ON pr.event_id = e.id
                            LEFT JOIN prices_reduction rp ON rp.prices_id = pr.id
                            LEFT JOIN reduction r ON r.id = rp.reduction_id
                            WHERE s.slug = '".$this->subdomain."' ";
                /*
                $query =   "SELECT r.title, r.slug, COUNT(*) as compteur
                            FROM event e, prices pr, reduction r, place p, city c, borough b
                            WHERE e.id = pr.event_id
                            AND r.id = pr.reduction_id
                            AND p.id = e.place_id
                            AND (c.id = p.city_id OR c.id = e.city_id)
                            AND b.id = c.borough_id
                            AND b.slug = '".$this->subdomain."' ";
                */

            } else {
                /*
                $query =   "SELECT r.title, r.slug, COUNT(e.id) as compteur
                            FROM event e, prices pr, reduction r
                            WHERE e.id = pr.event_id
                            AND r.id = pr.reduction_id ";
                */

                $query =   "SELECT r.title, r.slug, COUNT(e.id) as compteur
                            FROM event e
                            LEFT JOIN prices pr ON pr.event_id = e.id
                            LEFT JOIN prices_reduction rp ON rp.prices_id = pr.id
                            LEFT JOIN reduction r ON r.id = rp.reduction_id ";
            }

            $query.=   "AND e.date_end >= '$today'
                        AND r.slug is not null
                        GROUP BY r.title
                        ORDER BY compteur DESC
                        limit 10";

            $em = $this->doctrine->getManager();
            $statement = $em->getConnection()->prepare($query);
            $reductions = $statement->execute()->fetchAll();

            return $reductions;
        });
    }

}
