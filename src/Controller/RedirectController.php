<?php

namespace App\Controller;

use App\Service\Website;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class RedirectController extends AbstractController
{
    
    public function __construct(Website $website, ManagerRegistry $doctrine)
    {
        //$this->website = $website->getArray();
        $this->subdomain = $website->getArray()['address']['subdomain'];
        $this->doctrine = $doctrine;
    }

    /**
     * @Route("/ajouter-un-lieu")
     */
    public function ajouter_un_lieu(): Response
    {
        return $this->redirectToRoute("place_create");
    }

    /**
     * @Route("/annoncer-un-evenement")
     */
    public function annoncer_un_evenement(): Response
    {
        return $this->redirectToRoute("event_create");
    }

    
    /**
     * Redirection evenement ancien site, à supprimer en 2023
     * 
     * @Route("/agenda/sortie")
     */
    public function event_show_old(EventRepository $repo, Request $request)
    {
        if ($this->subdomain and $request->query->getInt('row') > 0){
            $event = $repo->findOneBy(['oldserial' => $this->subdomain ." ". $request->query->getInt('row')]);
            //dd($event);
        }

        if ($event){
            return $this->redirectToRoute('event_show', ['slug' => $event->getSlug() ]);
        } else {
            return $this->redirectToRoute('event_index');
        }
    }

    
    /**
     * Redirection annuaire ancien site, à supprimer en 2023
     * 
     * @Route("/agenda/programmation")
     * @Route("/agenda/carte/lieux_fiche.php")
     */
    public function place_show_old(Request $request)
    {
        if ($this->subdomain and $request->query->getInt('id_lieu') > 0){
            $place = $this->doctrine->getRepository(Place::class)->findOneBy(['Oldurl' => $this->subdomain .".curieux.net/agenda/programmation?id_lieu=". $request->query->getInt('id_lieu')]);
            //dd($place);
        }

        if ($place){
            return $this->redirectToRoute('place_show', ['slug' => $place->getSlug() ]);
        } else {
            return $this->redirectToRoute('place_index');
        }
    }

    /**
     * Redirection reduction ancien site, à supprimer en 2023
     * 
     * @Route("/agenda/selection")
     */
    public function reduction_show_old(Request $request)
    {
        $tabRubric = ['concert' => 'concert', 'soiree' => 'soiree', 'theatre' => 'spectacle', 'exposition' => 'exposition', 'cinema' => 'cinema', 'manifestation' => 'action-citoyenne', 'reunion-publique' => 'action-citoyenne', 'musique-classique' => 'concert', 'autre' => 'autre'];
        $tabCategory = ['danse'];
        if (isset($tabRubric[$request->query->get('nom')])){
            return $this->redirectToRoute('events_by_rubric', ['slug' => $tabRubric[$request->query->get('nom')] ]);
        }
        elseif (in_array($request->query->get('nom'),$tabCategory)){
            return $this->redirectToRoute('events_by_category', ['slug' => $request->query->get('nom') ]);
        }
        elseif ($request->query->get('genre') == "3" and $request->query->get('nom') ){
            return $this->redirectToRoute('search_by_filter', ['lieu' => '__'. $request->query->get('nom') ]); 
        }  
        elseif ($request->query->get('genre') == "reduction" and $request->query->get('nom') ){
            
            if ($request->query->get('nom') == "tarif-chomeur"){
                $nom = "tarif-demandeur-d-emploi";
            } else {
                $nom = $request->query->get('nom');
            }
            return $this->redirectToRoute('events_by_reduction', ['slug' => $nom ]);
        }    
        else {
            return $this->redirectToRoute('event_index');
        }
    }

    /**
     * Redirection image lieu ancien site, à supprimer en 2023
     * 
     * @Route("/agenda/images/lieux/_{id}-logo.{extension}")
     */
    public function imagePlaceRedirect($id, $extension)
    {
        if ($this->subdomain){
            $url = "https://img.curieux.net/p/" .$this->subdomain. "/_" .$id. "-logo." .$extension;
            return $this->redirect($url);            } 
        else {
            return $this->redirectToRoute('place_index');
        }
    }

    /**
     * Redirection image event ancien site, à supprimer en 2023
     * 
     * @Route("/agenda/images/visuel/{id}/{name}.{extension}")
     */
    public function imageEventOld($id, $name, $extension)
    {
        if ($this->subdomain){
            $url = "https://img.curieux.net/e/" .$this->subdomain. "/" .$id. "/" .$name. "." .$extension;
            return $this->redirect($url);            } 
        else {
            return $this->redirectToRoute('event_index');
        }
    }

    /**
     * Redirection annuaire ancien site, à supprimer en 2023
     * 
     * @Route("/agenda/carte/lieux_annuaire")
     */
    public function annuaireOld()
    {
        return $this->redirectToRoute('place_index');
    }
}
