<?php

namespace App\Controller;

use App\Entity\Artist;
use App\Service\Website;
use App\Form\Artist1Type;
use App\Repository\ArtistRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/test")
 */
class TestController extends AbstractController
{
    public function __construct(Website $website)
    {
        $this->website = $website->getArray();
        $this->subdomain = $website->getArray()['address']['subdomain'];
    }

    /**
     * @Route("/", name="test_index", methods={"GET"})
     */
    public function index(ArtistRepository $artistRepository): Response
    {
        return $this->render('test/index.html.twig', [
            'artists' => $artistRepository->findAll(),
            'website'   => $this->website,
        ]);
    }

    /**
     * @Route("/new", name="test_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $artist = new Artist();
        $form = $this->createForm(Artist1Type::class, $artist);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($artist);
            $entityManager->flush();

            return $this->redirectToRoute('test_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('test/new.html.twig', [
            'artist' => $artist,
            'form' => $form,
            'website'   => $this->website,
        ]);
    }

    /**
     * @Route("/{id}", name="test_show", methods={"GET"})
     */
    public function show(Artist $artist): Response
    {
        return $this->render('test/show.html.twig', [
            'artist' => $artist,
            'website'   => $this->website,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="test_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Artist $artist, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(Artist1Type::class, $artist);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('test_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('test/edit.html.twig', [
            'artist' => $artist,
            'form' => $form,
            'website'   => $this->website,
        ]);
    }

    /**
     * @Route("/{id}", name="test_delete", methods={"POST"})
     */
    public function delete(Request $request, Artist $artist, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$artist->getId(), $request->request->get('_token'))) {
            $entityManager->remove($artist);
            $entityManager->flush();
        }

        return $this->redirectToRoute('test_index', [], Response::HTTP_SEE_OTHER);
    }
}
