<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Service\Website;
use App\Form\CommentSearchType;
use App\Repository\CommentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CommentController extends AbstractController
{
    public function __construct(Website $website)
    {
        $this->website = $website->getArray();
        $this->subdomain = $website->getArray()['address']['subdomain'];
    }

    /**
     * Suppression des commentaires
     * 
     * @Route("/agenda/validation/signalement/supprimer", name="alerts_delete")
     * @Security("is_granted('ROLE_USER') and (user.getIsVolunteer() or is_granted('ROLE_ADMIN'))", message="Vous n'avez pas le droit d'accéder à cette page")
     * @return Response
     */
    public function alerts_delete(CommentRepository $repo, EntityManagerInterface $manager, PaginatorInterface $paginator, Request $request)
    { 
        if($request->get('anonyme') or $request->get('texte') ){
            $query = $repo->findByFilter($this->website['address']['subdomain'], true, $request->get('anonyme'), $request->get('texte'));
        } 
        else {
            return $this->redirectToRoute('alerts_validator');
        }

        $comments = $query->getResult();

        foreach($comments as $comment){
            $manager->remove($comment);
        }
        
        $manager->flush();
        
        return $this->redirectToRoute('alerts_validator');
    }

}
