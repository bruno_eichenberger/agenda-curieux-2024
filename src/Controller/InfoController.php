<?php

namespace App\Controller;

use App\Entity\Artist;
use App\Form\ArtistType;
use App\Service\Website;
use App\Entity\Organizer;
use App\Form\OrganizerType;
use App\Entity\ArtistSearch;
use App\Form\ArtistSearchType;
use App\Entity\OrganizerSearch;
use App\Service\UploaderHelper;
use App\Form\OrganizerSearchType;
use App\Repository\EventRepository;
use App\Repository\ArtistRepository;
use App\Repository\OrganizerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class InfoController extends AbstractController
{
    public function __construct(Website $website, ManagerRegistry $doctrine)
    {
        $this->website = $website->getArray();
        $this->subdomain = $website->getArray()['address']['subdomain'];
        $this->doctrine = $doctrine;
    }

    /**
     * @Route("/info", name="info_index")
     */
    public function index(ArtistRepository $repoArt, OrganizerRepository $repoOrg, PaginatorInterface $paginator, Request $request)
    {
        /*
        $artists = $paginator->paginate(
            $repoArt->findAllVisibleQuery($this->subdomain),
            $request->query->getInt('page', 1),
            5 
        );
        
        $organizers = $paginator->paginate(
            $repoOrg->findAllVisibleQuery($this->subdomain),
            $request->query->getInt('page', 1),
            5
        );
        */

        $countOfAllArtists = $repoArt->findAllArtists($this->subdomain)->execute();
        $artists = $repoArt->findTopArtist($this->subdomain)->execute();

        $countOfAllOrganizers = $repoOrg->findAllOrganizers($this->subdomain)->execute();
        $organizers = $repoOrg->findTopOrganizer($this->subdomain)->execute();

        $search = new ArtistSearch;
        $form = $this->createForm(ArtistSearchType::class,$search);
        $form->handleRequest($request);

        return $this->render('info/index.html.twig', [
            'page'                  => 'info',
            'subpage'               => NULL,
            'countOfAllArtists'     => $countOfAllArtists,
            'countOfAllOrganizers'  => $countOfAllOrganizers,
            'artists'               => $artists,
            'organizers'            => $organizers,
            'website'               => $this->website,
            'menu_artistkinds'      => $this->getCountsByArtistkind(),
            'menu_organizerkind'    => $this->getCountsByOrganizerkind(),
            'menu_cities'           => $this->getCountsByCity(),
            'form'                  => $form->createView(),
            'directory'             => NULL,
            'menu'                  => ['MENU_ARTISTKIND','MENU_ORGANIZERKIND','MENU_SEARCH']
        ]);
    }

    /**
     * @Route("/info/artiste", name="artist_index")
     */
    public function index_artist(ArtistRepository $repo, PaginatorInterface $paginator, Request $request)
    {
        $search = new ArtistSearch;
        $form = $this->createForm(ArtistSearchType::class,$search);
        $form->handleRequest($request);

        $artists = $paginator->paginate($repo->findAllSearchByFilter($search,$this->subdomain), $request->query->getInt('page', 1), 5 );

        return $this->render('info/artist/index.html.twig', [
            'page'              => 'info',
            'subpage'           => 'artist',
            'artists'           => $artists,
            'website'           => $this->website,
            'menu_artistkinds'  => $this->getCountsByArtistkind(),
            'menu_organizerkind'=> $this->getCountsByOrganizerkind(),
            'menu_cities'       => $this->getCountsByCity(),
            'form'              => $form->createView(),
            'directory'         => NULL,
            'menu'              => ['MENU_ARTISTKIND','MENU_SEARCH']
        ]);
    }

    /**
     * @Route("/info/artiste/specialite-{slug}", name="artists_by_artistkind")
     */
    public function search_by_artistkind(ArtistRepository $repo, PaginatorInterface $paginator, Request $request, $slug)
    {
        $artists = $paginator->paginate($repo->findByArtistkind($slug, $this->subdomain),$request->query->getInt('page', 1), 5 );

        $search = new ArtistSearch;
        $form = $this->createForm(ArtistSearchType::class,$search);
        $form->handleRequest($request);

        return $this->render('info/artist/index.html.twig', [
            'page'              => 'info',
            'subpage'           => 'artist',
            'artists'           => $artists,
            'website'           => $this->website,
            'menu_artistkinds'  => $this->getCountsByArtistkind(),
            'menu_organizerkind'=> $this->getCountsByOrganizerkind(),
            'menu_cities'       => $this->getCountsByCity(),
            'form'              => $form->createView(),
            'directory'         => $slug,
            'menu'              => ['MENU_ARTISTKIND','MENU_SEARCH']
        ]);
    }

    /**
     * Permet d'afficher les informations d'un artiste (description + évènements à venir)
     * 
     * @Route("/info/artiste/{slug}", name="artist_show")
     * 
     * @return Response
     */
    public function show_artist(Artist $artist, EventRepository $repo, PaginatorInterface $paginator, Request $request, $slug){
        
        $events = $paginator->paginate($repo->findByArtist($slug),$request->query->getInt('page', 1), 10 );

        return $this->render('info/artist/show.html.twig', [
            'page'      => 'info',
            'artist'    => $artist,
            'events'    => $events,
            'website'   => $this->website,
            'directory' => NULL
        ]);
    }
    
    /**
     * Créer un artiste
     * 
     * @Route("/ajouter-un-artiste", name="artist_create")
     * @return Response
     */
    public function create_artist(Request $request, EntityManagerInterface $manager, UploaderHelper $uploaderHelper){
        
        $artist = new Artist();
        $form = $this->createForm(ArtistType::class, $artist);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            /** @var UploadedFile $uploadedFile */
            $uploadedFile = $form['coverImageFile']->getData();

            if ($uploadedFile){
                $newFilename = $uploaderHelper->uploadArtistImage($uploadedFile);
                $artist->setCoverImage($newFilename);
            }

            if ($this->getUser()){
                $artist->setAuthor($this->getUser());
            }

            $manager->persist($artist);
            $manager->flush();

            $this->addFlash(
                'success',
                "L'artiste '<strong>{$artist->getTitle()}</strong>' a bien été enregistré !"
            );

            return $this->redirectToRoute('artist_show', [
                'slug' => $artist->getSlug()
            ]);
        }

        return $this->render('info/artist/new.html.twig', [
            
            'page'          => 'artist',
            'form'          => $form->createView(),
            'website'       => $this->website,
        ]);
    }

    /**
     * Permet d'éditer un artiste
     * 
     * @Route("/info/artiste/{slug}/modification", name="artist_edit")
     * @Security("is_granted('ROLE_USER') and artist.getAuthor(user)", message="Vous n'avez pas le droit de modifier ce artiste")
     * @return Response
     */
    public function edit_artist(Artist $artist, Request $request, EntityManagerInterface $manager){

        $form = $this->createForm(ArtistType::class, $artist);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            /*
            foreach($artist->getImages() as $image){
                $image->setArtist($artist);
                $manager->persist($image);
            }
            */
            // inutile car Injection dépendance
            //$manager = $this->doctrine->getManager();

            $manager->persist($artist);
            $manager->flush();

            $this->addFlash(
                'success',
                "Les modifications ont été effectué !"
            );

            return $this->redirectToRoute('artist_show', [
                'slug' => $artist->getSlug()
            ]);
        }

        return $this->render('info/artist/edit.html.twig', [
            'page'          => 'info',
            'website'       => $this->website,
            'form'          => $form->createView(),
            'artist'        => $artist
        ]);
    }

    /**
     * Permet de supprimer un artiste
     * 
     * @Route("/info/artiste/{slug}/delete", name="artist_delete")
     * @Security("is_granted('ROLE_USER') and artist.getAuthor(user)", message="Vous n'avez pas le droit de supprimer cet artiste")
     * 
     * @param Artist $artist
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delete_artist(Artist $artist, EntityManagerInterface $manager){
        $manager->remove($artist);
        $manager->flush();

        $this->addFlash(
            'success',
            "L'artiste '<strong>{$artist->getTitle()}</strong>' a bien été supprimé !"
        );

        return $this->redirectToRoute('artist_index');
    }

    /**
     * @Route("/info/organisateur", name="organizer_index")
     */
    public function index_organizer(OrganizerRepository $repo, PaginatorInterface $paginator, Request $request)
    {
        $search = new OrganizerSearch;
        $form = $this->createForm(OrganizerSearchType::class,$search);
        $form->handleRequest($request);

        $organizers = $paginator->paginate($repo->findAllOrganizers($this->subdomain), $request->query->getInt('page', 1), 5 );

        return $this->render('info/organizer/index.html.twig', [
            'page'              => 'info',
            'subpage'           => 'organizer',
            'organizers'        => $organizers,
            'website'           => $this->website,
            'menu_artistkinds'  => $this->getCountsByArtistkind(),
            'menu_organizerkind'=> $this->getCountsByOrganizerkind(),
            'menu_cities'       => $this->getCountsByCity(),
            'form'              => $form->createView(),
            'directory'         => NULL,
            'menu'              => ['MENU_ORGANIZERKIND','MENU_SEARCH']
        ]);
    }
    
    /**
     * @Route("/info/organisateur/genre-{slug}", name="organizers_by_organizerkind")
     */
    public function search_by_organizerkind(OrganizerRepository $repo, PaginatorInterface $paginator, Request $request, $slug)
    {
        $organizers = $paginator->paginate($repo->findByOrganizerkind($slug, $this->subdomain),$request->query->getInt('page', 1), 5 );

        $search = new OrganizerSearch;
        $form = $this->createForm(OrganizerSearchType::class,$search);
        $form->handleRequest($request);

        return $this->render('info/organizer/index.html.twig', [
            'page'              => 'info',
            'subpage'           => 'organizer',
            'organizers'        => $organizers,
            'website'           => $this->website,
            'menu_artistkinds'  => $this->getCountsByArtistkind(),
            'menu_organizerkind'=> $this->getCountsByOrganizerkind(),
            'menu_cities'       => $this->getCountsByCity(),
            'form'              => $form->createView(),
            'directory'         => $slug,
            'menu'              => ['MENU_ORGANIZERKIND','MENU_SEARCH']
        ]);
    }

    /**
     * Permet d'afficher les informations d'un organisateur (description + évènements à venir)
     * 
     * @Route("/info/organisateur/{slug}", name="organizer_show")
     * 
     * @return Response
     */
    public function show_organizer(Organizer $organizer, EventRepository $repo, PaginatorInterface $paginator, Request $request, $slug)
    {
        $events = $paginator->paginate($repo->findByOrganizer($slug),$request->query->getInt('page', 1), 10 );

        return $this->render('info/organizer/show.html.twig', [
            'page'      => 'info',
            'organizer' => $organizer,
            'events'    => $events,
            'website'   => $this->website,
            'directory' => NULL
        ]);
    }
    
    
    /**
     * Créer un organisateur
     * 
     * @Route("/ajouter-un-organisateur", name="organizer_create")
     * @return Response
     */
    public function create_organizer(Request $request, EntityManagerInterface $manager, UploaderHelper $uploaderHelper)
    {
        $organizer = new Organizer();
        $form = $this->createForm(OrganizerType::class, $organizer);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            /** @var UploadedFile $uploadedFile */
            $uploadedFile = $form['coverImageFile']->getData();

            if ($uploadedFile){
                $newFilename = $uploaderHelper->uploadOrganizerImage($uploadedFile);
                $organizer->setCoverImage($newFilename);
            }

            if ($this->getUser()){
                $organizer->setAuthor($this->getUser());
            }

            $manager->persist($organizer);
            $manager->flush();

            $this->addFlash(
                'success',
                "L'organisateur '<strong>{$organizer->getTitle()}</strong>' a bien été enregistré !"
            );

            return $this->redirectToRoute('organizer_show', [
                'slug' => $organizer->getSlug()
            ]);
        }

        return $this->render('info/organizer/new.html.twig', [
            
            'page'          => 'organizer',
            'form'          => $form->createView(),
            'website'       => $this->website,
        ]);
    }

    /**
     * Permet d'éditer un organisateur
     * 
     * @Route("/info/organisateur/{slug}/modification", name="organizer_edit")
     * @Security("is_granted('ROLE_USER') and organizer.getAuthor(user)", message="Vous n'avez pas le droit de modifier ce organisateur")
     * @return Response
     */
    public function edit_organizer(Organizer $organizer, Request $request, EntityManagerInterface $manager){

        $form = $this->createForm(OrganizerType::class, $organizer);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            /*
            foreach($organizer->getImages() as $image){
                $image->setOrganizer($organizer);
                $manager->persist($image);
            }
            */
            // inutile car Injection dépendance
            //$manager = $this->doctrine->getManager();

            $manager->persist($organizer);
            $manager->flush();

            $this->addFlash(
                'success',
                "Les modifications ont été effectué !"
            );

            return $this->redirectToRoute('organizer_show', [
                'slug' => $organizer->getSlug()
            ]);
        }

        return $this->render('info/organizer/edit.html.twig', [
            'page'          => 'info',
            'website'       => $this->website,
            'form'          => $form->createView(),
            'organizer'     => $organizer
        ]);
    }

    /**
     * Permet de supprimer un organisateur
     * 
     * @Route("/info/organisateur/{slug}/delete", name="organizer_delete")
     * @Security("is_granted('ROLE_USER') and organizer.getAuthor(user)", message="Vous n'avez pas le droit de supprimer cet organisateur")
     * 
     * @param Organizer $organizer
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delete_organizer(Organizer $organizer, EntityManagerInterface $manager){
        $manager->remove($organizer);
        $manager->flush();

        $this->addFlash(
            'success',
            "L'organisateur '<strong>{$organizer->getName()}</strong>' a bien été supprimé !"
        );

        return $this->redirectToRoute('organizer_index');
    }

    public function getCountsByArtistkind()
    {
        if ($this->subdomain)
        {
            $query =   "SELECT k.name, k.slug, COUNT(*) as compteur
                        FROM artistkind k, artist a, artist_event ae, event e, place p, city c, borough b
                        WHERE k.id = a.artistkind_id
                        AND a.id = ae.artist_id
                        AND e.id = ae.event_id
                        AND p.id = e.place_id
                        AND (c.id = p.city_id OR c.id = e.city_id)
                        AND b.id = c.borough_id
                        AND b.slug = '".$this->subdomain."'
                        GROUP BY k.name
                        ORDER BY compteur DESC";
        }
        else {
            $query =   "SELECT k.name, k.slug, COUNT(*) as compteur 
                        FROM artist a, artistkind k
                        WHERE a.artistkind_id = k.id
                        GROUP BY k.name
                        ORDER BY compteur DESC";
        }

        $em = $this->doctrine->getManager();
        $statement = $em->getConnection()->prepare($query);
        $statement->execute();
        
        return $statement->fetchAll();
    }

    public function getCountsByOrganizerkind()
    {
        if ($this->subdomain)
        {
            $query =   "SELECT k.name, k.slug, COUNT(e.id) as compteur
                        FROM organizerkind k, organizer o, artist_event ae, event e, place p, city c, borough b
                        WHERE k.id = o.organizerkind_id
                        AND o.id = e.organizer_id
                        AND p.id = e.place_id
                        AND (c.id = p.city_id OR c.id = e.city_id)
                        AND b.id = c.borough_id
                        AND b.slug = '".$this->subdomain."'
                        GROUP BY k.name
                        ORDER BY compteur DESC";
        }
        else {
            $query =   "SELECT k.name, k.slug, COUNT(*) as compteur 
                        FROM organizerkind k, organizer o
                        WHERE k.id = o.organizerkind_id
                        GROUP BY k.name
                        ORDER BY compteur DESC";
        }

        $em = $this->doctrine->getManager();
        $statement = $em->getConnection()->prepare($query);
        $statement->execute();
        
        return $statement->fetchAll();
    }

    public function getCountsByCity()
    {
        if ($this->subdomain){

            $query =   "SELECT c.name, c.slug, COUNT(*) as compteur
                        FROM festival f, event e, place p, city c, borough b
                        WHERE f.id = e.festival_id
                        AND p.id = e.place_id
                        AND (c.id = p.city_id OR c.id = e.city_id)
                        AND b.id = c.borough_id
                        AND b.slug = '".$this->subdomain."'
                        GROUP BY c.id
                        ORDER BY compteur DESC
                        LIMIT 10";
        } else {
            
            $query =   "SELECT c.name, c.slug, COUNT(*) as compteur
                        FROM festival f, event e, place p, city c
                        WHERE f.id = e.festival_id
                        AND p.id = e.place_id
                        AND c.id = p.city_id
                        GROUP BY c.id
                        ORDER BY compteur DESC
                        LIMIT 10";
        }

        $em = $this->doctrine->getManager();
        $statement = $em->getConnection()->prepare($query);
        $statement->execute();

        return $statement->fetchAll();
    }


}


