<?php

namespace App\Controller;

use App\Entity\User;
use App\Service\Website;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use League\OAuth2\Client\Provider\FacebookUser;
use Symfony\Component\Routing\Annotation\Route;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use KnpU\OAuth2ClientBundle\Client\Provider\FacebookClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class FacebookController extends AbstractController
{
    public function __construct(Website $website)
    {
        $this->website = $website->getArray();
        //$this->subdomain = $website->getArray()['address']['subdomain'];
    }

    /**
     * Link to this controller to start the "connect" process
     * @param ClientRegistry $clientRegistry
     *
     * @Route("/connect/facebook", name="connect_facebook_start")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function connectAction(ClientRegistry $clientRegistry)
    {
        return $clientRegistry
            ->getClient('facebook_main')
            ->redirect([
                'public_profile', 'email' // the scopes you want to access
            ])
        ;
    }

    /**
     * After going to Facebook, you're redirected back here
     * because this is the "redirect_route" you configured
     * in config/packages/knpu_oauth2_client.yaml
     *
     * @Route("/connect/facebook/check", name="connect_facebook_check")
     */
    public function connectCheckAction(Request $request, ClientRegistry $clientRegistry)
    {
        //dd($clientRegistry);
        //return $this->redirectToRoute('place_index');

        // ** if you want to *authenticate* the user, then
        // leave this method blank and create a Guard authenticator
        // (read below)

        /** @var FacebookClient $client */
        $client = $clientRegistry->getClient('facebook');


        try {
            // the exact class depends on which provider you're using          
            /** @var FacebookUser $fbUser */
            $fbUser = $client->fetchUser();

            //dd($fbUser);

            // 1) have they logged in with Facebook before? Easy!
            $existingUser = $manager->getRepository(User::class)->findOneBy(['facebookId' => $fbUser->getId()]);

            if ($existingUser) {
                $user = $existingUser;
            } else {
                // 2) do we have a matching user by email?
                if($fbUser->getEmail() !== null){
                    $user = $manager->getRepository(User::class)->findOneBy(['email' => $fbUser->getEmail()]);
                }

                if (!$user) {
                    $user = new User();

                    $user   ->setHash(uniqid())
                            ->setFacebookId($fbUser->getId())
                            ->setFirstName($fbUser->getFirstName())
                            ->setLastName($fbUser->getLastName())
                            ->setLevel(false);

                    if($fbUser->getEmail() !== null){
                        $user->setEmail($fbUser->getEmail());
                    }
                    
                    if($fbUser->getPictureUrl() !== null){
                        $user->setAvatar($fbUser->getPictureUrl());
                    }

                    $manager->persist($user);
                    $manager->flush();
                }
            }

            $error = $utils->getLastAuthenticationError();
            $username = $utils->getLastUsername();
    
            if ($error && $error->getMessage() != "Bad credentials."){
                $message = $error->getMessage();
            } else {
                $message = null;
            }

            $this->setUser($user);

            if ($this->getUser()){
                return $this->redirectToRoute('event_index');
            }

        } catch (IdentityProviderException $e) {
            // something went wrong!
            // probably you should return the reason to the user
            dd($e->getMessage());
        }

        return $this->render('account/login.html.twig', [
            'website'   => $this->website,
            'page'      => 'account',
            'hasError'  => $error !== null,
            'message'   => $message,
            'username'  => $username,
        ]);
    }
}