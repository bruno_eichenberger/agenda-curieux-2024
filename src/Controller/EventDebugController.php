<?php

namespace App\Controller;

use DateTime;
use App\Entity\Event;
use App\Entity\Place;
use App\Entity\Rubric;
use App\Entity\Ticket;
use App\Entity\Comment;
use App\Form\EventType;
use App\Service\Counts;
use App\Service\Website;
use App\Service\Widgets;
use App\Form\CommentType;
use Cocur\Slugify\Slugify;
use App\Entity\EventSearch;
use App\Service\ApiFacebook;
use App\Entity\EventRedirect;
use App\Form\EventSearchType;
use App\Entity\TicketResource;
use App\Service\TitlesOfPages;
use App\Form\EventRedirectType;
use App\Service\UploaderHelper;
use App\Repository\EventRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Contracts\Cache\ItemInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class EventDebugController extends AbstractController
{
    public function __construct(Website $website, ManagerRegistry $doctrine)
    {
        $this->website = $website->getArray();
        $this->subdomain = $website->getArray()['address']['subdomain'];
        $this->doctrine = $doctrine;
    }

    /**
     * Afficher les évènements de la semaine
     * 
     * @Route("/agenda2/", name="event_index2")
     * @return Response
     */
    public function index(EventRepository $repo, Request $request, CacheInterface $cache, Widgets $widgets)
    {
        $redirect = new EventRedirect;
        $formRedirect = $this->createForm(EventRedirectType::class,$redirect);
        $formRedirect->handleRequest($request);

        /*
        $search = new EventSearch;
        dd($search);
        $form = $this->createForm(EventSearchType::class,$search);
        $form->handleRequest($request);
        */


        $menu['day'] = $this->getCountsByDayOfTheWeek($cache);
        $menu['rubric'] = $this->getRubrics($cache);
        $menu['category'] = $this->getCategories($cache);
        $menu['reduction'] = $this->getReductions($cache);
        //$menu['form'] = $form->createView();
        $menu['form'] = [];
        $menu['collapse'] = [];
        //$menu['collapse'] = ['MENU_RUBRIC','MENU_CATEGORY','MENU_REDUCTION','MENU_SEARCH'];



        if ($this->subdomain){
            $nameOfCacheIndex = 'index_'.$this->subdomain;
        } else {
            $nameOfCacheIndex = 'index_all';
        }
        
        //$cache->delete($nameOfCacheIndex);

        $tabFinal = $cache->get($nameOfCacheIndex, function (ItemInterface $item) use ($repo, $menu) {
            
            $today = new DateTime();
            $tomorrow = new DateTime('tomorrow');

            if($today->format('H') == '23'){
                $item->expiresAt($tomorrow->setTime(0,0));
            } else {
                $item->expiresAfter(3600);
            }

            //$events = $this->separatorByTime($repo->findAllOfThisWeek($this->subdomain));
            $events = $repo->findAllOfThisWeek($this->subdomain);

            $tab['title'] = ["Aujourd'hui", "Demain", "Après-demain", "Dans 3 jours", "Dans 4 jours", "Dans 5 jours", "Dans 6 jours", "Dans 7 jours"];
            $tab['day'] = ["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"];
            $tab['month'] = ["décembre","janvier","février","mars","avril","mai","juin","juillet","août","septembre","octobre","novembre","décembre"];
            $tab['slug'] = ["aujourd-hui", "demain", "apres-demain", "dans-3-jours", "dans-4-jours", "dans-5-jours", "dans-6-jours", "dans-7-jours"];
            
            if (count($events) > 20){

                // Création du tableau vierge
                for($i = 0; $i < 7; $i++){

                    $dateOfDay = $menu['day'][$i]['date'];
                    $datetime = new DateTime();
                    $newDate = $datetime->createFromFormat('Y-m-d H:i', $dateOfDay." 00:00");

                    $textDate = $tab['day'][$newDate->format("w")] ." ". $newDate->format("j") ." ". $tab['month'][$newDate->format("n")] ." ". $newDate->format("Y");

                    if ($dateOfDay == $today->format('Y-m-d')){$anchor = "aujourd-hui";} 
                    elseif ($dateOfDay == $tomorrow->format('Y-m-d')){$anchor = "demain";}
                    else {$anchor = $tab['day'][$newDate->format('N')];}

                    $tabEvent[$dateOfDay] = [
                        'title'     => $tab['title'][$i],
                        'date'      => $textDate,
                        'anchor'    => $anchor,
                        'link'      => $tab['slug'][$i],
                        'events'    => [
                            'primary'   => null,
                            'secondary' => null
                        ]
                    ];
                }
                //dd($tabEvent);

                foreach($events as $event){
                    //$event->setDateText($this->dateSentence->getText($event));

                    foreach($tabEvent as $day => $data){
                        if($event->getDateStart()->format("Y-m-d") == $day or $event->getDateEnd()->format("Y-m-d") == $day){
                            $tabEvent[$day]['events']['primary'][] = $event;
                        }
                        if($event->getDateStart()->format("Y-m-d") < $day and $event->getDateEnd()->format("Y-m-d") > $day){
                            $tabEvent[$day]['events']['secondary'][] = $event ;
                        }
                    }
                }

                $tabEvent[$day]['events']['primary'] = $this->separator($tabEvent[$day]['events']['secondary']);
                $tabEvent[$day]['events']['secondary'] = $this->separator($tabEvent[$day]['events']['secondary']);
            } else {
                
                $events = $repo->findAllOfThisMonth($this->subdomain);

                $now = new DateTime();
                $week = $now->modify('+1 week');

                $now = new DateTime();
                $month = $now->modify('+1 month');

                $tabEvent['today']['title'] = $tab['day'][$today->format("w")] ." ". $today->format("j") ." ". $tab['month'][$today->format("n")] ." ". $today->format("Y");
                $tabEvent['today']['events'] = null;
                $tabEvent['week']['events'] = null;
                $tabEvent['month']['events'] = null;

                foreach($events as $event){
                    //$event->setDateText($this->dateSentence->getText($event));
                    if (
                        $event->getDateStart()->format("Y-m-d") == $today->format("Y-m-d") or 
                        $event->getDateEnd()->format("Y-m-d") == $today->format("Y-m-d"))
                    {
                        $tabEvent['today']['events'][] = $event;
                    } 
                    elseif (
                        $event->getDateEnd()->format("Y-m-d") < $week->format("Y-m-d")
                    ){
                        $tabEvent['week']['events'][] = $event;
                    } 
                    elseif (
                        $event->getDateEnd()->format("Y-m-d") < $month->format("Y-m-d")
                    ){
                        $tabEvent['month']['events'][] = $event;
                    }
                }
            }
            
            //dd($tabEvent);
            return $tabEvent;
        });

        //dd($this->website);

        // Si sous-domaine détecté
        if ($this->subdomain){

            // Si un slogan est enregistré
            if ($this->website['slogan']){
                $title['top'] = $this->website['slogan'];
            } 
            /*
            elseif ($this->website['city']['resident']){
                $title['top'] = "Agenda culturel de la région ". $this->website['city']['resident']['singular']['feminine'];
            }
            */ 
            else {
                $title['top'] = "Agenda culturel et participatif";
            }

            /*
            if ($this->website['city']['resident']){
                $title['secondary'] = "à " . $this->website['city']['name'] . " et alentours";
            } else {
                $title['secondary'] = null;
            }
            */
            $title['secondary'] = null;
        } else {
            $title['top'] = "Agenda culturel et participatif";
        }

        //dd($menu);

        $widget = $widgets->getWidgets($cache,$request,date('Y-m-d'));

        $formHeader = $formRedirect->createView();

        
        //dd($formHeader);

        //dd($tabFinal);
        return $this->render('event/index.html.twig', [
            'page'                  => 'event',
            'tabevents'             => $tabFinal,
            //'map'                 => $map,
            //'nextEvents'          => $nextEvents,
            //'nextEventsSecondary' => $nextEventsSecondary,
            'website'               => $this->website,
            'daysOfWeek'            => $this->getDaysOfTheWeek(),
            'menu'                  => $menu,
            'title'                 => $title,
            'widget'                => $widget,
            'searchPrimary'         => $formHeader
        ]);
    }

    function getDaysOfTheWeek()
    {
        $week = ["dimanche","lundi","mardi","mercredi","jeudi","vendredi","samedi","dimanche","lundi","mardi","mercredi","jeudi","vendredi","samedi"];
        $dayWeek = date('w');
        return [$week[$dayWeek], $week[$dayWeek+1], $week[$dayWeek+2], $week[$dayWeek+3], $week[$dayWeek+4], $week[$dayWeek+5], $week[$dayWeek+6], $week[$dayWeek+7]];
    }

    public function getCountsByDayOfTheWeek($cache)
    {
        return $cache->get('menu_events_by_days_'.$this->subdomain, function(ItemInterface $item){

            $item->expiresAfter(60*61);
           
            $today = date('Y-m-d');
            $dateOfNextWeek = date('Y-m-d', strtotime('+7 days'));

            if ($this->subdomain)
            {
                $query = "
                    SELECT e.date_start, e.date_end
                    FROM event e
                    LEFT JOIN place p ON p.id = e.place_id
                    LEFT JOIN city c ON c.id = p.city_id OR c.id = e.city_id
                    LEFT JOIN borough b ON b.id = c.borough_id
                    LEFT JOIN subdomain_borough sb ON sb.borough_id = c.borough_id
                    LEFT JOIN subdomain s ON s.id = sb.subdomain_id
                    WHERE s.slug = '".$this->subdomain."'
                    AND e.is_validated is true
                    AND e.date_start <= '$dateOfNextWeek' 
                    AND e.date_end >= '$today' ";
                /*
                $query = "
                    SELECT * 
                    FROM event e, place p, city c, borough b 
                    WHERE p.id = e.place_id
                    AND (c.id = p.city_id OR c.id = e.city_id)
                    AND b.id = c.borough_id
                    AND b.slug = '".$this->subdomain."'
                    AND date_start <= '$dateOfNextWeek' 
                    AND date_end >= '$today'
                    ORDER BY date_start";
                */
            }
            else
            {
                $query = "SELECT e.date_start, e.date_end FROM event e where e.date_start <= '$dateOfNextWeek' AND  e.date_end >= '$today' AND e.is_validated is true ";
            }

            $query.= "ORDER BY date_start";
            
            $em = $this->doctrine->getManager();
            $statement = $em->getConnection()->prepare($query);
            $events = $statement->execute()->fetchAll();

            $week = ["dimanche","lundi","mardi","mercredi","jeudi","vendredi","samedi","dimanche","lundi","mardi","mercredi","jeudi","vendredi","samedi"];
            $dayWeek = date('w');

            $day[0] = array('name'=>$week[$dayWeek],'date'=>date('Y-m-d', strtotime('+0 days')),'count'=>0);
            $day[1] = array('name'=>$week[$dayWeek+1],'date'=>date('Y-m-d', strtotime('+1 days')),'count'=>0);
            $day[2] = array('name'=>$week[$dayWeek+2],'date'=>date('Y-m-d', strtotime('+2 days')),'count'=>0);
            $day[3] = array('name'=>$week[$dayWeek+3],'date'=>date('Y-m-d', strtotime('+3 days')),'count'=>0);
            $day[4] = array('name'=>$week[$dayWeek+4],'date'=>date('Y-m-d', strtotime('+4 days')),'count'=>0);
            $day[5] = array('name'=>$week[$dayWeek+5],'date'=>date('Y-m-d', strtotime('+5 days')),'count'=>0);
            $day[6] = array('name'=>$week[$dayWeek+6],'date'=>date('Y-m-d', strtotime('+6 days')),'count'=>0);
            $day[7] = array('name'=>$week[$dayWeek+7],'date'=>date('Y-m-d', strtotime('+7 days')),'count'=>0);

            foreach($events as $key => $event) {
                for ($i=0; $i < count($day) ; $i++) { 
                    if($event["date_start"] <= $day[$i]["date"] && $event["date_end"] >= $day[$i]["date"] ){
                        $day[$i]["count"]++;
                    }
                }
            }
            return $day;
        });
        
    }

    public function getRubrics($cache)
    {
        return $cache->get('menu_events_by_rubrics_'.$this->subdomain, function(ItemInterface $item){

            $item->expiresAfter(60*59);

            $today = date('Y-m-d');

            if ($this->subdomain){

                $query =   "SELECT r.name, r.slug, r.color, COUNT(e.id) as compteur
                            FROM event e
                            LEFT JOIN place p ON p.id = e.place_id
                            LEFT JOIN city c ON c.id = p.city_id OR c.id = e.city_id
                            LEFT JOIN borough b ON b.id = c.borough_id
                            LEFT JOIN subdomain_borough sb ON sb.borough_id = c.borough_id
                            LEFT JOIN subdomain s ON s.id = sb.subdomain_id
                            LEFT JOIN rubric r ON r.id = e.rubric_id
                            WHERE s.slug = '".$this->subdomain."' ";
                /*
                $query =   "SELECT r.name, r.slug, COUNT(*) as compteur
                            FROM rubric r, borough b, city c, event e
                            LEFT JOIN place p ON p.id = e.place_id
                            WHERE (c.id = p.city_id OR c.id = e.city_id)
                            AND r.id = e.rubric_id
                            AND b.id = c.borough_id
                            AND b.slug = '".$this->subdomain."' ";
                */
            } else {
                
                $query =   "SELECT r.name, r.slug, r.color, COUNT(e.id) as compteur
                            FROM event e, rubric r
                            WHERE r.id = e.rubric_id ";
            }

            $query.=   "AND e.date_end >= '$today'
                        GROUP BY r.name
                        ORDER BY compteur DESC";

            $em = $this->doctrine->getManager();
            $statement = $em->getConnection()->prepare($query);
            $rubrics = $statement->execute()->fetchAll();

            $tabfinal = [];
            foreach($rubrics as $rubric){
                if($rubric['slug'] == "autre"){
                    $rubricAutre = $rubric;
                } else {
                    $tabfinal[] = $rubric;
                }
            }
            if (isset($rubricAutre)){
                $tabfinal[] = $rubricAutre;
            }
            
            return $tabfinal;
  
        });
    }

    public function getCategories($cache)
    {
        return $cache->get('menu_events_by_categorys_'.$this->subdomain, function(ItemInterface $item){

            $item->expiresAfter(60*60);
            
            $today = date('Y-m-d');
            
            if ($this->subdomain){

                $query =   "SELECT ca.name, ca.slug, COUNT(e.id) as compteur
                            FROM event e
                            LEFT JOIN place p ON p.id = e.place_id
                            LEFT JOIN city c ON c.id = p.city_id OR c.id = e.city_id
                            LEFT JOIN borough b ON b.id = c.borough_id
                            LEFT JOIN subdomain_borough sb ON sb.borough_id = c.borough_id
                            LEFT JOIN subdomain s ON s.id = sb.subdomain_id
                            LEFT JOIN event_category ec ON e.id = ec.event_id
                            LEFT JOIN category ca ON ca.id = ec.category_id
                            WHERE s.slug = '".$this->subdomain."' ";

                /*
                $query =   "SELECT ca.name, ca.slug, COUNT(*) as compteur
                            FROM event_category ec, category ca, city c, borough b, event e
                            LEFT JOIN place p ON p.id = e.place_id
                            WHERE e.id = ec.event_id
                            AND ca.id = ec.category_id
                            AND (c.id = p.city_id OR c.id = e.city_id)
                            AND b.id = c.borough_id
                            AND b.slug = '".$this->subdomain."' ";
                */
            } else {
                
                $query =   "SELECT ca.name, ca.slug, COUNT(e.id) as compteur
                            FROM event e, event_category ec, category ca
                            WHERE e.id = ec.event_id
                            AND ca.id = ec.category_id ";
            }

            $query.=   "AND e.date_end >= '$today'
                        GROUP BY ca.name
                        ORDER BY compteur DESC
                        limit 20";

            $em = $this->doctrine->getManager();
            $statement = $em->getConnection()->prepare($query);
            $categories = $statement->execute()->fetchAll();

            $tabfinal = [];
            foreach($categories as $category){
                if($category['slug'] !== NULL){
                    $tabfinal[] = $category;
                }
            }

            return $tabfinal;
        });
    }

    public function getReductions($cache)
    {
        return $cache->get('menu_events_by_reductions_'.$this->subdomain, function(ItemInterface $item){

            $item->expiresAfter(60*60);
            
            $today = date('Y-m-d');
            
            if ($this->subdomain){

                $query =   "SELECT r.title, r.slug, COUNT(e.id) as compteur
                            FROM event e
                            LEFT JOIN place p ON p.id = e.place_id
                            LEFT JOIN city c ON c.id = p.city_id OR c.id = e.city_id
                            LEFT JOIN borough b ON b.id = c.borough_id
                            LEFT JOIN subdomain_borough sb ON sb.borough_id = c.borough_id
                            LEFT JOIN subdomain s ON s.id = sb.subdomain_id
                            LEFT JOIN prices pr ON e.id = pr.event_id
                            LEFT JOIN reduction r ON r.id = pr.reduction_id
                            WHERE s.slug = '".$this->subdomain."' ";
                /*            
                $query =   "SELECT r.title, r.slug, COUNT(*) as compteur
                            FROM event e, prices pr, reduction r, place p, city c, borough b
                            WHERE e.id = pr.event_id
                            AND r.id = pr.reduction_id
                            AND p.id = e.place_id
                            AND (c.id = p.city_id OR c.id = e.city_id)
                            AND b.id = c.borough_id
                            AND b.slug = '".$this->subdomain."' ";
                */

            } else {
                
                $query =   "SELECT r.title, r.slug, COUNT(e.id) as compteur
                            FROM event e, prices pr, reduction r
                            WHERE e.id = pr.event_id
                            AND r.id = pr.reduction_id ";
            }

            $query.=   "AND e.date_end >= '$today'
                        GROUP BY r.title
                        ORDER BY compteur DESC
                        limit 10";

            $em = $this->doctrine->getManager();
            $statement = $em->getConnection()->prepare($query);
            $reductions = $statement->execute()->fetchAll();
            
            $tabfinal = [];
            foreach($reductions as $reduction){
                if($reduction['slug'] !== NULL){
                    $tabfinal[] = $reduction;
                }
            }
            
            return $tabfinal;
        });
    }


    
    /**
     * Permet d'afficher une seule annonce
     * 
     * @Route("/agenda/evenement/{slug}", name="event_show")
     * 
     * @return Response
     */
    public function show(Event $event, Request $request, EntityManagerInterface $manager, CacheInterface $cache, TitlesOfPages $titles, EventRepository $repo, PaginatorInterface $paginator, Counts $counts){

        // rediriger apres connexion
        $request->getSession()->set('_security.main.target_path', $request->getUri());

        // function show($slug, EventRepository $repo){
        // Je récupère l'annonce qui correspond au slug
        //$event = $repo->findOneBySlug($slug);

            //dd($this->container->get('security.token_storage')->getToken());

        /*
        foreach($event->getEditors() as $editor){
            dd($editor->getEditor());
        }
        
        $search = new EventSearch;
        $form = $this->createForm(EventSearchType::class,$search);
        $form->handleRequest($request);
        */
        $postComment = new Comment;
        $formComment = $this->createForm(CommentType::class,$postComment);
        $formComment->handleRequest($request);

        if($formComment->isSubmitted() && $formComment->isValid())
        {
            $comment = $formComment->getData();

            $comment    ->setIsChecked(false)
                        ->setIsSpammed(false)
                        ->setEvent($event)
                        ->setAuthor($this->getUser());

            if ($this->getUser() === null){
                $comment->setIsAlerted(true);
            }

            $manager->persist($comment);
            $manager->flush();

            $this->addFlash(
                'success',
                "Votre commentaire a été publié !"
            );

            return $this->redirectToRoute('event_show', [
                'slug' => $event->getSlug()
            ]);
        }

        
        //$event->setDateText($this->dateSentence->getText($event));

        $rubric = $event->getRubric();
        
        $nameOfCache = "events_by_rubric" .'_'. $this->subdomain .'_'. $rubric->getSlug() .'_'. $request->query->getInt('page', 1);
        //$cache->delete($nameOfCache);
        
        if($cache->getItem($nameOfCache)->get("value")){
            $data = $cache->getItem($nameOfCache)->get("value");
        } else {
            $data = null;
        }

        $menu['day'] = $this->getCountsByDayOfTheWeek($cache);
        $menu['rubric'] = $this->getRubrics($cache);
        $menu['category'] = $this->getCategories($cache);
        $menu['reduction'] = $this->getReductions($cache);
        //$menu['form'] = $form->createView();
        $menu['collapse'] = ['MENU_WEEK','MENU_SEARCH'];

        return $this->render('event/show.html.twig', [
            'page'              => 'event',
            'event'             => $event,
            'data'              => $data,
            'website'           => $this->website,
            'daysOfWeek'        => $this->getDaysOfTheWeek(),
            'formComment'       => $formComment->createView(),
            'menu'              => $menu
        ]);
    }


    public function separator($events)
    {
        if ($events){
            $lastTime = "";
            $lastDateStart = "";
            $lastDateEnd = "";
            foreach ($events as $event){

                if ($event->getDateStart()->format('Y-m-d') == date('Y-m-d')){
                    if (isset($lastTime) and $event->getTimeStart() == $lastTime){
                        $event->setSameDateTime(true);
                    }
                } else {
                    if (
                        isset($lastDateStart) and isset($lastDateEnd) and 
                        $event->getDateStart() == $lastDateStart and
                        $event->getDateEnd() == $lastDateEnd
                    ){
                        $event->setSameDateTime(true);
                    }
                }

                $lastDateStart = $event->getDateStart();
                $lastDateEnd = $event->getDateEnd();
                $lastTime = $event->getTimeStart();
            }
            return $events;
        }
        else {
            return null;
        }

    }

    public function separatorByTime($events)
    {
        $lastTime = "";
        foreach ($events as $event){
            if ($event->getTimeStart() == $lastTime){
                $event->setSameDateTime(true);
            }
            $lastTime = $event->getTimeStart();
        }
        return $events;
    }

    public function separatorByDate($events)
    {
        $lastDateStart = "";
        $lastDateEnd = "";
        foreach ($events as $event){
            if (
                isset($lastDateStart) and isset($lastDateEnd) and 
                $event->getDateStart() == $lastDateStart and
                $event->getDateEnd() == $lastDateEnd
            ){
                $event->setSameDateTime(true);
            }
            $lastDateStart = $event->getDateStart();
            $lastDateEnd = $event->getDateEnd();
        }

        return $events;
    }


}
