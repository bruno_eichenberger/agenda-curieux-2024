<?php

namespace App\Controller;

use App\Service\Website;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class LegalController extends AbstractController
{
    public function __construct(Website $website)
    {
        $this->website = $website->getArray();
        $this->subdomain = $website->getArray()['address']['subdomain'];
    }
    
    /**
     * @Route("/conditions-d-utilisation", name="legal_condition")
     */
    public function index()
    {
        return $this->render('legal/condition.html.twig', [
            'website'       => $this->website,
            'page'          => ''
        ]);
    }

    /**
     * @Route("/mentions-legales", name="legal_notice")
     */
    public function notice()
    {
        return $this->render('legal/notice.html.twig', [
            'website'       => $this->website,
            'page'          => ''
        ]);
    }

    /**
     * @Route("/regles-de-confidentialite", name="legal_confidentiality")
     */
    public function confidentiality()
    {
        return $this->render('legal/confidentiality.html.twig', [
            'website'       => $this->website,
            'page'          => ''
        ]);
    }
}
