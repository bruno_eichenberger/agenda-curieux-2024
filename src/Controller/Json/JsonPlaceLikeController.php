<?php

namespace App\Controller\Json;

use App\Entity\Place;
use App\Entity\PlaceLike;
use App\Repository\PlaceLikeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class JsonPlaceLikeController extends AbstractController
{
    /**
     * Permet de liker ou unliker les lieux
     * 
     * @Route("/place-like/{id}/like",name="place_like")
     *
     * @param Place $place
     * @param EntityManagerInterface $manager
     * @param PlaceLikeRepository $repo
     * @return void
     */
    public  function post(Place $place, EntityManagerInterface $manager, PlaceLikeRepository $likeRepo) : Response
    {
        $user = $this->getUser();

        if(!$user){
            return $this->json([
                'code' => 403,
                'message' => 'Unauthorized'
            ], 403);
        }

        if($place->isLikedByUser($user)){
            $like = $likeRepo->findOneBy([
                'place' => $place,
                'user' => $user
            ]);

            $manager->remove($like);
            $manager->flush();

            return $this->json([
                'code' => 200, 
                'message' => 'Like bien supprimé',
                'likes' => $likeRepo->count(['place' => $place])
            ], 200);
        }
        
        $like = new PlaceLike();

        $like   ->setPlace($place)
                ->setUser($user);

        $manager->persist($like);
        $manager->flush();

        return $this->json([
            'code' => 200, 
            'message' => 'Like bien ajouté',
            'likes' => $likeRepo->count(['place' => $place])
        ], 200);
    }

}
