<?php

namespace App\Controller\Json;

use App\Entity\Event;
use App\Entity\EventLike;
use App\Repository\EventLikeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class JsonEventLikeController extends AbstractController
{
    /**
     * Permet de liker ou unliker les évènements
     * 
     * @Route("/event-like/{id}/like",name="event_like", methods={"POST"})
     *
     * @param Event $event
     * @param EntityManagerInterface $manager
     * @param EventLikeRepository $repo
     * @return void
     */
    public function event_like_json(Event $event, EntityManagerInterface $manager, EventLikeRepository $likeRepo) : Response
    {
        $user = $this->getUser();

        if(!$user){
            return $this->json([
                'code' => 403,
                'message' => 'Unauthorized'
            ], 403);
        }

        if($event->isLikedByUser($user)){
            $like = $likeRepo->findOneBy([
                'event' => $event,
                'user' => $user
            ]);

            $manager->remove($like);
            $manager->flush();

            return $this->json([
                'code' => 200, 
                'message' => 'Like bien supprimé',
                'likes' => $likeRepo->count(['event' => $event])
            ], 200);
        }
        
        $like = new EventLike();

        $like   ->setEvent($event)
                ->setUser($user);

        $manager->persist($like);
        $manager->flush();

        return $this->json([
            'code' => 200, 
            'message' => 'Like bien ajouté',
            'likes' => $likeRepo->count(['event' => $event])
        ], 200);
    }

}
