<?php

namespace App\Controller\Json;

use App\Repository\CategoryRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class JsonCategoryController extends AbstractController
{
    /**
     * @Route("/search_category", name="tetranz_category_search")
     */
    public function search(CategoryRepository $repo)
    {
        $text = $_GET["q"];
        $categories = $repo->findByName($text);

        return $this->render('json/category.json.twig', [
            'categories' => $categories
        ]);
    } 
}
