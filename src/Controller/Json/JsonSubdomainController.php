<?php

namespace App\Controller\Json;

use App\Repository\SubdomainRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class JsonSubdomainController extends AbstractController
{
    /**
     * @Route("/search_subdomain", name="tetranz_subdomain_search")
     */
    public function searchSubdomain(SubdomainRepository $repo)
    {
        $text = $_GET["q"];
        $subdomains = $repo->findByName($text);

        return $this->render('json/subdomain.json.twig', [
            'subdomains' => $subdomains
        ]);
    }
}
