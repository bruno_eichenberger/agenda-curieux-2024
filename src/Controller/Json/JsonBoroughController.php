<?php

namespace App\Controller\Json;

use App\Repository\BoroughRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class JsonBoroughController extends AbstractController
{
    /**
     * @Route("/search_borough", name="tetranz_borough_search")
     */
    public function searchBorough(BoroughRepository $repo)
    {
        $text = $_GET["q"];
        $boroughs = $repo->findByName($text);

        return $this->render('json/borough.json.twig', [
            'boroughs' => $boroughs
        ]);
    }
}
