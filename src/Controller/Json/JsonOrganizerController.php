<?php

namespace App\Controller\Json;

use App\Repository\OrganizerRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class JsonOrganizerController extends AbstractController
{
    /**
     * @Route("/search_organizer", name="tetranz_organizer_search")
     */
    public function json_organizer(OrganizerRepository $repo)
    {
        $text = $_GET["q"];
        $organizers = $repo->findByName($text);

        return $this->render('json/organizer.json.twig', [
            'organizers' => $organizers
        ]);
    }

}
