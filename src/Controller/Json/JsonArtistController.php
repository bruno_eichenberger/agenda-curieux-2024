<?php

namespace App\Controller\Json;

use App\Repository\ArtistRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class JsonArtistController extends AbstractController
{
    /**
     * @Route("/search_artist", name="tetranz_artist_search")
     */
    public function json_artist(ArtistRepository $repo)
    {
        $text = $_GET["q"];
        $artists = $repo->findByName($text);

        return $this->render('json/artist.json.twig', [
            'artists' => $artists
        ]);
    }
}
