<?php

namespace App\Controller\Json;

use App\Repository\ReductionRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class JsonReductionController extends AbstractController
{
    /**
     * @Route("/search_reduction", name="tetranz_reduction_search")
     */
    public function json_reduction(ReductionRepository $repo)
    {
        $reductions = $repo->findByTitle($_GET["q"]);

        return $this->render('json/reduction.json.twig', [
            'reductions' => $reductions
        ]);
    }
}
