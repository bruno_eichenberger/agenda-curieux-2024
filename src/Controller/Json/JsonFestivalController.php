<?php

namespace App\Controller\Json;

use App\Repository\FestivalRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class JsonFestivalController extends AbstractController
{
   
    /**
     * @Route("/search_festival", name="tetranz_festival_search")
     */
    public function search(FestivalRepository $repo)
    {
        $text = $_GET["q"];
        $festivals = $repo->findByTitle($text);

        return $this->render('json/festival.json.twig', [
            'festivals' => $festivals
        ]);
    }
}
