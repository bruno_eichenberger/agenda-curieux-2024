<?php

namespace App\Controller\Json;

use App\Service\Website;
use App\Repository\EventRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class JsonEventController extends AbstractController
{
    /**
     * @Route("/event-autocomplete", name="event_autocomplete")
     */
    public function searchBySlug(EventRepository $repo, Website $website)
    {
        $text = $_GET["q"];
        $subdomain = $website->getArray()['address']['subdomain'];
        $events = $repo->findByTitle($text, $subdomain);

        return $this->render('json/event.json.twig', [
            'events' => $events
        ]);
    } 
}
