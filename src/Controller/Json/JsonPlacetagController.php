<?php

namespace App\Controller\Json;

use App\Repository\PlacetagRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class JsonPlacetagController extends AbstractController
{
    /**
     * @Route("/search_placetag", name="tetranz_placetag_search")
     */
    public function search(PlacetagRepository $repo)
    {
        $text = $_GET["q"];
        $placetags = $repo->findByName($text);

        return $this->render('json/placetag.json.twig', [
            'placetags' => $placetags
        ]);
    } 
}
