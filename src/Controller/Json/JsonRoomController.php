<?php

namespace App\Controller\Json;

use App\Repository\RoomRepository;
use App\Repository\PlaceRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class JsonRoomController extends AbstractController
{
    /**
     * @Route("/search_room", name="tetranz_room_search")
     */
    public function json_room(PlaceRepository $repo)
    {
        $place = $repo->find($_GET["id"]);
        
        return $this->render('json/room.json.twig', [
            'rooms' => $place->getRooms()
        ]);
    }
}
