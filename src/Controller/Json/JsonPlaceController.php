<?php

namespace App\Controller\Json;

use App\Repository\PlaceRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class JsonPlaceController extends AbstractController
{
    /**
     * @Route("/search_place", name="tetranz_place_search")
     */
    public function search(PlaceRepository $repo)
    {
        $text = $_GET["q"];
        $places = $repo->findByName($text);

        return $this->render('json/place.json.twig', [
            'places' => $places
        ]);
    }

    /**
     * @Route("/search_place_by_slug", name="place_by_slug_search")
     */
    public function searchBySlug(PlaceRepository $repo)
    {
        $text = $_GET["q"];
        $places = $repo->findByName($text);

        return $this->render('json/place_by_slug.json.twig', [
            'places' => $places
        ]);
    } 
}
