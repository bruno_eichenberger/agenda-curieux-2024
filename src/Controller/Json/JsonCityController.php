<?php

namespace App\Controller\Json;

use App\Repository\CityRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class JsonCityController extends AbstractController
{
    /**
     * @Route("/search_city", name="tetranz_city_search")
     */
    public function index(CityRepository $repo)
    {
        $text = $_GET["q"];
        $cities = $repo->findByName($text);

        return $this->render('json/city.json.twig', [
            'cities' => $cities
        ]);
    }

    /**
     * @Route("/search_city_by_slug", name="city_by_slug_search")
     */
    public function search(CityRepository $repo)
    {
        $text = $_GET["q"];
        $cities = $repo->findByName($text);

        return $this->render('json/city_by_slug.json.twig', [
            'cities' => $cities
        ]);
    } 

}
