<?php

namespace App\Controller\Json;

use App\Repository\UserRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class JsonUserController extends AbstractController
{
    /**
     * @Route("/search_user", name="tetranz_user_search")
     */
    public function search(UserRepository $repo)
    {
        $text = $_GET["q"];
        $users = $repo->findByName($text);

        return $this->render('json/user.json.twig', [
            'users' => $users
        ]);
    }
}
