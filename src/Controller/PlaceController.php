<?php

namespace App\Controller;

use App\Entity\City;
use App\Entity\User;
use App\Entity\Claim;
use App\Entity\Place;
use App\Entity\Comment;
use App\Form\PlaceType;
use App\Service\Counts;
use App\Entity\Placetag;
use App\Service\Website;
use App\Service\Widgets;
use App\Entity\Placekind;
use App\Entity\PlaceLike;
use App\Form\CommentType;
use App\Entity\PlaceSearch;
use App\Service\ArrayOfData;
use App\Form\PlaceSearchType;
use App\Service\TitlesOfPages;
use App\Service\UploaderHelper;
use App\Service\PronounTranslator;
use App\Repository\EventRepository;
use App\Repository\PlaceRepository;
use App\Repository\PlaceLikeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Contracts\Cache\ItemInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/annuaire")
 */
class PlaceController extends AbstractController
{
    public function __construct(Website $website, ManagerRegistry $doctrine)
    {
        $this->website = $website->getArray();
        $this->subdomain = $website->getArray()['address']['subdomain'];
        $this->doctrine = $doctrine;
    }

    /**
     * Afficher tous les lieux et recherche personnalisée avec plusieurs filtres
     * 
     * @Route("/", name="place_index")
     * @Route("/archive", name="old_place_index")
     */
    public function search_by_filter(PlaceRepository $repo, PaginatorInterface $paginator, Request $request, CacheInterface $cache, TitlesOfPages $titles, Widgets $widgets)
    {
        $search = new PlaceSearch;
        $form = $this->createForm(PlaceSearchType::class,$search);
        $form->handleRequest($request);

        // Si la recherche est vierge
        if ($search == new PlaceSearch){
            $menu = ['MENU_PLACEKIND','MENU_PLACETAG','MENU_CITY','MENU_SEARCH'];
            $showAll = false;
        } else {
            $menu = ['MENU_SEARCH'];
            $showAll = "show-all";
        }

        if($request->attributes->get('_route') == "place_index")
        {
            $places = $paginator->paginate($repo->findAllSearchByFilter($search, $this->subdomain, $showAll), $request->query->getInt('page', 1), 20 /*limite par page*/);
            $count['after'] = $places->getTotalItemCount();
            $count['button'] = count($repo->findAllSearchByFilter($search, $this->subdomain, $showAll, true, true)->getScalarResult());
            $title = $titles->getTitlesOfPlaceSearch($request, $count, "old_place_index");
            /*
            if ($search == new PlaceSearch){
                $widget = $widgets->getWidgets($cache,$request);
            } else {
                $widget = ["search"];
            }
            */
            $widget = $widgets->getWidgets($cache,$request);
            $widget['form'] = $form->createView();
            //dd($places);
        } 
        elseif ($request->attributes->get('_route') == "old_place_index")
        {
            $places = $paginator->paginate($repo->findAllSearchByFilter($search, $this->subdomain, $showAll, null, true), $request->query->getInt('page', 1), 20 /*limite par page*/);
            $count['after'] = $places->getTotalItemCount();
            $count['button'] = count($repo->findAllSearchByFilter($search, $this->subdomain, $showAll, true)->getScalarResult());
            $title = $titles->getTitlesOfPlaceSearch($request, $count, "place_index");
            $widget = ["search"];
        }

        if ($search == new PlaceSearch AND $request->attributes->get('_route') == "place_index"){
            $title['afterCount']['one'] = "à venir";
            $title['afterCount']['plural'] = $title['afterCount']['one'];
        } else {
            $title['afterCount']['one'] = "archivé";
            $title['afterCount']['plural'] = "archivés";
        }

        if ($search != new PlaceSearch){
            if ($count['after'] == 1){
                $title['primary'] = "Un lieu correspond à votre recherche";
            } else {
                $title['primary'] = "Les ".$count['after']." lieux correspondent à votre recherche";
            }
            $title['button'] = null;
        }

        return $this->render('place/index.html.twig', [
            'page'              => 'place',
            'places'            => $places,
            'title'             => $title,
            'website'           => $this->website,
            'menu_placekinds'   => $this->getCountsByPlacekind($cache),
            'menu_placetags'    => $this->getCountsByPlacetag($cache),
            'menu_cities'       => $this->getCountsByCity($cache),
            'form'              => $form->createView(),
            //'formWidget'        => $form->createView(),
            'menu'              => $menu,
            'widget'            => $widget
        ]);
    }

    /**
     * Rechercher par type de lieu
     * 
     * @Route("/type/{slug}", name="search_by_placekind")
     * @Route("/type/{slug}/archive", name="old_search_by_placekind")
     */
    public function places_by_placekind(PlaceRepository $repo, PaginatorInterface $paginator, Request $request, $slug, CacheInterface $cache, TitlesOfPages $titles, Widgets $widgets)
    {    
        $search = new PlaceSearch;
        $form = $this->createForm(PlaceSearchType::class,$search);
        $form->handleRequest($request);

        $placekind = $this->doctrine->getRepository(Placekind::class)->findOneBy(['slug' => $slug]);

        if($request->attributes->get('_route') == "search_by_placekind")
        {
            $places = $paginator->paginate($repo->findByPlacekind($slug, $this->subdomain), $request->query->getInt('page', 1), 20 /*limite par page*/);
            $count['after'] = $places->getTotalItemCount();
            $count['button'] = count($repo->findByPlacekind($slug, $this->subdomain, true, true)->getScalarResult());
            $title = $titles->getTitlesOfPlacekind($request, $count, "old_search_by_placekind", $placekind);
            $title['afterCount']['one'] = "à venir";
            $title['afterCount']['plural'] = "à venir";
        }
        elseif ($request->attributes->get('_route') == "old_search_by_placekind")
        {
            $places = $paginator->paginate($repo->findByPlacekind($slug, $this->subdomain, null, true), $request->query->getInt('page', 1), 20 /*limite par page*/);
            $count['after'] = $places->getTotalItemCount();
            $count['button'] = count($repo->findByPlacekind($slug, $this->subdomain, true)->getScalarResult());
            $title = $titles->getTitlesOfPlacekind($request, $count, "search_by_placekind", $placekind);
            $title['afterCount']['one'] = "archivé";
            $title['afterCount']['plural'] = "archivés";
        }

        return $this->render('place/index.html.twig', [
            'page'              => 'place',
            'places'            => $places,
            'title'             => $title,
            'website'           => $this->website,
            'menu_placekinds'   => $this->getCountsByPlacekind($cache),
            'menu_placetags'    => $this->getCountsByPlacetag($cache),
            'menu_cities'       => $this->getCountsByCity($cache),
            'form'              => $form->createView(),
            'menu'              => ['MENU_PLACEKIND','MENU_SEARCH'],
            'widget'            => $widgets->getWidgets($cache,$request)
        ]);
    }

    /**
     * Rechercher par mots-clés
     * 
     * @Route("/service/{slug}", name="search_by_placetag")
     * @Route("/service/{slug}/archive", name="old_search_by_placetag")
     */
    public function places_by_placetag(PlaceRepository $repo, PaginatorInterface $paginator, Request $request, $slug, CacheInterface $cache, TitlesOfPages $titles, Widgets $widgets)
    {    
        $search = new PlaceSearch;
        $form = $this->createForm(PlaceSearchType::class,$search);
        $form->handleRequest($request);

        $placetag = $this->doctrine->getRepository(Placetag::class)->findOneBy(['slug' => $slug]);

        
        if($request->attributes->get('_route') == "search_by_placetag")
        {
            $places = $paginator->paginate($repo->findByPlacetag($slug, $this->subdomain), $request->query->getInt('page', 1), 20 /*limite par page*/);
            $count['after'] = $places->getTotalItemCount();
            $count['button'] = count($repo->findByPlacetag($slug, $this->subdomain, true, true)->getScalarResult());
            $title = $titles->getTitlesOfPlacetag($request, $count, "old_search_by_placetag", $placetag);
            $title['afterCount']['one'] = "à venir";
            $title['afterCount']['plural'] = "à venir";
        }
        elseif ($request->attributes->get('_route') == "old_search_by_placetag")
        {
            $places = $paginator->paginate($repo->findByPlacetag($slug, $this->subdomain, null, true), $request->query->getInt('page', 1), 20 /*limite par page*/);
            $count['after'] = $places->getTotalItemCount();
            $count['button'] = count($repo->findByPlacetag($slug, $this->subdomain, true)->getScalarResult());
            $title = $titles->getTitlesOfPlacetag($request, $count, "search_by_placetag", $placetag);
            $title['afterCount']['one'] = "archivé";
            $title['afterCount']['plural'] = "archivés";
        }

        return $this->render('place/index.html.twig', [
            'page'              => 'place',
            'places'            => $places,
            'title'             => $title,
            'website'           => $this->website,
            'menu_placekinds'   => $this->getCountsByPlacekind($cache),
            'menu_placetags'    => $this->getCountsByPlacetag($cache),
            'menu_cities'       => $this->getCountsByCity($cache),
            'form'              => $form->createView(),
            'menu'              => ['MENU_PLACETAG','MENU_SEARCH'],
            'widget'            => $widgets->getWidgets($cache,$request)
        ]);
    }

    /**
     * Permet de rechercher par ville
     * 
     * @Route("/les-lieux-de-{slug}", name="search_by_city")
     * @Route("/les-lieux-de-{slug}/archive", name="old_search_by_city")
     */
    public function places_by_city(PlaceRepository $repo, PaginatorInterface $paginator, Request $request, $slug, CacheInterface $cache, TitlesOfPages $titles, Widgets $widgets)
    {
        $search = new PlaceSearch;
        $form = $this->createForm(PlaceSearchType::class,$search);
        $form->handleRequest($request);

        $city = $this->doctrine->getRepository(City::class)->findOneBy(['slug' => $slug]);

        if($request->attributes->get('_route') == "search_by_city")
        {
            $places = $paginator->paginate($repo->findByCity($slug, $this->subdomain), $request->query->getInt('page', 1), 20 /*limite par page*/);
            $count['after'] = $places->getTotalItemCount();
            $count['button'] = count($repo->findByCity($slug, $this->subdomain, true, true)->getScalarResult());
            $title = $titles->placesByCity($request, $count, "old_search_by_city", $city);
            $title['afterCount']['one'] = "à venir";
            $title['afterCount']['plural'] = "à venir";
        }
        elseif ($request->attributes->get('_route') == "old_search_by_city")
        {
            $places = $paginator->paginate($repo->findByCity($slug, $this->subdomain, null, true), $request->query->getInt('page', 1), 20 /*limite par page*/);
            $count['after'] = $places->getTotalItemCount();
            $count['button'] = count($repo->findByCity($slug, $this->subdomain, true)->getScalarResult());
            $title = $titles->placesByCity($request, $count, "search_by_city", $city);
            $title['afterCount']['one'] = "archivé";
            $title['afterCount']['plural'] = "archivés";
        }

        return $this->render('place/index.html.twig', [
            'page'              => 'place',
            'places'            => $places,
            'title'             => $title,
            'website'           => $this->website,
            'menu_placekinds'   => $this->getCountsByPlacekind($cache),
            'menu_placetags'    => $this->getCountsByPlacetag($cache),
            'menu_cities'       => $this->getCountsByCity($cache),
            'form'              => $form->createView(),
            'menu'              => ['MENU_CITY','MENU_SEARCH'],
            'widget'            => $widgets->getWidgets($cache,$request)
        ]);
    }
   
    /**
     * Permet de rechercher par favoris
     * 
     * @Security("is_granted('ROLE_USER')", message="Vous devez vous connecter pour voir vos évènements favoris")
     * 
     * @Route("/mes-lieux-favoris", name="places_by_like")
     */
    public function places_by_like(PlaceRepository $repo, PaginatorInterface $paginator, Request $request, CacheInterface $cache, TitlesOfPages $titles)
    {
        $places = $paginator->paginate($repo->findByLike($this->getUser()->getId()), $request->query->getInt('page', 1), 20 /*limite par page*/);

        $search = new PlaceSearch;
        $form = $this->createForm(PlaceSearchType::class,$search);
        $form->handleRequest($request);

        return $this->render('place/index.html.twig', [
            'page'              => 'place',
            'places'            => $places,
            'website'           => $this->website,
            'menu_placekinds'   => $this->getCountsByPlacekind($cache),
            'menu_placetags'    => $this->getCountsByPlacetag($cache),
            'menu_cities'       => $this->getCountsByCity($cache),
            'form'              => $form->createView(),
            'menu'              => ['MENU_SEARCH'],
            'directory'         => '',
            'title'             => [
                'one'       => "Votre lieu favori",
                'plural'    => "lieux favoris",
            ],
            'afterCount'        => [
                'one'       => "à venir",
                'plural'    => "à venir"
            ]
        ]);
    }

    /**
     * Rechercher par type de lieu
     * 
     * @Route("/max", name="search_place")
     * @Security("is_granted('ROLE_USER')", message="Vous ne pouvez pas accéder à cette page")
     */
    public function places_max(PlaceRepository $repo, PaginatorInterface $paginator, Request $request)
    {    
        $search = new PlaceSearch;
        $form = $this->createForm(PlaceSearchType::class,$search);
        $form->handleRequest($request);

        //$placekind = $this->doctrine->getRepository(Placekind::class)->findOneBy(['slug' => $slug]);
        //$places = $paginator->paginate($repo->findForVerification($this->subdomain), $request->query->getInt('page', 1), 20 /*limite par page*/);
        
        $places = $paginator->paginate($repo->findForVerification($this->subdomain, $request, $search), $request->query->getInt('page', 1), 20 /*limite par page*/);
        
        //places = $repo->findForVerification($this->subdomain)->getResult();
        //dd($places);

        return $this->render('place/max.html.twig', [
            'page'              => 'place',
            'places'            => $places,
            'website'           => $this->website,
            'form'              => $form->createView()
        ]);
    }



    /**
     * Créer un lieu
     * 
     * @Route("/ajouter-un-lieu", name="place_create")
     * @Security("is_granted('ROLE_USER')", message="Vous devez vous connecter pour créer un lieu")
     * @return Response
     */
    public function create(Request $request, EntityManagerInterface $manager, UploaderHelper $uploaderHelper){
        $place = new Place();

        $form = $this->createForm(PlaceType::class, $place);

        /*
        $form->getData()->setOpening([
            'days' => [
                'monday'    => ['open' => null,'close' => null],
                'tuesday'   => ['open' => null,'close' => null],
                'wednesday' => ['open' => null,'close' => null],
                'thursday'  => ['open' => null,'close' => null],
                'friday'    => ['open' => null,'close' => null],
                'saturday'  => ['open' => null,'close' => null],
                'sunday'    => ['open' => null,'close' => null]
            ],
            'update' => date('Y-m-d'),
            'onlyIfEvent' => false
        ]);

        $form->getData()->setTransport([
            'primary'   => ['service' => null, 'line' => null, 'stop' => null],
            'secondary' => ['service' => null, 'line' => null, 'stop' => null],
            'tertiary'  => ['service' => null, 'line' => null, 'stop' => null]
        ]);
        */

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            /** @var UploadedFile $uploadedFile */
            $uploadedFile = $form['coverImageFile']->getData();

            if ($uploadedFile){
                $newFilename = $uploaderHelper->uploadPlaceImage($uploadedFile);
                $place->setCoverImage($newFilename);
            }

            // Obligation de remplir la date d'ouverture et de fermeture en même temps
            $days = $request->get('opening')['days'];
            foreach ($days as $day => $tab){
                foreach ($tab as $time){
                    if ($time == ""){$days[$day] = ['open' => null, 'close' => null];}
                }
            }

            // Vérifie s'il y une modification apportée aux horaires d'ouverture ou si validation par checkbox
            if ($days != $form->getData()->getOpening()['days'] || 
                isset($request->get('opening')['update']) ||
                isset($request->get('opening')['onlyIfEvent']) != $form->getData()->getOpening()['onlyIfEvent']
            ){
                $openingTab = ['days' => $days, 'update' => date('Y-m-d'), 'onlyIfEvent' => isset($request->get('opening')['onlyIfEvent']) ];
                $place->setOpening($openingTab);
            }

            // Obligation de remplir au moins le moyen de transport et l'arret (ligne facultatif)
            // Permet également de ramener au moyen principale si celui-ci est vide
            $transport = $request->get('transport');
            if ($transport['primary']['service'] == "" || $transport['primary']['stop'] == ""){
                if ($transport['secondary']['service'] == "" || $transport['secondary']['stop'] == ""){
                    if ($transport['tertiary']['service'] == "" || $transport['tertiary']['stop'] == ""){
                        $transport['primary'] = ['service' => null, 'line' => null, 'stop' => null];
                    } else {
                        $transport['primary'] = $transport['tertiary'];
                        $transport['tertiary'] = ['service' => null, 'line' => null, 'stop' => null];
                    }
                    $transport['primary'] = ['service' => null, 'line' => null, 'stop' => null];
                } else {
                    $transport['primary'] = $transport['secondary'];
                    $transport['secondary'] = ['service' => null, 'line' => null, 'stop' => null];
                }
            } elseif ($transport['primary']['line'] == ""){$transport['primary']['line'] == null;}

            if ($transport['secondary']['service'] == "" || $transport['secondary']['stop'] == ""){
                if ($transport['tertiary']['service'] == "" || $transport['tertiary']['stop'] == ""){
                    $transport['secondary'] = ['service' => null, 'line' => null, 'stop' => null];
                } else {
                    $transport['secondary'] = $transport['tertiary'];
                    $transport['tertiary'] = ['service' => null, 'line' => null, 'stop' => null];
                }
            } elseif ($transport['secondary']['line'] == ""){$transport['secondary']['line'] == null;}

            if ($transport['tertiary']['service'] == "" || $transport['tertiary']['stop'] == ""){
                $transport['tertiary'] = ['service' => null, 'line' => null, 'stop' => null];
            } elseif ($transport['tertiary']['line'] == ""){$transport['tertiary']['line'] == null;}

            $place->setTransport($transport);

            if ($form->getData()->getPronoun()){
                if ($form['pronoun']->getData() == "L'"){$space = "";} else {$space = " ";}
                $place->setTitle($form['pronoun']->getData() .''. $space .''. $this->formattingTitle($form['name']->getData()));
            } else {
                $place->setTitle($this->formattingTitle($form->getData()->getName()));
            }
            $place->setName($this->formattingTitle($form['name']->getData()));

            if ($this->getUser()){
                $place  ->setAuthor($this->getUser())
                        ->addEditor($this->getUser());
            }
            
            /*
            $description = $form->getData()->getDescription();
            $description = ucfirst(trim($description, ", "));
            $description = str_replace("
","<br>",$description);
            $description = str_replace("<p>","",$description);
            $description = str_replace("</p>","",$description);
            $description = "<p>".$description."</p>";

            $place->setDescription($description);
            */

            foreach($place->getRooms() as $room){
                $room->setPlace($place);
                $manager->persist($room);
            }

            $manager->persist($place);
            $manager->flush();

            $this->addFlash(
                'success',
                "Le lieu <strong>{$place->getTitle()}</strong> a bien été enregistré !"
            );

            return $this->redirectToRoute('place_show', [
                'slug' => $place->getSlug()
            ]);
        }

        return $this->renderForm('place/new.html.twig', [
            'page'          => 'place',
            'formPlace'     => $form,
            'formOpening'   => $form->getData()->getOpening(),
            'formTransport' => $form->getData()->getTransport(),
            'website'       => $this->website,
        ]);
    }


    /**
     * Permet d'éditer un lieu
     * 
     * @Route("/{slug}/modification", name="place_edit")
     * @Security("is_granted('ROLE_USER') and (place.getEditors().contains(user) or is_granted('ROLE_ADMIN'))", message="Vous n'avez pas le droit de modifier ce lieu")
     * @return Response
     */
    public function edit(Place $place, Request $request, EntityManagerInterface $manager, UploaderHelper $uploaderHelper)
    {
        /*
        $description = $place->getDescription();
        $description = str_replace("<br>","
",$description);
        $description = str_replace("<p>","",$description);
        $description = str_replace("</p>","",$description);
        $place->setDescription(trim($description, ", "));
        */

        $rooms = [];
        foreach($place->getRooms() as $room){
            $rooms[] = $room;
        }

        $form = $this->createForm(PlaceType::class, $place);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){

            /** @var UploadedFile $uploadedFile */
            $uploadedFile = $form['coverImageFile']->getData();

            if ($uploadedFile){
                $newFilename = $uploaderHelper->uploadPlaceImage($uploadedFile);
                $place->setCoverImage($newFilename);
            }

            // Obligation de remplir la date d'ouverture et de fermeture en même temps
            $days = $request->get('opening')['days'];
            foreach ($days as $day => $tab){
                foreach ($tab as $time){
                    if ($time == ""){$days[$day] = ['open' => null, 'close' => null];}
                }
            }

            // Vérifie s'il y une modification apportée aux horaires d'ouverture ou si validation par checkbox
            if ($days != $form->getData()->getOpening()['days'] || 
                isset($request->get('opening')['update']) ||
                isset($request->get('opening')['onlyIfEvent']) != $form->getData()->getOpening()['onlyIfEvent']
            ){
                $openingTab = ['days' => $days, 'update' => date('Y-m-d'), 'onlyIfEvent' => isset($request->get('opening')['onlyIfEvent']) ];
                $place->setOpening($openingTab);
            }

            // Obligation de remplir au moins le moyen de transport et l'arret (ligne facultatif)
            // Permet également de ramener au moyen principale si celui-ci est vide
            $transport = $request->get('transport');
            if ($transport['primary']['service'] == "" || $transport['primary']['stop'] == ""){
                if ($transport['secondary']['service'] == "" || $transport['secondary']['stop'] == ""){
                    if ($transport['tertiary']['service'] == "" || $transport['tertiary']['stop'] == ""){
                        $transport['primary'] = ['service' => null, 'line' => null, 'stop' => null];
                    } else {
                        $transport['primary'] = $transport['tertiary'];
                        $transport['tertiary'] = ['service' => null, 'line' => null, 'stop' => null];
                    }
                    $transport['primary'] = ['service' => null, 'line' => null, 'stop' => null];
                } else {
                    $transport['primary'] = $transport['secondary'];
                    $transport['secondary'] = ['service' => null, 'line' => null, 'stop' => null];
                }
            } elseif ($transport['primary']['line'] == ""){$transport['primary']['line'] == null;}

            if ($transport['secondary']['service'] == "" || $transport['secondary']['stop'] == ""){
                if ($transport['tertiary']['service'] == "" || $transport['tertiary']['stop'] == ""){
                    $transport['secondary'] = ['service' => null, 'line' => null, 'stop' => null];
                } else {
                    $transport['secondary'] = $transport['tertiary'];
                    $transport['tertiary'] = ['service' => null, 'line' => null, 'stop' => null];
                }
            } elseif ($transport['secondary']['line'] == ""){$transport['secondary']['line'] == null;}

            if ($transport['tertiary']['service'] == "" || $transport['tertiary']['stop'] == ""){
                $transport['tertiary'] = ['service' => null, 'line' => null, 'stop' => null];
            } elseif ($transport['tertiary']['line'] == ""){$transport['tertiary']['line'] == null;}

            $place->setTransport($transport);

            if ($form->getData()->getPronoun()){
                if ($form['pronoun']->getData() == "L'"){$space = "";} else {$space = " ";}
                $newTitle = $form['pronoun']->getData() .''. $space .''. $this->formattingTitle($form['name']->getData());
                $place->setTitle($newTitle);
            } else {
                $place->setTitle($this->formattingTitle($form->getData()->getName()));
            }
            $place->setName($this->formattingTitle($place->getName()));
/*
            $description = $form->getData()->getDescription();
            $description = trim($description, ", ");
            
            $description = str_replace("

","</p><p>",$description);
            $description = str_replace("
","<br>",$description);
            $description = "<p>".$description."</p>";
            
            $place->setDescription($description);
*/
            // Supprime les associations entre les salles orphelines et des evenements
            foreach ($rooms as $room) {
                if (!$place->getRooms()->contains($room)) {
                    foreach ($room->getEvents() as $event) {
                        $room->removeEvent($event);
                    }
                }
            }


            // Ajoute les nouvelles salles
            foreach ($place->getRooms() as $room) {
                $place->addRoom($room);
                $manager->persist($room);

                $room->setPlace($place);
            }


            $manager->persist($place);
            $manager->flush();

            $this->addFlash(
                'success',
                "<div class='row'>
                    <div class='col'>
                    Le lieu '{$place->getTitle()}' a bien été modifié !<br>
                    </div>
                    <div class='col text-right'>
                        <a href='{$this->generateUrl('place_show', ['slug' => $place->getSlug()])}' class='btn btn-primary'>Revenir sur la page du lieu</a>
                    </div>
                </div>"
            );
        }
        
        return $this->render('place/edit.html.twig', [
            'website'           => $this->website,
            'page'              => 'place',
            'formPlace'         => $form->createView(),
            'formOpening'       => $form->getData()->getOpening(),
            'formTransport'     => $form->getData()->getTransport(),
            'place'             => $place
        ]);





/*

        $form = $this->createForm(PlaceType::class, $place);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            
            foreach($place->getImages() as $image){
                $image->setPlace($place);
                $manager->persist($image);
            }
            
            // inutile car Injection dépendance
            //$manager = $this->doctrine->getManager();

            $manager->persist($place);
            $manager->flush();

            $this->addFlash(
                'success',
                "Les modifications ont été effectué !"
            );

            return $this->redirectToRoute('place_show', [
                'slug' => $place->getSlug()
            ]);
        }

        return $this->render('place/edit.html.twig', [
            'page'      => 'place',
            'formPlace' => $form->createView(),
            'place'     => $place,
            'website'   => $this->website,
        ]);
        */
    }

    /**
     * Permet de supprimer un lieu
     * 
     * @Route("/{slug}/delete", name="place_delete")
     * @Security("is_granted('ROLE_ADMIN')", message="Vous n'avez pas le droit de supprimer ce lieu")
     * 
     * @param Place $place
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delete(Place $place, EntityManagerInterface $manager){
        $manager->remove($place);
        $manager->flush();

        $this->addFlash(
            'success',
            "Le lieu <strong>{$place->getTitle()}</strong> a bien été supprimé !"
        );

        return $this->redirectToRoute('place_index');
    }

    /**
     * Permet de réclamer les droits de modification d'un lieu
     * 
     * @Security("is_granted('ROLE_USER')", message="Il faut se connecter pour demander à modifier ce lieu")
     * @Route("/{slug}/demande-d-acces", name="place_claim")
     * 
     * @param Place $place
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function claim(Place $place, EntityManagerInterface $manager, $slug, PronounTranslator $pronoun){

        $claim = new Claim;
        $claim  ->setPlace($place)
                //->setDate(new \DateTime('now'))
                ->setOwner($this->getUser())
                //->setExplanation("Veuillez compléter votre profil pour que nous puissions vous identifier correctement")
        ;

        if (
            // Si aucune publication de lieu ou d'évènement
            $this->getUser()->getPlaces()->count() == "0" and
            $this->getUser()->getEvents()->count() == "0" and

            // S'il n'a pas déclaré être responsable du lieu
            $this->getUser()->getPlaceAffirmers()->contains($place) === false and

            // Si aucune précision sur son profil, Ex: membre d'une association ou d'un groupe de musique
            $this->getUser()->getOrganizers()->count() == "0" and
            $this->getUser()->getArtists()->count() == "0"
        ){
            $claim->setExplanation("Veuillez compléter votre profil pour que nous puissions vous identifier correctement");

            if ($this->getUser()->getIntroduction() or $this->getUser()->getPresentation()){
                $text = "Compléter votre profil si vous êtes responsable de ce lieu...";
            } else {
                $text = "Compléter votre profil en ajoutant un petit texte de présentation et en précisant si vous êtes responsable de ce lieu !";
            }

            $this->addFlash(
                'danger',
                "{$text}<br> Ainsi, nous vous accorderons le droit de modifier les informations {$pronoun->translate($place->getPronoun())}{$place->getName()} !<br>
                <a class='btn btn-primary btn-block mt-3' href='{$this->generateUrl('account_profile')}' >Améliorer mes informations personnelles</a>"
            );
    
            //return $this->redirectToRoute('place_show', ['slug' => $slug ]);
        }

        $manager->persist($claim);
        $manager->flush();

        $this->addFlash(
            'success',
            "Votre réclamation pour modifier les informations {$pronoun->translate($place->getPronoun())}{$place->getName()} - a été pris en compte !<br>
            Nous répondrons à votre demande dans les plus brefs délais"
        );

        return $this->redirectToRoute('place_show', ['slug' => $slug ]);
        
    }

    
    /**
     * Permet d'afficher les évènements et les infos d'un lieu
     * 
     * @Route("/{slug}", name="place_show")
     * @Route("/{slug}/archive", name="old_place_show")
     * 
     * @return Response
     */
    public function show(Place $place, EntityManagerInterface $manager, EventRepository $repo, PaginatorInterface $paginator, Request $request, $slug, CacheInterface $cache, TitlesOfPages $titles, Counts $counts, ArrayOfData $array)
    {
        $search = new PlaceSearch;
        $form = $this->createForm(PlaceSearchType::class,$search);
        $form->handleRequest($request);

        $postComment = new Comment;
        $formComment = $this->createForm(CommentType::class,$postComment);
        $formComment->handleRequest($request);

        if($formComment->isSubmitted() && $formComment->isValid())
        {
            $comment = $formComment->getData();

            $comment    ->setIsChecked(false)
                        ->setIsSpammed(false)
                        ->setPlace($place)
                        ->setAuthor($this->getUser());

            if ($this->getUser() === null){
                $comment->setIsAlerted(true);
            }

            $manager->persist($comment);
            $manager->flush();

            $this->addFlash(
                'success',
                "Votre commentaire a été publié !"
            );

            return $this->redirectToRoute('place_show', [
                'slug' => $place->getSlug()
            ]);
        }

        //dd($counts->getCounts($repo, $request, $place));

        
        if (strpos($request->attributes->get('_route'),"old_") !== false){
            $period = "before";
        } else {
            $period = null;
        }
        
        $pagination = $paginator->paginate($repo->findByPlace($place, $period),$request->query->getInt('page', 1), 20 );
        
        $title = $titles->getTitlesSimple(
            $request->attributes->get("_route"), 
            $counts->getCounts($repo, $request, $place),
            $place
        );
        //dd($title);

        $data = $array->createArrayOfData($pagination, $title);

        //dd($data);

        /*
        if($request->attributes->get('_route') == "place_show")
        {
            $events = $paginator->paginate($repo->findByPlace($place),$request->query->getInt('page', 1), 10 );
            $title = $titles->getTitlesOfEventsByPlace($request, $counts->getCounts($repo,$request,$place), "old_place_show", $place);
        } else {
            $events = $paginator->paginate($repo->findByPlace($place,"before"),$request->query->getInt('page', 1), 10 );
            $title = $titles->getTitlesOfEventsByPlace($request, $counts->getCounts($repo,$request,$place), "place_show", $place);
        }
        */

        return $this->render('place/show.html.twig', [
            'page'              => 'place',
            'place'             => $place,
            'data'              => $data,
            'website'           => $this->website,
            'menu_placekinds'   => $this->getCountsByPlacekind($cache),
            'menu_placetags'    => $this->getCountsByPlacetag($cache),
            'menu_cities'       => $this->getCountsByCity($cache),
            'form'              => $form->createView(),
            'formComment'       => $formComment->createView(),
            'title'             => $title,
            'menu'              => ['MENU_SEARCH'],
        ]);
    }

    /**
     * Formatage du nom du lieu (Pas tout en majuscule et 1er caractère en majuscule)
     */
    public function formattingTitle($name)
    {
        if ($name == mb_strtoupper($name)){
            $name = str_replace("'", "' ", $name);
            $name = ucwords(mb_strtolower($name));
            $name = str_replace("' ", "'", $name);
            $name = str_replace(" Du ", " du ", $name);
            $name = str_replace(" De ", " de ", $name);
            $name = str_replace(" Et ", " et ", $name);
            $name = str_replace(" À ", " à ", $name);
            $name = str_replace(" D'", " d'", $name);
            $name = str_replace(" L'", " l'", $name);
        }
        $name = str_replace("   ", " ", $name);
        $name = str_replace("  ", " ", $name);
        return $name;
    }
    
    public function getCountsByPlacekind($cache)
    {
        return $cache->get('menu_placekinds_'.$this->subdomain, function(ItemInterface $item){
           
            $item->expiresAfter(60*58);
        
            $today = date('Y-m-d');
            
            if ($this->subdomain){

                $query =   "SELECT k.name, k.slug, COUNT(DISTINCT p.id) as compteur 
                            FROM placekind k, place p, city c, borough b, event e
                            WHERE c.id = p.city_id
                            AND b.id = c.borough_id
                            AND e.place_id = p.id
                            AND b.slug = '".$this->subdomain."' ";
            } else {

                $query =   "SELECT k.name, k.slug, COUNT(DISTINCT p.id) as compteur 
                            FROM placekind k, place p, event e
                            WHERE e.place_id = p.id ";
            }
            
            $query.=   "AND (p.placekind_primary_id = k.id OR p.placekind_secondary_id = k.id OR p.placekind_tertiary_id = k.id) 
                        AND e.date_end >= '$today'
                        GROUP BY k.name
                        ORDER BY compteur DESC";

            $em = $this->doctrine->getManager();
            $statement = $em->getConnection()->prepare($query);

            return $statement->execute()->fetchAll();
        });

    }

    public function getCountsByPlacetag($cache)
    {
        return $cache->get('menu_placetags_'.$this->subdomain, function(ItemInterface $item){
           
            $item->expiresAfter(60*60);
    
            $today = date('Y-m-d');
            
            if ($this->subdomain){

                $query =   "SELECT t.name, t.slug, COUNT(DISTINCT p.id) as compteur 
                            FROM placetag t, placetag_place tp, place p, city c, borough b, event e
                            WHERE c.id = p.city_id
                            AND b.id = c.borough_id
                            AND e.place_id = p.id
                            AND b.slug = '".$this->subdomain."' ";
            } else {

                $query =   "SELECT t.name, t.slug, COUNT(DISTINCT p.id) as compteur 
                            FROM placetag t, placetag_place tp, place p, event e
                            WHERE e.place_id = p.id ";
            }
            
            $query.=   "AND p.id = tp.place_id
                        AND t.id = tp.placetag_id
                        AND e.date_end >= '$today'
                        GROUP BY t.name
                        ORDER BY compteur DESC";

            $em = $this->doctrine->getManager();
            $statement = $em->getConnection()->prepare($query);

            return $statement->execute()->fetchAll();
        });

    }

    public function getCountsByCity($cache)
    {
        return $cache->get('menu_cities_'.$this->subdomain, function(ItemInterface $item){

            $item->expiresAfter(60*60);
            
            $today = date('Y-m-d');

            if ($this->subdomain){

                $query =   "SELECT c.name, c.slug, COUNT(DISTINCT p.id) as compteur 
                            FROM place p, city c, borough b, event e
                            WHERE p.city_id = c.id
                            AND c.borough_id = b.id
                            AND b.slug = '".$this->subdomain."' ";
            } else {
                $query =   "SELECT c.name, c.slug, COUNT(DISTINCT p.id) as compteur 
                            FROM place p, city c, event e
                            WHERE p.city_id = c.id ";
            }
            
            $query.=   "AND e.place_id = p.id
                        AND e.date_end >= '$today'
                        GROUP BY c.name
                        ORDER BY compteur DESC
                        LIMIT 20";


            $em = $this->doctrine->getManager();
            $statement = $em->getConnection()->prepare($query);
    
            return $statement->execute()->fetchAll();
        });

    }
}