<?php

namespace App\Controller;

use App\Service\Website;
use App\Entity\Subdomain;
use App\Form\SubdomainType;
use App\Repository\BoroughRepository;
use App\Repository\SubdomainRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SubdomainController extends AbstractController
{
    public function __construct(Website $website)
    {
        $this->website = $website->getArray();
        $this->subdomain = $website->getArray()['address']['subdomain'];
    }
    
    /**
     * Permet d'ajouter un sous-domaine
     * 
     * @Route("/configuration/nouveau-site", name="new_subdomain")
     * @Security("is_granted('ROLE_ADMIN')", message="Vous n'avez pas le droit d'ajouter un sous-domaine")
     * @return Response
     */
    public function new(Request $request, EntityManagerInterface $manager)
    {
        $subdomain = new Subdomain;

        $form = $this->createForm(SubdomainType::class, $subdomain);
        $form->handleRequest($request);

        if ($subdomain->getMainBorough() === null){
            $this->addFlash(
                'danger',
                "Veuillez préciser un arrondissement principal !"
            );
        }
        elseif ($form->isSubmitted() && $form->isValid()){

            //dd($subdomain->getMainBorough());
            $subdomain->addBorough($subdomain->getMainBorough());

            $subdomain->setResident([
                'singular' => [
                    'masculine' => null,
                    'feminine' => null
                ],
                'plural' => [
                    'masculine' => null,
                    'feminine' => null
                ]
            ]);

            $subdomain->setData([
                'description'   => null,
                'keywords'      => null
            ]);

            $subdomain->setStatistical([
                'provider'  => null,
                'key'       => null
            ]);

            $subdomain->setNetwork([
                'facebook'  => null,
                'instagram' => null,
                'twitter'   => null,
                'youtube'   => null
            ]);

            $manager->persist($subdomain);
            $manager->flush();

            $this->addFlash(
                'success',
                "Le sous-domaine a été ajouté !"
            );

            return $this->redirectToRoute('configure_subdomain');
        }
        
        return $this->render('subdomain/new.html.twig', [
            'form'          => $form->createView(),
            'website'       => $this->website,
        ]);
    }

    /**
     * Permet de configurer le sous-domaine
     * 
     * @Route("/configuration", name="configure_subdomain")
     * @Security("is_granted('ROLE_ADMIN')", message="Vous n'avez pas le droit de modifier ce sous-domaine")
     * @return Response
     */
    public function edit(SubdomainRepository $repo, Request $request, EntityManagerInterface $manager)
    {
        $subdomain = $repo->findOneBy(['slug'=> $this->subdomain]);

        // Supprimer l'affichage de l'arrondissement principale dans les arrondissements secondaires
        if ($subdomain->getMainBorough() and $subdomain->getBoroughs()->contains($subdomain->getMainBorough())){
            $subdomain->removeBorough($subdomain->getMainBorough());
        }

        $cities = [];
        foreach($subdomain->getMainBorough()->getCity() as $city){
            $cities[] = $city->getName();
        }
        foreach($subdomain->getBoroughs() as $borough){
            foreach($borough->getCity() as $city){
                $cities[] = $city->getName();
            }
        }

        $form = $this->createForm(SubdomainType::class, $subdomain);

        $form->get('resident_singular_masculine')->setData($subdomain->getResident()['singular']['masculine']);
        $form->get('resident_singular_feminine')->setData($subdomain->getResident()['singular']['feminine']);
        $form->get('resident_plural_masculine')->setData($subdomain->getResident()['plural']['masculine']);
        $form->get('resident_plural_feminine')->setData($subdomain->getResident()['plural']['feminine']);

        $form->get('description')->setData($subdomain->getData()['description']);
        $form->get('keywords')->setData($subdomain->getData()['keywords']);

        if ($subdomain->getStatistical()){
            //$form->get('provider')->setData($subdomain->getData()['provider']);
            $form->get('keyInProvider')->setData($subdomain->getStatistical()['key']);
        }

        if ($subdomain->getNetwork()){
            $form->get('facebook')->setData($subdomain->getNetwork()['facebook']);
            $form->get('instagram')->setData($subdomain->getNetwork()['instagram']);
            $form->get('twitter')->setData($subdomain->getNetwork()['twitter']);
            $form->get('youtube')->setData($subdomain->getNetwork()['youtube']);
        }

        //dd($form->get('volunteers')->getData());

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){

            $subdomain->setResident([
                'singular' => [
                    'masculine' => $form->get('resident_singular_masculine')->getData(),
                    'feminine'  => $form->get('resident_singular_feminine')->getData()
                ],
                'plural' => [
                    'masculine' => $form->get('resident_plural_masculine')->getData(),
                    'feminine'  => $form->get('resident_plural_feminine')->getData()
                ]
            ]);

            $subdomain->setData([
                'description'   => $form->get('description')->getData(),
                'keywords'      => $form->get('keywords')->getData()
            ]);

            if (
                $this->getUser() and 
                in_array('ROLE_SUPERADMIN',$this->getUser()->getRoles())
            ){
                $subdomain->setStatistical([
                    //'provider'  => $form->get('provider')->getData(),
                    'provider'  => [
                        'name'  => "Google Analytics",
                        'url'   => "https://analytics.google.com"
                    ],
                    'key'   => $form->get('keyInProvider')->getData()
                ]);
            }

            $subdomain->setNetwork([
                'facebook'  => $form->get('facebook')->getData(),
                'instagram' => $form->get('instagram')->getData(),
                'twitter'   => $form->get('twitter')->getData(),
                'youtube'   => $form->get('youtube')->getData()
            ]);

            /*
            if ($form->get('description')->getData()){
                $meta['description'] = $form->get('description')->getData();
            }
            if ($form->get('keywords')->getData()){
                $meta['keywords'] = $form->get('keywords')->getData();
            }
            $subdomain->setData($meta);
            */

            // Ajouter l'arrondissement principale aux arrondissements secondaires
            if ($subdomain->getMainBorough() and $subdomain->getBoroughs()->contains($subdomain->getMainBorough()) === false){
                $subdomain->addBorough($subdomain->getMainBorough());
            }

            $manager->persist($subdomain);
            $manager->flush();

            $this->addFlash(
                'success',
                "Les modifications ont été effectuées !"
            );

            return $this->redirectToRoute('configure_subdomain');
        }
        
        return $this->renderForm('subdomain/edit.html.twig', [
            'form'          => $form,
            'subdomain'     => $subdomain,
            'cities'        => $cities,
            'website'       => $this->website,
        ]);
    }

    /**
     * Affichage d'une carte de villes
     * 
     * @Route("/configuration/carte", name="city_map")
     * @Security("is_granted('ROLE_ADMIN')", message="Vous n'avez pas le droit de modifier ce festival")
     * @return Response
     */
    public function carteDesVilles(SubdomainRepository $repo, BoroughRepository $repo2)
    {
        $subdomain = $repo->findOneBy(['slug'=> $this->subdomain]);

        $markers = [];
        foreach($subdomain->getMainBorough()->getCity() as $city){
            if ($city->getLatitude() and $city->getLongitude()){
                $markers[] = ["ville1", $city->getName(), $city->getLatitude(), $city->getLongitude(), $city->getName()];
            }
        }
        foreach($subdomain->getBoroughs() as $borough){
            foreach($borough->getCity() as $city){
                if ($city->getLatitude() and $city->getLongitude()){
                    $markers[] = ["ville", $city->getName(), $city->getLatitude(), $city->getLongitude(), $city->getName()];
                }
            }
        }
        $markersForPosition = $markers;

        $boroughs = $repo2->findAll();

        foreach($boroughs as $borough){
            if ($borough->getLatitude() and $borough->getLongitude()){
                $markers[] = ["arrondissement", $borough->getName(), $borough->getLatitude(), $borough->getLongitude(), $borough->getName()];
            }
        }
        
        //$markers[] = ["layer3", "texte 1", 51.7538, -1.25609, "passage souris 1"];
        //$markers[] = ["layer3", "texte 2", 51.752, -1.2577, "passage souris 2"];

        return $this->render('validation/map.html.twig', [
            'page'          => 'place',
            'markers'       => $markers,
            'map'           => $this->getPositionOfMap($markersForPosition, $borough),
            'website'       => $this->website
        ]);
    }


    /**
     * Affichage d'une carte des 350 villes principales
     * 
     * @Route("/configuration/carte-arrondissement", name="borough_map")
     * @Security("is_granted('ROLE_ADMIN')", message="Vous n'avez pas le droit de modifier ce festival")
     * @return Response
     */
    public function mapOfBoroughs(BoroughRepository $repo)
    {
        $boroughs = $repo->findAll();

        $markers = [];
        foreach($boroughs as $borough){
            if ($borough->getLatitude() and $borough->getLongitude()){
                $markers[] = ["layer3", $borough->getName(), $borough->getLatitude(), $borough->getLongitude(), $borough->getName()];
            }
        }
        
        //$markers[] = ["layer3", "texte 1", 51.7538, -1.25609, "passage souris 1"];
        //$markers[] = ["layer3", "texte 2", 51.752, -1.2577, "passage souris 2"];

        return $this->render('validation/map.html.twig', [
            'page'          => 'place',
            'markers'       => $markers,
            'map'           => $this->getPositionOfMap($markers, $borough),
            'website'       => $this->website
        ]);
    }

    public function getPositionOfMap($markers=[], $borough=null)
    {
        if($markers == []){

            if ($borough->getLatitude() and $borough->getLongitude() and $borough->getZoom()){
                $map['center'] = $borough->getLatitude() .",". $borough->getLongitude();
                $map['zoom'] = $borough->getZoom();
            } else {
                $map['center'] = "47, 1.5";
                $map['zoom'] = "5";
            }
        } else {

            //dd($map['events']);
            $latMin = null;
            $latMax = null;
            $lonMin = null;
            $lonMax = null;

            foreach($markers as $marker){
                if ($latMin === null){$latMin = $marker['2'];}
                elseif ($marker['2'] < $latMin){$latMin = $marker['2'];}
                
                if ($latMin === null){$latMin = $marker['2'];}
                elseif ($marker['2'] > $latMax){$latMax = $marker['2'];}
                
                if ($lonMin === null){$lonMin = $marker['3'];}
                elseif ($marker['3'] < $lonMin){$lonMin = $marker['3'];}
                
                if ($lonMax === null){$lonMax = $marker['3'];}
                elseif ($marker['3'] > $lonMax){$lonMax = $marker['3'];}
            }

            $latDif = $latMax - $latMin;
            $lonDif = $lonMax - $lonMin;
            //dd($latDif ." - ". $lonDif);

            if ($latDif < 0.02 and $lonDif < 0.02){$map['zoom'] = "14";}
            elseif ($latDif < 0.04 and $lonDif < 0.04){$map['zoom'] = "13";}
            elseif ($latDif < 0.1 and $lonDif < 0.1){$map['zoom'] = "12";}
            elseif ($latDif < 0.18 and $lonDif < 0.18){$map['zoom'] = "11";}
            elseif ($latDif < 0.35 and $lonDif < 0.35){$map['zoom'] = "10";}
            elseif ($latDif < 0.7 and $lonDif < 0.7){$map['zoom'] = "9";}
            elseif ($latDif < 1.5 and $lonDif < 1.5){$map['zoom'] = "8";}
            elseif ($latDif < 2.3 and $lonDif < 2.3){$map['zoom'] = "7";}
            elseif ($latDif < 4 and $lonDif < 4){$map['zoom'] = "6";}
            elseif ($latDif < 5.5 and $lonDif < 5.5){$map['zoom'] = "5";}
            else{$map['zoom'] = "4";}

            $latMoy = ($latMax + $latMin) / 2;
            $lonMoy = ($lonMax + $lonMin) / 2;

            $map['center'] = $latMoy.",".$lonMoy;
        }

        return $map;

    }
}
