<?php

namespace App\Controller;

use DateTime;
use ZipArchive;
use App\Entity\City;
use App\Entity\Place;
use App\Entity\Rubric;
use App\Service\Website;
use App\Entity\Ticketing;
use Cocur\Slugify\Slugify;
use App\Entity\TicketSearch;
use App\Entity\TicketResource;
use App\Form\TicketSearchType;
use App\Service\UploaderHelper;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Contracts\Cache\ItemInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Contracts\Cache\CacheInterface;
use App\Repository\TicketResourceRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TicketController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager, Website $website, ManagerRegistry $doctrine)
    {
        $this->entityManager = $entityManager;
        $this->website = $website->getArray();
        $this->subdomain = $website->getArray()['address']['subdomain'];
        $this->doctrine = $doctrine;
    }
    
    /**
     * @Route("/billetterie/synchronisation", name="ticket_index")
     */
    public function index(Request $request, TicketResourceRepository $repo, CacheInterface $cache)
    {
        $ticketing_by_name = [];
        $ticketings = $this->doctrine->getRepository(Ticketing::class)->findAll();
        foreach ($ticketings as $ticketing){
            $ticketing_by_name[$ticketing->getName()] = $ticketing;
        }

        /*
        $ticketing_by_name = $cache->get('cache_ticketings', function(ItemInterface $item){
            $item->expiresAfter(60*59);
            $ticketings = $this->doctrine->getRepository(Ticketing::class)->findAll();
            foreach ($ticketings as $ticketing){
                $ticketing_by_name[$ticketing->getName()] = $ticketing;
            }
            return $ticketing_by_name;
        });
        */
        
        $count = ["new" => 0, "update" => 0, "all" => 0 ];

        if ($request->query->get('flux'))
        {
            ini_set("memory_limit", "-1");
            $slugify = new Slugify();

            $rubric_by_slug = [];
            $rubrics = $this->doctrine->getRepository(Rubric::class)->findAll();
            foreach ($rubrics as $rubric){
                $rubric_by_slug[$rubric->getSlug()] = $rubric;
            }
/*
            $place_by_slug = [];
            $places = $this->doctrine->getRepository(Place::class)->findAll();
            foreach ($places as $place){
                $place_by_slug[$slugify->slugify($place->getName()) . "-" . $place->getCity()->getSlug()] = $place;
                $place_by_slug[$slugify->slugify($place->getTitle()) . "-" . $place->getCity()->getSlug()] = $place;
            }

            $city_by_slug = [];
            $cities = $this->doctrine->getRepository(City::class)->findAll();
            foreach ($cities as $city){
                $city_by_slug[$city->getSlug()] = $city;
            }

            /*
            $rubric_by_slug = $cache->get('cache_rubrics', function(ItemInterface $item){
                $item->expiresAfter(60*59);
                $rubrics = $this->doctrine->getRepository(Rubric::class)->findAll();
                foreach ($rubrics as $rubric){
                    $rubric_by_slug[$rubric->getSlug()] = $rubric;
                }
                return $rubric_by_slug;
            });

            $place_by_slug = $cache->get('cache_places', function(ItemInterface $item){
                $item->expiresAfter(60*59);
                $places = $this->doctrine->getRepository(Place::class)->findAll();
                $slugify = new Slugify();
                foreach ($places as $place){
                    $place_by_slug[$slugify->slugify($place->getName()) . "-" . $place->getCity()->getSlug()] = $place;
                    $place_by_slug[$slugify->slugify($place->getTitle()) . "-" . $place->getCity()->getSlug()] = $place;
                }
                return $place_by_slug;
            });

            $city_by_slug = $cache->get('cache_cities', function(ItemInterface $item){
                $item->expiresAfter(60*59);
                $cities = $this->doctrine->getRepository(City::class)->findAll();
                foreach ($cities as $city){
                    $city_by_slug[$city->getSlug()] = $city;
                }
                return $city_by_slug;
            });
            */

            $resources = $this->doctrine->getRepository(TicketResource::class)->findAll();
            foreach ($resources as $resource){$resource_by_id[$resource->getIdResource()] = $resource;}
            
            if ($request->query->get('flux') == "Ticketmaster")
            {
                $flux = $ticketing_by_name["Ticketmaster"];
                //dd($flux);

                //$json = file_get_contents('uploads/data/ticketmaster.json');

                $page = $request->query->getInt('page');
                
                $departements = ["51","52","54","55","57","67","68","88"];
                if ($request->query->get('dep')){
                    $dep = $request->query->get('dep');
                    $next = $dep + 1;
                } else {
                    $dep = 0;
                    $next = 1;
                }


                $json = file_get_contents('https://app.ticketmaster.com/discovery/v2/events?apikey=7VDvqykz9BxdfkVyb7eVxESribUANcpU&locale=fr-FR&countryCode=FR&locale=fr-FR&size=100&stateCode='.$departements[$dep].'&page='.$page); 
                $array = json_decode($json, TRUE);

                //dd($array);

                foreach($array['_embedded']['events'] as $data){

                    //dd($data);

                    if (isset($resource_by_id[$data['id']])){
                        $ticket = $resource_by_id[$data['id']];
                        $count["update"]++;
                    } else {
                        $ticket = new TicketResource();
                        $count["new"]++;
                    }
                    $count["all"]++;

                    //$dateStart = DateTime::createFromFormat('d/m/Y', $brut[1])->format('Y-m-d');
                    $dateStart = $data['dates']['start']['localDate'];
                    
                    /*
                    if ($data['spanMultipleDays']){
                        dump($data['spanMultipleDays']);
                    } else {
                        
                    }
                    */

                    $dateEnd = $dateStart;

                    $timeStart = $data['dates']['start']['localTime'];

                    $dataPlace = $data['_embedded']['venues'][0];

                    if (isset($dataPlace['location']['latitude'])){$latitude = $dataPlace['location']['latitude'];} else {$latitude = null;}
                    if (isset($dataPlace['location']['longitude'])){$longitude = $dataPlace['location']['longitude'];} else {$longitude = null;}

                    $placeResource = [
                        'id'        => $dataPlace['id'],
                        'name'      => $dataPlace['name'],
                        'address'   => $dataPlace['address']['line1'],
                        'postal'    => $dataPlace['postalCode'],
                        'city'      => $dataPlace['city']['name'],
                        'country'   => $dataPlace['country']['countryCode'],
                        'latitude'  => $latitude,
                        'longitude' => $longitude
                    ];
                    /*
                    foreach ($places as $placeBDD){
                        if (
                            (
                                empty($placeResource['city']) or 
                                strpos($placeBDD->getCity()->getSlug(),$slugify->slugify($placeResource['city'])) !== false
                            ) and (
                                strpos( $placeBDD->getSlug(), $slugify->slugify($placeResource['name']) ) !== false or 
                                strpos( $slugify->slugify(implode($placeBDD->getMultiTitles())), $slugify->slugify($placeResource['name']) ) !== false 
                            )
                        ){
                            $ticket->setPlace($placeBDD);
                            break;
                        }
                    }
                    if (isset($city_by_slug[$slugify->slugify($placeResource['city'])])){
                        $ticket->setCity($city_by_slug[$slugify->slugify($placeResource['city'])]);
                    }
                    */

                    $ticket ->setTitle($data['name'])
                            ->setIdResource($data['id'])
                            ->setDateStart(new DateTime($dateStart))
                            ->setDateEnd(new DateTime($dateEnd))
                            ->setTimeStart(new DateTime($timeStart))
                            ->setPlaceResource($placeResource)
                            ->setTicketing($ticketing_by_name["Ticketmaster"])
                            //->setCoverImage($data['images'][0]['url'])
                            ->setLinkAffiliate($data['url'])
                            ;
                        
                        
                    if (isset($data['description'])){
                        $ticket->setDescription($data['description']);
                        //$ticket->setDescription(utf8_decode($data['description']));
                        //$ticket->setDescription(html_entity_decode(mb_convert_encoding($data['description'],'HTML-ENTITIES')));
                    }
                    
                    if (isset($data['priceRanges'])){
                        $ticket ->setPrice($data['priceRanges'][0]['min'])
                                ->setCurrency($data['priceRanges'][0]['currency']);
                    }

                    $this->entityManager->persist($ticket);
                }

                $this->entityManager->flush();

                if(isset($array['_links']['next'])){
                    $page++;
                    return $this->redirectToRoute('ticket_index', ['flux' => $request->query->get('flux'), 'page' => $page, 'dep' => $dep]);
                } 
                elseif(isset($departements[$next])){
                    return $this->redirectToRoute('ticket_index', ['flux' => $request->query->get('flux'),'dep' => $next]);
                }

                /*
                $nbPerPage = 10;
                
                if ($request->query->get('page')){
                    $page = $request->query->get('page');
                } else {
                    $page = 0;
                }
                
                for ($i = ($page * $nbPerPage); $i < (($page + 1) * $nbPerPage) and isset($xml->product[$i]); $i++) 
                {

                }
                
                if ($count["all"] == $nbPerPage){
                    $page++;
                    return $this->redirectToRoute('ticket_index', ['flux' => $request->query->get('flux'),'page' => $page]);
                }
                */

            }
            elseif ($request->query->get('flux') == "Carrefour")
            {
                $xml = simplexml_load_file('uploads/data/carrefour.xml');
                /*
                $xml = $cache->get('flux_carrefour', function(ItemInterface $item){
                    $item->expiresAfter(60*45);
                    return simplexml_load_file('uploads/data/carrefour.xml');
                });
                */

                $nbPerPage = 1000;
                
                if ($request->query->get('page')){
                    $page = $request->query->get('page');
                } else {
                    $page = 0;
                }
                for ($i = ($page * $nbPerPage); $i < (($page + 1) * $nbPerPage) and isset($xml->product[$i]); $i++) {
                //for ($i = 0; isset($xml->product[$i]); $i++) {
                //for ($i = 0; $i < 20; $i++) {
                    $data = $xml->product[$i];

                    if (isset($resource_by_id[$data->aw_product_id->__toString()])){
                        $ticket = $resource_by_id[$data->aw_product_id->__toString()];
                        $count["update"]++;
                    } else {
                        $ticket = new TicketResource();
                        $count["new"]++;
                    }
                    $count["all"]++;

                    $brut = $data->product_short_description->__toString();
                    $brut = str_replace(" le ", "§", $brut);
                    $brut = str_replace(" du ", "§", $brut);
                    $brut = str_replace(" au ", "§", $brut);
                    $brut = explode("§", $brut);

                    $dateStart = DateTime::createFromFormat('d/m/Y', $brut[1])->format('Y-m-d');
                    if (isset($brut[2])){
                        $dateEnd = DateTime::createFromFormat('d/m/Y', $brut[2])->format('Y-m-d');
                    } else {
                        $dateEnd = $dateStart;
                    }

                    $place = explode(",", $brut[0]);
                    if (isset($place[1]) && trim($place[1]) != ""){
                        $city = $place[1];
                    } else {
                        $city = null;
                    }

                    $placeResource = [
                        'id'        => null,
                        'name'      => trim($place[0]),
                        'address'   => null,
                        'postal'    => null,
                        'city'      => trim($city),
                        'country'   => null,
                        'latitude'  => null,
                        'longitude' => null
                    ];

                    /*
                    if (isset($place_by_slug[$slugify->slugify($placeResource['name']) . "-" . $slugify->slugify($placeResource['city'])])){
                        $ticket->setPlace($place_by_slug[$slugify->slugify($placeResource['name']) . "-" . $slugify->slugify($placeResource['city'])]);
                    }
                    

                    foreach ($places as $placeBDD){
                        if (
                            (
                                empty($placeResource['city']) or 
                                strpos($placeBDD->getCity()->getSlug(),$slugify->slugify($placeResource['city'])) !== false
                            ) and (
                                strpos( $placeBDD->getSlug(), $slugify->slugify($placeResource['name']) ) !== false or 
                                strpos( $slugify->slugify(implode($placeBDD->getMultiTitles())), $slugify->slugify($placeResource['name']) ) !== false 
                            )
                        ){
                            $ticket->setPlace($placeBDD);
                            //break;
                        }
                    }

                    if (isset($city_by_slug[$slugify->slugify($placeResource['city'])])){
                        $ticket->setCity($city_by_slug[$slugify->slugify($placeResource['city'])]);
                    }
                    */
                    
                    $tagGenre = explode("-->", $data->genre);
                    if (strpos($tagGenre[0],"Théâtre et Humour") !== false){$ticket->setRubric($rubric_by_slug["spectacle"]);}
                    elseif (strpos($tagGenre[0],"Musique") !== false){$ticket->setRubric($rubric_by_slug["concert"]);}

                    $ticket ->setTitle($data->product_name->__toString())
                            ->setIdResource($data->aw_product_id->__toString())
                            ->setDateStart(new DateTime($dateStart))
                            ->setDateEnd(new DateTime($dateEnd))
                            ->setPlaceResource($placeResource)
                            ->setTicketing($ticketing_by_name["Carrefour"])
                            ->setCoverImage($data->merchant_image_url)
                            ->setPrice($data->search_price)
                            ->setCurrency($data->currency)
                            ->setDescription(html_entity_decode(mb_convert_encoding($data->description->__toString(),'HTML-ENTITIES')))
                            ->setLinkAffiliate($data->aw_deep_link)
                            ;

                    $this->entityManager->persist($ticket);
                }

                
                $this->entityManager->flush();

                if ($count["all"] == $nbPerPage){
                    $page++;
                    return $this->redirectToRoute('ticket_index', ['flux' => $request->query->get('flux'),'page' => $page]);
                }       
            }
            elseif ($request->query->get('flux') == "Digitick")
            {
                $xml = simplexml_load_file('uploads/data/digitick.xml');
                
                /*
                $xml = $cache->get('flux_digitick', function(ItemInterface $item){
                    $item->expiresAfter(60*50);
                    return simplexml_load_file('uploads/data/digitick.xml');
                });
                */

                $nbPerPage = 100;
                
                if ($request->query->get('page')){
                    $page = $request->query->get('page');
                } else {
                    $page = 0;
                }
                
                for ($i = ($page * $nbPerPage); $i < (($page + 1) * $nbPerPage) and isset($xml->product[$i]); $i++) {
                //for ($i = 0; isset($xml->product[$i]); $i++) {
                //for ($i = 200; $i < 400; $i++) {

                    $data = $xml->product[$i];
                    if (isset($resource_by_id[$data->aw_product_id->__toString()])){
                        $ticket = $resource_by_id[$data->aw_product_id->__toString()];
                        $count["update"]++;
                    } else {
                        $ticket = new TicketResource();
                        $count["new"]++;
                    }
                    $count["all"]++;

                    $wordsAddress = explode(" ",$data->venue_address->__toString());
                    foreach($wordsAddress as $word){
                        if (is_numeric($word) and $word>=1000){$postal = $word;}
                    }
                    if(isset($postal)){
                        $tabAddress = explode($postal,$data->venue_address->__toString());
                        //dump($tabAddress);
                        if (isset($tabAddress[0])){$address = trim($tabAddress[0]);} else {$address = null;}
                        if (isset($tabAddress[1])){$city = trim($tabAddress[1]);} else {$city = null;}
                    } else {
                        $postal = null;
                        $address = null;
                        $city = null;
                    }

                    $placeResource = [
                        'id'        => null,
                        'name'      => $data->venue_name->__toString(),
                        'address'   => $address,
                        'postal'    => $postal,
                        'city'      => $city,
                        'country'   => null,
                        'latitude'  => $data->latitude->__toString(),
                        'longitude' => $data->longitude->__toString()
                    ];
                    
                    /*
                    foreach ($places as $placeBDD){
                        if (
                            (
                                empty($placeResource['city']) or 
                                strpos($placeBDD->getCity()->getSlug(),$slugify->slugify($placeResource['city'])) !== false
                            ) and (
                                strpos( $placeBDD->getSlug(), $slugify->slugify($placeResource['name']) ) !== false or 
                                strpos( $slugify->slugify(implode($placeBDD->getMultiTitles())), $slugify->slugify($placeResource['name']) ) !== false 
                            )
                        ){
                            $ticket->setPlace($placeBDD);
                            break;
                        }
                    }

                    if (isset($city_by_slug[$slugify->slugify($placeResource['city'])])){
                        $ticket->setCity($city_by_slug[$slugify->slugify($placeResource['city'])]);
                    }
                    */

                    $ticket ->setTitle($data->event_name)
                            ->setIdResource($data->aw_product_id)
                            ->setDateStart(new DateTime($data->event_date))
                            ->setDateEnd(new DateTime($data->event_date))
                            //->setTimeStart(new DateTime())
                            ->setPlaceResource($placeResource)
                            ->setTicketing($ticketing_by_name["Digitick"])
                            //->setMultiDateTime()
                            //->setCoverImage($data->aw_image_url)
                            ->setPrice($data->search_price)
                            ->setCurrency($data->currency)
                            ->setDescription(html_entity_decode(mb_convert_encoding($data->description->__toString(),'HTML-ENTITIES')))
                            //->setDescriptionShort($data->product_short_description->__toString())
                            ->setLinkAffiliate($data->aw_deep_link)
                            ->setKeywords(explode(',',$data->genre))
                            ;
                        
                    //dd($ticket);
                    $this->entityManager->persist($ticket);
                }       

                $this->entityManager->flush();

                if ($count["all"] == $nbPerPage){
                    $page++;
                    return $this->redirectToRoute('ticket_index', ['flux' => $request->query->get('flux'),'page' => $page]);
                }    
            }
            elseif ($request->query->get('flux') == "Fnac")
            {
                // Téléchargement et mise en cache du flux
                $tab = $cache->get('flux_fnac', function(ItemInterface $item){
                    $item->expiresAfter(60*55);
                    $xml = simplexml_load_file('uploads/data/fnac.xml');
                    $array = [];
                    foreach ($xml as $data){

                        $multiDates = explode(";",$data->custom_1->__toString());
                        $tabMultiDateTime = [];
                        foreach($multiDates as $multiDate){
                            if ($multiDate != ""){
                                $tabMultiDateTime[] = DateTime::createFromFormat('d/m/Y H:i', $multiDate)->format('Y-m-d H:i');
                            }
                        }

                        $array[] = [
                            "id"        => $data->aw_product_id->__toString(),
                            "title"     => $data->product_name->__toString(),
                            "dateStart" => new DateTime(current($tabMultiDateTime)),
                            "dateEnd"   => new DateTime(end($tabMultiDateTime)),
                            "multiDate" => $tabMultiDateTime,
                            "cover"     => $data->merchant_image_url->__toString(),
                            "price"     => $data->search_price->__toString(),
                            "currency"  => $data->currency->__toString(),
                            "text"      => html_entity_decode(mb_convert_encoding($data->description->__toString(),'HTML-ENTITIES')),
                            "info"      => html_entity_decode(mb_convert_encoding($data->product_short_description->__toString(),'HTML-ENTITIES')),
                            "link"      => $data->aw_deep_link->__toString(),
                            "category"  => $data->merchant_category->__toString(),
                            "place"     => [
                                'id'        => $data->custom_5->__toString(),
                                'name'      => $data->custom_2->__toString(),
                                'address'   => $data->custom_6->__toString(),
                                'postal'    => $data->custom_4->__toString(),
                                'city'      => $data->venue_address->__toString(),
                                'country'   => $data->custom_3->__toString(),
                                'latitude'  => $data->latitude->__toString(),
                                'longitude' => $data->longitude->__toString()
                            ]
                        ];
                        
                    }
                    return $array;
                });
                //dd($tab);

                // Traitement du tableau
                $nbPerPage = 100;
                $page = $request->query->getInt('page');
                
                for ($i = ($page * $nbPerPage); $i < (($page + 1) * $nbPerPage) and isset($tab[$i]); $i++) {

                    $event = $tab[$i];
                    
                    if (isset($resource_by_id[$event['id']])){
                        $ticket = $resource_by_id[$event['id']];
                        $count["update"]++;
                    } else {
                        $ticket = new TicketResource();
                        $count["new"]++;
                    }
                    $count["all"]++;

                    $ticket ->setIdResource($event['id'])
                            ->setTitle($event['title'])
                            ->setDateStart($event['dateStart'])
                            ->setDateEnd($event['dateEnd'])
                            ->setTimeStart($event['dateStart'])
                            ->setPlaceResource($event['place'])
                            ->setMultiDateTime($event['multiDate'])
                            ->setCoverImage($event['cover'])
                            ->setPrice($event['price'])
                            ->setCurrency($event['currency'])
                            ->setDescription($event['text'])
                            ->setDescriptionShort($event['info'])
                            ->setLinkAffiliate($event['link'])
                            ->setTicketing($ticketing_by_name["Fnac"])
                    ;

                    //dd($ticket);
                    foreach ($this->getTabCatFnac() as $key => $value)
                    {
                        if ($key == $event['category'])
                        {
                            $rubric = $rubric_by_slug[$value['rubric']];
                            $ticket->setRubric($rubric);
                            $ticket->setKeywords(explode(',',$value['name']));
                        }
                    }
                    $this->entityManager->persist($ticket);
                }


/*


                //$xml = simplexml_load_file('http://affiliation.curieux.net/telechargement/data/fnac.xml');
                $xml = simplexml_load_file('uploads/data/fnac.xml');


                $nbPerPage = 1000;
                
                if ($request->query->get('page')){
                    $page = $request->query->get('page');
                } else {
                    $page = 0;
                }
                
                for ($i = ($page * $nbPerPage); $i < (($page + 1) * $nbPerPage) and isset($xml->product[$i]); $i++) {
                //for ($i = 0; $i < 200; $i++) {
                //for ($i = 0; isset($xml->product[$i]); $i++) {

                    $data = $xml->product[$i];

                    //dd($data->search_price);
                    $placeResource = [
                        'id'        => $data->custom_5->__toString(),
                        'name'      => $data->custom_2->__toString(),
                        'address'   => $data->custom_6->__toString(),
                        'postal'    => $data->custom_4->__toString(),
                        'city'      => $data->venue_address->__toString(),
                        'country'   => $data->custom_3->__toString(),
                        'latitude'  => $data->latitude->__toString(),
                        'longitude' => $data->longitude->__toString(),
                    ];

                    // $data->custom_5->__toString() // STJU2
                    //dump($data);
                    
                    if (isset($resource_by_id[$data->aw_product_id->__toString()])){
                        $ticket = $resource_by_id[$data->aw_product_id->__toString()];
                        $count["update"]++;
                    } else {
                        $ticket = new TicketResource();
                        $count["new"]++;
                    }
                    $count["all"]++;
                    
                    
                    //if (isset($place_by_slug[$slugify->slugify($data->custom_2->__toString()) . "-" . $slugify->slugify($data->venue_address->__toString())])){
                    //    $ticket->setPlace($place_by_slug[$slugify->slugify($data->custom_2->__toString()) . "-" . $slugify->slugify($data->venue_address->__toString())]);
                    //}
                    


                    $multiDates = explode(";",$data->custom_1->__toString());
                    $tabMultiDateTime = [];
                    foreach($multiDates as $multiDate){
                        if ($multiDate != ""){
                            $tabMultiDateTime[] = DateTime::createFromFormat('d/m/Y H:i', $multiDate)->format('Y-m-d H:i');
                        }
                    }
                    
                    $ticket ->setTitle($data->product_name->__toString())
                            ->setIdResource($data->aw_product_id->__toString())
                            ->setDateStart(new DateTime(current($tabMultiDateTime)))
                            ->setDateEnd(new DateTime(end($tabMultiDateTime)))
                            ->setTimeStart(new DateTime(current($tabMultiDateTime)))
                            ->setPlaceResource($placeResource)
                            ->setTicketing($ticketing_by_name["Fnac"])
                            ->setMultiDateTime($tabMultiDateTime)
                            ->setCoverImage($data->merchant_image_url)
                            ->setPrice($data->search_price)
                            ->setCurrency($data->currency)
                            ->setDescription(html_entity_decode(mb_convert_encoding($data->description->__toString(),'HTML-ENTITIES')))
                            ->setDescriptionShort(html_entity_decode(mb_convert_encoding($data->product_short_description->__toString(),'HTML-ENTITIES')))
                            ->setLinkAffiliate($data->aw_deep_link)
                    ;

                    */
                    /*
                    // Recherche d'un ville en BDD 
                    if (isset($data->venue_address) and isset($city_by_slug[$slugify->slugify($data->venue_address->__toString())])){
                        $city = $city_by_slug[$slugify->slugify($data->venue_address->__toString())];

                        $ticket->setCity($city);
                        //$this->entityManager->persist($city);
                    }

                    // Recherche d'un lieu en BDD
                    foreach ($place_by_slug as $key => $placeBDD){
                        if (
                            (
                                empty($data->venue_address->__toString()) or 
                                strpos($placeBDD->getCity()->getSlug(),$slugify->slugify($data->venue_address->__toString())) !== false
                            ) and (
                                strpos( $placeBDD->getSlug(), $slugify->slugify($data->custom_2->__toString()) ) !== false or 
                                strpos( $slugify->slugify(implode($placeBDD->getMultiTitles())), $slugify->slugify($data->custom_2->__toString()) ) !== false 
                            )
                        ){
                            $ticket->setPlace($placeBDD);
                            break;
                        }
                    }
                    */
                    /*
                    foreach ($this->getTabCatFnac() as $key => $value)
                    {
                        if ($key == $data->merchant_category)
                        {
                            $rubric = $rubric_by_slug[$value['rubric']];
                            $ticket->setRubric($rubric);
                            $ticket->setKeywords(explode(',',$value['name']));
                            //$this->entityManager->persist($rubric);
                        }
                    }
                    
                    $this->entityManager->persist($ticket);
                }*/
                //dd($ticket); 
                
                $this->entityManager->flush();
                

                if ($count["all"] == $nbPerPage){
                    dd($nbPerPage);
                    $page++;
                    //return $this->redirect('?flux='.$request->query->get('flux').'&page='.$page);
                    return $this->redirectToRoute('ticket_index', ['flux' => $request->query->get('flux'),'page' => $page]);
                }
                
                /**/
                //if ($count["update"] != 0 and $count["new"] != 0 and $request->query->get('page') != 5){}
                    
                

            }
        } 

        $count['withoutCity'] = $repo->findWithoutLocation('city', true)->getSingleScalarResult();;
        $count['withoutPlace'] = $repo->findWithoutLocation('place', true)->getSingleScalarResult();;
        
        return $this->render('ticket/index.html.twig', [
            'website'       => $this->website,
            'ticketings'    => $ticketing_by_name,
            'count'         => $count
        ]);
    }

    /**
     * @Route("/billetterie/fusion/{text}", name="ticket_fusion")
     */
    public function fusion($text, Request $request, TicketResourceRepository $repo)
    {
        ini_set("memory_limit", "-1");
        $page = $request->query->getInt('page');
        $slugify = new Slugify();
        $nbPerPage = 1000;

        //if ($text == "ville"){
            //$cities = $this->doctrine->getRepository(City::class)->findAll();
            /*
            $resources = $repo->findWithoutLocation('city')->execute();

            $slugify = new Slugify();
            
            foreach ($resources as $resource){

                //dd($resource->getPlaceResource());

                if ($resource->getPlaceResource()){
                    $resourceCity = $slugify->slugify($resource->getPlaceResource()["city"]);
                    $resourcePostal = $resource->getPlaceResource()["postal"];

                    foreach ($cities as $city){
                        if(
                            $resourceCity == $city->getSlug() and (
                                $resourcePostal and
                                $city->getPostcodes()->contains($resourcePostal)
                            )
                        ){
                            $resource->getCity($city);
                            $this->entityManager->persist($resource);
                            break;
                        }
                    }

                    $this->entityManager->flush();
                }
            }*/

            

            $resources = $repo->findWithoutLocation($text, null, $page)->execute();
            //dd($resources);

            for ($i = ($page * $nbPerPage); $i < (($page + 1) * $nbPerPage) and isset($resources[$i]); $i++) {
                //dd($resources[$i]);
                $resource = $resources[$i];

                $slugCity = $slugify->slugify($resource->getPlaceResource()["city"]);
                $slugPlace = $slugify->slugify($resource->getPlaceResource()["name"]);
                $postcode = $resource->getPlaceResource()["postal"];

                //dd($slugPlace);

                if ($text == "city"){
                    $city = $this->doctrine->getRepository(City::class)->searchCity($slugCity,$postcode);
                    
                    if ($city){
                        $resource->setCity($city[0]);
                        $this->entityManager->persist($resource);
                    }
                }
                elseif ($text == "place"){
                    $place = $this->doctrine->getRepository(Place::class)->searchPlace($slugPlace,$slugCity);
                    
                    if ($place){
                        $resource->setPlace($place[0]);
                        $this->entityManager->persist($resource);
                    }
                }

                $this->entityManager->flush();
            }

            /*
            $j=0;
            foreach ($resources as $resource){

                $slug = $slugify->slugify($resource->getPlaceResource()["city"]);
                $postcode = $resource->getPlaceResource()["postal"];

                $city = $this->doctrine->getRepository(City::class)->searchCity($slug,$postcode);
                
                if ($city){
                    //dd($city);
                    $resource->setCity($city[0]);
                    $this->entityManager->persist($resource);
                    $j++;
                }

                if ($j > 1000){
                    $this->entityManager->flush();
                    return $this->redirectToRoute('ticket_index');
                }
            }*/
            $this->entityManager->flush();

            if ($i == 1000){
                return $this->redirectToRoute('ticket_fusion', ["text"=>$text, "page"=>$i++]);
            } else {
                return $this->redirectToRoute('ticket_index');
            }
        //}
        //elseif ($text == "lieu"){
            /*
            $places = $this->doctrine->getRepository(Place::class)->findAll();
            $resources = $repo->findWithoutLocation('place')->execute();
            foreach ($resources as $resource){
                dd($resource);
            }
            */
            

            $this->entityManager->flush();

        return $this->redirectToRoute('ticket_index');

    }

    /**
     * Recherche personnalisée avec plusieurs filtres
     * 
     * @Route("/billetterie", name="ticket_by_filter")
     * @return Response
     */
    public function search(TicketResourceRepository $repo, PaginatorInterface $paginator, Request $request)
    {

        $search = new TicketSearch;
        $form = $this->createForm(TicketSearchType::class,$search);

        //dd($form);

        $form->handleRequest($request);
        
        $pagination = $paginator->paginate($repo->findAllSearchQuery($search, $this->website['address']['subdomain']), $request->query->getInt('page', 1), 100 /*limite par page*/);

        $tickets = $pagination->getItems();

        $fusion = [];
        $title = null;
        $dateStart = null;
        $dateEnd = null;
        $i = 0;
        foreach ($tickets as $ticket){

            if (
                $title == $ticket->getTitle() and 
                $dateStart == $ticket->getDateStart() and 
                $dateEnd == $ticket->getDateEnd()
            ){} 
            else {
                $i++;
            }
            
            $title = $ticket->getTitle();
            $dateStart = $ticket->getDateStart();
            $dateEnd = $ticket->getDateEnd();

            $fusion[$i]['event'] = $ticket;
            $fusion[$i]['ticketings'][] = [
                'name' => $ticket->getTicketing()->getName(),
                'link' => $ticket->getLinkAffiliate(),
                'id' => $ticket->getId()
            ];

        }

        //dd($fusion);

        $pageTitle = "Le flux des billetteries (".$pagination->getTotalItemCount().")";

        return $this->render('ticket/search.html.twig', [
            'pagination'        => $pagination,
            'events'            => $fusion,
            'website'           => $this->website,
            'form'              => $form->createView(),
            'title'             => $pageTitle
        ]);
    }

    /**
     * @Route("/billetterie/telechargement", name="ticket_download")
     */
    public function download(Request $request, UploaderHelper $uploaderHelper)
    {
        $ticketings = $this->doctrine->getRepository(Ticketing::class)->findAll();

        foreach($ticketings as $ticketing){
            if ($ticketing->getId() == $request->query->get('id')){

                $file = file_get_contents($ticketing->getSource());
                $destination = 'uploads/data/'. $ticketing->getLinked() .'.zip';
                file_put_contents($destination, $file);

                $zipArchive = new ZipArchive();
                $result = $zipArchive->open('uploads/data/'. $ticketing->getLinked() .'.zip');
                if ($result === TRUE) {
                    $zipArchive ->renameName($zipArchive->getNameIndex(0),$ticketing->getLinked());
                    $zipArchive ->extractTo('uploads/data',$ticketing->getLinked());
                    $zipArchive ->close();
                    $this->addFlash(
                        'success',
                        "Télechargement et décompression effectués avec succès !"
                    );
                } else {
                    $this->addFlash(
                        'warning',
                        "Une erreur est survenu !"
                    );
                }
            }
        }

        return $this->render('ticket/download.html.twig', [
            'website'       => $this->website,
            'ticketings'    => $ticketings
        ]);

        /*
        $links = [
            "fnac"          => 'https://productdata.awin.com/datafeed/download/apikey/83b88e251508aaadf5877111cb1c5a32/language/fr/fid/23455/columns/aw_product_id,product_name,search_price,currency,alternate_image,aw_deep_link,custom_1,custom_2,custom_3,custom_4,custom_5,custom_6,Tickets%3Avenue_address,Tickets%3Alongitude,Tickets%3Alatitude,product_short_description,description,Tickets%3Agenre,merchant_product_id,merchant_image_url,merchant_category/format/xml-tree/compression/zip/adultcontent/1/',
            "ticketmaster"  => 'https://app.ticketmaster.com/discovery/v2/events?apikey=7VDvqykz9BxdfkVyb7eVxESribUANcpU&locale=fr-FR&countryCode=FR', // &city=strasbourg&size=50&page=1
            "carrefour"     => "https://productdata.awin.com/datafeed/download/apikey/83b88e251508aaadf5877111cb1c5a32/language/fr/fid/29961/columns/product_name,product_short_description,description,merchant_image_url,search_price,currency,aw_thumb_url,alternate_image_three,Tickets%3Agenre,merchant_product_id,aw_product_id,merchant_category,merchant_deep_link,aw_deep_link/format/xml-tree/compression/zip/adultcontent/1/",
            "digitick"      => 'https://productdata.awin.com/datafeed/download/apikey/83b88e251508aaadf5877111cb1c5a32/language/fr/fid/22739/columns/aw_deep_link,product_name,aw_product_id,merchant_product_id,merchant_image_url,description,merchant_category,search_price,merchant_name,merchant_id,category_name,aw_image_url,currency,store_price,delivery_cost,merchant_deep_link,last_updated,display_price,data_feed_id,in_stock,is_for_sale,merchant_thumb_url,Tickets%3Aevent_date,Tickets%3Aevent_name,Tickets%3Avenue_name,Tickets%3Avenue_address,Tickets%3Aavailable_from,Tickets%3Agenre,Tickets%3Amin_price,Tickets%3Alongitude,Tickets%3Alatitude,Tickets%3Aevent_location_address,Tickets%3Aevent_location_city,Tickets%3Aevent_location_country,Tickets%3Aevent_duration,Tickets%3Amax_price,pre_order,web_offer,valid_to/format/xml-tree/compression/zip/adultcontent/1/',
            "ticketac"      => 'http://adxml.publicidees.com/xml.php?progid=2986&partid=51257',
            "datatourisme"  => 'https://diffuseur.datatourisme.gouv.fr/webservice/ce859609d795085d9dc6400859cdaef4/3d884177-7469-44a1-a3f4-f64f1597c46a'    
        ];

        if (array_key_exists($request->query->get('flux'), $links)){

            $dataUrl = $links[$request->query->get('flux')];


            if ($request->query->get('flux') == "fnac"){

                //if ($URL_page==""){$URL_page="1";}
                $xml = simplexml_load_file($dataUrl);
                //$xml = new SimpleXMLElement($dataUrl); 
 
            }

            $uploadedFile = simplexml_load_file($dataUrl);

            $newFilename = $uploaderHelper->uploadTicketData($uploadedFile);
            dd($newFilename);



            if ($flux=="datatourisme"){$format="rdf";}
            elseif ($flux=="ticketmaster" || $flux=="fnac" || $flux=="digitick" || $flux=="carrefour"){$format="zip";}
            else {$format="xml";}
            $fichierCopie2="data/$flux.$format";
            
            $fichierErreur="alerte/erreur$flux.txt";
            $texteErreur="Derniere erreur le : ". date('Y-m-d') ." - en Détail : ". date('c');

           
            
            if ($URL_decompression=="oui" && ($flux=="ticketmaster" || $flux=="fnac" || $flux=="carrefour" || $flux=="digitick" )){
                    echo "Si vous avez patienté 5 minutes, la décompression est effectuée sinon cliquer <a href='decompression.php?flux=ticketmaster' >ici</a>
                    <br><br>Vous devez mettre à jour les fichiers <b>« simplifiés »</b> :
                    <a href='simplificationTM.php?x=1' target='_blank' >1</a>
                    <a href='simplificationTM.php?x=2' target='_blank' >2</a>
                    <a href='simplificationTM.php?x=3' target='_blank' >3</a>
                    
                    <br><u>Option non exploitée :</u> Vous pouvez mettre à jour les fichiers <b>« fragmentés »</b> :
                    <a href='simplification.php?flux=ticketmaster&x=1' target='_blank' >1</a>
                    <a href='simplification.php?flux=ticketmaster&x=2' target='_blank' >2</a>
                    <a href='simplification.php?flux=ticketmaster&x=3' target='_blank' >3</a>
                    <a href='simplification.php?flux=ticketmaster&x=4' target='_blank' >4</a>
                    <a href='simplification.php?flux=ticketmaster&x=5' target='_blank' >5</a>
                    <a href='simplification.php?flux=ticketmaster&x=6' target='_blank' >6</a>
                    <a href='simplification.php?flux=ticketmaster&x=7' target='_blank' >7</a>
                    <a href='simplification.php?flux=ticketmaster&x=8' target='_blank' >8</a>
                    <a href='simplification.php?flux=ticketmaster&x=9' target='_blank' >9</a>
                    <a href='simplification.php?flux=ticketmaster&x=10' target='_blank' >10</a>
                    ";
            }
            
            elseif (!$FichierEnTab22=copy($fichierdistant, $fichierCopie2)){
            
                echo "La copie du fichier  « $flux.$format » a échoué...";
                $pointeur2 = fopen($fichierErreur, "w");
                fwrite($pointeur2,"$texteErreur");
                fclose ( $pointeur2 );
                echo "<br>Voir le fichier <a href='alerte/erreur$flux.txt' target='_blank' >erreur$flux.txt</a>";
                
            } else {
            
                echo "Le fichier « $flux.$format » a été mis à jour avec succès ! ";
                // echo "<a href='$fichierCopie2' target='_blank'>Voir le fichier</a>";
                if ($flux=="ticketmaster" || $flux=="fnac" || $flux=="carrefour" || $flux=="digitick"){
                    echo "<br><b>Patienter 5 minutes</b> puis cliquer <a href='decompression.php?flux=$flux' >ici</a> pour décompresser le fichier « $flux.$format »...";
                }
            }
           

            if ($flux=="carrefour"){
            echo "
            <br><br>Vous devez mettre à jour les fichiers <b>« fragmentés »</b> :
            <a href='simplification.php?flux=carrefour&x=1' target='_blank' >1</a>
            <a href='simplification.php?flux=carrefour&x=2' target='_blank' >2</a>
            <a href='simplification.php?flux=carrefour&x=3' target='_blank' >3</a>
            <a href='simplification.php?flux=carrefour&x=4' target='_blank' >4</a>
            ";
            }
            
            echo "<br><br>";
            
            }
            else {
            
                echo "<h3 style='color:#AAA'>Cliquer sur un des flux ci-dessus pour le télécharger</h3><br>";
                
                $nb_fichier = 0;
                echo '<ul style="width:450px; margin-left:5px;list-style-type:circle">';
                if($dossier = opendir('./data')){
                    while(false !== ($fichier = readdir($dossier)))
                    {
                        if($fichier != '.' && $fichier != '..')
                        {
                        $nb_fichier++;
                        $tailleFichier=filesize("data/$fichier");
                        $tailleFichier=round($tailleFichier/1000000, 2);
                        $dateFichier=filemtime("data/$fichier");
                        $dateFichier=date("d/m/Y",$dateFichier);
                        echo '<li><a target="_blank" href="./data/' . $fichier . '">' . $fichier . '</a> <span style=float:right; >' .$tailleFichier. ' MB &nbsp; &nbsp; ' .$dateFichier. ' </span></li>';
                        
                        }
                     
                    }
                echo '</ul><br />';
                echo ' &nbsp; &nbsp; Il y a <strong>' . $nb_fichier .'</strong> fichiers dans le dossier « <a target=_blank href=data/ >data</a> »';
                closedir($dossier);
             
                }
                else {echo 'Le dossier n\' a pas pu être ouvert';}
            
            
            
            

            echo "</div>";
             
            

            echo "<div style='margin:20px auto; width:80%; border:2px solid red; padding:10px; text-align:center; color:red;'>
            <h2>Après le téléchargement, il faut synchroniser les fichiers en cliquant <a href='/synchronisation' >ici</a></h2>
            </div>";
        
        }
*/
        
    }

    public function getTabCatFnac(): array
    {
        return [
            "1MC" => ["name" => "Musique", "rubric" => "concert"],
            "11V" => ["name" => "Variété,Comédie musicale", "rubric" => "spectacle"],
            "CMU" => ["name" => "Comédie musicale", "rubric" => "spectacle"],
            "VAF" => ["name" => "Variété et chanson française", "rubric" => "concert"],
            "VAI" => ["name" => "Variété internationale", "rubric" => "concert"],
            "12P" => ["name" => "Pop-Rock,Musique électronique", "rubric" => "concert"],
            "HAR" => ["name" => "Hard-rock,Métal", "rubric" => "concert"],
            "MEL" => ["name" => "Musique électronique", "rubric" => "concert"],
            "POP" => ["name" => "Pop-rock,Folk", "rubric" => "concert"],
            "12C" => ["name" => "Clubbing,Gala", "rubric" => "soiree"],
            "CLU" => ["name" => "Clubbing", "rubric" => "soiree"],
            "SOI" => ["name" => "Gala,Soirée étudiante", "rubric" => "soiree"],
            "REV" => ["name" => "Réveillon", "rubric" => "soiree"],
            "13R" => ["name" => "Rap,Reggae,Soul-Funk", "rubric" => "concert"],
            "RAP" => ["name" => "Rap,Hip-hop,Slam", "rubric" => "concert"],
            "REG" => ["name" => "Reggae", "rubric" => "concert"],
            "SOU" => ["name" => "R'n'B,Soul,Funk", "rubric" => "concert"],
            "14J" => ["name" => "Jazz,Blues,Gospel", "rubric" => "concert"],
            "BLU" => ["name" => "Blues,Country", "rubric" => "concert"],
            "CIC" => ["name" => "Ciné-concert", "rubric" => "soiree"],
            "GOS" => ["name" => "Gospel", "rubric" => "concert"],
            "JAZ" => ["name" => "Jazz", "rubric" => "concert"],
            "15M" => ["name" => "Musiques du monde", "rubric" => "concert"],
            "MAC" => ["name" => "Musiques des Caraïbes & Amérique latine", "rubric" => "concert"],
            "MTF" => ["name" => "Musiques de France & Europe", "rubric" => "concert"],
            "RAI" => ["name" => "Musiques d'Orient & Maghreb", "rubric" => "concert"],
            "MAI" => ["name" => "Musiques d'Asie, Inde & Océanie", "rubric" => "concert"],
            "MAF" => ["name" => "Musiques d'Afrique", "rubric" => "concert"],
            "16C" => ["name" => "Musique classique et Opéra", "rubric" => "concert"],
            "CHO" => ["name" => "Chant choral", "rubric" => "concert"],
            "CIC" => ["name" => "Ciné-concert", "rubric" => "cinema"],
            "LYR" => ["name" => "Lyrique", "rubric" => "concert"],
            "MBA" => ["name" => "Musique baroque", "rubric" => "concert"],
            "MCL" => ["name" => "Musique classique", "rubric" => "concert"],
            "MCO" => ["name" => "Musique contemporaine", "rubric" => "concert"],
            "MSA" => ["name" => "Musique sacrée", "rubric" => "concert"],
            "OPE" => ["name" => "Opéra", "rubric" => "concert"],
            "OPT" => ["name" => "Opérette", "rubric" => "concert"],
            "2TH" => ["name" => "Théâtre,Humour", "rubric" => "spectacle"],
            "21T" => ["name" => "Théâtre", "rubric" => "spectacle"],
            "TCL" => ["name" => "Théâtre classique", "rubric" => "spectacle"],
            "TCO" => ["name" => "Théâtre contemporain", "rubric" => "spectacle"],
            "MUC" => ["name" => "Théâtre musical", "rubric" => "spectacle"],
            "ETH" => ["name" => "Théâtre pour enfants", "rubric" => "spectacle"],
            "SES" => ["name" => "Seul en scène", "rubric" => "spectacle"],
            "VAU" => ["name" => "Vaudeville", "rubric" => "spectacle"],
            "DEB" => ["name" => "Débat", "rubric" => "spectacle"],
            "COM" => ["name" => "Comédie", "rubric" => "spectacle"],
            "23H" => ["name" => "Humour", "rubric" => "spectacle"],
            "HUM" => ["name" => "Humour", "rubric" => "spectacle"],
            "ONE" => ["name" => "One man show", "rubric" => "spectacle"],
            "COM" => ["name" => "Comédie", "rubric" => "spectacle"],
            "MUC" => ["name" => "Théâtre musical", "rubric" => "spectacle"],
            "CTH" => ["name" => "Café-théâtre", "rubric" => "spectacle"],
            "26M" => ["name" => "Mime,Marionnette", "rubric" => "spectacle"],
            "MAR" => ["name" => "Marionnette", "rubric" => "spectacle"],
            "MIM" => ["name" => "Mime", "rubric" => "spectacle"],
            "27L" => ["name" => "Conte,Lecture,Poésie", "rubric" => "spectacle"],
            "CTE" => ["name" => "Conte", "rubric" => "spectacle"],
            "LEC" => ["name" => "Lecture", "rubric" => "spectacle"],
            "POE" => ["name" => "Poésie", "rubric" => "spectacle"],
            "3DA" => ["name" => "Danse", "rubric" => "spectacle"],
            "31D" => ["name" => "Danse", "rubric" => "spectacle"],
            "DCL" => ["name" => "Danse classique", "rubric" => "spectacle"],
            "DCO" => ["name" => "Danse contemporaine", "rubric" => "spectacle"],
            "DTR" => ["name" => "Danse traditionnelle", "rubric" => "spectacle"],
            "ASD" => ["name" => "Danse", "rubric" => "spectacle"],
            "DDM" => ["name" => "Danse du monde", "rubric" => "spectacle"],
            "4SP" => ["name" => "Sport", "rubric" => "sport"],
            "40F" => ["name" => "Sport collectif", "rubric" => "sport"],
            "FOO" => ["name" => "Football", "rubric" => "sport"],
            "RUG" => ["name" => "Rugby", "rubric" => "sport"],
            "BAS" => ["name" => "Basketball", "rubric" => "sport"],
            "HAN" => ["name" => "Handball", "rubric" => "sport"],
            "VBA" => ["name" => "Volley-ball", "rubric" => "sport"],
            "HSG" => ["name" => "Hockey sur glace", "rubric" => "sport"],
            "41S" => ["name" => "Sport mécanique", "rubric" => "sport"],
            "SPM" => ["name" => "Sport mécanique", "rubric" => "sport"],
            "49C" => ["name" => "Sport individuel", "rubric" => "sport"],
            "EQU" => ["name" => "Equitation", "rubric" => "sport"],
            "CYC" => ["name" => "Cyclisme", "rubric" => "sport"],
            "ATH" => ["name" => "Athlétisme", "rubric" => "sport"],
            "TEN" => ["name" => "Tennis", "rubric" => "sport"],
            "PAT" => ["name" => "Patinage artistique", "rubric" => "sport"],
            "NAT" => ["name" => "Natation", "rubric" => "sport"],
            "49B" => ["name" => "Sport de combat", "rubric" => "sport"],
            "BOX" => ["name" => "Boxe", "rubric" => "sport"],
            "CAT" => ["name" => "Catch", "rubric" => "sport"],
            "ASC" => ["name" => "Sport de combat", "rubric" => "sport"],
            "ARM" => ["name" => "Arts martiaux", "rubric" => "sport"],
            "49A" => ["name" => "Sport", "rubric" => "sport"],
            "ASP" => ["name" => "Sport", "rubric" => "sport"],
            "5FA" => ["name" => "Spectacle", "rubric" => "spectacle"],
            "51S" => ["name" => "Spectacle,Jeune public", "rubric" => "spectacle"],
            "ETH" => ["name" => "Théâtre pour enfants", "rubric" => "stage"],
            "AEN" => ["name" => "Atelier pour enfants", "rubric" => "stage"],
            "CIE" => ["name" => "Cinéma jeune public", "rubric" => "cinema"],
            "EXE" => ["name" => "Exposition,Musée pour enfants", "rubric" => "exposition"],
            "MAG" => ["name" => "Spectacle de magie", "rubric" => "spectacle"],
            "MUE" => ["name" => "Musique,Concert pour enfants", "rubric" => "concert"],
            "52B" => ["name" => "Spectacle", "rubric" => "spectacle"],
            "GSP" => ["name" => "Grand spectacle", "rubric" => "spectacle"],
            "GLA" => ["name" => "Spectacle sur glace", "rubric" => "spectacle"],
            "SPE" => ["name" => "Spectacle équestre", "rubric" => "spectacle"],
            "52A" => ["name" => "Spectacles de plein air", "rubric" => "spectacle"],
            "FEU" => ["name" => "Feux d'artifice", "rubric" => "autre"],
            "SEL" => ["name" => "Son et lumière", "rubric" => "autre"],
            "CAR" => ["name" => "Carnaval", "rubric" => "autre"],
            "54C" => ["name" => "Cirque", "rubric" => "autre"],
            "CTR" => ["name" => "Cirque traditionnel", "rubric" => "autre"],
            "NCI" => ["name" => "Nouveau cirque", "rubric" => "autre"],
            "55E" => ["name" => "Musée", "rubric" => "exposition"],
            "EXE" => ["name" => "Musée pour enfants", "rubric" => "exposition"],
            "6AR" => ["name" => "Art", "rubric" => "exposition"],
            "61M" => ["name" => "Musée,Monument", "rubric" => "exposition"],
            "VGU" => ["name" => "Visite guidée", "rubric" => "exposition"],
            "AVI" => ["name" => "Pass visite,musée", "rubric" => "exposition"],
            "VIM" => ["name" => "Visite de monument", "rubric" => "exposition"],
            "SUM" => ["name" => "Musée", "rubric" => "exposition"],
            "62E" => ["name" => "Exposition,Conférence,Atelier", "rubric" => "stage"],
            "AEX" => ["name" => "Pass,Abonnement", "rubric" => "exposition"],
            "ECO" => ["name" => "Exposition + Conférence", "rubric" => "exposition"],
            "MEX" => ["name" => "Musée + Exposition", "rubric" => "exposition"],
            "SUC" => ["name" => "Musée + Conférence", "rubric" => "exposition"],
            "ADU" => ["name" => "Atelier pour adultes", "rubric" => "exposition"],
            "CON" => ["name" => "Conférence", "rubric" => "exposition"],
            "EXP" => ["name" => "Exposition", "rubric" => "exposition"],
            "7TL" => ["name" => "Tourisme,Loisirs", "rubric" => "autre"],
            "71P" => ["name" => "Parc", "rubric" => "autre"],
            "AQU" => ["name" => "Aquarium", "rubric" => "autre"],
            "ATT" => ["name" => "Parc d'attraction", "rubric" => "autre"],
            "PAN" => ["name" => "Parc animalier", "rubric" => "autre"],
            "PAP" => ["name" => "Pass parc", "rubric" => "autre"],
            "73C" => ["name" => "Cabaret,Restauration spectacle", "rubric" => "autre"],
            "CAB" => ["name" => "Cabaret,Revue", "rubric" => "autre"],
            "DSP" => ["name" => "Restauration,Repas spectacle", "rubric" => "autre"],
            "74F" => ["name" => "Salon,Foire", "rubric" => "autre"],
            "LON" => ["name" => "Salon,Foire", "rubric" => "autre"],
            "75E" => ["name" => "Excursion,Tourisme", "rubric" => "autre"], 
            "EXC" => ["name" => "Excursion", "rubric" => "autre"],
            "FLU" => ["name" => "Croisière fluviale", "rubric" => "autre"],
            "DCR" => ["name" => "Dîner,Déjeuner croisière", "rubric" => "autre"],
            "HOT" => ["name" => "Séjours", "rubric" => "autre"],
            "76S" => ["name" => "Pratiquez un sport", "rubric" => "autre"],
            "CSP" => ["name" => "Club de sport", "rubric" => "autre"],
            "KAR" => ["name" => "Karting", "rubric" => "autre"],
            "SKI" => ["name" => "Forfait de ski", "rubric" => "autre"],
            "STS" => ["name" => "Stage sportif", "rubric" => "autre"],
            "79A" => ["name" => "Loisirs", "rubric" => "autre"],
            "ALO" => ["name" => "Loisirs", "rubric" => "autre"],
            "API" => ["name" => "Pilotage", "rubric" => "autre"],
            "ABE" => ["name" => "Bien-être", "rubric" => "autre"],
            "AGA" => ["name" => "Gastronomie,Ateliers de cuisine", "rubric" => "stage"],
            "ACC" => ["name" => "Accrobranche", "rubric" => "autre"],
            "AOR" => ["name" => "Loisirs", "rubric" => "autre"],
            "CCA" => ["name" => "Carte cadeau", "rubric" => "autre"],
            "79C" => ["name" => "Coffret", "rubric" => "autre"],
            "COB" => ["name" => "Coffret Bien-être", "rubric" => "autre"],
            "COS" => ["name" => "Coffret Séjour", "rubric" => "autre"],
            "COG" => ["name" => "Coffret Gastronomie", "rubric" => "autre"],
            "COA" => ["name" => "Coffret Activité", "rubric" => "autre"],
            "COE" => ["name" => "Coffret Enfant", "rubric" => "autre"],
            "CHM" => ["name" => "Coffret Homme", "rubric" => "autre"],
            "CFE" => ["name" => "Coffret Femme", "rubric" => "autre"],
            "CPS" => ["name" => "Coffret Sport,Pilotage", "rubric" => "autre"],
            "CAO" => ["name" => "Coffret atypique", "rubric" => "autre"],
            "CMT" => ["name" => "Coffret Multithématique", "rubric" => "autre"],
            "CLU" => ["name" => "Coffret Luxe", "rubric" => "autre"],
            "CDI" => ["name" => "Coffret divers", "rubric" => "autre"],
            "8CI" => ["name" => "Cinéma", "rubric" => "cinema"],
            "81F" => ["name" => "Cinéma", "rubric" => "cinema"], 
            "FIL" => ["name" => "Film", "rubric" => "cinema"],
            "ACI" => ["name" => "Abonnement,Pass cinéma", "rubric" => "cinema"],
            "ACT" => ["name" => "Action", "rubric" => "cinema"],
            "ANI" => ["name" => "Animation", "rubric" => "cinema"],
            "ARM" => ["name" => "Arts martiaux", "rubric" => "cinema"],
            "AVE" => ["name" => "Aventure", "rubric" => "cinema"],
            "AVP" => ["name" => "Avant-première", "rubric" => "cinema"],
            "BIO" => ["name" => "Biopic", "rubric" => "cinema"],
            "BOL" => ["name" => "Bollywood", "rubric" => "cinema"],
            "CIC" => ["name" => "Ciné-concert", "rubric" => "cinema"],
            "CIE" => ["name" => "Jeune public", "rubric" => "cinema"],
            "CLA" => ["name" => "Classique", "rubric" => "cinema"],
            "COD" => ["name" => "Comédie dramatique", "rubric" => "cinema"],
            "COM" => ["name" => "Comédie", "rubric" => "cinema"],
            "DEA" => ["name" => "Dessin animé", "rubric" => "cinema"],
            "DIV" => ["name" => "Divers", "rubric" => "cinema"],
            "DOC" => ["name" => "Documentaire", "rubric" => "cinema"],
            "DRA" => ["name" => "Drame", "rubric" => "cinema"],
            "EPO" => ["name" => "Epouvante-horreur", "rubric" => "cinema"],
            "ERO" => ["name" => "Erotique", "rubric" => "cinema"],
            "ESP" => ["name" => "Espionnage", "rubric" => "cinema"],
            "FAN" => ["name" => "Fantastique", "rubric" => "cinema"],
            "FCE" => ["name" => "Comédie érotique", "rubric" => "cinema"],
            "FFA" => ["name" => "Famille", "rubric" => "cinema"],
            "GUE" => ["name" => "Guerre", "rubric" => "cinema"],
            "HIS" => ["name" => "Historique", "rubric" => "cinema"],
            "JUD" => ["name" => "Judiciaire", "rubric" => "cinema"],
            "MED" => ["name" => "Médical", "rubric" => "cinema"],
            "MOB" => ["name" => "Mobisode", "rubric" => "cinema"],
            "MUS" => ["name" => "Musical", "rubric" => "cinema"],
            "NUI" => ["name" => "Nuit à thème", "rubric" => "cinema"],
            "PEP" => ["name" => "Péplum", "rubric" => "cinema"],
            "POL" => ["name" => "Policier", "rubric" => "cinema"],
            "ROC" => ["name" => "Retransmission", "rubric" => "cinema"],
            "ROM" => ["name" => "Romance", "rubric" => "cinema"],
            "SCF" => ["name" => "Science fiction", "rubric" => "cinema"],
            "SOA" => ["name" => "Soap", "rubric" => "cinema"],
            "THR" => ["name" => "Thriller", "rubric" => "cinema"],
            "WEB" => ["name" => "Web série", "rubric" => "cinema"],
            "WES" => ["name" => "Western", "rubric" => "cinema"]
        ];
    }



    /*
            $genreFnac = [
                "1MC" => "Musique/Concerts",
                "11V" => "Variété/Comédie musicale",
                "CMU" => "Comédie musicale",
                "VAF" => "Variété et chanson françaises",
                "VAI" => "Variété internationale",
                "12P" => "Pop-Rock/Musique électronique",
                "HAR" => "Hard-rock/Métal",
                "MEL" => "Musique électronique",
                "POP" => "Pop-rock / Folk",
                "12C" => "Clubbing/Soirées/Galas",
                "CLU" => "Clubbing & Soirées",
                "SOI" => "Galas/Soirées étudiantes",
                "REV" => "Réveillon",
                "13R" => "Rap/Reggae/Soul-Funk",
                "RAP" => "Rap/Hip-hop/Slam",
                "REG" => "Reggae",
                "SOU" => "R'n'B/Soul/Funk",
                "14J" => "Jazz/Blues/Gospel",
                "BLU" => "Blues/Country",
                "CIC" => "Ciné-concert",
                "GOS" => "Gospel",
                "JAZ" => "Jazz",
                "15M" => "Musiques du monde",
                "MAC" => "Musiques des Caraïbes & Amérique latine",
                "MTF" => "Musiques de France & Europe",
                "RAI" => "Musiques d'Orient & Maghreb",
                "MAI" => "Musiques d'Asie, Inde & Océanie",
                "MAF" => "Musiques d'Afrique",
                "16C" => "Musique classique et Opéra",
                "CHO" => "Chant choral",
                "CIC" => "Ciné-concert",
                "LYR" => "Lyrique",
                "MBA" => "Musique baroque",
                "MCL" => "Musique classique",
                "MCO" => "Musique contemporaine",
                "MSA" => "Musique sacrée",
                "OPE" => "Opéra",
                "OPT" => "Opérette",
                
                "2TH" => "Théâtre/Humour",
                "21T" => "Théâtre",
                "TCL" => "Théâtre classique",
                "TCO" => "Théâtre contemporain",
                "MUC" => "Théâtre musical",
                "ETH" => "Théâtre pour enfants",
                "SES" => "Seul en scène",
                "VAU" => "Vaudeville",
                "DEB" => "Débat",
                "COM" => "Comédie",
                "23H" => "Humour",
                "HUM" => "Humoristes",
                "ONE" => "One man/woman show",
                "COM" => "Comédie",
                "MUC" => "Théâtre musical",
                "CTH" => "Café-théâtre",
                "26M" => "Mime/Marionnette",
                "MAR" => "Marionnette",
                "MIM" => "Mime",
                "27L" => "Conte/Lecture/Poésie",
                "CTE" => "Conte",
                "LEC" => "Lecture",
                "POE" => "Poésie",

                "3DA" => "Danse",
                "31D" => "Danse",
                "DCL" => "Danse classique",
                "DCO" => "Danse contemporaine",
                "DTR" => "Danse traditionnelle",
                "ASD" => "Autre spectacle de danse",
                "DDM" => "Danse du monde",

                "4SP" => "Sports",
                "40F" => "Sports collectifs",
                "FOO" => "Football",
                "RUG" => "Rugby",
                "BAS" => "Basketball",
                "HAN" => "Handball",
                "VBA" => "Volley-ball",
                "HSG" => "Hockey sur glace",
                "41S" => "Sports mécaniques",
                "SPM" => "Sports mécaniques",
                "49C" => "Sports individuels",
                "EQU" => "Equitation",
                "CYC" => "Cyclisme",
                "ATH" => "Athlétisme",
                "TEN" => "Tennis",
                "PAT" => "Patinage artistique",
                "NAT" => "Natation",
                "49B" => "Sports de combat BOX Boxe",
                "CAT" => "Catch",
                "ASC" => "Autres sports de combat",
                "ARM" => "Arts martiaux",
                "49A" => "Autre sport ASP Autre sport",

                "5FA" => "Spectacles",
                "51S" => "Spectacles/Expos jeune public",
                "ETH" => "Théâtre pour enfants",
                "AEN" => "Atelier pour enfants",
                "CIE" => "Cinéma jeune public",
                "EXE" => "Exposition/Musée pour enfants",
                "MAG" => "Spectacle de magie",
                "MUE" => "Musique/concert pour enfants",
                "52B" => "Spectacles",
                "GSP" => "Grand spectacle",
                "GLA" => "Spectacle sur glace",
                "SPE" => "Spectacle équestre",
                "52A" => "Spectacles de plein air",
                "FEU" => "Feux d'artifice",
                "SEL" => "Son et lumière",
                "CAR" => "Carnaval",
                "54C" => "Cirque",
                "CTR" => "Cirque traditionnel",
                "NCI" => "Nouveau cirque",
                "55E" => "Exposition/Musée",
                "EXE" => "Exposition/Musée pour enfants",

                "6AR" => "Arts/musées",
                "61M" => "Musée/Monuments",
                "VGU" => "Visites guidées",
                "AVI" => "Pass visites/musées",
                "VIM" => "Visite de monuments",
                "SUM" => "Musée",
                "62E" => "Exposition/Conférence/Ateliers",
                "AEX" => "Pass/Abonnement expos",
                "ECO" => "Exposition + Conférence",
                "MEX" => "Musée + Exposition",
                "SUC" => "Musée + Conférence",
                "ADU" => "Atelier pour adultes",
                "CON" => "Conférence",
                "EXP" => "Exposition",

                "7TL" => "Tourisme/Loisirs",
                "71P" => "Parc AQU Aquarium",
                "ATT" => "Parc d'attraction",
                "PAN" => "Parc animalier",
                "PAP" => "Pass parc",
                "73C" => "Cabaret/Restauration spectacle",
                "CAB" => "Cabaret/Revue",
                "DSP" => "Restauration/Repas spectacle",
                "74F" => "Salon/Foire",
                "LON" => "Salon/Foire",
                "75E" => "Excursion/Tourisme", 
                "EXC" => "Excursions",
                "FLU" => "Croisière fluviale",
                "DCR" => "Dîner/Déjeuner croisière",
                "HOT" => "Séjours",
                "76S" => "Pratiquez un sport",
                "CSP" => "Club de sport",
                "KAR" => "Karting",
                "SKI" => "Forfait de ski",
                "STS" => "Stage sportif",
                "79A" => "Activités de loisirs",
                "ALO" => "Activités de loisirs divers",
                "API" => "Pilotage",
                "ABE" => "Bien-être",
                "AGA" => "Gastronomie/Ateliers de cuisine",
                "ACC" => "Accrobranche",
                "AOR" => "Loisirs originaux",
                "CCA" => "Carte cadeau",
                "79C" => "Coffrets COB Coffrets Bien-être",
                "COS" => "Coffrets Séjours",
                "COG" => "Coffrets Gastronomie",
                "COA" => "Coffrets Activités",
                "COE" => "Coffrets Enfants",
                "CHM" => "Coffrets Homme",
                "CFE" => "Coffrets Femme",
                "CPS" => "Coffrets Sport/Pilotage",
                "CAO" => "Coffrets Originaux/Atypiques",
                "CMT" => "Coffrets Multithématiques",
                "CLU" => "Coffrets Luxe",
                "CDI" => "Coffrets divers",
                
                "8CI" => "Cinéma",
                "81F" => "Cinéma", 
                "FIL" => "Film",
                "ACI" => "Abonnement/Pass cinéma",
                "ACT" => "Action",
                "ANI" => "Animation",
                "ARM" => "Arts martiaux",
                "AVE" => "Aventure",
                "AVP" => "Avant-première",
                "BIO" => "Biopic",
                "BOL" => "Bollywood",
                "CIC" => "Ciné-concert",
                "CIE" => "Cinéma jeune public",
                "CLA" => "Classique",
                "COD" => "Comédie dramatique",
                "COM" => "Comédie",
                "DEA" => "Dessin animé",
                "DIV" => "Divers",
                "DOC" => "Documentaire",
                "DRA" => "Drame",
                "EPO" => "Epouvante-horreur",
                "ERO" => "Erotique",
                "ESP" => "Espionnage",
                "FAN" => "Fantastique",
                "FCE" => "Comédie érotique",
                "FFA" => "Famille",
                "GUE" => "Guerre",
                "HIS" => "Historique",
                "JUD" => "Judiciaire",
                "MED" => "Médical",
                "MOB" => "Mobisode",
                "MUS" => "Musical",
                "NUI" => "Nuit à thème (cinéma)",
                "PEP" => "Péplum",
                "POL" => "Policier",
                "ROC" => "Retransmission Opéra/Concert",
                "ROM" => "Romance",
                "SCF" => "Science fiction",
                "SOA" => "Soap",
                "THR" => "Thriller",
                "WEB" => "Web série",
                "WES" => "Western"
            ];

    */
}
