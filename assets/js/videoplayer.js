/*
 * Speed Vault Video Gallery
 *
 * @author HÃ©lio Chun - https://www.facebook.com/user128
 * @version 1.1.0 - Shape Shifter
 * @description Lazy load video gallery for Youtube, Vimeo, DailyMotion and HTML5 videos
 * Updated by Bruno Eichenberger
 */

$( function() {
	var $svDebugMode = false; // Set to true to debug
	
	
	// Chose your player's title prefix
	var $svTitlePrefix = 'S&eacute;lectionner une vid&eacute;o dans la liste &#8600;';
	var $svTitleText = 'Curious TV';
	
	// Choose controls bar icons title tag description
	var $svResizeTextPrefix = 'Redimensionner le lecteur';
	var $svVideoDescrPrefix = 'Description de l&rsquo;&eacute;v&egrave;nement';
	var $svCloseVideoPrefix = 'Fermer le lecteur vid&eacute;o';
	
	var $svTitreTop = '';
	var $svTitreTop2 = 'La playlist';
	
	// plugin begins
	var $svThumb = $('.svThumb'),
	$nowPlayingText = '<div class="sv-nowplaying"><h3 class="sv-title svTitleFix">'+ $svTitlePrefix +'</h3></div>';
	

	$('#svList').wrapAll('<div class="mQuery900" ></div>');
	$('#svBlocVideo').wrapAll('<div id="svBox"></div>');
	
	
	$('#svBox').prepend('<div class="sv-player">\
		<div id="svControls">\
			<div class="svFloatLeft"><span id="SVcloseBtn" class="svUIbtn iconazble ic-times" aria-hidden="true" title="'+$svCloseVideoPrefix+'"></span></div>\
			<div class="svClearfix"></div>\
			<div class="svFloatCenter">\
				<span class="svCenter">'+$svTitreTop+'</span>\
				<span class="svCenter2">'+$svTitreTop2+'</span>\
			</div><div class="svFloatRight"><span id="sv-zoom" class="svUIbtn iconazble ic-resize" aria-hidden="true" title="'+$svResizeTextPrefix+'"></span><span class="svfire-desc svUIbtn iconazble ic-question-equal" title="'+$svVideoDescrPrefix+'"></span></div>\
		</div>\
		<div class="sv-videowrapper"></div>\
		<div class="sv-infobox">'+$nowPlayingText+'</div>\
		<div class="svDescr-wrapper"><div class="sv-desc"></div></div>\
	</div>');

	
	
	var $svBox = $("#svBox"),
	$svVideoWrapper = $('.sv-videowrapper'),
	$svNowPlaying = $('.sv-nowplaying'),
	$svFireDesc = $('.svfire-desc'),
	$svDescr = $('.sv-desc'),
	$svTitreTop = $('.svCenter'),
	$svTitreTop2 = $('.svCenter2'),
	$svDescWrapper = $('.svDescr-wrapper'),
	$imgOverlay = '<div class="sv-overlay"></div>';
	$('#svBox').append('<div class="svClearfix"></div>');
	
	
	//$titreTopTxtComplet='+ $titreTopTxt +'<div class="titreFacultatif"> tsest</div>';
	
	
	
	$svVideoWrapper.hide();
	/*
	$svNowPlaying.hide();
	$svMenu.hide();
	*/
	$('#svBox').hide();
	
	// checks if #svBox was resized to add its same height to #svList
	function checkSVboxWidth() {
		
		$('#svBox').show();
		
		if ($svBox.hasClass('mQuery900')){
			$('#svList').outerHeight($('.sv-player').height() + (-40) );
		}
		// else {$('#svList').css('height','auto');}
		
		if ($svBox.hasClass('mQuery900')){
			$('#svBlocVideo').outerHeight($('.sv-player').height() );
		} else {
			$('#svBlocVideo').css('height','auto');
		}
	}
	
	// Bind event listener
	$(window).resize(checkSVboxWidth);

	
	// Scroll to Top on image click
	
	$svThumb.on('click', function() {
		var $thumbPath2 = $('.svThumb img').attr("data-anchor"),
		$thumbAnchor2 = $("#" + $thumbPath2),
		$leftAbsolute2 = $(".html,body"),
		$thumbPosition2 = $thumbAnchor2.offset().top + (-75);
		
		if ($svDebugMode === true) {
			console.log('Image or $svThumb was clicked!');
			console.log('Auto scroll activated. If it\'s in Grid list style, the website will be scrolled to the very top.');
		}
		$leftAbsolute2.animate({scrollTop: $thumbPosition2});
		/**/
		$svThumb.removeClass('svCurrentVideo');
		$(this).addClass('svCurrentVideo');
		
		// Check for description availability
		if ($(this).find('.svpre-desc').length === 0) {
			$svFireDesc.hide();
			$svDescWrapper.hide();
		} else {
			$svFireDesc.show();
			$svDescWrapper.show();
		}
		return false;
	});
	
	
	// close Videos
	$svBox.on( 'click', '#SVcloseBtn', function() {
		$('.sv-mainvideo').remove();
		$svTitreTop.html('Nouveaut&eacute; 2018');
		$svTitreTop2.html(': La playlist');
		$svFireDesc.hide();
		$svDescWrapper.hide();
		$svVideoWrapper.hide();
		$svThumb.removeClass('svCurrentVideo');
		$svThumb.removeClass('svControls');
		$svNowPlaying.html($nowPlayingText);	
		checkSVboxWidth();
		/*
		$svVideoWrapper.hide();
		$svNowPlaying.hide();
		*/
		$('#svBox').hide();
		$('#svList').css('height','100px');
		
		return false;
	});
	
	// description trigger
	$svFireDesc.hover(function () {
		$svDescr.slideDown(200);
	}, function(){
		$svDescr.slideUp(200);
	});
	
	// removes mQuery900 making the player bigger for desktops (shape-shifter)
	/**/
	$svBox.on( 'click', '#sv-zoom', function() {
		$svBox.toggleClass('mQuery900');
		checkSVboxWidth();
		return false;
	});
	
	
	// Youtube Video
	$('.sv-youtube').each(function() {
		
		var $this = $(this),
		$videoID = $this.attr('data-videoID'),
		$eventID = $this.attr('data-eventID'),
		$lieuID = $this.attr('data-lieuID'),
		$imageURL = $this.attr('data-imageURL'),
		$titreTopTxt = $this.find('.svpre-topjour').text(),
		$titreTopTxt2 = $this.find('.svpre-toplieu').text(),
		$jourTxt = $this.find('.svpre-jour').text(),
		$titleTxt = $this.find('.svpre-title').text(),
		$lieuTxt = $this.find('.svpre-lieu').text(),
		$tarifTxt = $this.find('.svpre-tarif').text(),
		$descriptionTxt = $this.find('.svpre-desc').text(),
		$baliseImage='<a class="blocImagePlayer" target="_blank" href="'+ $eventID +'" ><img title="'+ $titleTxt +'" src="'+ $imageURL +'"/></a>',
		$ytVideo = '<div class="sv-mainvideo"><iframe width="560" height="315" src="https://www.youtube.com/embed/' + $videoID + '?showinfo=1&rel=0&autoplay=1" frameborder="0" allowfullscreen></iframe></div>';
			
		if ($lieuID =="0" ) {
		var $this = $(this),
		$lieuTxt = $this.find('.svpre-lieu').text(),
		$blocLieuTxt = '<div class="lieuSortie" id>'+ $lieuTxt +'</div>';
		}
		else {
		var $this = $(this),
		$lieuTxt = $this.find('.svpre-lieu').text(),
		$blocLieuTxt = '<a target="_blank" title="Voir la programmation du lieu" href="../../agenda/programmation?id_lieu='+ $lieuID +'" ><div class="lieuSortie" id>'+ $lieuTxt +'</div></a>';
		}
		
		
		if ($imageURL=="" ) {
		var $this = $(this),
		$aucuneImage='aucuneImage',
		$baliseImage='';
		} else {
		var $this = $(this),
		$aucuneImage='';
		}
		
		// Check for custom thumbnails
		if ($this.find('img').length === 1) {
			var $svGetCustomThumb = $this.find('img').attr("src");
			$ytThumb = '<div class="svThumb-wrapper"><img src="'+ $svGetCustomThumb +'" data-anchor="svBox"/>'+ $imgOverlay + '</div>';
		} else {
			$ytThumb = '<div class="svThumb-wrapper"><img src="https://img.youtube.com/vi/' + $videoID + '/mqdefault.jpg" data-anchor="svBox"/>'+ $imgOverlay + '</div>';
		}
		$this.prepend($ytThumb);
		
		// Generate player on click
		$this.on('click', function() {
			$svVideoWrapper.html($ytVideo).hide().slideDown("fast");
			
			$svNowPlaying.html('<div class="sv-title" id="'+ $aucuneImage +'">'+ $baliseImage +'<div class="blocTextePlayer"><div class="dateTexteVideo">'+ $jourTxt +'</div>\
			<a target="_blank" title="Voir toutes les informations" href="'+ $eventID +'" ><div id="titreEventCarte">'+ $titleTxt +'</div></a>\
			<div class="blocDetailVideo">'+ $blocLieuTxt +'<div class="entreeSortie">'+ $tarifTxt +'</div>\
			</div></div>');
			
			$svDescr.html($descriptionTxt);
			$svTitreTop.html($titreTopTxt);
			$svTitreTop2.html($titreTopTxt2);
			setTimeout(checkSVboxWidth, 200);
			return false;
		});
	});

	
	
	
	// Vimeo Video
	$('.sv-vimeo').each(function() {
		
		var $this = $(this),
		$videoID = $this.attr('data-videoID'),
		$eventID = $this.attr('data-eventID'),
		$lieuID = $this.attr('data-lieuID'),
		$imageURL = $this.attr('data-imageURL'),
		$titreTopTxt = $this.find('.svpre-topjour').text(),
		$titreTopTxt2 = $this.find('.svpre-toplieu').text(),
		$jourTxt = $this.find('.svpre-jour').text(),
		$titleTxt = $this.find('.svpre-title').text(),
		$lieuTxt = $this.find('.svpre-lieu').text(),
		$tarifTxt = $this.find('.svpre-tarif').text(),
		$descriptionTxt = $this.find('.svpre-desc').text(),
		$baliseImage='<a class="blocImagePlayer" target="_blank" href="'+ $eventID +'" ><img title="'+ $titleTxt +'" src="'+ $imageURL +'"/></a>',
		$ytVideo = '<div class="sv-mainvideo"><iframe src="https://player.vimeo.com/video/' + $videoID + '?title=1&byline=1&portrait=1&badge=1&autoplay=1" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> </div>';
			
		if ($lieuID =="0" ) {
		var $this = $(this),
		$lieuTxt = $this.find('.svpre-lieu').text(),
		$blocLieuTxt = '<div class="lieuSortie" id>'+ $lieuTxt +'</div>';
		}
		else {
		var $this = $(this),
		$lieuTxt = $this.find('.svpre-lieu').text(),
		$blocLieuTxt = '<a target="_blank" title="Voir la programmation du lieu" href="../../agenda/programmation?id_lieu='+ $lieuID +'" ><div class="lieuSortie" id>'+ $lieuTxt +'</div></a>';
		}
		
		
		if ($imageURL=="" ) {
		var $this = $(this),
		$aucuneImage='aucuneImage',
		$baliseImage='';
		} else {
		var $this = $(this),
		$aucuneImage='';
		}
		
				// Check for custom thumbnails
		if ($this.find('img').length === 1) {
				var $svGetCustomThumb = $this.find('img').attr("src");
				$this.prepend('<div class="svThumb-wrapper"><img src="'+ $svGetCustomThumb +'" width="268" data-anchor="svBox"/>'+ $imgOverlay + '</div>');
		} else {
			$.getJSON('https://vimeo.com/api/v2/video/' + $videoID + '.json', function(data) {
			var $vimeoThumb = data[0].thumbnail_large;
				$this.prepend('<div class="svThumb-wrapper"><img src="' + $vimeoThumb + '" width="268" data-anchor="svBox"/>'+ $imgOverlay + '</div>');
			});
		}
		
		// Generate player on click
		$this.on('click', function() {
			$svVideoWrapper.html($ytVideo).hide().slideDown("fast");
			
			$svNowPlaying.html('<div class="sv-title" id="'+ $aucuneImage +'">'+ $baliseImage +'<div class="blocTextePlayer"><div class="dateTexteVideo">'+ $jourTxt +'</div>\
			<a target="_blank" title="Voir toutes les informations" href="'+ $eventID +'" ><div id="titreEventCarte">'+ $titleTxt +'</div></a>\
			<div class="blocDetailVideo">'+ $blocLieuTxt +'<div class="entreeSortie">'+ $tarifTxt +'</div>\
			</div></div>');
			
			$svDescr.html($descriptionTxt);
			$svTitreTop.html($titreTopTxt);
			$svTitreTop2.html($titreTopTxt2);
			setTimeout(checkSVboxWidth, 200);
			return false;
		});
	});

		
	
	// DailyMotion Video
	$('.sv-dailyMotion').each(function() {
		
		var $this = $(this),
		$videoID = $this.attr('data-videoID'),
		$eventID = $this.attr('data-eventID'),
		$lieuID = $this.attr('data-lieuID'),
		$imageURL = $this.attr('data-imageURL'),
		$titreTopTxt = $this.find('.svpre-topjour').text(),
		$titreTopTxt2 = $this.find('.svpre-toplieu').text(),
		$jourTxt = $this.find('.svpre-jour').text(),
		$titleTxt = $this.find('.svpre-title').text(),
		$lieuTxt = $this.find('.svpre-lieu').text(),
		$tarifTxt = $this.find('.svpre-tarif').text(),
		$descriptionTxt = $this.find('.svpre-desc').text(),
		$baliseImage='<a class="blocImagePlayer" target="_blank" href="'+ $eventID +'" ><img title="'+ $titleTxt +'" src="'+ $imageURL +'"/></a>',
		$ytVideo = '<div class="sv-mainvideo"><iframe frameborder="0" width="640" height="360" src="//www.dailymotion.com/embed/video/' + $videoID + '?autoplay=1" allowfullscreen></iframe></div>';
			
		if ($lieuID =="0" ) {
		var $this = $(this),
		$lieuTxt = $this.find('.svpre-lieu').text(),
		$blocLieuTxt = '<div class="lieuSortie" id>'+ $lieuTxt +'</div>';
		}
		else {
		var $this = $(this),
		$lieuTxt = $this.find('.svpre-lieu').text(),
		$blocLieuTxt = '<a target="_blank" title="Voir la programmation du lieu" href="../../agenda/programmation?id_lieu='+ $lieuID +'" ><div class="lieuSortie" id>'+ $lieuTxt +'</div></a>';
		}
		
		
		if ($imageURL=="" ) {
		var $this = $(this),
		$aucuneImage='aucuneImage',
		$baliseImage='';
		} else {
		var $this = $(this),
		$aucuneImage='';
		}
		
		// Check for custom thumbnails
		if ($this.find('img').length === 1) {
			var $svGetCustomThumb = $this.find('img').attr("src");
			$this.prepend('<div class="svThumb-wrapper"><img src="'+ $svGetCustomThumb +'" width="268" data-anchor="svBox"/>'+ $imgOverlay + '</div>');
		} else {
			$.getJSON('https://api.dailymotion.com/video/' + $videoID + '?fields=id,thumbnail_url', function(data) {
				$dailyMThumb = data.thumbnail_url;
				$this.prepend('<div class="svThumb-wrapper"><img src="' + $dailyMThumb + '" width="268" data-anchor="svBox"/>'+ $imgOverlay + '</div>');
			});
		}
		
		// Generate player on click
		$this.on('click', function() {
			$svVideoWrapper.html($ytVideo).hide().slideDown("fast");
			
			$svNowPlaying.html('<div class="sv-title" id="'+ $aucuneImage +'">'+ $baliseImage +'<div class="blocTextePlayer"><div class="dateTexteVideo">'+ $jourTxt +'</div>\
			<a target="_blank" title="Voir toutes les informations" href="'+ $eventID +'" ><div id="titreEventCarte">'+ $titleTxt +'</div></a>\
			<div class="blocDetailVideo">'+ $blocLieuTxt +'<div class="entreeSortie">'+ $tarifTxt +'</div>\
			</div></div>');
			
			$svDescr.html($descriptionTxt);
			$svTitreTop.html($titreTopTxt);
			$svTitreTop2.html($titreTopTxt2);
			setTimeout(checkSVboxWidth, 200);
			return false;
		});
	});

	
	
	// Vimeo Video
	/*
	$('.sv-vimeo').each(function() {
		var $this = $(this),
		$videoID = $this.attr('data-videoID'),
		$dateTxt = $this.find('.svpre-date').text(),
		$titleTxt = $this.find('.svpre-title').text(),
		$descriptionTxt = $this.find('.svpre-desc').text(),
		$vimeoVideo = '<div class="sv-mainvideo"> <iframe src="https://player.vimeo.com/video/' + $videoID + '?title=1&byline=1&portrait=1&badge=1&autoplay=1" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> </div>';
		
		// Check for custom thumbnails
		if ($this.find('img').length === 1) {
				var $svGetCustomThumb = $this.find('img').attr("src");
				$this.prepend('<div class="svThumb-wrapper"><img src="'+ $svGetCustomThumb +'" width="268" data-anchor="svBox"/>'+ $imgOverlay + '</div>');
		} else {
			$.getJSON('https://vimeo.com/api/v2/video/' + $videoID + '.json', function(data) {
			var $vimeoThumb = data[0].thumbnail_large;
				$this.prepend('<div class="svThumb-wrapper"><img src="' + $vimeoThumb + '" width="268" data-anchor="svBox"/>'+ $imgOverlay + '</div>');
			});
		}
		
		// Generate player on click
		$this.on('click', function() {
			$svVideoWrapper.html($vimeoVideo).hide().slideDown("fast");
			$svNowPlaying.html('<h3 class="sv-title">2<div class="dateTexteCarte">'+ $dateTxt +'</div><div id="titreEventCarte">'+ $titleTxt +'</div><a href="../../agenda/programmation?id_lieu=583" ><div class="lieuSortie">Le Graffalgar</div></a><div class="entreeSortie">Plus de 20 Euros</div>');
			$svDescr.html($descriptionTxt);
			setTimeout(checkSVboxWidth, 200);
			return false;
		});
	});

	
	// DailyMotion Video
	$('.sv-dailymotion').each(function() {
		var $this = $(this),
		$videoID = $this.attr('data-videoID'),
		$dateTxt = $this.find('.svpre-date').text(),
		$titleTxt = $this.find('.svpre-title').text(),
		$descriptionTxt = $this.find('.svpre-desc').text(),
		$dailyMVideo = '<div class="sv-mainvideo"> <iframe frameborder="0" width="640" height="360" src="//www.dailymotion.com/embed/video/' + $videoID + '?autoplay=1" allowfullscreen></iframe> </div>';

		// Check for custom thumbnails
		if ($this.find('img').length === 1) {
			var $svGetCustomThumb = $this.find('img').attr("src");
			$this.prepend('<div class="svThumb-wrapper"><img src="'+ $svGetCustomThumb +'" width="268" data-anchor="svBox"/>'+ $imgOverlay + '</div>');
		} else {
			$.getJSON('https://api.dailymotion.com/video/' + $videoID + '?fields=id,thumbnail_url', function(data) {
				$dailyMThumb = data.thumbnail_url;
				$this.prepend('<div class="svThumb-wrapper"><img src="' + $dailyMThumb + '" width="268" data-anchor="svBox"/>'+ $imgOverlay + '</div>');
			});
		}
		
		// Generate player on click
		$this.on('click', function() {
			$svVideoWrapper.html($dailyMVideo).hide().slideDown("fast");
			$svNowPlaying.html('<h3 class="sv-title">3'+ $dateTxt +'</br>'+ $titleTxt +'</div>');
			$svDescr.html($descriptionTxt);
			setTimeout(checkSVboxWidth, 200);
			return false;
		});
	});
	
	// HTML5 Video
	$('.sv-htmlvideo').each(function() {
		var $this = $(this),
		$videoID = $this.attr('data-videoURL'),
		$titleTxt = $this.find('.svpre-title').text(),
		$descriptionTxt = $this.find('.svpre-desc').text(),
		$html5Video = '<div class="sv-mainvideo">\
		<video class="sv-localVideo" width="320" height="240" controls controlsList="nodownload" autoplay>\
		  <source src="' + $videoID + '" type="video/mp4">\
		  Your browser does not support the HTML5 video tag.\
		</video>\
		</div>';

		// Check for custom thumbnails
		if ($this.find('img').length === 1) {
			var $svGetCustomThumb = $this.find('img').attr("src");
			$this.prepend('<div class="svThumb-wrapper"><img src="'+ $svGetCustomThumb +'" width="268" data-anchor="svBox"/>'+ $imgOverlay + '</div>');
		} else {
			$this.prepend('<div class="svThumb-wrapper"><img src="./images/no-thumb.jpg" width="268" data-anchor="svBox"/>'+ $imgOverlay + '</div>');
		}
		
		// Generate player on click
		$this.on('click', function() {
			$svVideoWrapper.html($html5Video).hide().slideDown("fast");
			$svNowPlaying.html('<h3 class="sv-title">4'+ $titleTxt +'</div>');
			$svDescr.html($descriptionTxt);
			setTimeout(checkSVboxWidth, 200);
			return false;
		});
	});
	*/
	
	
	
	$('.svThumb, #sv-zoom').on('click', function() {
		if($svBox.find('.sv-mainvideo').length) {
			var check = function(){
				if ($('#svBox').hasClass('mQuery900')){
					var $spotlightAnchor1 = $(".svCurrentVideo"),
					$bodySection1 = $("#svList"),
					$spotlightPosition1 = $spotlightAnchor1.position().top + $bodySection1.scrollTop() ;
					
					if ($svDebugMode === true) {
						console.log('Auto scroll activated. Normal vertical list style will be scrolled to the current video.');
						console.log('Current distance of video playing from the top of the list is: '+ $spotlightPosition1);
					}
					$bodySection1.animate({scrollTop: $spotlightPosition1}, 500);
				}
				else {
					setTimeout(check, 250); // check again in a second
				}
			}
			check();
		}
	});

	// Debugginh mode console message
	if ($svDebugMode === true) {
		console.log("Debugging Mode Activated!");
	}
	// virtual media queries.
	// created this way in case gallery is being implemented on a draggable element
	/**/
	function checkWidth() {
		var $svBox = $("#svBox"),
		$svBoxParent = $svBox.parent().width();
	
		
		if ($svBoxParent >= 900) {
			$svBox.removeClass();
			$svBox.addClass('mQuery900');
			if ($svDebugMode === true) {
				console.log("900mQuery");
			}
		}
		
		else if ($svBoxParent >= 700) {
			$svBox.removeClass();
			$svBox.addClass('blocPlayer');
			if ($svDebugMode === true) {
				console.log("700mQuery");
			}
		}
		/*  else if ($svBoxParent <= 550) {
			$svBox.removeClass();
			$svBox.addClass('blocPlayer');
			if ($svDebugMode === true) {
				console.log("550mQuery");
			}
		}*/
		
	}
	// Execute on load
	checkWidth();
	
	// Bind event listener
	$(window).resize(checkWidth);
		
/*	*/
	// New algorithm update version 1.1 to shorten long titles
	$(".svpre-title").each(function () {

		var $numWords = $(this);
		if($numWords){
			if ($numWords.text().length > 45) {
				$numWords.text($numWords.text().substr(0,45)+' ...')
			}
		}
		
	});
	


	
	
	
});
