//require('./jquery.js');

/*var $ = require('jquery');*/

//global.$ = global.Jquery = $;

//require('./fancybox.min.js');
//require('fancybox');
//require('@fancyapps/fancybox');


//import $ from 'jquery';
import 'bootstrap';

/*
$('#place_city').autocompleter({
    url_list: '/city_search',
    url_get: '/city_get/'
});

console.log("Ca marche !!!");
*/

var $body = $('body');

$('.sidebar-toggle').on('click', function () {
    $body.toggleClass('sidebar-open');
});


function responsiveCollapseView() {
    let desktopView = $(document).width();
    if(desktopView >= "1200"){
        $('.show-xl, .show-lg, .show-md, .show-sm').addClass('show')
    }
    else if(desktopView >= "992"){
        $('.show-lg, .show-md, .show-sm').addClass('show')
        $('.show-xl').removeClass('show')
    }
    else if(desktopView >= "768"){
        $('.show-md, .show-sm').addClass('show')
        $('.show-lg, .show-xl').removeClass('show')
    }
    else if(desktopView >= "576"){
        $('.show-sm').addClass('show')
        $('.show-xl, .show-lg, .show-md').removeClass('show')
    }
    else {
        $('.show-xl, .show-lg, .show-md, .show-sm').removeClass('show')
    }
}

$(window).on('load', function () {
    responsiveCollapseView();
    $(window).resize(function () {
        responsiveCollapseView()
    })
});